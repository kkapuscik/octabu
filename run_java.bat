@rem    -Xms<size>        set initial Java heap size
@rem    -Xmx<size>        set maximum Java heap size
@rem    -Xss<size>        set java thread stack size

@java -Xmx256m -Xss1m -cp octabujava/bin;octgraphjava/bin com.octaedr.octabu.app.Octabu

