/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.util;

import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.graph.Vertex;

/**
 * <h3>Default graph element provider class</h3>
 * 
 * <p>This class was made to simplify creating other element providers,
 * which only needs to override needed methods.
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class DefaultElementProvider implements IGraphElementProvider {

    /* (non-Javadoc)
     * @see com.octaedr.octabu.graph.util.IGraphElementProvider#createGraph()
     */
    public Graph createGraph() {
        return new Graph();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.graph.util.IGraphElementProvider#createVertex()
     */
    public Vertex createVertex() {
        return new Vertex();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.graph.util.IGraphElementProvider#createEdge()
     */
    public Edge createEdge() {
        return new Edge();
    }

}
