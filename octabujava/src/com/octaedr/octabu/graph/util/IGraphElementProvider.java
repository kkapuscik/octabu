/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.util;

import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.graph.Vertex;

/**
 * <h3>Graph element provider interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IGraphElementProvider {

    /**
     * <h3>Create graph object</h3>
     * 
     * @return
     * Created graph object.
     */
    public Graph createGraph();
    
    /**
     * <h3>Create vertex object</h3>
     * 
     * @return
     * Created vertex object.
     */
    public Vertex createVertex();

    /**
     * <h3>Create edge object</h3>
     * 
     * @return
     * Created edge object.
     */
    public Edge createEdge();
    
}
