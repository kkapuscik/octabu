/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.util;

import java.util.Iterator;

import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.graph.Vertex;
import com.octaedr.octgraph.exception.OctgraphException;
import com.octaedr.octgraph.graph.IDirectedVertex;

/**
 * <h3>Graph duplicator class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class GraphDuplicator {

    /**
     * <h3>Duplicate graph</h3>
     * 
     * @param oldGraph - graph to duplicate.
     * @param provider - provider for new graph elements.
     * 
     * @return
     * Duplicated graph.
     * 
     * @throws OctgraphException
     * if any OCTgraph exception occured during duplication.
     */
    static public Graph duplicate(Graph oldGraph, IGraphElementProvider provider) throws OctgraphException {
        /* copy graph */
        Graph newGraph = provider.createGraph();
        newGraph.setInfo(oldGraph.getInfo());

        /* copy vertices */
        Iterator vertexIterator = oldGraph.getAllVertices();
        while(vertexIterator.hasNext()) {
            Vertex oldVertex = (Vertex)vertexIterator.next();
            
            Vertex newVertex = provider.createVertex();
            newVertex.setKey(oldVertex.getKey());
            newVertex.addToGraph(newGraph);
        }
        
        /* copy edges */
        Iterator edgeIterator = oldGraph.getAllEdges();
        while(edgeIterator.hasNext()) {
            Edge oldEdge = (Edge)edgeIterator.next();
            Object oldSourceKey = ((Vertex)oldEdge.getSource()).getKey();
            Object oldTargetKey = ((Vertex)oldEdge.getTarget()).getKey();
            
            Edge newEdge = provider.createEdge();
            newEdge.setVertices(
                    (IDirectedVertex)newGraph.findVertex(oldSourceKey),
                    (IDirectedVertex)newGraph.findVertex(oldTargetKey));
            newEdge.setTime(oldEdge.getTime());
            newEdge.setCost(oldEdge.getCost());
            newEdge.addToGraph(newGraph);
        }
        
        return newGraph;
    }
    
}
