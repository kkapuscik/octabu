/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.graph;

import com.octaedr.octgraph.exception.NotSupportedException;
import com.octaedr.octgraph.generic.graph.DirectedGraph;
import com.octaedr.octgraph.generic.keytrait.KeyManager;
import com.octaedr.octgraph.generic.manager.DirectedAdjacencyList;
import com.octaedr.octgraph.graph.IVertex;
import com.octaedr.octgraph.manager.IDirectedGraphManager;

/**
 * <h3>OCTabu specialized graph class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class Graph extends DirectedGraph {

    /** <h3>Information about the graph</h3> */
    private GraphInfo infoObject;

    /**
     * <h3>Constructor</h3>
     * 
     * @param manager - graph elements manager.
     */
    public Graph(IDirectedGraphManager manager) {
        super(manager, new KeyManager(), null);
    }
    
    /**
     * <h3>Constructor</h3>
     */
    public Graph() {
        super(new DirectedAdjacencyList(), new KeyManager(), null);
    }
    
    /**
     * <h3>Set graph info</h3>
     * 
     * @param info - graph info to set.
     */
    public void setInfo(GraphInfo info) {
        this.infoObject = info;
    }

    /**
     * <h3>Get graph info</h3>
     * 
     * @return
     * Graph info if it was previously set or null.
     */
    public GraphInfo getInfo() {
        return this.infoObject;
    }
    
    /**
     * <h3>Find vertex by key</h3>
     * 
     * @param key - key of the vertex to find.
     * 
     * @throws NotSupportedException 
     * if findVertex operation is not supported.
     */
    public IVertex findVertex(int key) throws NotSupportedException {
        return findVertex(new Integer(key));
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return "[Graph]" +
            " vertices=" + getAllVerticesCount() +
            " edges=" + getAllEdgesCount();
    }
    
    
}
