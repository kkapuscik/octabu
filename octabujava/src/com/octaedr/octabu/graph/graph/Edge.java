/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.graph;

import com.octaedr.octgraph.generic.graph.DirectedEdge;

/**
 * <h3>OCTabu specialized edge class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class Edge extends DirectedEdge {

    /** <h3>Edge time criterium value</h3> */
    double timeValue = Double.NaN;
    /** <h3>Edge cost criterium value</h3> */
    double costValue = Double.NaN;
    
    /**
     * <h3>Set time criterium value</h3>
     * 
     * @param time - value to set.
     */
    public void setTime(double time) {
        this.timeValue = time;
    }
    
    /**
     * <h3>Set cost criterium value</h3>
     * 
     * @param cost - value to set.
     */
    public void setCost(double cost) {
        this.costValue = cost;
    }

    /**
     * <h3>Get time criterium value</h3>
     * 
     * @return
     * Current criterium value.
     */
    public double getTime() {
        return this.timeValue;
    }
    
    /**
     * <h3>Get cost criterium value</h3>
     * 
     * @return
     * Current criterium value.
     */
    public double getCost() {
        return this.costValue;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        Vertex source = (Vertex)getSource();
        Vertex target = (Vertex)getTarget();
        Integer sourceKey = null;
        Integer targetKey = null;
        if(source != null) {
            sourceKey = (Integer)source.getKey();
        }
        if(target != null) {
            targetKey = (Integer)target.getKey();
        }
        
        return "[Edge]" + 
            " source=" + sourceKey + 
            " target=" + targetKey +
            " cost=" + this.costValue +
            " time=" + this.timeValue;
    }
    
    
}
