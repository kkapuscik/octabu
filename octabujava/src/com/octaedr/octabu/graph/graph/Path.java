/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.graph;

import java.util.Iterator;
import java.util.ListIterator;

import com.octaedr.octgraph.exception.BrokenPathException;
import com.octaedr.octgraph.generic.graph.DirectedPath;
import com.octaedr.octgraph.graph.IEdge;

/**
 * <h3>OCTabu specialized graph path class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class Path extends DirectedPath {

    /** <h3>Path time value</h3> */
    protected double time;
    /** <h3>Path cost value</h3> */
    protected double cost;
    
    /**
     * <h3>Constructor</h3>
     */
    public Path() {
        super();
        this.time = 0.0;
        this.cost = 0.0;
    }
    
    /**
     * <h3>Constructor</h3>
     * 
     * @param other - path to copy data from.
     */
    public Path(Path other) {
        super(other);
        this.time = other.time;
        this.cost = other.cost;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.generic.graph.DirectedPath#addFirst(com.octaedr.octgraph.graph.IEdge)
     */
    public void addFirst(IEdge edge) throws BrokenPathException {
        super.addFirst(edge);
        this.time += ((Edge)edge).getTime();
        this.cost += ((Edge)edge).getCost();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.generic.graph.DirectedPath#addLast(com.octaedr.octgraph.graph.IEdge)
     */
    public void addLast(IEdge edge) throws BrokenPathException {
        super.addLast(edge);
        this.time += ((Edge)edge).getTime();
        this.cost += ((Edge)edge).getCost();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.generic.graph.DirectedPath#removeFirst()
     */
    public IEdge removeFirst() {
        Edge edge = (Edge)super.removeFirst();
        if(edge != null) {
            this.time -= edge.getTime();
            this.cost -= edge.getCost();
        }
        return edge;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.generic.graph.DirectedPath#removeLast()
     */
    public IEdge removeLast() {
        Edge edge = (Edge)super.removeLast();
        if(edge != null) {
            this.time -= edge.getTime();
            this.cost -= edge.getCost();
        }
        return edge;
    }

    /**
     * <h3>Get path time value</h3>
     * 
     * @return
     * Path time value.
     */
    public double getTime() {
        return this.time;
    }
    
    /**
     * <h3>Get path cost value</h3>
     * 
     * @return
     * Path cost value.
     */
    public double getCost() {
        return this.cost;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        if(getEdgeCount() == 0) {
            return "[Path] (time=0.0, cost=0.0) -empty-";
        }

        StringBuffer buffer = new StringBuffer();
        synchronized(buffer) {
            buffer.append("[Path] (time=" + this.time + ", cost=" + this.cost + ") ");
            
            boolean first = true;
            Iterator edgeIterator = getEdges();
            while(edgeIterator.hasNext()) {
                Edge edge = (Edge)edgeIterator.next();
                if(first) {
                    first = false;
                    Vertex sourceVertex = (Vertex)edge.getSource();
                    buffer.append(sourceVertex.getKey());
                }
                Vertex targetVertex = (Vertex)edge.getTarget();
                buffer.append("->");
                buffer.append(targetVertex.getKey());                
            }
        }
        return buffer.toString();
    }

    /**
     * <h3>Check if current path dominates given</h3>
     * 
     * @param other - path given for comparision.
     * 
     * @return
     * True if current graph dominates this given, false otherwise.
     */
    public boolean dominate(Path other) {
        if((this.cost < other.cost) &&
                (this.time <= other.time)) {
            return true;
        }
        if((this.time < other.time) &&
                (this.cost <= other.cost)) {
            return true;
        }
        return false;
    }

    /**
     * <h3>Check if current path is equal to given</h3>
     * 
     * @param other - path given for comparision.
     * 
     * @return
     * True if paths are same, false otherwise.
     */
    public boolean sameAs(Path other) {
        if((this.cost == other.cost) &&
                (this.time == other.time)) {
            
            ListIterator thisIterator = getEdges();
            ListIterator otherIterator = getEdges();
            
            while(thisIterator.hasNext() && otherIterator.hasNext()) {
                if(thisIterator.next() != otherIterator.next()) {
                    return false;
                }
            }
            
            if(thisIterator.hasNext() == otherIterator.hasNext()) {
                return true;
            }
        }
        return false;
    }

    
}
