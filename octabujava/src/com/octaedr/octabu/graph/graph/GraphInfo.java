/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.graph;

import java.util.Iterator;
import java.util.Vector;

/**
 * <h3>Graph info class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class GraphInfo {

    /**
     * <h3>Graph info entry class</h3>
     * 
     * @author Krzysztof Kapuscik
     * @version 1.0
     */
    public class Entry {
        
        /** <h3>Entry key</h3> */
        private String entryKey;
        /** <h3>Entry value</h3> */
        private String entryValue;
        
        /**
         * <h3>Constuctor</h3>
         * 
         * @param key - entry key.
         * @param value - entry value.
         */
        public Entry(String key, String value) {
            this.entryKey = key;
            this.entryValue = value;
        }

        /**
         * <h3>Get key</h3>
         * 
         * @return
         * Entry key.
         */
        public String getKey() {
            return this.entryKey;
        }
        
        /**
         * <h3>Get value</h3>
         * 
         * @return
         * Entry value.
         */
        public String getValue() {
            return this.entryValue;
        }
    }
    
    /** <h3>Vector of entries</h3> */
    private Vector<Entry> entries = new Vector<Entry>();
    
    /** <h3>Name of generator</h3> */
    private String generatorName = "Manual";
    
    
    /**
     * <h3>Set generator name</h3>
     * 
     * @param generator - generator name to set.
     */
    public void setGenerator(String generator) {
        this.generatorName = generator;
    }
    
    /**
     * <h3>Get generator name</h3>
     * 
     * @return
     * Generator name.
     */
    public String getGenerator() {
        return this.generatorName;
    }
    
    /**
     * <h3>Add info entry</h3>
     * 
     * @param key - entry key.
     * @param value - entry value.
     */
    public void addEntry(String key, String value) {
        this.entries.add(new Entry(key, value));
    }

    /**
     * <h3>Add info entry</h3>
     * 
     * <p>This is an overloaded member to simplify use of this class.</p>
     * 
     * @param key - entry key.
     * @param value - entry value.
     */
    public void addEntry(String key, int value) {
        addEntry(key, new Integer(value).toString());
    }
    
    /**
     * <h3>Add info entry</h3>
     * 
     * <p>This is an overloaded member to simplify use of this class.</p>
     * 
     * @param key - entry key.
     * @param value - entry value.
     */
    public void addEntry(String key, double value) {
        addEntry(key, new Double(value).toString());
    }    
    
    /**
     * <h3>Get interator over all entries</h3>
     * 
     * @return
     * Iterator over collection on entries.
     */
    public Iterator<Entry> getEntries() {
        return this.entries.iterator();
    }
}
