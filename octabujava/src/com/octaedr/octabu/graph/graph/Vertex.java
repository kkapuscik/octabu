/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.graph;

import com.octaedr.octgraph.generic.graph.DirectedVertex;
import com.octaedr.octgraph.generic.keytrait.IntegerKeyTrait;
import com.octaedr.octgraph.keytrait.IKeyTrait;
import com.octaedr.octgraph.keytrait.IKeyTraitListener;

/**
 * <h3>OCTabu specialized vertex class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class Vertex extends DirectedVertex implements IKeyTrait {

    /** <h3>Internal key trait object</h3> */
    private IntegerKeyTrait keyTrait = new IntegerKeyTrait();
    
    /**
     * <h3>Set vertex key</h3>
     * 
     * @param key - key to set.
     * 
     * @return
     * True if key was successfully set or false otherwise.
     */
    public boolean setKey(int key) {
        return setKey(new Integer(key));
    }
    
    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IKeyTrait#setKey(Object)
     */
    public boolean setKey(Object key) {
        return this.keyTrait.setKey(key);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IKeyTrait#getKey()
     */
    public Object getKey() {
        return this.keyTrait.getKey();
    }

    /**
     * <h3>Get key value as integer</h3>
     * 
     * @return
     * Key value as integer.
     */
    public int getIntKey() {
        return ((Integer)getKey()).intValue();
    }
    
    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IKeyTrait#addKeyTraitListener(com.octaedr.octgraph.IKeyTraitListener)
     */
    public void addKeyTraitListener(IKeyTraitListener listener) {
        this.keyTrait.addKeyTraitListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IKeyTrait#removeKeyTraitListener(com.octaedr.octgraph.IKeyTraitListener)
     */
    public void removeKeyTraitListener(IKeyTraitListener listener) {
        this.keyTrait.removeKeyTraitListener(listener);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return "[Vertex]" + " key=" + getKey();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return getIntKey();
    }

    
}
