/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.algorithm;

import com.octaedr.octabu.app.plugin.IAlgorithm;
import com.octaedr.octabu.graph.graph.Graph;

/**
 * <h3>Utility class for algorithm execution</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class AlgorithmExecutor {

    /**
     * <h3>Execute algorithm</h3>
     * 
     * @param algorithm - algorithm to execute.
     * @param graph - graph to process.
     * @param source - source vertex key.
     * @param target - target vertex key.
     * 
     * @return
     * Algorithm results.
     */
    static public AlgorithmResults execute(IAlgorithm algorithm, Graph graph, int source, int target) {
        AlgorithmResults results = new AlgorithmResults();
        results.setSuccess();

        results.startGeneralTimer();

        /* copy graph */
        results.startCopyingTimer();
        Graph preparedGraph = algorithm.prepareGraph(graph);
        results.stopCopyingTimer();

        /* process graph */
        results.startProcessingTimer();
        algorithm.process(results, preparedGraph, source, target);
        results.stopProcessingTimer();

        /* prepare results */
        results.startResultsTimer();
        algorithm.prepareResults(results, preparedGraph, source, target);
        results.stopResultsTimer();
        
        results.stopGeneralTimer();
        return results;
    }
}
