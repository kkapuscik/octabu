/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.algorithm;

import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.TreeSet;

import com.octaedr.octabu.graph.graph.Path;
import com.octaedr.octabu.util.Logger;
import com.octaedr.octabu.util.TimeCounter;

/**
 * <h3>Algorithm results container class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class AlgorithmResults {

    /**
     * <h3>Path values ranges</h3>
     * 
     * @author Krzysztof Kapuscik
     * @version 1.0
     */
    public class ValueRanges {
        public double minTimeValue = 0.0;
        public double maxTimeValue = 0.0;
        public double minCostValue = 0.0;
        public double maxCostValue = 0.0;
        public boolean valid = false;
        
        public ValueRanges() {
            // nothing to do
        }
        
        public ValueRanges(double minTime, double maxTime,
                double minCost, double maxCost) {
            this.minTimeValue = minTime;
            this.maxTimeValue = maxTime;
            this.minCostValue = minCost;
            this.maxCostValue = maxCost;
            this.valid = true;
        }
    }
    
    /**
     * <h3>Comparator class for Path objects</h3>
     * 
     * @author Krzysztof Kapuscik
     * @version 1.0
     */
    private class PathComparator implements Comparator<Path> {

        /*
         * (non-Javadoc)
         * @see Comparator#compare(T, T)
         */
        public int compare(Path path1, Path path2) {
            if(path1.getTime() < path2.getTime()) {
                return -1;
            } else if(path1.getTime() > path2.getTime()) {
                return 1;
            } else {
                if(path1.getCost() < path2.getCost()) {
                    return -1;
                } else if(path1.getCost() > path2.getCost()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
        
    }
    
    /** <h3>Collection of paths</h3> */
    private TreeSet<Path> paths = new TreeSet<Path>(new PathComparator());
    
    /** 
     * <h3>Status flag</h3> 
     * 
     * <p>Default value: failed</p>
     */
    private boolean status = false;

    /** <h3>Algorithm name</h3> */
    private String algorithmName = "-not set-";

    /** <h3>Graph copying time counter</h3> */
    private TimeCounter copyingCounter = new TimeCounter();
    /** <h3>Processing time counter</h3> */
    private TimeCounter processingCounter = new TimeCounter();
    /** <h3>Preparing results time counter</h3> */
    private TimeCounter resultsCounter = new TimeCounter();
    /** <h3>Total time counter</h3> */
    private TimeCounter totalCounter = new TimeCounter();
    /** <h3>Absolute start time</h3> */
    private Date startTime = new Date(0);
    /** <h3>Absolute finish time</h3> */
    private Date endTime = new Date(0);

    
    /**
     * <h3>Add path to results</h3>
     * 
     * @param path - path to add.
     */
    public void addPath(Path path) {
        this.paths.add(path);
    }
    
    /**
     * <h3>Get all paths</h3>
     * 
     * @return
     * Iterator over paths collection.
     */
    public Iterator<Path> getPaths() {
        return this.paths.iterator();
    }
    
    /**
     * <h3>Check if algorithm has failed.</h3>
     * 
     * @return
     * True if algorithm failed, false otherwise.
     */
    public boolean hasFailed() {
        return !this.status;
    }

    /**
     * <h3>Check if algorithm has succeeded.</h3>
     * 
     * @return
     * True if algorithm succeeded, false otherwise.
     */
    public boolean hasSucceeded() {
        return this.status;
    }

    /**
     * <h3>Set algorithm failed status</h3>
     */
    public void setFail() {
        this.status = false;
    }

    /**
     * <h3>Set algorithm suceeded status</h3>
     */
    public void setSuccess() {
        this.status = true;
    }
    
    /**
     * <h3>Set algorithm name</h3>
     * 
     * @param name - algorithm name to set.
     */
    public void setAlgorithmName(String name) {
        this.algorithmName = name;
    }
    
    /**
     * <h3>Get algorithm name</h3>
     * 
     * @return
     * Algorithm name.
     */
    public String getAlgorithmName() {
        return this.algorithmName;
    }

    public void startCopyingTimer() {
        this.copyingCounter.start();
    }
    
    public void stopCopyingTimer() {
        this.copyingCounter.stop();
    }
    
    public void startProcessingTimer() {
        this.processingCounter.start();
    }
    
    public void stopProcessingTimer() {
        this.processingCounter.stop();
    }

    public void startResultsTimer() {
        this.resultsCounter.start();
    }
    
    public void stopResultsTimer() {
        this.resultsCounter.stop();
    }

    public void startGeneralTimer() {
        this.startTime = new Date();
        this.totalCounter.start();
    }
    
    public void stopGeneralTimer() {
        this.totalCounter.stop();
        this.endTime = new Date();
    }

    /**
     * <h3>Prepare report about algorithm results</h3>
     * 
     * @param printPaths - flag which enables/disables printing paths.
     * 
     * @return
     * Report formated in string.
     */
    public String prepareReport(boolean printPaths) {
        StringBuilder builder = new StringBuilder(10 * 1024);
        
        builder.append("---------------------------------------------\n");
        builder.append("Result for algorithm: " + this.algorithmName + "\n");
        builder.append("---------------------------------------------\n");
        builder.append("Status: " + (this.status ? "succeeded" :"failed" + "\n"));
        if(this.status) {
            builder.append("\n");
            builder.append("Execution time\n");
            builder.append("* Start time: " + this.startTime + "\n");
            builder.append("* End time:   " + this.endTime + "\n");
            builder.append("\n");
            builder.append("Detailed execution time\n");
            builder.append("* Copying graph     (ms) = " + this.copyingCounter.getTimeDiff() + "\n");
            builder.append("* Processing graph  (ms) = " + this.processingCounter.getTimeDiff() + "\n");
            builder.append("* Preparing results (ms) = " + this.resultsCounter.getTimeDiff() + "\n");
            builder.append("                           ------------------\n");
            builder.append("*              Work (ms) = " + (this.resultsCounter.getTimeDiff() + this.processingCounter.getTimeDiff()) + "\n");
            builder.append("*             Total (ms) = " + this.totalCounter.getTimeDiff() + "\n");
            builder.append("\n");
            builder.append("* Total number of paths: " + this.paths.size() + "\n");

            if(printPaths) {
                builder.append("\n");
                builder.append("Paths\n");
                Iterator pathIterator = getPaths();
                while(pathIterator.hasNext()) {
                    builder.append("* ");
                    builder.append(pathIterator.next());
                    builder.append("\n");
                }
            }
        }
        builder.append("---------------------------------------------\n");

        return builder.toString();
    }
    
    /**
     * <h3>Get ranges of criterium values</h3>
     * 
     * @return
     * Ranges of criterium values.
     */
    public ValueRanges getValueRanges() {
        if(this.paths.size() > 0) {
            Path first = this.paths.first();
            Path last = this.paths.last();
            
            return new ValueRanges(
                    first.getTime(), last.getTime(),
                    last.getCost(), first.getCost());
        }

        /* return 'invalid' ranges */
        return new ValueRanges();
    }
    
    /**
     * <h3>Remove dominated paths</h3>
     */
    public void removeDominatedPaths() {
        boolean first = true;
        Path goodPath = null;

        Logger.logln("[AlgorithmResults] Removing dominated paths");
        Logger.logln("[AlgorithmResults] - number of paths before: " + this.paths.size());

        Iterator<Path> iterator = this.paths.iterator();
        while(iterator.hasNext()) {
            Path path = iterator.next();
            
            if(first) {
                goodPath = path;
                first = false;
            } else {
                if(goodPath.dominate(path)) {
                    Logger.logln("[AlgorithmResults] Removing dominated path");
                    Logger.logln("[AlgorithmResults] - path: " + path);
                    Logger.logln("[AlgorithmResults] - dominated by: " + goodPath);
                    iterator.remove();
                } else {
                    goodPath = path;
                }
            }
        }
        
        Logger.logln("[AlgorithmResults] Removing dominated paths");
        Logger.logln("[AlgorithmResults] - number of paths after: " + this.paths.size());
    }
}
