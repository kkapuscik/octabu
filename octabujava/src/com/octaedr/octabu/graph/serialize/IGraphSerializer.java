/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.serialize;


/**
 * <h3>Graph serializer interface</h3>
 * 
 * <p>This interface links writer and reader functionality
 * into one functional element.</p>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IGraphSerializer extends IGraphWriter, IGraphReader {
    // nothing to do
}
