/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.serialize;

import java.io.File;
import java.io.IOException;

import com.octaedr.octabu.graph.graph.Graph;

/**
 * <h3>Graph serialization reader interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IGraphReader {

    /**
     * <h3>Read graph from file</h3>
     * 
     * @param file - file to read graph from.
     * 
     * @return
     * Graph created from description contained in file.
     * 
     * @throws IOException
     * if any IO operation has failed.
     */
    public Graph readGraph(File file) throws IOException;

}
