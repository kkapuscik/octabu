/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.serialize;

import java.io.File;
import java.io.IOException;

import com.octaedr.octabu.graph.graph.Graph;

/**
 * <h3>Graph serialization write interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IGraphWriter {

    /**
     * <h3>Write graph to file</h3>
     * 
     * @param graph - graph to write.
     * @param file - data target file.
     * 
     * @throws IOException
     * if any IO operation has failed.
     */
    void writeGraph(Graph graph, File file) throws IOException;

}
