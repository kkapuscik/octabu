/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.serialize.text;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.graph.GraphInfo;
import com.octaedr.octabu.graph.graph.Vertex;
import com.octaedr.octabu.graph.serialize.IGraphReader;
import com.octaedr.octgraph.exception.OctgraphException;
import com.octaedr.octgraph.graph.IDirectedVertex;

/**
 * <h3>Text file serialization reader class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class TextGraphReader implements IGraphReader {

    private Graph graph;
    
    /**
     * <h3>Decode string from storage-friendly form</h3>
     * 
     * @param str - string to decode.
     * 
     * @return
     * Decoded string.
     */
    private String decodeString(String str) {
        return str.replace('^', ' ');
    }

    /**
     * <h3>Read graph file</h3>
     * 
     * @param reader - reader providing the data.
     * 
     * @return
     * True on succes, false on failure.
     * 
     * @throws IOException
     * if any IO error happened.
     */
    private boolean readGraphFile(BufferedReader reader) throws IOException {
        String line;
        line = reader.readLine();
        if(!line.equals("[GRAPHDATABEGIN]")) {
            return false;
        }
        
        for(;;) {
            try {
                line = reader.readLine();
                StringTokenizer tokenizer = new StringTokenizer(line, " ");
                String token = tokenizer.nextToken();
                
                if(token.equals("[GRAPHDATAEND]")) {
                    return true;
                } else if (token.equals("[GRAPH]")) {
                    if((this.graph != null) || !readGraph(reader)) {
                        return false;
                    }
                } else if(token.equals("[VERTICES]")) {
                    if((this.graph == null) || !readVertices(reader)) {
                        return false;
                    }
                } else if(token.equals("[EDGES]")) {
                    if((this.graph == null) || !readEdges(reader)) {
                        return false;
                    }
                } else {
                    return false;
                }
                
            } catch (NoSuchElementException exception) {
                return false;
            }
        }
    }
    
    /**
     * <h3>Read graph info</h3>
     * 
     * @param reader - reader providing the data.
     * 
     * @return
     * True on succes, false on failure.
     * 
     * @throws IOException
     * if any IO error happened.
     */
    private boolean readGraph(BufferedReader reader) throws IOException {
        String generatorString = null;

        this.graph = new Graph();
        this.graph.setInfo(new GraphInfo());
        
        for(;;) {
            try {
                String line = reader.readLine();
                StringTokenizer tokenizer = new StringTokenizer(line, " ");
                String token = tokenizer.nextToken();
                
                if(token.equals("[GRAPHEND]")) {
                    if(generatorString != null) {
                        this.graph.getInfo().setGenerator(generatorString);
                        return true;
                    }
                    return false;
                } else if (token.equals("[INFOENTRY]")) {
                    if(!readInfoEntry(reader)) {
                        return false;
                    }
                } else if (token.equals("-GENERATOR")) {
                    if(generatorString == null) {
                        generatorString = decodeString(tokenizer.nextToken());
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
                
            } catch (NoSuchElementException exception) {
                return false;
            }
        }
    }
    
    /**
     * <h3>Read graph info entry</h3>
     * 
     * @param reader - reader providing the data.
     * 
     * @return
     * True on succes, false on failure.
     * 
     * @throws IOException
     * if any IO error happened.
     */
    private boolean readInfoEntry(BufferedReader reader) throws IOException {
        String keyString = null;
        String valueString = null;
        
        for(;;) {
            try {
                String line = reader.readLine();
                StringTokenizer tokenizer = new StringTokenizer(line, " ");
                String token = tokenizer.nextToken();
                
                if(token.equals("[INFOENTRYEND]")) {
                    if((keyString != null) && (valueString != null)) {
                        this.graph.getInfo().addEntry(keyString, valueString);
                        return true;
                    }
                    return false;
                } else if (token.equals("-KEY")) {
                    if(keyString == null) {
                        keyString = decodeString(tokenizer.nextToken());
                    } else {
                        return false;
                    }
                } else if (token.equals("-VALUE")) {
                    if(valueString == null) {
                        valueString = decodeString(tokenizer.nextToken());
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
                
            } catch (NoSuchElementException exception) {
                return false;
            }
        }
    }

    /**
     * <h3>Read vertices list</h3>
     * 
     * @param reader - reader providing the data.
     * 
     * @return
     * True on succes, false on failure.
     * 
     * @throws IOException
     * if any IO error happened.
     */
    private boolean readVertices(BufferedReader reader) throws IOException {
        for(;;) {
            try {
                String line = reader.readLine();
                StringTokenizer tokenizer = new StringTokenizer(line, " ");
                String token = tokenizer.nextToken();
                
                if(token.equals("[VERTICESEND]")) {
                    return true;
                } else if (token.equals("[VERTEX]")) {
                    if(!readVertex(reader)) {
                        return false;
                    }
                } else {
                    return false;
                }
                
            } catch (NoSuchElementException exception) {
                return false;
            }
        }
    }
    
    /**
     * <h3>Read vertex</h3>
     * 
     * @param reader - reader providing the data.
     * 
     * @return
     * True on succes, false on failure.
     * 
     * @throws IOException
     * if any IO error happened.
     */
    private boolean readVertex(BufferedReader reader) throws IOException {
        String keyString = null;
        
        for(;;) {
            String line = reader.readLine();
            StringTokenizer tokenizer = new StringTokenizer(line, " ");
            try {
                String token = tokenizer.nextToken();
                
                if(token.equals("[VERTEXEND]")) {
                    if(keyString != null) {
                        int key = Integer.parseInt(keyString);
                        
                        Vertex vertex = new Vertex();
                        vertex.setKey(key);
                        vertex.addToGraph(this.graph);
                        
                        return true;
                    }
                    return false;
                } else if (token.equals("-KEY")) {
                    if(keyString == null) {
                        keyString = decodeString(tokenizer.nextToken());
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }                
            } catch (NoSuchElementException exception) {
                return false;
            } catch (OctgraphException exception) {
                return false;
            }
        }
    }
    
    /**
     * <h3>Read edges list</h3>
     * 
     * @param reader - reader providing the data.
     * 
     * @return
     * True on succes, false on failure.
     * 
     * @throws IOException
     * if any IO error happened.
     */
    private boolean readEdges(BufferedReader reader) throws IOException {
        for(;;) {
            try {
                String line = reader.readLine();
                StringTokenizer tokenizer = new StringTokenizer(line, " ");
                String token = tokenizer.nextToken();
                
                if(token.equals("[EDGESEND]")) {
                    return true;
                } else if (token.equals("[EDGE]")) {
                    if(!readEdge(reader)) {
                        return false;
                    }
                } else {
                    return false;
                }
                
            } catch (NoSuchElementException exception) {
                return false;
            }
        }
    }
    
    /**
     * <h3>Read edge</h3>
     * 
     * @param reader - reader providing the data.
     * 
     * @return
     * True on succes, false on failure.
     * 
     * @throws IOException
     * if any IO error happened.
     */
    private boolean readEdge(BufferedReader reader) throws IOException {
        String source = null;
        String target = null;
        String time = null;
        String cost = null;
        
        for(;;) {
            String line = reader.readLine();
            StringTokenizer tokenizer = new StringTokenizer(line, " ");
            try {
                String token = tokenizer.nextToken();
                
                if(token.equals("[EDGEEND]")) {
                    if((source != null) && (target != null) &&
                            (time != null) && (cost != null)) {
                        int sourceKey = Integer.parseInt(source);
                        int targetKey = Integer.parseInt(target);
                        double timeValue = Double.parseDouble(time);
                        double costValue = Double.parseDouble(cost);
                        
                        Edge edge = new Edge();
                        edge.setCost(costValue);
                        edge.setTime(timeValue);
                        edge.setVertices(
                                (IDirectedVertex)this.graph.findVertex(sourceKey),
                                (IDirectedVertex)this.graph.findVertex(targetKey));
                        edge.addToGraph(this.graph);
                        
                        return true;
                    }
                    return false;
                } else if (token.equals("-SOURCE")) {
                    if(source == null) {
                        source = decodeString(tokenizer.nextToken());
                    } else {
                        return false;
                    }
                } else if (token.equals("-TARGET")) {
                    if(target == null) {
                        target = decodeString(tokenizer.nextToken());
                    } else {
                        return false;
                    }
                } else if (token.equals("-COST")) {
                    if(cost == null) {
                        cost = decodeString(tokenizer.nextToken());
                    } else {
                        return false;
                    }
                } else if (token.equals("-TIME")) {
                    if(time == null) {
                        time = decodeString(tokenizer.nextToken());
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }                
            } catch (NoSuchElementException exception) {
                return false;
            } catch (OctgraphException exception) {
                return false;
            }
        }
    }
    
    
    /* (non-Javadoc)
     * @see com.octaedr.octabu.graph.serialize.IGraphReader#readGraph(java.io.File)
     */
    public Graph readGraph(File file) throws IOException {
        this.graph = null;

        BufferedReader reader = new BufferedReader(new FileReader(file));
        if(readGraphFile(reader)) {
            return this.graph;
        }

        return null;
    }

}
