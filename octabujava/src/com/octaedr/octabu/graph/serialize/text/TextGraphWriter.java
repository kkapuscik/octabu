/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.serialize.text;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.graph.GraphInfo;
import com.octaedr.octabu.graph.graph.Vertex;
import com.octaedr.octabu.graph.serialize.IGraphWriter;

/**
 * <h3>Text file serialization writer class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class TextGraphWriter implements IGraphWriter {

    /**
     * <h3>Encode string to storage-friendly form</h3>
     * 
     * @param str - string to encode.
     * 
     * @return
     * Encoded string.
     */
    private String encodeString(String str) {
        return str.replace(' ', '^');
    }
    
    /**
     * <h3>Write graph data to writer</h3>
     * 
     * @param writer - file writer to use.
     * @param vertex - graph to write.
     * 
     * @throws IOException
     * if IO operation error happened.
     */
    private void writeGraphData(BufferedWriter writer, Graph graph) throws IOException {
        /* write object header */
        writer.write("[GRAPH]");
        writer.newLine();
        
        GraphInfo info = graph.getInfo();

        /* write generator name */
        writer.write("-GENERATOR " + encodeString(info.getGenerator()));
        writer.newLine();

        /* write info entries */
        Iterator entryIterator = info.getEntries();
        while(entryIterator.hasNext()) {
            GraphInfo.Entry entry = (GraphInfo.Entry)entryIterator.next();
            writer.write("[INFOENTRY]");
            writer.newLine();
            writer.write("-KEY " + encodeString(entry.getKey()));
            writer.newLine();
            writer.write("-VALUE " + encodeString(entry.getValue()));
            writer.newLine();
            writer.write("[INFOENTRYEND]");
            writer.newLine();
        }

        writer.write("[GRAPHEND]");
        writer.newLine();
    }
    
    /**
     * <h3>Write edge data to writer</h3>
     * 
     * @param writer - file writer to use.
     * @param vertex - edge to write.
     * 
     * @throws IOException
     * if IO operation error happened.
     */
    private void writeEdgeData(BufferedWriter writer, Edge edge) throws IOException {
        /* write object header */
        writer.write("[EDGE]");
        writer.newLine();

        /* prepare data */
        Vertex sourceVertex = (Vertex)edge.getSource();
        Vertex targetVertex = (Vertex)edge.getTarget();
        int sourceVertexKey = 0;
        int targetVertexKey = 0;
        if(sourceVertex != null) {
            sourceVertexKey = sourceVertex.getIntKey();
        }
        if(targetVertex != null) {
            targetVertexKey = targetVertex.getIntKey();
        }
        
        /* write data */
        writer.write("-SOURCE " + new Integer(sourceVertexKey).toString());
        writer.newLine();
        writer.write("-TARGET " + new Integer(targetVertexKey).toString());
        writer.newLine();
        writer.write("-TIME " + new Double(edge.getTime()).toString());
        writer.newLine();
        writer.write("-COST " + new Double(edge.getCost()).toString());
        writer.newLine();

        writer.write("[EDGEEND]");
        writer.newLine();
    }

    /**
     * <h3>Write vertex data to writer</h3>
     * 
     * @param writer - file writer to use.
     * @param vertex - vertex to write.
     * 
     * @throws IOException
     * if IO operation error happened.
     */
    private void writeVertexData(BufferedWriter writer, Vertex vertex) throws IOException {
        /* write object header */
        writer.write("[VERTEX]");
        writer.newLine();

        /* write data */
        writer.write("-KEY " + new Integer(vertex.getIntKey()).toString());
        writer.newLine();

        writer.write("[VERTEXEND]");
        writer.newLine();
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.serialize.IGraphWriter#writeGraph(com.octaedr.octabu.graph.graph.Graph, java.io.File)
     */
    public void writeGraph(Graph graph, File file) throws IOException {
        /* create writer */
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));   
        
        writer.write("[GRAPHDATABEGIN]");
        writer.newLine();

        /* write graph data */
        writeGraphData(writer, graph);
        
        /* write vertices data */
        writer.write("[VERTICES]");
        writer.newLine();
        Iterator vertexIterator = graph.getAllVertices();
        while(vertexIterator.hasNext()) {
            writeVertexData(writer, (Vertex)vertexIterator.next());
        }
        writer.write("[VERTICESEND]");
        writer.newLine();
        
        /* write edges data */
        writer.write("[EDGES]");
        writer.newLine();
        Iterator edgeIterator = graph.getAllEdges();
        while(edgeIterator.hasNext()) {
            writeEdgeData(writer, (Edge)edgeIterator.next());
        }
        writer.write("[EDGESEND]");
        writer.newLine();

        /* write end header */
        writer.write("[GRAPHDATAEND]");
        writer.newLine();
        
        writer.close();
    }

}
