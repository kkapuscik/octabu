/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.serialize.text;

import java.io.File;
import java.io.IOException;

import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.serialize.IGraphSerializer;

/**
 * <h3>Text serialization class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class TextGraphSerializer implements IGraphSerializer {

    /** <h3>Internal reader object</h3> */
    private TextGraphReader reader = new TextGraphReader();
    /** <h3>Internal writer object</h3> */
    private TextGraphWriter writer = new TextGraphWriter();

    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.serialize.IGraphWriter#writeGraph(com.octaedr.octabu.graph.graph.Graph, java.io.File)
     */
    public void writeGraph(Graph graph, File file) throws IOException {
        this.writer.writeGraph(graph, file);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.serialize.IGraphReader#readGraph(java.io.File)
     */
    public Graph readGraph(File file) throws IOException {
        return this.reader.readGraph(file);
    }
}
