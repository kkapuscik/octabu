/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.serialize.xml;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.graph.GraphInfo;
import com.octaedr.octabu.graph.graph.Vertex;
import com.octaedr.octabu.graph.serialize.IGraphReader;
import com.octaedr.octgraph.exception.OctgraphException;
import com.octaedr.octgraph.graph.IDirectedVertex;

/**
 * <h3>Xml serialization reader class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class XmlGraphReader extends Object implements IGraphReader {

    /**
     * <h3>SAX parser event handler class</h3>
     * 
     * @author Krzysztof Kapuscik
     * @version 1.0
     */
    private class SaxHandler extends DefaultHandler {

        /** <h3>Handler state - invalid</h3> */
        static final private int STATE_INVALID      =   0;
        /** <h3>Handler state - reading document root</h3> */
        static final private int STATE_ROOT         =   1;
        /** <h3>Handler state - reading 'octabu' element</h3> */
        static final private int STATE_OCTABU       =   2;
        /** <h3>Handler state - reading 'graph' element</h3> */
        static final private int STATE_GRAPH        =   3;
        /** <h3>Handler state - reading 'info' element</h3> */
        static final private int STATE_INFO         =   4;
        /** <h3>Handler state - reading 'entries' element</h3> */
        static final private int STATE_ENTRIES      =   5;
        /** <h3>Handler state - reading 'vertices' element</h3> */
        static final private int STATE_VERTICES     =   6;
        /** <h3>Handler state - reading 'edges' element</h3> */
        static final private int STATE_EDGES        =   7;

        /** <h3>Current parser state</h3> */
        private int state = STATE_INVALID;
        /** <h3>OCTabu element found flag</h3> */
        private boolean octabuFound;
        /** <h3>Graph created from description</h3> */
        private Graph graph;

        /**
         * <h3>Get graph created from XML description</h3>
         * 
         * @return
         * Created graph or null if there was no graph description.
         */
        public Graph getGraph() {
            return this.graph;
        }

        
        /* (non-Javadoc)
         * @see org.xml.sax.helpers.DefaultHandler#startDocument()
         */
        public void startDocument() throws SAXException {
            this.state = STATE_ROOT;
            this.octabuFound = false;
            this.graph = null;
        }

        /* (non-Javadoc)
         * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
         */
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            switch(this.state) {
                case STATE_ROOT:
                    if(qName.equals("octabu")) {
                        this.state = beginOctabu(attributes);
                    } else {
                        this.state = STATE_INVALID;
                    }
                    break;
                case STATE_OCTABU:
                    if(qName.equals("graph")) {
                        this.state = beginGraph();
                    } else {
                        this.state = STATE_INVALID;
                    }
                    break;
                case STATE_GRAPH:
                    if(qName.equals("info")) {
                        this.state = beginInfo(attributes);
                    } else if(qName.equals("vertices")) {
                        this.state = beginVertices();
                    } else if(qName.equals("edges")) {
                        this.state = beginEdges();
                    } else {
                        this.state = STATE_INVALID;
                    }
                    break;
                case STATE_INFO:
                    if(qName.equals("entries")) {
                        this.state = beginEntries();
                    } else {
                        this.state = STATE_INVALID;
                    }
                    break;
                case STATE_ENTRIES:
                    if(qName.equals("entry")) {
                        this.state = beginEntry(attributes);
                    } else {
                        this.state = STATE_INVALID;
                    }
                    break;
                case STATE_VERTICES:
                    if(qName.equals("vertex")) {
                        this.state = beginVertex(attributes);
                    } else {
                        this.state = STATE_INVALID;
                    }
                    break;
                case STATE_EDGES:
                    if(qName.equals("edge")) {
                        this.state = beginEdge(attributes);
                    } else {
                        this.state = STATE_INVALID;
                    }
                    break;
                default:
                    this.state = STATE_INVALID;
                    break;
            }

            if(this.state == STATE_INVALID) {
                throw new SAXException("Invalid XML document");
            }
        }

        /* (non-Javadoc)
         * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
         */
        public void endElement(String uri, String localName, String qName) throws SAXException {
            switch(this.state) {
            case STATE_ROOT:
                // nothing to do
                break;
            case STATE_OCTABU:
                // nothing to do
                break;
            case STATE_GRAPH:
                if(qName.equals("graph")) {
                    this.state = STATE_OCTABU;
                }
                break;
            case STATE_INFO:
                if(qName.equals("info")) {
                    this.state = STATE_GRAPH;
                }
                break;
            case STATE_ENTRIES:
                if(qName.equals("entries")) {
                    this.state = STATE_INFO;
                }
                break;
            case STATE_VERTICES:
                if(qName.equals("vertices")) {
                    this.state = STATE_GRAPH;
                }
                break;
            case STATE_EDGES:
                if(qName.equals("edges")) {
                    this.state = STATE_GRAPH;
                }
                break;
            default:
                this.state = STATE_INVALID;
                break;
            }
            if(this.state == STATE_INVALID) {
                throw new SAXException("Invalid XML document");
            }
        }

        /**
         * <h3>Begin 'octabu' element</h3>
         * 
         * @param attributes - element attributes.
         * 
         * @return
         * Next parser state.
         */
        private int beginOctabu(Attributes attributes) {
            if(!this.octabuFound) {
                this.octabuFound = true;
                if(attributes.getValue("type").equals("graph")) {
                    return STATE_OCTABU;
                }
            }
            return STATE_INVALID;
        }

        /**
         * <h3>Begin 'graph' element</h3>
         * 
         * @return
         * Next parser state.
         */
        private int beginGraph() {
            if(this.graph == null) {
                this.graph = new Graph();
                return STATE_GRAPH;
            }
            return STATE_INVALID;
        }

        /**
         * <h3>Begin 'info' element</h3>
         * 
         * @param attributes - element attributes.
         * 
         * @return
         * Next parser state.
         */
        private int beginInfo(Attributes attributes) {
            if(this.graph.getInfo() == null) {
                GraphInfo info = new GraphInfo();
                String generator = attributes.getValue("generator");
                if(generator != null) {
                    info.setGenerator(generator);
                }
                this.graph.setInfo(info);
                return STATE_INFO;
            }
            return STATE_INVALID;
        }

        /**
         * <h3>Begin 'entries' element</h3>
         * 
         * @return
         * Next parser state.
         */
        private int beginEntries() {
            return STATE_ENTRIES;
        }

        /**
         * <h3>Begin 'entry' element</h3>
         * 
         * @param attributes - element attributes.
         * 
         * @return
         * Next parser state.
         */
        private int beginEntry(Attributes attributes) {
            String key = attributes.getValue("key");
            String value = attributes.getValue("value");
            
            if((key != null) && (value != null)) {
                this.graph.getInfo().addEntry(key, value);
                return this.state;
            }
            return STATE_INVALID;
        }

        /**
         * <h3>Begin 'vertices' element</h3>
         * 
         * @return
         * Next parser state.
         */
        private int beginVertices() {
            return STATE_VERTICES;
        }

        /**
         * <h3>Begin 'vertex' element</h3>
         * 
         * @param attributes - element attributes.
         * 
         * @return
         * Next parser state.
         */
        private int beginVertex(Attributes attributes) {
            String key = attributes.getValue("key");
            if(key != null) {
                try {
                    Vertex vertex = new Vertex();
                    vertex.setKey(Integer.parseInt(key));
                    vertex.addToGraph(this.graph);
                    return this.state;
                } catch (OctgraphException exception) {
                    exception.printStackTrace();
                }
            }
            return STATE_INVALID;
        }

        /**
         * <h3>Begin 'edges' element</h3>
         * 
         * @return
         * Next parser state.
         */
        private int beginEdges() {
            return STATE_EDGES;
        }

        /**
         * <h3>Begin 'edge' element</h3>
         * 
         * @param attributes - element attributes.
         * 
         * @return
         * Next parser state.
         */
        private int beginEdge(Attributes attributes) {
            String source = attributes.getValue("source");
            String target = attributes.getValue("target");
            String time = attributes.getValue("time");
            String cost = attributes.getValue("cost");
            if((source != null) && (target != null) &&
                    (time != null) && (cost != null)) {
                try {
                    Edge edge = new Edge();
                    edge.setSource((IDirectedVertex)
                            this.graph.findVertex(Integer.parseInt(source)));
                    edge.setTarget((IDirectedVertex)
                            this.graph.findVertex(Integer.parseInt(target)));
                    edge.setTime(Double.parseDouble(time));
                    edge.setCost(Double.parseDouble(cost));
                    edge.addToGraph(this.graph);
                    return this.state;
                } catch (OctgraphException exception) {
                    exception.printStackTrace();
                }
            }
            return STATE_INVALID;
        }

    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.serialize.IGraphReader#readGraph(java.io.File)
     */
    public Graph readGraph(File file) throws IOException {
        
        /* create XML parser event handler */
        SaxHandler handler = new SaxHandler();

        /* get factory */
        SAXParserFactory factory = SAXParserFactory.newInstance();

        /* parse data */
        try {
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(file, handler);
            return handler.getGraph();
        } catch (ParserConfigurationException exception) {
            exception.printStackTrace();
        } catch (SAXException exception) {
            exception.printStackTrace();
        }

        return null;
    }

}
