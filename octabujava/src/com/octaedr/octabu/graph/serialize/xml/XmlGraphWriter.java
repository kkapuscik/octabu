/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.graph.serialize.xml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.graph.GraphInfo;
import com.octaedr.octabu.graph.graph.Vertex;
import com.octaedr.octabu.graph.serialize.IGraphWriter;

/**
 * <h3>Xml serialization writer class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class XmlGraphWriter implements IGraphWriter {

    /**
     * <h3>Create 'vertex' element</h3>
     * 
     * @param document - xml document object.
     * @param parent - parent dom node.
     * @param vertex - vertex element to write.
     */
    private void createVertexElement(Document document, Element parent, Vertex vertex) {
        /* create entry element */
        Element vertexElement = document.createElement("vertex");
        vertexElement.setAttribute("key", new Integer(vertex.getIntKey()).toString());
        parent.appendChild(vertexElement);
    }
    
    /**
     * <h3>Create 'vertices' element</h3>
     * 
     * @param document - xml document object.
     * @param parent - parent dom node.
     * @param vertexIterator - iterator over all graph vertices.
     */
    private void createVerticesElement(Document document, Element parent, Iterator vertexIterator) {
        /* create entries element */
        Element verticesElement = document.createElement("vertices");
        parent.appendChild(verticesElement);

        while(vertexIterator.hasNext()) {
            Vertex vertex = (Vertex)vertexIterator.next();
            createVertexElement(document, verticesElement, vertex);
        }
    }

    /**
     * <h3>Create 'edge' element</h3>
     * 
     * @param document - xml document object.
     * @param parent - parent dom node.
     * @param edge - edge element to write.
     */
    private void createEdgeElement(Document document, Element parent, Edge edge) {
        /* create entry element */
        Element vertexElement = document.createElement("edge");
        if(edge.getSource() != null) {
            Vertex vertex = (Vertex)edge.getSource();
            vertexElement.setAttribute("source", new Integer(vertex.getIntKey()).toString());            
        }
        if(edge.getTarget() != null) {
            Vertex vertex = (Vertex)edge.getTarget();
            vertexElement.setAttribute("target", new Integer(vertex.getIntKey()).toString());            
        }
        vertexElement.setAttribute("time", new Double(edge.getTime()).toString());
        vertexElement.setAttribute("cost", new Double(edge.getCost()).toString());
        parent.appendChild(vertexElement);
    }

    /**
     * <h3>Create 'edges' element</h3>
     * 
     * @param document - xml document object.
     * @param parent - parent dom node.
     * @param edgeIterator - iterator over all graph edges.
     */
    private void createEdgesElement(Document document, Element parent, Iterator edgeIterator) {
        /* create entries element */
        Element edgesElement = document.createElement("edges");
        parent.appendChild(edgesElement);

        while(edgeIterator.hasNext()) {
            Edge edge = (Edge)edgeIterator.next();
            createEdgeElement(document, edgesElement, edge);
        }
    }

    /**
     * <h3>Create graph info 'entry' element</h3>
     * 
     * @param document - xml document object.
     * @param parent - parent dom node.
     * @param entry - entry element to write.
     */
    private void createGraphInfoEntryElement(Document document, Element parent, GraphInfo.Entry entry) {
        /* create entry element */
        Element entryElement = document.createElement("entry");
        entryElement.setAttribute("key", entry.getKey());
        entryElement.setAttribute("value", entry.getValue());
        parent.appendChild(entryElement);
    }
    
    /**
     * <h3>Create graph info 'entries' element</h3>
     * 
     * @param document - xml document object.
     * @param parent - parent dom node.
     * @param entryIterator - iterator over all info entries.
     */
    private void createGraphInfoEntriesElement(Document document, Element parent, Iterator entryIterator) {
        /* create entries element */
        Element entriesElement = document.createElement("entries");
        parent.appendChild(entriesElement);

        while(entryIterator.hasNext()) {
            GraphInfo.Entry entry = (GraphInfo.Entry)entryIterator.next();
            createGraphInfoEntryElement(document, entriesElement, entry);
        }
    }
    
    /**
     * <h3>Create graph 'info' element</h3>
     * 
     * @param document - xml document object.
     * @param parent - parent dom node.
     * @param info - info element to write.
     */
    private void createGraphInfoElement(Document document, Element parent, GraphInfo info) {
        /* create graph info element */
        Element graphInfoElement = document.createElement("info");
        graphInfoElement.setAttribute("generator", info.getGenerator());
        parent.appendChild(graphInfoElement);

        /* create entries elements */
        createGraphInfoEntriesElement(document, graphInfoElement, info.getEntries());
    }
    
    /**
     * <h3>Create 'graph' element</h3>
     * 
     * @param document - xml document object.
     * @param parent - parent dom node.
     * @param graph - graph element to write.
     */
    private void createGraphElement(Document document, Element parent, Graph graph) {
        /* create graph element */
        Element graphElement = document.createElement("graph");

        createGraphInfoElement(document, graphElement, graph.getInfo());
        createVerticesElement(document, graphElement, graph.getAllVertices());
        createEdgesElement(document, graphElement, graph.getAllEdges());

        parent.appendChild(graphElement);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.serialize.IGraphWriter#writeGraph(com.octaedr.octabu.graph.graph.Graph, java.io.File)
     */
    public void writeGraph(Graph graph, File file) {
        DocumentBuilder builder;
        try {
            /* create document */
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();
            Document document = builder.newDocument();
            
            Element octabuElement = document.createElement("octabu");
            octabuElement.setAttribute("type", "graph");
            document.appendChild(octabuElement);
            createGraphElement(document, document.getDocumentElement(), graph);
            
            /* write to file */
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(new DOMSource(document),
                        new StreamResult(new FileOutputStream(file)));
        } catch (ParserConfigurationException exception) {
            exception.printStackTrace();
        } catch (TransformerConfigurationException exception) {
            exception.printStackTrace();
        } catch (TransformerFactoryConfigurationError exception) {
            exception.printStackTrace();
        } catch (FileNotFoundException exception) {
            exception.printStackTrace();
        } catch (TransformerException exception) {
            exception.printStackTrace();
        }
    }


}
