/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.exception;

/**
 * <h3>OCTabu base exception class</h3.
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class OctabuException extends Exception {

    /** <h3>Generated serial number</h3> */
    private static final long serialVersionUID = 4588156872137612378L;

    /**
     * <h3>Constructor</h3>
     */
    public OctabuException() {
        super();
    }

    /**
     * <h3>Constructor</h3>
     * 
     * @param message - detailed exception message.
     */
    public OctabuException(String message) {
        super(message);
    }

}
