/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.exception;

/**
 * <h3>OCTabu error class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class OctabuError extends Error {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = 7345455512086067041L;

    /**
     * <h3>Constructor</h3>
     */
    public OctabuError() {
        super();
    }

    /**
     * <h3>Constructor</h3>
     * 
     * @param message - error message.
     */
    public OctabuError(String message) {
        super(message);
    }

}
