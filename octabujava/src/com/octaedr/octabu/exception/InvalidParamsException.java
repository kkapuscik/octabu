/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.exception;

/**
 * <h3>Invalid parameter exception</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class InvalidParamsException extends OctabuException {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = -5560324704586325926L;

    /**
     * <h3>Constructor</h3>
     */
    public InvalidParamsException() {
        super();
    }

    /**
     * <h3>Constructor</h3>
     * 
     * @param message - exception message
     */
    public InvalidParamsException(String message) {
        super(message);
    }

}
