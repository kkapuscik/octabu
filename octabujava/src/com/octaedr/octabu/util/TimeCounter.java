/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.util;

import java.util.Date;

/**
 * <h3>Time counter utility class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class TimeCounter {

    /** <h3>Start time</h3> */
    private long startTime = Long.MAX_VALUE;

    /** <h3>End time</h3> */
    private long endTime = Long.MAX_VALUE;

    /**
     * <h3>Start time measurement</h3>
     */
    public void start() {
        this.startTime = new Date().getTime();
        this.endTime = Long.MAX_VALUE;
    }

    /**
     * <h3>Finish time measurement</h3>
     */
    public void stop() {
        this.endTime = new Date().getTime();
    }

    /**
     * <h3>Check if this counter has valid state</h3>
     * 
     * @return
     * True if start() and stop() operations was performed
     * and time measured correctly, false otherwise.
     */
    public boolean isValid() {
        return (this.startTime != Long.MAX_VALUE)
                && (this.endTime != Long.MAX_VALUE);
    }

    /**
     * <h3>Get difference between end and start time</h3>
     * 
     * @return
     * Time difference between end and start.
     */
    public long getTimeDiff() {
        if (!isValid()) {
            return Integer.MAX_VALUE;
        }
        return this.endTime - this.startTime;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        if (!isValid()) {
            return "[TimeCounter] -not set-";
        }
        return "[TimeCounter] diff=" + getTimeDiff();
    }

}
