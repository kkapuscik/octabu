/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */


package com.octaedr.octabu.util;

/**
 * <h3>Standard IO output logger adapter class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class StdOutputLogWriter extends Object implements ILoggerListener {

    /**
     * <h3>Initialize log writer</h3>
     */
    static public void init() {
        StdOutputLogWriter instance = new StdOutputLogWriter();
        Logger.addLoggerListener(instance);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.octaedr.octabu.util.ILoggerListener#messageLogged(java.lang.String,
     *      int)
     */
    public void messageLogged(String message, int cathegory) {
        System.out.print(message);
        if (cathegory >= Logger.CATHEGORY_WARNING) {
            System.err.print(message);
        }
    }

}
