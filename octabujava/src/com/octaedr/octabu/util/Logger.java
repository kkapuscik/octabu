/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */


package com.octaedr.octabu.util;

import java.util.HashSet;
import java.util.Iterator;

/**
 * <h3>Logging utility class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class Logger {

    /** <h3>Trace cathegory</h3> */
    public static final int CATHEGORY_TRACE = 0;

    /** <h3>Message (standard log) cathegory</h3> */
    public static final int CATHEGORY_MESSAGE = 1;

    /** <h3>Warning cathegory</h3> */
    public static final int CATHEGORY_WARNING = 2;

    /** <h3>Error cathegory</h3> */
    public static final int CATHEGORY_ERROR = 3;

    /** <h3>Collection of logger listeners</h3> */
    static private HashSet<ILoggerListener> loggerListeners = new HashSet<ILoggerListener>();

    /** <h3>Logging disabled flag</h3> */
    static boolean loggingDisabled = false;
    

    /**
     * <h3>Constructor</h3>
     * 
     * <p>
     * Private constructor to disallow object creation.
     * </p>
     */
    private Logger() {
        // do nothing
    }

    /**
     * <h3>Add logger listener</h3>
     * 
     * @param listener -
     *            listener to add.
     */
    static public void addLoggerListener(ILoggerListener listener) {
        synchronized (loggerListeners) {
            loggerListeners.add(listener);
        }
    }

    /**
     * <h3>Remove logger listener</h3>
     * 
     * @param listener -
     *            listener to remove.
     */
    static public void removeLoggerListener(ILoggerListener listener) {
        synchronized (loggerListeners) {
            loggerListeners.remove(listener);
        }
    }

    /**
     * <h3>Send message to all listeners</h3>
     * 
     * @param message -
     *            message to send.
     */
    static private void sendMessage(String message, int cathegory) {
        synchronized (loggerListeners) {
            Iterator<ILoggerListener> iterator = loggerListeners.iterator();
            while (iterator.hasNext()) {
                iterator.next().messageLogged(message, cathegory);
            }
        }
    }

    /**
     * <h3>Log standard message</h3>
     * 
     * @param message -
     *            message to log.
     */
    static public void log(String message) {
        if(loggingDisabled) {
            return;
        }
        sendMessage(message, CATHEGORY_MESSAGE);
    }

    /**
     * <h3>Log standard message</h3>
     * 
     * <p>
     * This function is equal to log().
     * </p>
     * 
     * @param message -
     *            message to log.
     */
    static public void logMessage(String message) {
        if(loggingDisabled) {
            return;
        }
        sendMessage(message, CATHEGORY_MESSAGE);
    }

    /**
     * <h3>Log trace message</h3>
     * 
     * @param message -
     *            message to log.
     */
    static public void logTrace(String message) {
        if(loggingDisabled) {
            return;
        }
        sendMessage("[TRACE] " + message, CATHEGORY_TRACE);
    }

    /**
     * <h3>Log warning message</h3>
     * 
     * @param message -
     *            message to log.
     */
    static public void logWarning(String message) {
        if(loggingDisabled) {
            return;
        }
        sendMessage("[WARNING] " + message, CATHEGORY_WARNING);
    }

    /**
     * <h3>Log error message</h3>
     * 
     * @param message -
     *            message to log.
     */
    static public void logError(String message) {
        if(loggingDisabled) {
            return;
        }
        sendMessage("[WARNING] " + message, CATHEGORY_ERROR);
    }
    
    /**
     * <h3>Log standard message with newline at end</h3>
     * 
     * @param message -
     *            message to log.
     */
    static public void logln(String message) {
        if(loggingDisabled) {
            return;
        }
        log(message + "\n");
    }

    /**
     * <h3>Log standard message with newline at end</h3>
     * 
     * <p>
     * This function is equal to log().
     * </p>
     * 
     * @param message -
     *            message to log.
     */
    static public void loglnMessage(String message) {
        if(loggingDisabled) {
            return;
        }
        logMessage(message + "\n");
    }

    /**
     * <h3>Log trace message with newline at end</h3>
     * 
     * @param message -
     *            message to log.
     */
    static public void loglnTrace(String message) {
        if(loggingDisabled) {
            return;
        }
        logTrace(message + "\n");
    }

    /**
     * <h3>Log warning message with newline at end</h3>
     * 
     * @param message -
     *            message to log.
     */
    static public void loglnWarning(String message) {
        if(loggingDisabled) {
            return;
        }
        logWarning(message + "\n");
    }

    /**
     * <h3>Log error message with newline at end</h3>
     * 
     * @param message -
     *            message to log.
     */
    static public void loglnError(String message) {
        if(loggingDisabled) {
            return;
        }
        logError(message + "\n");
    }

    /**
     * <h3>Log exception</h3>
     * 
     * @param exception - exception to be logged.
     */
    static public void logException(Exception exception) {
        if(loggingDisabled) {
            return;
        }
        loglnWarning(exception.toString());
        StackTraceElement[] stackTrace = exception.getStackTrace();
        for(int i = 0; i < stackTrace.length; ++i) {
            loglnWarning("   #" + (stackTrace.length - i) + ": " + stackTrace[i].toString());
        }
    }

    /**
     * <h3>Disable logging</h3>
     */
    static public void disableLogging() {
        loggingDisabled = true;
    }

    /**
     * <h3>Enable logging</h3>
     */
    static public void enableLogging() {
        loggingDisabled = false;
    }
    
    /**
     * <h3>Get logging status</h3>
     * 
     * @return
     * True if logging enabled, false if disabled.
     */
    static public boolean isLoggingEnabled() {
        return loggingDisabled ? false : true;
    }
}
