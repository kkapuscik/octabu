/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.util;

/**
 * <h3>Logger listener interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface ILoggerListener {

    /**
     * <h3>New message has been logged</h3>
     * 
     * @param message -
     *            message text.
     * @param cathegory -
     *            message cathegory.
     */
    public void messageLogged(String message, int cathegory);

}
