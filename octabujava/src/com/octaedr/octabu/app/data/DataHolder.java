/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.app.data;

import java.awt.Color;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import com.octaedr.octabu.graph.algorithm.AlgorithmResults;
import com.octaedr.octabu.graph.graph.Graph;

/**
 * <h3>Data holder class that manages all application data</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class DataHolder {

    /** <h3>Results / Draw dots option identifier</h3> */
    public static final String OPTION_RESULTS_DRAW_DOTS = "results/dots";
    /** <h3>Results / Draw lines option identifier</h3> */
    public static final String OPTION_RESULTS_DRAW_LINES = "results/lines";
    /** <h3>Results / Draw regions option identifier</h3> */
    public static final String OPTION_RESULTS_DRAW_REGIONS = "results/regions";
    /** <h3>Results / Draw detailed solutions option identifier</h3> */
    public static final String OPTION_RESULTS_DRAW_DETAILED_SOLUTIONS = null;
    /** <h3>Image / Draw border option identifier</h3> */
    public static final String OPTION_IMAGE_DRAW_BORDER = "image/border";
    /** <h3>Image / Draw dots option identifier</h3> */
    public static final String OPTION_IMAGE_DRAW_DOTS = "image/dots";
    /** <h3>Image / Draw lines option identifier</h3> */
    public static final String OPTION_IMAGE_DRAW_LINES = "image/lines";
    /** <h3>Image / Draw regions option identifier</h3> */
    public static final String OPTION_IMAGE_DRAW_REGIONS = "image/regions";
    /** <h3>Image / Width option identifier</h3> */
    public static final String OPTION_IMAGE_WIDTH = "image/width";
    /** <h3>Image / Height option identifier</h3> */
    public static final String OPTION_IMAGE_HEIGHT = "image/height";
    /** <h3>Image / Format option identifier</h3> */
    public static final String OPTION_IMAGE_FORMAT = "image/format";
    /** <h3>Algorithm execute / Source vertex option identifier</h3> */
    public static final String OPTION_ALGORITHM_SOURCE_VERTEX = "algorithm/sourcevertex";
    /** <h3>Algorithm execute / Target vertex option identifier</h3> */
    public static final String OPTION_ALGORITHM_TARGET_VERTEX = "algorithm/targetvertex";

    
    /** <h3>The only instance of data holder</h3> */
    private static DataHolder theInstance = new DataHolder();

    
    /**
     * <h3>Results entry class</h3>
     * 
     * @author Krzysztof Kapuscik
     * @version 1.0
     */
    public class ResultsEntry {
        /** <h3>Stored results object</h3> */
        private AlgorithmResults algorithmResults;
        /** <h3>Color assigned to results</h3> */
        private Color assignedColor;
        
        /**
         * <h3>Constructor</h3>
         * 
         * @param results - results object.
         * @param color - assigned color.
         */
        public ResultsEntry(AlgorithmResults results, Color color) {
            this.algorithmResults = results;
            this.assignedColor = color;
        }

        /**
         * <h3>Get stored results object</h3>
         * 
         * @return
         * Results object stored.
         */
        public AlgorithmResults getResults() {
            return this.algorithmResults;
        }

        /**
         * <h3>Get color assigned to results</h3>
         * 
         * @return
         * Color assigned to results.
         */
        public Color getColor() {
            return this.assignedColor;
        }
        
        /**
         * <h3>Set color assigned to results</h3>
         * 
         * @param color - color to set.
         */
        public void setColor(Color color) {
            this.assignedColor = color;
        }
    }
    
    /** <h3>Currently processed graph</h3> */
    private Graph currentGraph;
    /** <h3>Results of algorithm execution</h3> */
    private LinkedList<ResultsEntry> resultsList =
            new LinkedList<ResultsEntry>();
    
    /** <h3>Table of available colors</h3> */
    private Color[] colorsTable = new Color[] {
            new Color(255, 0, 0),
            new Color(0, 255, 0),
            new Color(0, 0, 255),
            new Color(128, 128, 0),
            new Color(128, 0, 128),
            new Color(0, 128, 128)
    };
    /** <h3>Index of next color to use</h3 */
    private int nextColor = 0;
    
    /** <h3>Collection of data holder listeners</h3> */
    private HashSet<IDataHolderListener> listenersCollection =
            new HashSet<IDataHolderListener>();
    
    /** <h3>Map of options and their values</h3. */
    private HashMap<String,Object> options = 
            new HashMap<String,Object>();

    
    /**
     * <h3>Get instance of octabu data holder</h3>
     * 
     * @return
     * The only instance of application data holder.
     */
    public static DataHolder getInstance() {
        return theInstance;
    }
    
    /**
     * <h3>Constructor</h3>
     * 
     * <p>Private constructor so object of this class could not
     * be created directly.</p>
     */
    private DataHolder() {
        initOptions();
    }

    /**
     * <h3>Add data holder listener</h3>
     * 
     * @param listener - listener to add.
     */
    public void addDataHolderListener(IDataHolderListener listener) {
        synchronized(this.listenersCollection) {
            this.listenersCollection.add(listener);
            
            listener.graphSet(this.currentGraph);
            listener.resultsChanged();

            Iterator<Entry<String,Object>> optionIterator =
                    this.options.entrySet().iterator();
            while(optionIterator.hasNext()) {
                Entry<String,Object> entry = optionIterator.next();
                listener.optionValueChanged(
                        entry.getKey(), entry.getValue());
            }
        }
    }

    /**
     * <h3>Remove data holder listener</h3>
     * 
     * @param listener - listener to remove.
     */
    public void removeDataHolderListener(IDataHolderListener listener) {
        synchronized(this.listenersCollection) {
            this.listenersCollection.remove(listener);
        }
    }
    
    /**
     * <h3>Emit graph set event to all listeners</h3>
     * 
     * @param graph - graph object to pass as param.
     */
    private void emitGraphSetEvent(Graph graph) {
        synchronized(this.listenersCollection) {
            Iterator<IDataHolderListener> iterator =
                this.listenersCollection.iterator();
            while(iterator.hasNext()) {
                IDataHolderListener listener = iterator.next();                
                listener.graphSet(graph);
            }
        }
    }
    
    /**
     * <h3>Emit option changed event to all listeners</h3>
     * 
     * @param option - option name to pass as param.
     * @param value - option value to pass as param.
     */
    private void emitOptionChangedEvent(String option, Object value) {
        synchronized(this.listenersCollection) {
            Iterator<IDataHolderListener> iterator =
                this.listenersCollection.iterator();
            while(iterator.hasNext()) {
                IDataHolderListener listener = iterator.next();                
                listener.optionValueChanged(option, value);
            }
        }
    }
    
    /**
     * <h3>Emit results changed event to all listeners</h3>
     */
    private void emitResultsChangedEvent() {
        synchronized(this.listenersCollection) {
            Iterator<IDataHolderListener> iterator =
                this.listenersCollection.iterator();
            while(iterator.hasNext()) {
                IDataHolderListener listener = iterator.next();                
                listener.resultsChanged();
            }
        }
    }
    
    /**
     * <h3>Get stored graph</h3>
     * 
     * @return
     * Stored graph object.
     */
    public Graph getGraph() {
        return this.currentGraph;
    }
    
    /**
     * <h3>Set stored graph</h3>
     * 
     * @param graph - graph object to store.
     */
    public void setGraph(Graph graph) {
        clearResults();
        
        this.currentGraph = graph;
        emitGraphSetEvent(graph);
        
        if(graph != null) {
            setOption(OPTION_ALGORITHM_SOURCE_VERTEX, 1);
            setOption(OPTION_ALGORITHM_TARGET_VERTEX, graph.getAllVerticesCount());
        } else {
            setOption(OPTION_ALGORITHM_SOURCE_VERTEX, 0);
            setOption(OPTION_ALGORITHM_TARGET_VERTEX, 0);            
        }
    }

    /**
     * <h3>Get unused color</h3>
     * 
     * @return
     * Unused color.
     */
    private Color getUnusedColor() {
        Color color = this.colorsTable[this.nextColor];
        this.nextColor++;
        if(this.nextColor >= this.colorsTable.length) {
            this.nextColor = 0;
        }
        return color;
    }

    /**
     * <h3>Add algorithm results</h3>
     * 
     * @param results - results to add.
     */
    public void addAlgorithmResults(AlgorithmResults results) {
        this.resultsList.add(new ResultsEntry(results, getUnusedColor()));
        emitResultsChangedEvent();
    }
    
    /**
     * <h3>Get all algorithm results</h3>
     * 
     * @return
     * Iterator over algorithm results collection.
     */
    public Iterator<ResultsEntry> getResults() {
        return this.resultsList.iterator();
    }

    /**
     * <h3>Remove all algorithm results</h3>
     */
    public void clearResults() {
        this.resultsList.clear();
        emitResultsChangedEvent();
    }

    /**
     * <h3>Initialize options with default values</h3>
     */
    public void initOptions() {
        setOption(OPTION_RESULTS_DRAW_DOTS, true);
        setOption(OPTION_RESULTS_DRAW_LINES, true);
        setOption(OPTION_RESULTS_DRAW_REGIONS, true);
        setOption(OPTION_RESULTS_DRAW_DETAILED_SOLUTIONS, false);
        setOption(OPTION_IMAGE_DRAW_BORDER, true);
        setOption(OPTION_IMAGE_DRAW_DOTS, true);
        setOption(OPTION_IMAGE_DRAW_LINES, true);
        setOption(OPTION_IMAGE_DRAW_REGIONS, true);
        setOption(OPTION_IMAGE_WIDTH, 800);
        setOption(OPTION_IMAGE_HEIGHT, 600);
        setOption(OPTION_IMAGE_FORMAT, "png");
        setOption(OPTION_ALGORITHM_SOURCE_VERTEX, 0);
        setOption(OPTION_ALGORITHM_TARGET_VERTEX, 0);
    }

    /**
     * <h3>Set option value</h3>
     * 
     * @param option - option name.
     * @param value - option value.
     */
    public void setOption(String option, Object value) {
        this.options.put(option, value);
        emitOptionChangedEvent(option, value);
    }

    /**
     * <h3>Get boolean value of an option</h3>
     * 
     * @param option - option name.
     * 
     * @return
     * Option value or false if option does not exist.
     */
    public boolean getOptionBoolean(String option) {
        Object value = this.options.get(option);
        if(value != null) {
            return ((Boolean)value).booleanValue();
        }
        return false;
    }

    /**
     * <h3>Get integer value of an option</h3>
     * 
     * @param option - option name.
     * 
     * @return
     * Option value or 0 if option does not exist.
     */
    public int getOptionInteger(String option) {
        Object value = this.options.get(option);
        if(value != null) {
            return ((Integer)value).intValue();
        }
        return 0;
    }

    /**
     * <h3>Get integer value of an option</h3>
     * 
     * @param option - option name.
     * 
     * @return
     * Option value or null if option does not exist.
     */
    public String getOptionString(String option) {
        Object value = this.options.get(option);
        if(value != null) {
            return (String)value;
        }
        return null;
    }
    
    /**
     * <h3>Get number of algorithm results</h3>
     * 
     * @return
     * Number of algorithm results stored.
     */
    public int getResultsCount() {
        return this.resultsList.size();
    }
    
    /**
     * <h3>Get graph vertex count</h3>
     * 
     * @return
     * Graph vertex count or 0 if no graph is currently set.
     */
    public int getGraphVertexCount() {
        if(this.currentGraph != null) {
            return this.currentGraph.getAllVerticesCount();
        }
        return 0;
    }
}
