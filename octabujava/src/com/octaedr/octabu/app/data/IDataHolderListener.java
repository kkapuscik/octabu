/**
 * Created:     2005-11-14
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.app.data;

import com.octaedr.octabu.graph.graph.Graph;

/**
 * <h3>Data holder listener</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IDataHolderListener {

    /**
     * <h3>Method called when graph was set</h3>
     * 
     * @param graph - graph set in data holder.
     */
    public void graphSet(Graph graph);

    /**
     * <h3>Method called when results data has changed</h3>
     */
    public void resultsChanged();
    
    /**
     * <h3>Option value changed</h3>
     * 
     * @param option - name of the option that changed value.
     * @param value - new option value.
     */
    public void optionValueChanged(String option, Object value);
}
