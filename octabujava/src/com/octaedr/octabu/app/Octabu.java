/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.app;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.octaedr.octabu.app.plugin.PluginManager;
import com.octaedr.octabu.gui.mainframe.MainFrame;
import com.octaedr.octabu.plugin.algorithm.brumbaugh.BrumbaughAlgorithmPlugin;
import com.octaedr.octabu.plugin.algorithm.dijkstra.DijkstraAlgorithmPlugin;
import com.octaedr.octabu.plugin.algorithm.scalarization.ScalarizationAlgorithmPlugin;
import com.octaedr.octabu.plugin.algorithm.skriver.SkriverAlgorithmPlugin;
import com.octaedr.octabu.plugin.algorithm.tabu.TabuAlgorithmPlugin;
import com.octaedr.octabu.plugin.generator.full.FullGeneratorPlugin;
import com.octaedr.octabu.plugin.generator.random.RandomGeneratorPlugin;
import com.octaedr.octabu.plugin.generator.skriver.SkriverGeneratorPlugin;
import com.octaedr.octabu.util.Logger;
import com.octaedr.octabu.util.StdOutputLogWriter;

/**
 * Main OCTabu application class
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class Octabu {

    /** <h3>OCTabu version</h3> */
    public static final String VERSION_STRING  =   "2005-11-16";
    /** <h3>OCTabu copyrights</h3> */
    public static final String COPYRIGHT_STRING = "(C) 2005 Krzysztof Kapuscik";

    /** <h3>OCTabu main frame</h3> */
    private static MainFrame octabuFrame;

    /**
     * <h3>Get OCTabu main frame</h3>
     * 
     * @return
     * OCTabu main frame or null if not set.
     */
    public static MainFrame getOctabuFrame() {
        return octabuFrame;
    }
    
    /**
     * <h3>Set main OCTabu frame</h3>
     * 
     * @param frame - frame to set as OCTabu main frame.
     */
    private static void setOctabuFrame(MainFrame frame) {
        if(octabuFrame != null) {
            Logger.logError("[OCTabu] OCTabu frame already set!");
            System.exit(0);
        }
        octabuFrame = frame;
    }
    
    /**
     * Main application function.
     * 
     * @param args -
     *            application arguments.
     */
    public static void main(String[] args) {

        /* initialize logging functionality */
        StdOutputLogWriter.init();

        Logger.logln("[OCTabu] Starting OCTabu " + VERSION_STRING);        
        
        /* create frame */
        Logger.logln("[OCTabu] Creating main frame");        
        MainFrame mainFrame = new MainFrame("OCTabu " + VERSION_STRING);

        /* set main OCTabu frame */
        Logger.logln("[OCTabu] Setting OCTabu main frame");
        setOctabuFrame(mainFrame);

        /* provide frame close code */
        mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        mainFrame.addWindowListener(new WindowAdapter() {
            /* (non-Javadoc)
             * @see java.awt.event.WindowAdapter#windowClosing(java.awt.event.WindowEvent)
             */
            public void windowClosing(WindowEvent e) {
                if(JOptionPane.showConfirmDialog(
                        Octabu.getOctabuFrame(),
                        "Are you sure you want to quit?\n" +
                        "(all unsaved data will be lost)",
                        "OCTabu - Quit",
                        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

                    Logger.logln("[OCTabu] Closing OCTabu application");
                    Octabu.getOctabuFrame().dispose();
                }
            }
        });

        /* set initial size of frame */
        mainFrame.setBounds(100, 100, 800, 600);

        /* register plugins */
        Logger.logln("[OCTabu] Registering plugins");        
        registerPlugins();

        /* show frame */
        mainFrame.setVisible(true);
    }

    /**
     * <h3>Register OCTabu plugins</h3>
     */
    static private void registerPlugins() {
        /* register generators */
        Logger.logln("[OCTabu] Registering generator plugins");        
        PluginManager.getInstance().addPlugin(new FullGeneratorPlugin());
        PluginManager.getInstance().addPlugin(new RandomGeneratorPlugin());
        PluginManager.getInstance().addPlugin(new SkriverGeneratorPlugin());
        
        /* register algorithms */
        Logger.logln("[OCTabu] Registering algorithm plugins");
        PluginManager.getInstance().addPlugin(new DijkstraAlgorithmPlugin());
        PluginManager.getInstance().addPlugin(new ScalarizationAlgorithmPlugin());
        PluginManager.getInstance().addPlugin(new BrumbaughAlgorithmPlugin());
        PluginManager.getInstance().addPlugin(new SkriverAlgorithmPlugin());
        PluginManager.getInstance().addPlugin(new TabuAlgorithmPlugin());
    }
}
