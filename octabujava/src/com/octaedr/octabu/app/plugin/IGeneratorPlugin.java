/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.app.plugin;

import com.octaedr.octabu.exception.OctabuException;
import com.octaedr.octabu.graph.graph.Graph;

/**
 * <h3>OCTabu generator plugin interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IGeneratorPlugin extends IPlugin {

    /**
     * <h3>Generate graph using plugin options</h3>
     * 
     * @return
     * Generated graph.
     * 
     * @throws OctabuException
     * when any error happened during generation.
     */
    public Graph generate() throws OctabuException;
    
}
