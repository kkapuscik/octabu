/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.app.plugin;


/**
 * <h3>OCTabu algorithm plugin interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IAlgorithmPlugin extends IPlugin, IAlgorithm {
    // nothing to do
}
