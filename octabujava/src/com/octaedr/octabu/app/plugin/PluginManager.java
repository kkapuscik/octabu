/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.app.plugin;

import java.util.HashSet;
import java.util.Iterator;

import com.octaedr.octabu.util.Logger;

/**
 * <h3>Plugin manager class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class PluginManager {

    /** <h3>The only instance of plugin manager</h3> */
    private static PluginManager theInstance = new PluginManager();
    
    /** <h3>Collection of manager listeners</h3> */
    HashSet<IPluginManagerListener> listenerCollection =
            new HashSet<IPluginManagerListener>();

    /** <h3>Algorithm plugins collection</h3> */
    HashSet<IAlgorithmPlugin> algorithmPlugins =
            new HashSet<IAlgorithmPlugin>();

    /** <h3>Generator plugins collection</h3> */
    HashSet<IGeneratorPlugin> generatorPlugins =
            new HashSet<IGeneratorPlugin>();
    
    /**
     * <h3>Constructor</h3>
     *
     * <p>Private constructor to disable creating objects of this class.</p>
     */
    private PluginManager() {
        // nothing to do
    }

    /**
     * <h3>Get instance of plugin manager</h3>
     * 
     * @return
     * Plugin manager instance.
     */
    public static PluginManager getInstance() {
        return theInstance;
    }
    
    /**
     * <h3>Add algorithm plugin</h3>
     * 
     * @param plugin - plugin to add.
     */
    public void addPlugin(IAlgorithmPlugin plugin) {
        synchronized(this.algorithmPlugins) {
            Logger.logln("[PluginManager] Registering algorithm plugin: " + plugin.getName());
            this.algorithmPlugins.add(plugin);
            callListenersAlgorithmPluginAdded(plugin);
        }
    }
    
    /**
     * <h3>Add generator plugin</h3>
     * 
     * @param plugin - plugin to add.
     */
    public void addPlugin(IGeneratorPlugin plugin) {
        synchronized(this.generatorPlugins) {
            Logger.logln("[PluginManager] Registering generator plugin: " + plugin.getName());
            this.generatorPlugins.add(plugin);
            callListenersGeneratorPluginAdded(plugin);
        }
    }
    
    /**
     * <h3>Add manager listener to listeners collection</h3>
     * 
     * @param listener - listener to add.
     */
    public void addPluginManagerListener(IPluginManagerListener listener) {
        synchronized(this.listenerCollection) {
            this.listenerCollection.add(listener);
        }
    }
    
    /**
     * <h3>Remove manager listener to listeners collection</h3>
     * 
     * @param listener - listener to remove.
     */
    public void removePluginManagerListener(IPluginManagerListener listener) {
        synchronized(this.listenerCollection) {
            this.listenerCollection.remove(listener);
        }
    }
    
    /**
     * <h3>Call listeners algorithm plugin added action</h3>
     * 
     * @param plugin - plugin added to manager.
     */
    private void callListenersAlgorithmPluginAdded(IAlgorithmPlugin plugin) {
        synchronized(this.listenerCollection) {
            Iterator<IPluginManagerListener> iterator =
                this.listenerCollection.iterator();
            while(iterator.hasNext()) {
                iterator.next().algorithmPluginAdded(plugin);
            }
        }
    }

    /**
     * <h3>Call listeners generator plugin added action</h3>
     * 
     * @param plugin - plugin added to manager.
     */
    private void callListenersGeneratorPluginAdded(IGeneratorPlugin plugin) {
        synchronized(this.listenerCollection) {
            Iterator<IPluginManagerListener> iterator =
                this.listenerCollection.iterator();
            while(iterator.hasNext()) {
                iterator.next().generatorPluginAdded(plugin);
            }
        }
    }

}
