/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.app.plugin;

import com.octaedr.octabu.graph.algorithm.AlgorithmResults;
import com.octaedr.octabu.graph.graph.Graph;

/**
 * <h3>Algorithm interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IAlgorithm {

    /**
     * <h3>Prepare graph for algorithm</h3>
     * 
     * @param graph - graph to process.
     * 
     * @return
     * Prepared graph (could be same as given if no preparation is needed) or
     * null on error.
     */
    public Graph prepareGraph(Graph graph);

    /**
     * <h3>Process algorithm</h3>
     * 
     * @param results - algorithm results object.
     * @param graph - prepared graph to process.
     * @param source - source vertex key.
     * @param target - target vertex key.
     */
    public void process(AlgorithmResults results, Graph graph, int source, int target);

    /**
     * <h3>Prepare algorithm results</h3>
     * 
     * @param results - algorithm results object.
     * @param graph - prepared graph to process.
     * @param source - source vertex key.
     * @param target - target vertex key.
     */
    public void prepareResults(AlgorithmResults results, Graph graph, int source, int target);
    
}
