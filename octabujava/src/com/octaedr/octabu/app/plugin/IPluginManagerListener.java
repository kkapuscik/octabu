/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.app.plugin;


/**
 * <h3>Plugin manager listener interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IPluginManagerListener {

    /**
     * <h3>Generator plugin has been added to manager</h3>
     * 
     * @param plugin - plugin added to manager.
     */
    public void generatorPluginAdded(IGeneratorPlugin plugin);

    /**
     * <h3>Algorithm plugin has been added to manager</h3>
     * 
     * @param plugin - plugin added to manager.
     */
    public void algorithmPluginAdded(IAlgorithmPlugin plugin);
    
}
