/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.app.plugin;

import javax.swing.JPanel;

/**
 * <h3>OCTabu plugin interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IPlugin {

    /**
     * <h3>Get short plugin name</h3>
     * 
     * <p>This method should return short plugin name
     * without spaces and using only alphanumerical
     * characters and '/' character (ex. "algorithm/dijkstra").<p>
     * 
     * @return
     * Short plugin name.
     */
    public String getShortName();

    /**
     * <h3>Get plugin name</h3>
     * 
     * <p>This method should return human readable plugin name
     * (ex. "Dijkstra Algorithm 1.0").
     * 
     * @return
     * Human readable plugin name.
     */
    public String getName();

    /**
     * <h3>Get gui panel that allows setting plugin options</h3>
     * 
     * @return
     * Options panel or null if does not have any.
     */
    public JPanel getOptionsPanel();

    /**
     * <h3>Check if plugin options values are valid<h3>
     * 
     * @return
     * True if options values are valid, false otherwise.
     */
    public boolean checkOptions();
    
    /**
     * <h3>Set plugin options to valid values</h3>
     * 
     * <p>This method validates plugin options values and
     * sets them so they are suitable for plugin execution.
     * In these method two things must be done: (1) values
     * validation and (2) options panel refresh if any of
     * the values has changes.</p>
     */
    public void validateOptions();
}
