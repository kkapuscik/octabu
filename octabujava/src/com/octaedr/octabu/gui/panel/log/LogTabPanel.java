/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.gui.panel.log;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import com.octaedr.octabu.gui.action.Action;
import com.octaedr.octabu.gui.action.ActionManager;
import com.octaedr.octabu.gui.action.IActionPerformedListener;
import com.octaedr.octabu.util.ILoggerListener;
import com.octaedr.octabu.util.Logger;

/**
 * <h3>Log panel class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class LogTabPanel extends JPanel {

    /**
     * <h3>Refresh timer task class</h3>
     * 
     * @author Krzysztof Kapuscik
     */
    private class RefreshTimerTask extends TimerTask implements ILoggerListener {

        /** <h3>Task owner</h3> */
        private LogTabPanel ownerPanel;

        /** <h3>Messages list</h3> */
        private LinkedList<String> messageList = new LinkedList<String>();

        /**
         * <h3>Constructor</h3>
         * 
         * @param owner -
         *            owner of this refresh task.
         */
        public RefreshTimerTask(LogTabPanel owner) {
            this.ownerPanel = owner;
            Logger.addLoggerListener(this);
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Runnable#run()
         */
        public void run() {
            synchronized (this.messageList) {
                /* get iterator over stored items */
                Iterator<String> messageIterator = this.messageList.iterator();

                /* send message to panel */
                while (messageIterator.hasNext()) {
                    this.ownerPanel.addLogMessage(messageIterator.next());
                }

                /* clear list of messages */
                this.messageList.clear();
            }
        }

        /*
         * (non-Javadoc)
         * 
         * @see com.octaedr.octabu.util.ILoggerListener#messageLogged(java.lang.String,
         *      int)
         */
        public void messageLogged(String message, int cathegory) {
            synchronized (this.messageList) {
                this.messageList.add(message);
            }
        }

    }

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = -309623684726333420L;

    /** <h3>Log view refresh period</h3> */
    private static final int REFRESH_PERIOD = 2000;

    /** <h3>Refresh timer object</h3> */
    private Timer refreshTimer;

    /** <h3>Timer task for refreshing log contents</h3> */
    private RefreshTimerTask timerTask;

    /** <h3>Text area with log</h3> */
    private JTextArea logTextArea;

    /** <h3>Logging status button</h3> */
    private JButton loggingStatusButton;

    
    
    /**
     * <h3>Constructor</h3>
     */
    public LogTabPanel() {
        /* set layout */
        super(new BorderLayout());

        /* create subpanels */
        add(createLogPanel(), BorderLayout.CENTER);
        add(createActionsPanel(), BorderLayout.EAST);

        /* create refreshing task */
        createRefreshTask();

        /* connect code to actions */
        connectActions();
    }

    /**
     * <h3>Create panel refresh task</h3>
     */
    private void createRefreshTask() {
        /* create timer task that refreshes log panel contents */
        this.timerTask = new RefreshTimerTask(this);

        /* create refresh timer */
        this.refreshTimer = new Timer(true);
        this.refreshTimer.schedule(this.timerTask, new Date(), REFRESH_PERIOD);
    }

    /**
     * <h3>Create panel with log</h3>
     * 
     * @return Created panel.
     */
    private JPanel createLogPanel() {
        /* create text area that will show the log */
        this.logTextArea = new JTextArea();
        this.logTextArea.setEditable(false);
        this.logTextArea.setBorder(new BevelBorder(BevelBorder.LOWERED));

        /* create panel for log view */
        JPanel infoPanel;
        infoPanel = new JPanel(new BorderLayout());
        infoPanel.setBorder(new TitledBorder("Log"));
        infoPanel.add(new JScrollPane(this.logTextArea), BorderLayout.CENTER);
        
        return infoPanel;
    }

    /**
     * <h3>Create panel with action controls</h3>
     * 
     * @return Created panel.
     */
    private JPanel createActionsPanel() {

        Action loggingAction;

        /* create panel with actions */
        JPanel actionsPanel;
        actionsPanel = new JPanel(new GridLayout(2, 1, 0, 5));
        actionsPanel.add(new JButton(ActionManager.getInstance().getAction(
                ActionManager.LOG_CLEAR)));

        if(Logger.isLoggingEnabled()) {
            loggingAction = ActionManager.getInstance().getAction(
                    ActionManager.LOG_DISABLE);
        } else {
            loggingAction = ActionManager.getInstance().getAction(
                    ActionManager.LOG_ENABLE);
        }
        
        this.loggingStatusButton = new JButton(loggingAction);
        actionsPanel.add(this.loggingStatusButton);
        
        /* create panel to layout elements correctly */
        JPanel layoutPanel;
        layoutPanel = new JPanel(new BorderLayout());
        layoutPanel.setBorder(new TitledBorder("Actions"));
        layoutPanel.add(actionsPanel, BorderLayout.NORTH);

        return layoutPanel;
    }

    /**
     * <h3>Connect actions to code</h3>
     */
    private void connectActions() {
        ActionManager.getInstance().getAction(ActionManager.LOG_CLEAR)
                .addActionPerformedListener(new IActionPerformedListener() {

                    /*
                     * (non-Javadoc)
                     * 
                     * @see com.octaedr.octabu.gui.action.IActionPerformedListener#actionPerformed(com.octaedr.octabu.gui.action.Action,
                     *      java.awt.event.ActionEvent)
                     */
                    public void actionPerformed(Action action, ActionEvent event) {
                        clearLogMessages();
                    }

                });

        ActionManager.getInstance().getAction(ActionManager.LOG_ENABLE)
                .addActionPerformedListener(new IActionPerformedListener() {

            /*
             * (non-Javadoc)
             * 
             * @see com.octaedr.octabu.gui.action.IActionPerformedListener#actionPerformed(com.octaedr.octabu.gui.action.Action,
             *      java.awt.event.ActionEvent)
             */
            public void actionPerformed(Action action, ActionEvent event) {
                enableLogging();
            }

        });

        ActionManager.getInstance().getAction(ActionManager.LOG_DISABLE)
                .addActionPerformedListener(new IActionPerformedListener() {

            /*
             * (non-Javadoc)
             * 
             * @see com.octaedr.octabu.gui.action.IActionPerformedListener#actionPerformed(com.octaedr.octabu.gui.action.Action,
             *      java.awt.event.ActionEvent)
             */
            public void actionPerformed(Action action, ActionEvent event) {
                disableLogging();
            }

        });

    }

    /**
     * <h3>Enable logging</h3>
     */
    protected void enableLogging() {
        Logger.enableLogging();
        this.loggingStatusButton.setAction(
                ActionManager.getInstance().getAction(ActionManager.LOG_DISABLE));
    }
    
    /**
     * <h3>Disable logging</h3>
     */
    protected void disableLogging() {
        Logger.disableLogging();
        this.loggingStatusButton.setAction(
                ActionManager.getInstance().getAction(ActionManager.LOG_ENABLE));
    }
    
    /**
     * <h3>Add log message to log view</h3>
     * 
     * @param message -
     *            message to add.
     */
    void addLogMessage(String message) {
        this.logTextArea.append(message);
    }

    /**
     * <h3>Clear all log message</h3>
     */
    void clearLogMessages() {
        this.logTextArea.setText(null);
    }

}
