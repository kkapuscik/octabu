/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.gui.panel.graph;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import com.octaedr.octabu.app.data.DataHolder;
import com.octaedr.octabu.app.data.IDataHolderListener;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.graph.GraphInfo;
import com.octaedr.octabu.gui.action.ActionManager;

/**
 * <h3>Graph tab panel</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class GraphTabPanel extends JPanel implements IDataHolderListener {

    /** <h3>Serial unique identifier</h3> */
    private static final long serialVersionUID = 3415140947921305863L;

    /** <h3>Graph info text area control</h3> */
    private JTextArea infoTextArea;

    /**
     * <h3>Constructor.</h3>
     */
    public GraphTabPanel() {
        super(new BorderLayout());

        add(createInfoPanel(), BorderLayout.CENTER);
        add(createActionsPanel(), BorderLayout.EAST);
        
        DataHolder.getInstance().addDataHolderListener(this);
    }

    /**
     * <h3>Create info subpanel</h3>
     * 
     * @return
     * Created subpanel.
     */
    private JPanel createInfoPanel() {
        JPanel infoPanel;

        this.infoTextArea = new JTextArea();
        this.infoTextArea.setEditable(false);
        this.infoTextArea.setBorder(new BevelBorder(BevelBorder.LOWERED));

        infoPanel = new JPanel(new BorderLayout());
        infoPanel.setBorder(new TitledBorder("Graph info"));
        infoPanel.add(new JScrollPane(this.infoTextArea), BorderLayout.CENTER);

        return infoPanel;
    }

    /**
     * <h3>Create actions subpanel</h3>
     * 
     * @return
     * Created panel.
     */
    private JPanel createActionsPanel() {

        JPanel layoutPanel;
        JPanel actionsPanel;

        actionsPanel = new JPanel(new GridLayout(5, 1, 0, 5));
        actionsPanel.add(new JButton(ActionManager.getInstance().getAction(
                ActionManager.GRAPH_DISPOSE)));
        actionsPanel.add(new JButton(ActionManager.getInstance().getAction(
                ActionManager.GRAPH_OPEN)));
        actionsPanel.add(new JButton(ActionManager.getInstance().getAction(
                ActionManager.GRAPH_SAVE)));
        actionsPanel.add(new JButton(ActionManager.getInstance().getAction(
                ActionManager.GRAPH_SAVE_INFO)));
        actionsPanel.add(new JButton(ActionManager.getInstance().getAction(
                ActionManager.VIEW_GENERATORS)));

        layoutPanel = new JPanel(new BorderLayout());
        layoutPanel.setBorder(new TitledBorder("Actions"));
        layoutPanel.add(actionsPanel, BorderLayout.NORTH);

        return layoutPanel;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.data.IDataHolderListener#graphSet(com.octaedr.octabu.graph.graph.Graph)
     */
    public void graphSet(Graph graph) {
        if(graph != null) {
            /* clear old text */
            this.infoTextArea.setText(null);

            /* get graph info */
            GraphInfo info = graph.getInfo();
            if(info != null) {
                /* print generator data */
                this.infoTextArea.append("Generator: ");
                this.infoTextArea.append(info.getGenerator());
                this.infoTextArea.append("\n\n");
                
                /* print info entries */
                Iterator<GraphInfo.Entry> iterator = info.getEntries();
                while(iterator.hasNext()) {
                    GraphInfo.Entry entry = iterator.next();

                    this.infoTextArea.append(entry.getKey() + ": " +
                            entry.getValue() + "\n");
                }                
            } else {
                this.infoTextArea.setText("No graph info available");
            }
        } else {
            this.infoTextArea.setText("No graph");
        }
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.data.IDataHolderListener#resultsChanged()
     */
    public void resultsChanged() {
        // nothing to do
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.data.IDataHolderListener#optionValueChanged(java.lang.String, java.lang.Object)
     */
    public void optionValueChanged(String option, Object value) {
        // nothing to do
    }
}
