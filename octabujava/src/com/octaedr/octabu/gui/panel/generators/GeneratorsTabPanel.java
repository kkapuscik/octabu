/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.gui.panel.generators;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.octaedr.octabu.app.plugin.IAlgorithmPlugin;
import com.octaedr.octabu.app.plugin.IGeneratorPlugin;
import com.octaedr.octabu.app.plugin.IPluginManagerListener;
import com.octaedr.octabu.app.plugin.PluginManager;
import com.octaedr.octabu.gui.action.ActionManager;

/**
 * <h3>Generators tab panel</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class GeneratorsTabPanel extends JPanel implements IPluginManagerListener {

    /** <h3>Serial unique identifier.</h3> */
    private static final long serialVersionUID = 9018907030277821233L;

    private static final String NO_OPTIONS_PANEL_NAME = "NO/OPTIONS";
    
    /** <h3>Generator selection combo box control</h3> */
    private JComboBox generatorsCombo;
    /** <h3>Options panel</h3> */
    private JPanel optionsPanel;
    /** <h3>Panel shown when plugin has no options panel</h3> */
    private JPanel noOptionsPanel;
    /** <h3>Layout for options panel</h3> */
    private CardLayout optionsPanelLayout;

    /** <h3>Algorithm plugins name mappings collection</h3> */
    private HashMap<String, IGeneratorPlugin> generatorsCollection = 
            new HashMap<String, IGeneratorPlugin>();
    
    /**
     * <h3>Constructor.</h3>
     */
    public GeneratorsTabPanel() {
        /* create panel with border layout */
        super(new BorderLayout());

        /* create subpanels */
        add(createSelectionPanel(), BorderLayout.NORTH);
        add(createOptionsPanel(), BorderLayout.CENTER);

		/* connect code to actions */
        connectActions();

        /* register as a plugin manager listener */
        PluginManager.getInstance().addPluginManagerListener(this);
    }

    /**
     * <h3>Create options subpanel</h3>
     * 
     * @return
     * Created subpanel.
     */
    private Component createOptionsPanel() {
        this.optionsPanelLayout = new CardLayout();
        this.optionsPanel = new JPanel(this.optionsPanelLayout);
        this.optionsPanel.setBorder(
                new CompoundBorder(
                        new TitledBorder("Generator settings"),
                        new EmptyBorder(3,3,3,3)));

        this.noOptionsPanel = new JPanel(new BorderLayout());
        this.noOptionsPanel.add(
                new JLabel("- No options available -", JLabel.CENTER),
                BorderLayout.CENTER);

        this.optionsPanel.add(this.noOptionsPanel, NO_OPTIONS_PANEL_NAME);

        return this.optionsPanel;
    }

    /**
     * <h3>Create selection subpanel</h3>
     * 
     * @return
     * Created subpanel. 
     */
    private JPanel createSelectionPanel() {
        JPanel infoPanel;

        this.generatorsCombo = new JComboBox();

        JButton generateButton = new JButton(
                ActionManager.getInstance().getAction(
                        ActionManager.GENERATOR_GENERATE));

        infoPanel = new JPanel(new BorderLayout(5, 0));
        infoPanel.setBorder(new TitledBorder("Current generator"));
        infoPanel.add(this.generatorsCombo, BorderLayout.CENTER);
        infoPanel.add(generateButton, BorderLayout.EAST);

        return infoPanel;
    }

    /**
     * <h3>Connect actions to code</h3>
     */
    private void connectActions() {
        this.generatorsCombo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                comboSelectionChanged();
            }
        });
    }
    
    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPluginManagerListener#generatorPluginAdded(com.octaedr.octabu.app.IGeneratorPlugin)
     */
    public void generatorPluginAdded(IGeneratorPlugin plugin) {
        String pluginName = plugin.getName();
        this.generatorsCollection.put(pluginName, plugin);
        this.generatorsCombo.addItem(pluginName);
        
        JComponent optionsComponent = plugin.getOptionsPanel();
        if(optionsComponent != null) {
            JScrollPane scrollPane = new JScrollPane(optionsComponent);
            scrollPane.setBorder(null);
            this.optionsPanel.add(scrollPane, pluginName);
        }
        
        comboSelectionChanged();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPluginManagerListener#algorithmPluginAdded(com.octaedr.octabu.app.IAlgorithmPlugin)
     */
    public void algorithmPluginAdded(IAlgorithmPlugin plugin) {
        // nothing to do
    }
    
    /**
     * <h3>Combo box selection changed</h3>
     */
    synchronized void comboSelectionChanged() {
        /* get selected plugin */
        IGeneratorPlugin plugin = getCurrentPlugin();

        /* set new options panel */
        String panelName;
        if(plugin.getOptionsPanel() != null) {
            panelName = plugin.getName();
        } else {
            panelName = NO_OPTIONS_PANEL_NAME;
        }

        this.optionsPanelLayout.show(this.optionsPanel, panelName);
    }

    /**
     * <h3>Switch to given plugin</h3>
     * 
     * @param plugin - plugin that should be made current.
     */
    public void switchToPlugin(IGeneratorPlugin plugin) {
        if(this.generatorsCollection.containsValue(plugin)) {
            this.generatorsCombo.setSelectedItem(plugin.getName());
        }
    }

    /**
     * <h3>Get currently selected plugin</h3>
     * 
     * @return
     * Currently selected plugin or null if none is selected.
     */
    public IGeneratorPlugin getCurrentPlugin() {
        /* get selected plugin */
        String selectedItem = (String)this.generatorsCombo.getSelectedItem();
        return this.generatorsCollection.get(selectedItem);
    }
}
