/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.gui.panel.results;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import com.octaedr.octabu.app.data.DataHolder;
import com.octaedr.octabu.app.data.IDataHolderListener;
import com.octaedr.octabu.graph.algorithm.AlgorithmResults;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.gui.action.ActionManager;

/**
 * <h3>Results tab panel</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class ResultsTabPanel extends JPanel implements IDataHolderListener {

    /** <h3>Serial unique identifier</h3> */
    private static final long serialVersionUID = 3415140947921305863L;

    /** <h3>Results log text area control</h3> */
    private JTextArea logTextArea;
    
    /**
     * <h3>Constructor.</h3>
     */
    public ResultsTabPanel() {
        super(new BorderLayout());

        add(createResultsPanel(), BorderLayout.CENTER);
        add(createActionsPanel(), BorderLayout.EAST);
        
        DataHolder.getInstance().addDataHolderListener(this);
    }

    /**
     * <h3>Create results subpanel</h3>
     * 
     * @return
     * Created subpanel.
     */
    private JPanel createResultsPanel() {
        JPanel infoPanel;

        this.logTextArea = new JTextArea();
        this.logTextArea.setEditable(false);
        this.logTextArea.setBorder(new BevelBorder(BevelBorder.LOWERED));
        this.logTextArea.setFont(new Font("Courier New", Font.PLAIN, 14));

        ResultsGfxView resultsImage = new ResultsGfxView();

        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        splitPane.add(resultsImage);
        splitPane.add(new JScrollPane(this.logTextArea));
        splitPane.setDividerLocation(250);

        infoPanel = new JPanel(new BorderLayout());
        infoPanel.setBorder(new TitledBorder("Results"));
        infoPanel.add(splitPane, BorderLayout.CENTER);

        return infoPanel;
    }

    /**
     * <h3>Create actions subpanel.</h3>
     * 
     * @return
     * Created actions subpanel.
     */
    private JComponent createActionsPanel() {

        JPanel layoutPanel;
        JPanel actionsPanel;

        actionsPanel = new JPanel(new GridLayout(4, 1, 0, 5));
        actionsPanel.add(new JButton(ActionManager.getInstance().getAction(
                ActionManager.RESULTS_SAVE_IMAGE)));
        actionsPanel.add(new JButton(ActionManager.getInstance().getAction(
                ActionManager.RESULTS_SAVE_LOG)));
        actionsPanel.add(new JButton(ActionManager.getInstance().getAction(
                ActionManager.RESULTS_CLEAR)));
        actionsPanel.add(new JButton(ActionManager.getInstance().getAction(
                ActionManager.RESULTS_OPTIONS)));

        layoutPanel = new JPanel(new BorderLayout());
        layoutPanel.setBorder(new TitledBorder("Actions"));
        layoutPanel.add(actionsPanel, BorderLayout.NORTH);

        return layoutPanel;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.data.IDataHolderListener#graphSet(com.octaedr.octabu.graph.graph.Graph)
     */
    public void graphSet(Graph graph) {
        // nothing to do
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.data.IDataHolderListener#resultsChanged()
     */
    public void resultsChanged() {
        this.logTextArea.setText(null);
        
        Iterator<DataHolder.ResultsEntry> iterator =
                DataHolder.getInstance().getResults();
        
        while(iterator.hasNext()) {
            DataHolder.ResultsEntry entry = iterator.next();
            AlgorithmResults results = entry.getResults();
            String report = results.prepareReport(true);
            
            this.logTextArea.append(report + "\n");
        }
    }

    /**
     * <h3>Get results log</h3>
     * 
     * @return
     * Results log string or null if no log is currently available.
     */
    public String getResultsLog() {
        return this.logTextArea.getText();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.data.IDataHolderListener#optionValueChanged(java.lang.String, java.lang.Object)
     */
    public void optionValueChanged(String option, Object value) {
        // nothing to do
    }
}
