/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.gui.panel.results;

import java.awt.Graphics;

import javax.swing.JComponent;

import com.octaedr.octabu.app.data.DataHolder;
import com.octaedr.octabu.app.data.IDataHolderListener;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.gui.util.ResultsRenderer;

/**
 * <h3>Results graphical view class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class ResultsGfxView extends JComponent implements IDataHolderListener {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = 4405321455390417805L;

    /** <h3>Draw results dots option value</h3> */
    private boolean drawDots = true;
    /** <h3>Draw dominance lines option value</h3> */
    private boolean drawLines = true;
    /** <h3>Draw dominated regions option value</h3> */
    private boolean drawRegions = true;
    /** <h3>Draw detailed solution data</h3> */
    private boolean drawDetailedSolutionData = false;
    
    /**
     * <h3>Constructor</h3>
     */
    public ResultsGfxView() {
        DataHolder.getInstance().addDataHolderListener(this);
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    protected void paintComponent(Graphics g) {

        ResultsRenderer renderer = new ResultsRenderer(
                false, this.drawDots,
                this.drawLines, this.drawRegions,
                this.drawDetailedSolutionData);
        renderer.render(getWidth(), getHeight(), g);
        
        super.paintComponent(g);
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.data.IDataHolderListener#graphSet(com.octaedr.octabu.graph.graph.Graph)
     */
    public void graphSet(Graph graph) {
        // nothing to do
    }
    
    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.data.IDataHolderListener#resultsChanged()
     */
    public void resultsChanged() {
        repaint();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.data.IDataHolderListener#optionValueChanged(java.lang.String, java.lang.Object)
     */
    public void optionValueChanged(String option, Object value) {
        if((option == DataHolder.OPTION_RESULTS_DRAW_DOTS) ||
                (option == DataHolder.OPTION_RESULTS_DRAW_LINES) ||
                (option == DataHolder.OPTION_RESULTS_DRAW_REGIONS) ||
                (option == DataHolder.OPTION_RESULTS_DRAW_DETAILED_SOLUTIONS)) {

            DataHolder holder = DataHolder.getInstance();

            this.drawDots = holder.getOptionBoolean(DataHolder.OPTION_RESULTS_DRAW_DOTS);
            this.drawLines = holder.getOptionBoolean(DataHolder.OPTION_RESULTS_DRAW_LINES);
            this.drawRegions = holder.getOptionBoolean(DataHolder.OPTION_RESULTS_DRAW_REGIONS);
            this.drawDetailedSolutionData = holder.getOptionBoolean(DataHolder.OPTION_RESULTS_DRAW_DETAILED_SOLUTIONS);
            repaint();
        }
    }

}
