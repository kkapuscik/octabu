/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.gui.mainframe;

import com.octaedr.octabu.gui.action.IActionPerformedListener;

/**
 * <h3>Action performed listener adapter class.</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public abstract class ActionPerformedAdapter implements IActionPerformedListener {

    /** <h3>Adapter owner.</h3> */
    private MainFrame ownerFrame;

    /**
     * <h3>Constructor.</h3>
     * 
     * @param owner -
     *            adapter owner.
     */
    public ActionPerformedAdapter(MainFrame owner) {
        this.ownerFrame = owner;
    }

    /**
     * <h3>Get action owner</h3>
     * 
     * @return
     * Action owner.
     */
    public MainFrame getOwner() {
        return this.ownerFrame;
    }

}
