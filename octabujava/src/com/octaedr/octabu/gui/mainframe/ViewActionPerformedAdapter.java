/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.gui.mainframe;

/**
 * <h3>Action performed adapter for view actions</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public abstract class ViewActionPerformedAdapter extends ActionPerformedAdapter {

    /** <h3>Tab number</h3> */
    protected int tabNumber;

    /**
     * <h3>Constructor</h3>
     * 
     * @param owner - action owner.
     * @param tabNum - tab number for which this action is created.
     */
    public ViewActionPerformedAdapter(MainFrame owner, int tabNum) {
        super(owner);
        this.tabNumber = tabNum;
    }

    /**
     * <h3>Get stored tab number</h3>
     * 
     * @return
     * Stored tab number.
     */
    public int getTabNum() {
        return this.tabNumber;
    }

}
