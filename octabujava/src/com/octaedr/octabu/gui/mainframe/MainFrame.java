/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.gui.mainframe;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

import com.octaedr.octabu.app.Octabu;
import com.octaedr.octabu.app.data.DataHolder;
import com.octaedr.octabu.app.plugin.IAlgorithmPlugin;
import com.octaedr.octabu.app.plugin.IGeneratorPlugin;
import com.octaedr.octabu.app.plugin.IPluginManagerListener;
import com.octaedr.octabu.app.plugin.PluginManager;
import com.octaedr.octabu.exception.OctabuException;
import com.octaedr.octabu.graph.algorithm.AlgorithmExecutor;
import com.octaedr.octabu.graph.algorithm.AlgorithmResults;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.graph.GraphInfo;
import com.octaedr.octabu.graph.serialize.xml.XmlGraphSerializer;
import com.octaedr.octabu.gui.action.Action;
import com.octaedr.octabu.gui.action.ActionManager;
import com.octaedr.octabu.gui.action.IActionPerformedListener;
import com.octaedr.octabu.gui.action.LookAndFeelAction;
import com.octaedr.octabu.gui.dialog.ExecuteAlgorithmDialog;
import com.octaedr.octabu.gui.dialog.ResultsOptionsDialog;
import com.octaedr.octabu.gui.dialog.SaveImageOptionsDialog;
import com.octaedr.octabu.gui.panel.algorithms.AlgorithmsTabPanel;
import com.octaedr.octabu.gui.panel.generators.GeneratorsTabPanel;
import com.octaedr.octabu.gui.panel.graph.GraphTabPanel;
import com.octaedr.octabu.gui.panel.log.LogTabPanel;
import com.octaedr.octabu.gui.panel.results.ResultsTabPanel;
import com.octaedr.octabu.gui.util.ResultsRenderer;
import com.octaedr.octabu.util.Logger;

/**
 * <h3>Main OCTabu GUI frame.</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class MainFrame extends JFrame implements IPluginManagerListener {

    private class ExtensionFileFilter extends FileFilter {

        private String[] extensionsTable;
        private String descriptionText;

        public ExtensionFileFilter(String[] extensions, String description) {
            this.extensionsTable = extensions;
            this.descriptionText = description;
        }
        
        /* (non-Javadoc)
         * @see javax.swing.filechooser.FileFilter#accept(java.io.File)
         */
        public boolean accept(File f) {
            if(f.isDirectory()) {
                return false;
            }
            
            if(this.extensionsTable.length == 0) {
                return true;
            }

            String extension = getExtension(f);
            for(int i = 0; i < this.extensionsTable.length; ++i) {
                if(extension.equalsIgnoreCase(this.extensionsTable[i])) {
                    return true;
                }
            }
            
            return false;
        }

        /* (non-Javadoc)
         * @see javax.swing.filechooser.FileFilter#getDescription()
         */
        public String getDescription() {
            String result = this.descriptionText;
            if(this.extensionsTable.length == 0) {
                result += " (*.*)";
            } else {
                result += " (";
                for(int i = 0; i < this.extensionsTable.length; ++i) {
                    result += "*." + this.extensionsTable[i];
                }
                result += ")";
            }

            return result;
        }
        
        /**
         * <h3>Get given file extension</h3>
         * 
         * @param f - file which extension to return.
         * 
         * @return
         * String with file extension (may be empty).
         */
        protected String getExtension(File f) {
            String path = f.getAbsolutePath();
            int lastDot = path.lastIndexOf('.');
            int lastSep = path.lastIndexOf(File.separatorChar);
            if(lastDot == -1) {
                return "";
            } else if(lastSep != -1) {
                if(lastSep >= lastDot) {
                    return "";
                }
            }
            return path.substring(lastDot + 1);
        }
    }
    
    
    /** <h3>Serial unique identifier.</h3> */
    private static final long serialVersionUID = 7498692381610778114L;

    /** <h3>Index of graph tab</h3> */
    private static final int TAB_INDEX_GRAPH = 0;

    /** <h3>Index of generators tab</h3> */
    private static final int TAB_INDEX_GENERATORS = 1;

    /** <h3>Index of algorithms tab</h3> */
    private static final int TAB_INDEX_ALGORITHMS = 2;

    /** <h3>Index of results tab</h3> */
    private static final int TAB_INDEX_RESULTS = 3;

    /** <h3>Index of log tab</h3> */
    private static final int TAB_INDEX_LOG = 4;

    /** <h3>Tabs control.</h3> */
    private JTabbedPane mainTabs;

    /** <h3>Status bar label.</h3> */
    private JLabel statusLabel;

    /** <h3>Graph tab panel</h3> */
    private GraphTabPanel graphTabPanel;
    /** <h3>Generators tab panel</h3> */
    private GeneratorsTabPanel generatorsTabPanel;
    /** <h3>Algorithms tab panel</h3> */
    private AlgorithmsTabPanel algorithmsTabPanel;
    /** <h3>Results tab panel</h3> */
    private ResultsTabPanel resultsTabPanel;
    /** <h3>Log tab panel</h3> */
    private LogTabPanel logTabPanel;
    
    /** <h3>Generators menu</h3> */
    private JMenu menuGenerators;
    /** <h3>Algorithms menu</h3> */
    private JMenu menuAlgorithms;

    
    /**
     * <h3>Constructor.</h3>
     * 
     * Create main frame control.
     * 
     * @param title -
     *            title of the frame to create.
     * 
     * @throws HeadlessException -
     *             if GraphicsEnvironment.isHeadless() returns true.
     * 
     * @see JFrame#JFrame(java.lang.String)
     */
    public MainFrame(String title) throws HeadlessException {
        /* create frame */
        super(title);

        /* create gui elements */
        add(createTabs(), BorderLayout.CENTER);
        add(createStatusBar(), BorderLayout.SOUTH);
        setJMenuBar(createMenu());

        /* connect actions */
        connectActions();
        
        /* register in plugin manager */
        PluginManager.getInstance().addPluginManagerListener(this);
    }

    /**
     * <h3>Create frame menu.</h3>
     * 
     * @return Created menu bar object.
     */
    private JMenuBar createMenu() {
        JMenuBar menuBar;
        JMenu menuFile;
        JMenu menuView;
        JMenu menuResults;
        JMenu menuHelp;

        /* create file menu */
        menuFile = new JMenu("File");
        menuFile.add(ActionManager.getInstance().getAction(
                ActionManager.GRAPH_DISPOSE));
        menuFile.add(new JSeparator());
        menuFile.add(ActionManager.getInstance().getAction(
                ActionManager.GRAPH_OPEN));
        menuFile.add(new JSeparator());
        menuFile.add(ActionManager.getInstance().getAction(
                ActionManager.GRAPH_SAVE));
        menuFile.add(ActionManager.getInstance().getAction(
                ActionManager.GRAPH_SAVE_INFO));
        menuFile.add(new JSeparator());
        menuFile.add(ActionManager.getInstance().getAction(
                ActionManager.APP_EXIT));

        /* create view menu */
        menuView = new JMenu("View");
        menuView.add(ActionManager.getInstance().getAction(
                ActionManager.VIEW_GRAPH));
        menuView.add(ActionManager.getInstance().getAction(
                ActionManager.VIEW_GENERATORS));
        menuView.add(ActionManager.getInstance().getAction(
                ActionManager.VIEW_ALGORITHMS));
        menuView.add(ActionManager.getInstance().getAction(
                ActionManager.VIEW_RESULTS));
        menuView.add(ActionManager.getInstance().getAction(
                ActionManager.VIEW_LOG));

        /* create generators menu */
        this.menuGenerators = new JMenu("Generators");

        /* create algorithms menu */
        this.menuAlgorithms = new JMenu("Algorithms");

        /* create results menu */
        menuResults = new JMenu("Results");
        menuResults.add(ActionManager.getInstance().getAction(
                ActionManager.RESULTS_SAVE_IMAGE));
        menuResults.add(ActionManager.getInstance().getAction(
                ActionManager.RESULTS_SAVE_LOG));
        menuResults.add(new JSeparator());
        menuResults.add(ActionManager.getInstance().getAction(
                ActionManager.RESULTS_CLEAR));
        menuResults.add(ActionManager.getInstance().getAction(
                ActionManager.RESULTS_OPTIONS));

        /* create help menu */
        menuHelp = new JMenu("Help");
        menuHelp.add(ActionManager.getInstance().getAction(
                ActionManager.APP_HELP));
        menuHelp.add(ActionManager.getInstance().getAction(
                ActionManager.APP_ABOUT));

        /* create look&feel menu */
        LookAndFeelAction[] lafActions = ActionManager.getInstance()
                .getLookAndFeelActions();
        JMenu menuLaf = null;
        if (lafActions.length > 0) {
            menuLaf = new JMenu("LookAndFeel");
            for (int i = 0; i < lafActions.length; ++i) {
                menuLaf.add(lafActions[i]);
            }
        }

        /* create menu bar and attach menues to it */
        menuBar = new JMenuBar();
        menuBar.add(menuFile);
        menuBar.add(menuView);
        menuBar.add(this.menuGenerators);
        menuBar.add(this.menuAlgorithms);
        menuBar.add(menuResults);
        if (menuLaf != null) {
            menuBar.add(menuLaf);
        }
        menuBar.add(menuHelp);

        return menuBar;
    }

    /**
     * <h3>Connect look&feel actions.</h3>
     */
    private void connectLookAndFeelActions() {
        /* connect look&feel actions */
        IActionPerformedListener lookAndFeelListener = new ActionPerformedAdapter(
                this) {
            public void actionPerformed(Action action, ActionEvent event) {
                LookAndFeelAction lafAction = (LookAndFeelAction) action;
                getOwner().setLookAndFeel(lafAction.getLafClassName());
            }
        };
        LookAndFeelAction[] lafActions = ActionManager.getInstance()
                .getLookAndFeelActions();
        for (int i = 0; i < lafActions.length; ++i) {
            lafActions[i].addActionPerformedListener(lookAndFeelListener);
        }
    }

    /**
     * <h3>Connect application actions.</h3>
     */
    private void connectApplicationActions() {
        Action actionObject;

        /* connect exit action */
        actionObject = ActionManager.getInstance().getAction(
                ActionManager.APP_EXIT);
        actionObject
                .addActionPerformedListener(new ActionPerformedAdapter(this) {
                    public void actionPerformed(Action action, ActionEvent event) {
                        getOwner().quit();
                    }
                });

        /* connect help action */
        actionObject = ActionManager.getInstance().getAction(
                ActionManager.APP_HELP);
        actionObject
                .addActionPerformedListener(new ActionPerformedAdapter(this) {
                    public void actionPerformed(Action action, ActionEvent event) {
                        getOwner().showHelp();
                    }
                });

        /* connect about action */
        actionObject = ActionManager.getInstance().getAction(
                ActionManager.APP_ABOUT);
        actionObject
                .addActionPerformedListener(new ActionPerformedAdapter(this) {
                    public void actionPerformed(Action action, ActionEvent event) {
                        getOwner().about();
                    }
                });
    }

    /**
     * <h3>Connect view actions.</h3>
     */
    private void connectViewActions() {
        Action actionObject;

        actionObject = ActionManager.getInstance().getAction(
                ActionManager.VIEW_GRAPH);
        actionObject.addActionPerformedListener(new ViewActionPerformedAdapter(
                this, TAB_INDEX_GRAPH) {
            public void actionPerformed(Action action, ActionEvent event) {
                getOwner().switchToTab(getTabNum());
            }
        });

        actionObject = ActionManager.getInstance().getAction(
                ActionManager.VIEW_GENERATORS);
        actionObject.addActionPerformedListener(new ViewActionPerformedAdapter(
                this, TAB_INDEX_GENERATORS) {
            public void actionPerformed(Action action, ActionEvent event) {
                getOwner().switchToTab(getTabNum());
            }
        });

        actionObject = ActionManager.getInstance().getAction(
                ActionManager.VIEW_ALGORITHMS);
        actionObject.addActionPerformedListener(new ViewActionPerformedAdapter(
                this, TAB_INDEX_ALGORITHMS) {
            public void actionPerformed(Action action, ActionEvent event) {
                getOwner().switchToTab(getTabNum());
            }
        });

        actionObject = ActionManager.getInstance().getAction(
                ActionManager.VIEW_RESULTS);
        actionObject.addActionPerformedListener(new ViewActionPerformedAdapter(
                this, TAB_INDEX_RESULTS) {
            public void actionPerformed(Action action, ActionEvent event) {
                getOwner().switchToTab(getTabNum());
            }
        });

        actionObject = ActionManager.getInstance().getAction(
                ActionManager.VIEW_LOG);
        actionObject.addActionPerformedListener(new ViewActionPerformedAdapter(
                this, TAB_INDEX_LOG) {
            public void actionPerformed(Action action, ActionEvent event) {
                getOwner().switchToTab(getTabNum());
            }
        });

    }

    /**
     * <h3>Connect graph actions</h3>
     */
    private void connectGraphActions() {
        ActionManager.getInstance().getAction(
                ActionManager.GRAPH_DISPOSE).addActionPerformedListener(
                        new ActionPerformedAdapter(this) {
                            public void actionPerformed(Action action, ActionEvent event) {
                                disposeGraph();
                            }
                        });
        ActionManager.getInstance().getAction(
                ActionManager.GRAPH_OPEN).addActionPerformedListener(
                        new IActionPerformedListener() {
                            public void actionPerformed(Action action, ActionEvent event) {
                                openGraph();
                            }
                        });
        ActionManager.getInstance().getAction(
                ActionManager.GRAPH_SAVE).addActionPerformedListener(
                        new IActionPerformedListener() {
                            public void actionPerformed(Action action, ActionEvent event) {
                                saveGraph();
                            }
                        });
        ActionManager.getInstance().getAction(
                ActionManager.GRAPH_SAVE_INFO).addActionPerformedListener(
                        new IActionPerformedListener() {
                            public void actionPerformed(Action action, ActionEvent event) {
                                saveGraphInfo();
                            }
                        });
    }
    
    /**
     * <h3>Save results log</h3>
     */
    protected void saveResultsLog() {
        JFileChooser chooser = new JFileChooser();
        FileFilter currentFilter;

        chooser.setAcceptAllFileFilterUsed(false);
        chooser.addChoosableFileFilter(
                currentFilter = new ExtensionFileFilter(
                        new String[] {"log"}, "Log Text Files"));
        chooser.addChoosableFileFilter(
                currentFilter = new ExtensionFileFilter(
                        new String[] {"txt"}, "Text Files"));
        chooser.addChoosableFileFilter(
                new ExtensionFileFilter(
                        new String[] {}, "All Files"));
        chooser.setFileFilter(currentFilter);
        chooser.setDialogTitle("OCTabu - Save results log");
        if(chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile().getAbsoluteFile();
                        
            Logger.logln("[MainFrame] Save results log - file selected: " +
                    file.getPath());
            
            try {
                String resultsLog = this.resultsTabPanel.getResultsLog();
                
                FileWriter fileWriter = new FileWriter(file);
                fileWriter.write(resultsLog);
                fileWriter.close();
            } catch (IOException exception) {
                Logger.logWarning("[MainFrame] Exception occured during saving of results log");
                Logger.logException(exception);
            }
        }
    }
    
    /**
     * <h3>Save graph info</h3>
     */
    protected void saveGraphInfo() {
        JFileChooser chooser = new JFileChooser();
        FileFilter currentFilter;

        chooser.setAcceptAllFileFilterUsed(false);
        chooser.addChoosableFileFilter(
                currentFilter = new ExtensionFileFilter(
                        new String[] {"txt"}, "Text Files"));
        chooser.addChoosableFileFilter(
                new ExtensionFileFilter(
                        new String[] {}, "All Files"));
        chooser.setFileFilter(currentFilter);
        chooser.setDialogTitle("OCTabu - Save graph info");
        if(chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile().getAbsoluteFile();
                        
            Logger.logln("[MainFrame] Save graph info - file selected: " +
                    file.getPath());
            
            try {
                StringBuilder stringBuilder = new StringBuilder();

                Graph graph = DataHolder.getInstance().getGraph();
                GraphInfo info = graph.getInfo();
                if(info != null) {
                    /* print generator data */
                    stringBuilder.append("Generator: ");
                    stringBuilder.append(info.getGenerator());
                    stringBuilder.append("\n\n");
                    
                    /* print info entries */
                    Iterator<GraphInfo.Entry> iterator = info.getEntries();
                    while(iterator.hasNext()) {
                        GraphInfo.Entry entry = iterator.next();

                        stringBuilder.append(entry.getKey() + ": " +
                                entry.getValue() + "\n");
                    }                
                } else {
                    stringBuilder.append("No graph info available");
                }
                
                FileWriter fileWriter = new FileWriter(file);
                fileWriter.write(stringBuilder.toString());
                fileWriter.close();
            } catch (IOException exception) {
                Logger.logWarning("[MainFrame] Exception occured during saving of graph info");
                Logger.logException(exception);
            }
        }
    }

    /**
     * <h3>Save graph</h3>
     */
    protected void saveGraph() {
        JFileChooser chooser = new JFileChooser();
        FileFilter currentFilter;

        chooser.setAcceptAllFileFilterUsed(false);
        chooser.addChoosableFileFilter(
                currentFilter = new ExtensionFileFilter(
                        new String[] {"ogr"}, "OCTabu Graph Files"));
        chooser.addChoosableFileFilter(
                new ExtensionFileFilter(
                        new String[] {}, "All Files"));
        chooser.setFileFilter(currentFilter);
        chooser.setDialogTitle("OCTabu - Save graph");
        if(chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile().getAbsoluteFile();
                        
            Logger.logln("[MainFrame] Save graph - file selected: " +
                    file.getPath());
            
            XmlGraphSerializer serializer = new XmlGraphSerializer();
            try {
                serializer.writeGraph(
                        DataHolder.getInstance().getGraph(), file);
            } catch (IOException exception) {
                Logger.logWarning("[MainFrame] Exception occured during saving of graph");
                Logger.logException(exception);
            }
        }
    }

    /**
     * <h3>Open graph</h3>
     */
    protected void openGraph() {
        JFileChooser chooser = new JFileChooser();
        FileFilter currentFilter;

        chooser.setAcceptAllFileFilterUsed(false);
        chooser.addChoosableFileFilter(
                currentFilter = new ExtensionFileFilter(
                        new String[] {"ogr"}, "OCTabu Graph Files"));
        chooser.addChoosableFileFilter(
                new ExtensionFileFilter(
                        new String[] {}, "All Files"));
        chooser.setFileFilter(currentFilter);
        chooser.setDialogTitle("OCTabu - Open graph");
        DataHolder.getInstance().setGraph(null);

        if(chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile().getAbsoluteFile();
            
            Logger.logln("[MainFrame] Open graph - file selected: " +
                    file.getPath());
            
            XmlGraphSerializer serializer = new XmlGraphSerializer();
            try {
                Graph graph = serializer.readGraph(file);
                DataHolder.getInstance().setGraph(graph);
            } catch (IOException exception) {
                Logger.logWarning("[MainFrame] Exception occured during reading of graph");
                Logger.logException(exception);
            }
        }
    }

    /**
     * <h3>Dispose graph</h3>
     */
    protected void disposeGraph() {
        if(JOptionPane.showConfirmDialog(
                this,
                "Do you really want to dispose current graph?",
                "OCTabu - Dispose graph",
                JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
            return;
        }
        
        Logger.logln("[MainFrame] Disposing old graph");
        DataHolder.getInstance().setGraph(null);
    }

    /**
     * <h3>Connect generate graph action</h3>
     */
    private void connectGenerateAction() {
        ActionManager.getInstance().getAction(
                ActionManager.GENERATOR_GENERATE).addActionPerformedListener(
                        new IActionPerformedListener() {
                            public void actionPerformed(Action action, ActionEvent event) {
                                generateGraph();
                            }
                        });
    }
    
    /**
     * <h3>Connect execute algorithm action</h3> 
     */
    private void connectExecuteAction() {
        ActionManager.getInstance().getAction(
                ActionManager.ALGORITHM_EXECUTE).addActionPerformedListener(
                        new IActionPerformedListener() {
                            public void actionPerformed(Action action, ActionEvent event) {
                                executeAlgorithm();
                            }
                        });
    }

    /**
     * <h3>Execute algorithm</h3>
     */
    protected void executeAlgorithm() {
        IAlgorithmPlugin plugin = this.algorithmsTabPanel.getCurrentPlugin();
        if(plugin != null) {
            ExecuteAlgorithmDialog dialog = new ExecuteAlgorithmDialog(
                    this, "OCTabu - Execute algorith", true);
            dialog.setVisible(true);
            if(dialog.result) {
                AlgorithmResults results = AlgorithmExecutor.execute(
                        plugin,
                        DataHolder.getInstance().getGraph(),
                        DataHolder.getInstance().getOptionInteger(
                                DataHolder.OPTION_ALGORITHM_SOURCE_VERTEX), 
                            DataHolder.getInstance().getOptionInteger(
                                    DataHolder.OPTION_ALGORITHM_TARGET_VERTEX));
                results.setAlgorithmName(plugin.getName());
    
                if(results.hasSucceeded()) {
                    DataHolder.getInstance().addAlgorithmResults(results);
                    switchToTab(TAB_INDEX_RESULTS);
                } else {
                    JOptionPane.showMessageDialog(
                            this,
                            "OCTabu - Algorithm execution",
                            "Algorithm execution has failed.",
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        }
    }

    /**
     * <h3>Generate graph</h3>
     */
    protected void generateGraph() {
        IGeneratorPlugin plugin = this.generatorsTabPanel.getCurrentPlugin();
        if(plugin != null) {
            /* check options */
            if(plugin.checkOptions() == false) {
                if(JOptionPane.showConfirmDialog(
                        this,
                        "Generator options are invalid.\n" +
                        "Would you like to fix them?",
                        "OCTabu - Generate graph",
                        JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                    return;
                }
                plugin.validateOptions();
            }
            
            if(DataHolder.getInstance().getGraph() != null) {
                if(JOptionPane.showConfirmDialog(
                        this,
                        "Previous graph must be disposed before generating new one.\n" +
                        "Would you like to continue and dispose old graph?",
                        "OCTabu - Generate graph",
                        JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                    return;
                }
            }
            
            try {
                Logger.logln("[MainFrame] Disposing old graph");
                DataHolder.getInstance().setGraph(null);

                Logger.logln("[MainFrame] Generating graph using plugin: " + plugin.getName());
                Graph graph = plugin.generate();

                Logger.logln("[MainFrame] Setting generated graph as current");
                DataHolder.getInstance().setGraph(graph);
                
                switchToTab(TAB_INDEX_GRAPH);
            } catch (OctabuException exception) {
                Logger.loglnWarning("[MainFrame] Exception occured during graph generation"
                        + " (" + exception.getMessage() + ")");
                Logger.logException(exception);
                JOptionPane.showMessageDialog(this,
                        "Exception occured during graph generation",
                        "OCTabu - Generate graph",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    /**
     * <h3>Connect actions.</h3>
     */
    private void connectActions() {
        connectLookAndFeelActions();
        connectApplicationActions();
        connectViewActions();
        connectGraphActions();
        connectGenerateAction();
        connectExecuteAction();
        connectResultsActions();
    }

    /**
     * <h3>Connect results actions</h3>
     */
    private void connectResultsActions() {
        ActionManager.getInstance().getAction(
                ActionManager.RESULTS_SAVE_LOG).addActionPerformedListener(
                        new IActionPerformedListener() {
                            public void actionPerformed(Action action, ActionEvent event) {
                                saveResultsLog();
                            }
                        });
        ActionManager.getInstance().getAction(
                ActionManager.RESULTS_SAVE_IMAGE).addActionPerformedListener(
                        new IActionPerformedListener() {
                            public void actionPerformed(Action action, ActionEvent event) {
                                saveResultsImage();
                            }
                        });
        ActionManager.getInstance().getAction(
                ActionManager.RESULTS_OPTIONS).addActionPerformedListener(
                        new IActionPerformedListener() {
                            public void actionPerformed(Action action, ActionEvent event) {
                                setResultsOptions();
                            }
                        });
        ActionManager.getInstance().getAction(
                ActionManager.RESULTS_CLEAR).addActionPerformedListener(
                        new IActionPerformedListener() {
                            public void actionPerformed(Action action, ActionEvent event) {
                                clearResults();
                            }
                        });
    }

    /**
     * <h3>Clear results</h3>
     */
    protected void clearResults() {
        DataHolder.getInstance().clearResults();
    }

    /**
     * <h3>Set results options</h3>
     */
    protected void setResultsOptions() {
        ResultsOptionsDialog dialog = new ResultsOptionsDialog(
                this, "OCTabu - Results options", true);
        
        dialog.setVisible(true);
    }

    /**
     * <h3>Save results image</h3>
     */
    protected void saveResultsImage() {
        SaveImageOptionsDialog dialog = new SaveImageOptionsDialog(
                this, "OCTabu - Save image options", true);
        dialog.setVisible(true);
        if(dialog.result) {
            
            JFileChooser chooser = new JFileChooser();
            FileFilter currentFilter;

            chooser.setAcceptAllFileFilterUsed(false);
            chooser.addChoosableFileFilter(
                    currentFilter = new ExtensionFileFilter(
                            new String[] {"png"}, "PNG Images"));
            chooser.addChoosableFileFilter(
                    new ExtensionFileFilter(
                            new String[] {}, "All Files"));
            chooser.setFileFilter(currentFilter);
            chooser.setDialogTitle("OCTabu - Save results image");
            if(chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                File file = chooser.getSelectedFile().getAbsoluteFile();

                Logger.logln("[MainFrame] Save results image - file selected: " +
                        file.getPath());
                
                DataHolder holder = DataHolder.getInstance();
                
                int width = holder.getOptionInteger(DataHolder.OPTION_IMAGE_WIDTH);
                int height = holder.getOptionInteger(DataHolder.OPTION_IMAGE_HEIGHT);
                String format = holder.getOptionString(DataHolder.OPTION_IMAGE_FORMAT);
                boolean drawBorder = holder.getOptionBoolean(DataHolder.OPTION_IMAGE_DRAW_BORDER);
                boolean drawDots = holder.getOptionBoolean(DataHolder.OPTION_IMAGE_DRAW_DOTS);
                boolean drawLines = holder.getOptionBoolean(DataHolder.OPTION_IMAGE_DRAW_LINES);
                boolean drawRegions = holder.getOptionBoolean(DataHolder.OPTION_IMAGE_DRAW_REGIONS);
                
                BufferedImage image = new BufferedImage(
                        width, height, BufferedImage.TYPE_4BYTE_ABGR);
                ResultsRenderer renderer = new ResultsRenderer(
                        drawBorder, drawDots, drawLines, drawRegions);
                renderer.render(width, height, image.createGraphics());
                
                try {
                    ImageIO.write(image, format, file);
                } catch (IOException exception) {
                    Logger.logWarning("[MainFrame] Exception occured during saving of results image");
                    exception.printStackTrace();
                } catch (IllegalArgumentException exception) {
                    Logger.logWarning("[MainFrame] Exception occured during saving of results image");
                    exception.printStackTrace();
                }
            }
        }
    }

    /**
     * <h3>Create frame tabs.</h3>
     * 
     * @return Create tabbed pane component.
     */
    private JTabbedPane createTabs() {
        this.mainTabs = new JTabbedPane(JTabbedPane.TOP);

        add(this.mainTabs);

        this.graphTabPanel = new GraphTabPanel();
        this.generatorsTabPanel = new GeneratorsTabPanel();
        this.algorithmsTabPanel = new AlgorithmsTabPanel();
        this.resultsTabPanel = new ResultsTabPanel();
        this.logTabPanel = new LogTabPanel();

        this.mainTabs.insertTab("Graph", null, this.graphTabPanel,
                "Graph information and actions", TAB_INDEX_GRAPH);
        this.mainTabs.insertTab("Generators", null, this.generatorsTabPanel,
                "Graph generators", TAB_INDEX_GENERATORS);
        this.mainTabs
                .insertTab("Algorithms", null, this.algorithmsTabPanel,
                        "Algorithm parametrization and execution",
                        TAB_INDEX_ALGORITHMS);
        this.mainTabs.insertTab("Results", null, this.resultsTabPanel,
                "Presentation of results", TAB_INDEX_RESULTS);
        this.mainTabs.insertTab("Log", null, this.logTabPanel,
                "Presentation of results", TAB_INDEX_LOG);

        return this.mainTabs;
    }

    /**
     * <h3>Create status bar.</h3>
     * 
     * @return Created status bar component.
     */
    private JComponent createStatusBar() {
        this.statusLabel = new JLabel("Ready.");
        return this.statusLabel;
    }

    /**
     * <h3>Set look and feel to use given class.</h3>
     * 
     * @param lafClassName -
     *            l&f class to use.
     */
    protected void setLookAndFeel(String lafClassName) {
        try {
            Logger.logln("[MainFrame] Setting look&feel to: " + lafClassName);
            UIManager.setLookAndFeel(lafClassName);
            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception ex) {
            Logger.logln("[MainFrame] Setting look&feel " + lafClassName + " failed");
            Logger.logException(ex);
        }
    }

    /**
     * <h3>Show application help.</h3>
     */
    protected void showHelp() {
        JOptionPane.showMessageDialog(this, "Help not available yet, sorry!",
                "OCTabu", JOptionPane.WARNING_MESSAGE);
    }

    /**
     * <h3>Show basic information about application.</h3>
     */
    protected void about() {
        JOptionPane.showMessageDialog(this,
                "OCTabu " + Octabu.VERSION_STRING + "\n" +
                Octabu.COPYRIGHT_STRING, "About OCTabu",
                JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * <h3>Quit application.</h3>
     */
    protected void quit() {
        dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }

    /**
     * <h3>Switch view to given tab.</h3>
     * 
     * @param index -
     *            index of tab that should be set current.
     */
    protected void switchToTab(int index) {
        this.mainTabs.setSelectedIndex(index);
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPluginManagerListener#generatorPluginAdded(com.octaedr.octabu.app.IGeneratorPlugin)
     */
    public void generatorPluginAdded(IGeneratorPlugin plugin) {
        /* create action */
        Action actionObject = new Action(plugin.getName());
        /* add action to manager */
        ActionManager.getInstance().addAction(plugin.getShortName(), actionObject);

        /* add action to menu */
        this.menuGenerators.add(actionObject);
        
        /* connect action */
        actionObject.addActionPerformedListener(new ViewGeneratorActionPerformedAdapter(this, plugin) {
            public void actionPerformed(Action action, ActionEvent event) {
                getOwner().viewGenerator(getPlugin());
            }
        });
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPluginManagerListener#algorithmPluginAdded(com.octaedr.octabu.app.IAlgorithmPlugin)
     */
    public void algorithmPluginAdded(IAlgorithmPlugin plugin) {
        /* create action */
        Action actionObject = new Action(plugin.getName());
        /* add action to manager */
        ActionManager.getInstance().addAction(plugin.getShortName(), actionObject);
        
        /* add action to menu */
        this.menuAlgorithms.add(actionObject);

        /* connect action */
        actionObject.addActionPerformedListener(new ViewAlgorithmActionPerformedAdapter(this, plugin) {
            public void actionPerformed(Action action, ActionEvent event) {
                getOwner().viewAlgorithm(getPlugin());
            }
        });
    }

    /**
     * <h3>Switch view to given generator</h3>
     * 
     * @param plugin - generator plugin to be viewed.
     */
    protected void viewGenerator(IGeneratorPlugin plugin) {
        switchToTab(TAB_INDEX_GENERATORS);
        this.generatorsTabPanel.switchToPlugin(plugin);
    }

    /**
     * <h3>Switch view to given algorithm</h3>
     * 
     * @param plugin - algorithm plugin to be viewed.
     */
    protected void viewAlgorithm(IAlgorithmPlugin plugin) {
        switchToTab(TAB_INDEX_ALGORITHMS);
        this.algorithmsTabPanel.switchToPlugin(plugin);
    }

}
