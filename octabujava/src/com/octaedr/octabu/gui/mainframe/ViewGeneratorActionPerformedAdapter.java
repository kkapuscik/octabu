/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.gui.mainframe;

import com.octaedr.octabu.app.plugin.IGeneratorPlugin;

/**
 * <h3>Adapter class for view generator action performed listener</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
abstract public class ViewGeneratorActionPerformedAdapter extends ActionPerformedAdapter {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = 3885035456892539875L;

    /** <h3>Algorithm plugin that should be viewed</h3> */
    private IGeneratorPlugin generatorPlugin;
    
    /**
     * <h3>Constructor</h3>
     * 
     * @param owner - action owner frame.
     * @param plugin - associated generator plugin.
     */
    public ViewGeneratorActionPerformedAdapter(MainFrame owner, IGeneratorPlugin plugin) {
        super(owner);

        this.generatorPlugin = plugin;
    }
    
    /**
     * <h3>Get generator plugin</h3>
     * 
     * @return
     * Associated generator plugin.
     */
    public IGeneratorPlugin getPlugin() {
        return this.generatorPlugin;
    }
    
}
