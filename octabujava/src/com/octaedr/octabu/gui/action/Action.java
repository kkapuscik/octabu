/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.gui.action;

import java.awt.event.ActionEvent;
import java.util.HashSet;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.KeyStroke;

/**
 * <h3>GUI action.</h3>
 * 
 * @author Krzysztof Kapuscik
 */
public class Action extends AbstractAction {

    /** <h3>Generated serial version UID.</h3> */
    private static final long serialVersionUID = 6538378216140421389L;

    /** <h3>Collection of ActionPerformed listeners.</h3> */
    private HashSet<IActionPerformedListener> actionPerformedListeners = new HashSet<IActionPerformedListener>();

    /**
     * <h3>Constructor.</h3>
     * 
     * <p>
     * Create action with no attributes set.
     * </p>
     */
    public Action() {
        super();
    }

    /**
     * <h3>Constructor.</h3>
     * 
     * <p>
     * Create action with given name.
     * </p>
     * 
     * @param name -
     *            name of the action to create.
     */
    public Action(String name) {
        super();
        setName(name);
    }

    /**
     * <h3>Constructor.</h3>
     * 
     * <p>
     * Create action with specified attributes.
     * </p>
     * 
     * @param name -
     *            name of the action to create.
     * @param accelerator -
     *            accelerator key.
     */
    public Action(String name, KeyStroke accelerator) {
        super();
        setName(name);
        setAcceleratorKey(accelerator);
    }

    /**
     * <h3>Constructor.</h3>
     * 
     * <p>
     * Create action with specified attributes.
     * </p>
     * 
     * @param name -
     *            name of the action to create.
     * @param accelerator -
     *            accelerator key.
     * @param shortDescription -
     *            short description of action.
     */
    public Action(String name, KeyStroke accelerator, String shortDescription) {
        super();
        setName(name);
        setAcceleratorKey(accelerator);
        setShortDescription(shortDescription);
    }

    /**
     * <h3>Set accelerator key.</h3>
     * 
     * @param keyStroke -
     *            accelerator key to set.
     * 
     * @see javax.swing.Action#ACCELERATOR_KEY
     */
    public void setAcceleratorKey(KeyStroke keyStroke) {
        putValue(ACCELERATOR_KEY, keyStroke);
    }

    /**
     * <h3>Set action command key.</h3>
     * 
     * @param commandKey -
     *            action command key to set.
     * 
     * @see javax.swing.Action#ACTION_COMMAND_KEY
     */
    public void setActionCommandKey(String commandKey) {
        putValue(ACTION_COMMAND_KEY, commandKey);
    }

    /**
     * <h3>Set long description.</h3>
     * 
     * @param description -
     *            description to set.
     * 
     * @see javax.swing.Action#LONG_DESCRIPTION
     */
    public void setLongDescription(String description) {
        putValue(LONG_DESCRIPTION, description);
    }

    /**
     * <h3>Set mnemonic key</h3>
     * 
     * @param key -
     *            mnemonic key to set.
     * 
     * @see javax.swing.Action#MNEMONIC_KEY
     */
    public void setMnemonicKey(int key) {
        putValue(MNEMONIC_KEY, key);
    }

    /**
     * <h3>Set action name.</h3>
     * 
     * @param name -
     *            action name to set.
     * 
     * @see javax.swing.Action#NAME
     */
    public void setName(String name) {
        putValue(NAME, name);
    }

    /**
     * <h3>Set short description.</h3>
     * 
     * @param description -
     *            description to set.
     * 
     * @see javax.swing.Action#SHORT_DESCRIPTION
     */
    public void setShortDescription(String description) {
        putValue(SHORT_DESCRIPTION, description);
    }

    /**
     * <h3>Set small icon.</h3>
     * 
     * @param icon -
     *            icon to set.
     * 
     * @see javax.swing.Action#SMALL_ICON
     */
    public void setSmallIcon(Icon icon) {
        putValue(SMALL_ICON, icon);
    }

    /**
     * <h3>Get accelerator key.</h3>
     * 
     * @return Accelerator key assigned to action or null if not set.
     * 
     * @see javax.swing.Action#ACCELERATOR_KEY
     */
    public KeyStroke getAcceleratorKey() {
        return (KeyStroke) getValue(ACCELERATOR_KEY);
    }

    /**
     * <h3>Get action command key.</h3>
     * 
     * @return Command key assigned to action or null if not set.
     * 
     * @see javax.swing.Action#ACTION_COMMAND_KEY
     */
    public String getActionCommandKey() {
        return (String) getValue(ACTION_COMMAND_KEY);
    }

    /**
     * <h3>Get long description.</h3>
     * 
     * @return Action long description or null if not set.
     * 
     * @see javax.swing.Action#LONG_DESCRIPTION
     */
    public String getLongDescription() {
        return (String) getValue(LONG_DESCRIPTION);
    }

    /**
     * <h3>Get mnemonic key</h3>
     * 
     * @return Mnemonic key assigned to action or null if not set.
     * 
     * @see javax.swing.Action#MNEMONIC_KEY
     */
    public Integer getMnemonicKey() {
        return (Integer) getValue(MNEMONIC_KEY);
    }

    /**
     * <h3>Get action name.</h3>
     * 
     * @return Action name or null if not set.
     * 
     * @see javax.swing.Action#NAME
     */
    public String getName() {
        return (String) getValue(NAME);
    }

    /**
     * <h3>Get short description.</h3>
     * 
     * @return Short description of action or null if not set.
     * 
     * @see javax.swing.Action#SHORT_DESCRIPTION
     */
    public String getShortDescription() {
        return (String) getValue(SHORT_DESCRIPTION);
    }

    /**
     * <h3>Get small icon.</h3>
     * 
     * @return Action small icon or null if not set.
     * 
     * @see javax.swing.Action#SMALL_ICON
     */
    public Icon getSmallIcon() {
        return (Icon) getValue(SMALL_ICON);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
        emitActionPerformedEvent(e);
    }

    /**
     * <h3>Add action performed listener.</h3>
     * 
     * @param listener -
     *            listener to add.
     */
    public void addActionPerformedListener(IActionPerformedListener listener) {
        synchronized (this.actionPerformedListeners) {
            this.actionPerformedListeners.add(listener);
        }
    }

    /**
     * <h3>Remove action performed listener.</h3>
     * 
     * @param listener -
     *            listener to remove.
     */
    public void removeActionPerformedListener(IActionPerformedListener listener) {
        synchronized (this.actionPerformedListeners) {
            this.actionPerformedListeners.remove(listener);
        }
    }

    /**
     * <h3>Emit action performed event to all listeners.</h3>
     * 
     * @param event -
     *            ActionEvent describing the action.
     */
    private void emitActionPerformedEvent(ActionEvent event) {
        synchronized (this.actionPerformedListeners) {
            Iterator<IActionPerformedListener> iterator = this.actionPerformedListeners
                    .iterator();
            while (iterator.hasNext()) {
                IActionPerformedListener listener = iterator.next();
                listener.actionPerformed(this, event);
            }
        }
    }
}
