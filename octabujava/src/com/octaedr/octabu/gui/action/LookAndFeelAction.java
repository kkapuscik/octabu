/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.gui.action;

/**
 * <h3>Look&Feel specialized action class.</h3>
 * 
 * @author Krzysztof Kapuscik
 */
public class LookAndFeelAction extends Action {

    /** <h3>Generated serial version UID.</h3> */
    private static final long serialVersionUID = -6807417276407760195L;

    /** <h3>Key of Look&Feel Class Name action property.</h3> */
    static final public String LAF_CLASS_NAME = "OCTAEDR_LAF_CLASS_NAME";

    /**
     * <h3>Constructor.</h3>
     * 
     * <p>
     * Creates action with given name and class name.
     * </p>
     * 
     * @param name -
     *            action name.
     * @param lafClassName -
     *            name of L&F class.
     */
    public LookAndFeelAction(String name, String lafClassName) {
        super(name);
        setLafClassName(lafClassName);
    }

    /**
     * <h3>Set look&feel class name property.</h3>
     * 
     * @param lafClassName -
     *            class name to set.
     */
    public void setLafClassName(String lafClassName) {
        putValue(LAF_CLASS_NAME, lafClassName);
    }

    /**
     * <h3>Get look&feel class name property.</h3>
     * 
     * @return Look&Feel class name or null if was not set.
     */
    public String getLafClassName() {
        return (String) getValue(LAF_CLASS_NAME);
    }

}
