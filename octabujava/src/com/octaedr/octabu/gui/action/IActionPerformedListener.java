/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.gui.action;

import java.awt.event.ActionEvent;

/**
 * <h3>Action performed listener.</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IActionPerformedListener {

    /**
     * <h3>Method invoked when action has been performed.</h3>
     * 
     * @param action -
     *            action that has been performed.
     * @param event -
     *            action event.
     */
    void actionPerformed(Action action, ActionEvent event);

}
