/**
 * Created:     2005-11-12
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.gui.action;

import java.awt.event.InputEvent;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import com.octaedr.octabu.app.data.DataHolder;
import com.octaedr.octabu.app.data.IDataHolderListener;
import com.octaedr.octabu.graph.graph.Graph;


/**
 * <h3>Manager class for set of actions.</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class ActionManager implements IDataHolderListener {

    /** <h3>Dispose graph action identifier</h3> */
    static final public String GRAPH_DISPOSE = "graph/dispose";

    /** <h3>Open graph action identifier</h3> */
    static final public String GRAPH_OPEN = "graph/open";

    /** <h3>Save graph action identifier</h3> */
    static final public String GRAPH_SAVE = "graph/save";

    /** <h3>Save graph info action identifier</h3> */
    static final public String GRAPH_SAVE_INFO = "graph/saveinfo";

    /** <h3>Exit application action identifier</h3> */
    static final public String APP_EXIT = "app/exit";

    /** <h3>Application help action identifier</h3> */
    static final public String APP_HELP = "app/help";

    /** <h3>Application about action identifier</h3> */
    static final public String APP_ABOUT = "app/about";

    /** <h3>View graph info panel action identifier</h3> */
    static final public String VIEW_GRAPH = "view/graph";

    /** <h3>View generators panel action identifier</h3> */
    static final public String VIEW_GENERATORS = "view/generators";

    /** <h3>View algorithms panel action identifier</h3> */
    static final public String VIEW_ALGORITHMS = "view/algorithms";

    /** <h3>View results panel action identifier</h3> */
    static final public String VIEW_RESULTS = "view/results";

    /** <h3>View log panel action identifier</h3> */
    static final public String VIEW_LOG = "view/log";

    /** <h3>Save results image action identifier</h3> */
    static final public String RESULTS_SAVE_IMAGE = "results/saveimage";

    /** <h3>Save results log action identifier</h3> */
    static final public String RESULTS_SAVE_LOG = "results/savelog";

    /** <h3>Clear results action identifier</h3> */
    static final public String RESULTS_CLEAR = "results/clear";

    /** <h3>Show results options action identifier</h3> */
    static final public String RESULTS_OPTIONS = "results/options";

    /** <h3>Clear log action identifier</h3> */
    static final public String LOG_CLEAR = "log/clear";

    /** <h3>Enable logging action identifier</h3> */
    static final public String LOG_ENABLE = "log/enable";

    /** <h3>Disable logging action identifier</h3> */
    static final public String LOG_DISABLE = "log/disable";

    /** <h3>Execute algorithm action identifier</h3> */
    static final public String ALGORITHM_EXECUTE = "algorithm/execute";
    
    /** <h3>Generate graph action identifier</h3> */
    static final public String GENERATOR_GENERATE = "generator/generate";
    
    /** <h3>Prefix for look&feel actions</h3> */
    static final public String LOOK_AND_FEEL_PREFIX = "lookandfeel/";

    
    /** <h3>The only instance of action manager</h3> */
    static private ActionManager instance = new ActionManager();

    /** <h3>Actions container</h3> */
    private HashMap<String, Action> actions = new HashMap<String, Action>();

    /**
     * <h3>Constructor</h3>
     * 
     * <p>
     * Create initialized action manager.
     * </p>
     */
    private ActionManager() {
        createActions();
        
        DataHolder.getInstance().addDataHolderListener(this);
    }

    /**
     * <h3>Get instance of action manager.</h3>
     * 
     * @return Action manager instance.
     */
    static public ActionManager getInstance() {
        return instance;
    }

    /**
     * <h3>Create action objects.</h3>
     */
    private void createActions() {
        /* create look&feel actions */
        LookAndFeelInfo[] infos = UIManager.getInstalledLookAndFeels();
        if (infos.length > 0) {
            for (int i = 0; i < infos.length; ++i) {
                LookAndFeelAction action = new LookAndFeelAction(infos[i]
                        .getName(), infos[i].getClassName());
                action.setShortDescription("Set look & feel to "
                        + infos[i].getName());
                this.actions.put(LOOK_AND_FEEL_PREFIX + infos[i].getName(),
                        action);
            }
        }

        /* create graph actions */
        this.actions.put(GRAPH_DISPOSE, new Action("Dispose graph", KeyStroke
                .getKeyStroke('N', InputEvent.CTRL_DOWN_MASK),
                "Restart experiments (dispose current graph)"));

        this.actions.put(GRAPH_OPEN, new Action("Open graph...", KeyStroke
                .getKeyStroke('O', InputEvent.CTRL_DOWN_MASK),
                "Load graph from a file"));

        this.actions.put(GRAPH_SAVE, new Action("Save graph...", KeyStroke
                .getKeyStroke('S', InputEvent.CTRL_DOWN_MASK),
                "Save graph to a file"));

        this.actions.put(GRAPH_SAVE_INFO, new Action("Save graph info...", null,
                "Save graph information to a file"));

        /* create application actions */
        this.actions.put(APP_EXIT, new Action("Exit", KeyStroke.getKeyStroke(
                'X', InputEvent.ALT_DOWN_MASK), "Exit application"));

        this.actions
                .put(APP_HELP, new Action("Help", null, "Show OCTabu help"));

        this.actions.put(APP_ABOUT, new Action("About...", null,
                "Show informations about OCTabu"));

        /* create view actions */
        this.actions.put(VIEW_GRAPH, new Action("View graph", KeyStroke
                .getKeyStroke('1', InputEvent.CTRL_DOWN_MASK),
                "View graph informations"));

        this.actions.put(VIEW_GENERATORS, new Action("View generators",
                KeyStroke.getKeyStroke('2', InputEvent.CTRL_DOWN_MASK),
                "Set generators setting and create graphs"));

        this.actions.put(VIEW_ALGORITHMS, new Action("View algorithms",
                KeyStroke.getKeyStroke('3', InputEvent.CTRL_DOWN_MASK),
                "Set algorithms settings and execute them"));

        this.actions.put(VIEW_RESULTS, new Action("View results", KeyStroke
                .getKeyStroke('4', InputEvent.CTRL_DOWN_MASK),
                "View results of experiments"));

        this.actions.put(VIEW_LOG, new Action("View log", KeyStroke
                .getKeyStroke('5', InputEvent.CTRL_DOWN_MASK),
                "View application log messages"));

        /* create results actions */
        this.actions.put(RESULTS_SAVE_IMAGE, new Action("Save image...",
                KeyStroke.getKeyStroke('I', InputEvent.CTRL_DOWN_MASK),
                "Save results image to a file"));

        this.actions.put(RESULTS_SAVE_LOG, new Action("Save log...", KeyStroke
                .getKeyStroke('L', InputEvent.CTRL_DOWN_MASK),
                "Save results log to a file"));

        this.actions.put(RESULTS_CLEAR, new Action("Clear", KeyStroke
                .getKeyStroke('E', InputEvent.CTRL_DOWN_MASK),
                "Clear all results"));

        this.actions.put(RESULTS_OPTIONS, new Action("Options...", KeyStroke
                .getKeyStroke('P', InputEvent.CTRL_DOWN_MASK),
                "Set results options"));

        this.actions.put(LOG_CLEAR, new Action("Clear log", null,
                "Clear all messages in log"));

        this.actions.put(LOG_ENABLE, new Action("Enable logging", null,
                "Enable logging debug messages"));

        this.actions.put(LOG_DISABLE, new Action("Disable logging", null,
                "Disable logging debug messages"));

        this.actions.put(ALGORITHM_EXECUTE, new Action("Execute", null,
                "Execute currently selected algorithm"));

        this.actions.put(GENERATOR_GENERATE, new Action("Generate", null,
                "Generate graph using currently selected generator"));
    }

    /**
     * <h3>Get action associated with specified key.</h3>
     * 
     * @param key -
     *            key to which action has to be associated.
     * 
     * @return Action associated with specified key or null if such association
     *         does not exist.
     */
    public Action getAction(String key) {
        return this.actions.get(key);
    }

    /**
     * <h3>Get array with all look&feel actions.</h3>
     * 
     * @return Array of look&feel actions.
     */
    public LookAndFeelAction[] getLookAndFeelActions() {
        int counter;
        Iterator<Entry<String, Action>> iterator;
        LookAndFeelAction[] resultArray;

        /* count number of look&feel actions */
        counter = 0;
        iterator = this.actions.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<String, Action> entry = iterator.next();

            if (entry.getKey().length() > LOOK_AND_FEEL_PREFIX.length()) {
                String prefix = entry.getKey().substring(0,
                        LOOK_AND_FEEL_PREFIX.length());

                if (prefix.equals(LOOK_AND_FEEL_PREFIX)) {
                    if (entry.getValue() instanceof LookAndFeelAction) {
                        counter++;
                    }
                }
            }
        }

        /* create results array */
        resultArray = new LookAndFeelAction[counter];

        /* put actions in array */
        counter = 0;
        iterator = this.actions.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<String, Action> entry = iterator.next();

            if (entry.getKey().length() > LOOK_AND_FEEL_PREFIX.length()) {
                String prefix = entry.getKey().substring(0,
                        LOOK_AND_FEEL_PREFIX.length());

                if (prefix.equals(LOOK_AND_FEEL_PREFIX)) {
                    if (entry.getValue() instanceof LookAndFeelAction) {
                        resultArray[counter] = (LookAndFeelAction) entry
                                .getValue();
                        counter++;
                    }
                }
            }
        }

        return resultArray;
    }
    
    /**
     * <h3>Add action to managed actions collection</h3>
     * 
     * @param name - name of the action.
     * @param action - action object.
     */
    public void addAction(String name, Action action) {
        this.actions.put(name, action);
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.data.IDataHolderListener#graphSet(com.octaedr.octabu.graph.graph.Graph)
     */
    public void graphSet(Graph graph) {
        boolean enableFlag = graph != null;
        
        getAction(GRAPH_DISPOSE).setEnabled(enableFlag);
        getAction(GRAPH_SAVE).setEnabled(enableFlag);
        getAction(GRAPH_SAVE_INFO).setEnabled(enableFlag);
        getAction(ALGORITHM_EXECUTE).setEnabled(enableFlag);
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.data.IDataHolderListener#resultsChanged()
     */
    public void resultsChanged() {
        boolean enableFlag =
            (DataHolder.getInstance().getResultsCount() > 0);

        getAction(RESULTS_CLEAR).setEnabled(enableFlag);
        getAction(RESULTS_SAVE_IMAGE).setEnabled(enableFlag);
        getAction(RESULTS_SAVE_LOG).setEnabled(enableFlag);
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.data.IDataHolderListener#optionValueChanged(java.lang.String, java.lang.Object)
     */
    public void optionValueChanged(String option, Object value) {
        // nothing to do
    }
}
