/**
 * Created:     2005-11-16
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.gui.dialog;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.octaedr.octabu.app.data.DataHolder;


/**
 * <h3>Save image dialog class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class SaveImageOptionsDialog extends JDialog implements ActionListener {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = -819468261646367133L;

    /** <h3>Dialog cancel button</h3> */
    private JButton cancelButton;
    /** <h3>Dialog ok button</h3> */
    private JButton okButton;
    /** <h3>Image width spinner control</h3> */
    private JSpinner widthSpinner;
    /** <h3>Image height spinner control</h3> */
    private JSpinner heightSpinner;
    /** <h3>Image format conbo box control</h3> */
    private JComboBox imageFormatCombo;
    /** <h3>Draw border option check box control</h3> */
    private JCheckBox drawBorderCheckBox;
    /** <h3>Draw dots option check box control</h3> */
    private JCheckBox drawDotsCheckBox;
    /** <h3>Draw lines option check box control</h3> */
    private JCheckBox drawLinesCheckBox;
    /** <h3>Draw regions option check box control</h3> */
    private JCheckBox drawRegionsCheckBox;

    /** <h3>Dialog result</h3> */
    public boolean result = false;
    
    /**
     * <h3>Constructor</h3>
     * 
     * @param frame - parent frame.
     * @param title - dialog title.
     * @param modal - modal mode flag.
     */
    public SaveImageOptionsDialog(Frame frame, String title, boolean modal) {
        super(frame, title, modal);
        
        createElements();

        int x, y;
        Rectangle frameBounds = frame.getBounds();
        Dimension dimension = getPreferredSize();
        dimension.width = dimension.width * 3 / 2;
        dimension.height = dimension.height * 3 / 2;
        
        x = frameBounds.x + frameBounds.width / 2 - dimension.width / 2;
        y = frameBounds.y + frameBounds.height / 2 - dimension.height / 2;
        setBounds(x, y, dimension.width, dimension.height);
    }

    /**
     * <h3>Create dialog elements</h3>
     */
    private void createElements() {
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        add(mainPanel);

        mainPanel.add(createOptions(), BorderLayout.CENTER);
        mainPanel.add(createButtons(), BorderLayout.SOUTH);
    }

    /**
     * <h3>Create dialog buttons</h3>
     * 
     * @return
     * Created buttons subpanel.
     */
    private JComponent createButtons() {
        JPanel buttonsPanel = new JPanel(new GridLayout(1, 2, 50, 0));
        buttonsPanel.setBorder(new EmptyBorder(10, 0, 0, 0));

        buttonsPanel.add(createOkButton());
        buttonsPanel.add(createCancelButton());
        
        return buttonsPanel;
    }

    /**
     * <h3>Create cancel button</h3>
     * 
     * @return
     * Created cancel button.
     */
    private Component createCancelButton() {
        this.cancelButton = new JButton("Cancel");
        this.cancelButton.addActionListener(this);
        return this.cancelButton;
    }

    /**
     * <h3>Create Ok button</h3>
     * 
     * @return
     * Created Ok button.
     */
    private Component createOkButton() {
        this.okButton = new JButton("Ok");
        this.okButton.addActionListener(this);
        return this.okButton;
    }

    /**
     * <h3>Create image width option subpanel</h3>
     * 
     * @return
     * Created subpanel.
     */
    private JComponent createWidthOption() {
        GridLayout layout = new GridLayout(1, 2, 5, 0);
        JPanel optionPanel = new JPanel(layout);
        
        this.widthSpinner = new JSpinner(
                new SpinnerNumberModel(
                        800, 1, 10000, 1));

        optionPanel.add(new JLabel("Image width"));
        optionPanel.add(this.widthSpinner);

        return optionPanel;
    }
    
    /**
     * <h3>Create image height option subpanel</h3>
     * 
     * @return
     * Created subpanel.
     */
    private JComponent createHeightOption() {
        GridLayout layout = new GridLayout(1, 2, 5, 0);
        JPanel optionPanel = new JPanel(layout);
        
        this.heightSpinner = new JSpinner(
                new SpinnerNumberModel(
                        600, 1, 10000, 1));

        optionPanel.add(new JLabel("Image height"));
        optionPanel.add(this.heightSpinner);

        return optionPanel;
    }
    
    /**
     * <h3>Create image format option subpanel</h3>
     * 
     * @return
     * Created subpanel.
     */
    private JComponent createFormatOption() {
        GridLayout layout = new GridLayout(1, 2, 5, 0);
        JPanel optionPanel = new JPanel(layout);

        this.imageFormatCombo = new JComboBox(
                new String[] { "png" });
//        this.imageFormatCombo = new JComboBox(
//                ImageIO.getWriterFormatNames());

        optionPanel.add(new JLabel("Image format"));
        optionPanel.add(this.imageFormatCombo);

        return optionPanel;
    }

    /**
     * <h3>Create options subpanel</h3>
     * 
     * @return
     * Created subpanel.
     */
    private JComponent createOptions() {
        GridLayout layout = new GridLayout(7, 1, 0, 5);
        JPanel optionsPanel = new JPanel(layout);
        optionsPanel.setBorder(
                new CompoundBorder(
                        new TitledBorder("Options"),
                        new EmptyBorder(5, 5, 5, 5)));
        
        this.drawBorderCheckBox = new JCheckBox("Draw image border", true);
        this.drawDotsCheckBox = new JCheckBox("Draw result dots", true);
        this.drawLinesCheckBox = new JCheckBox("Draw domiance lines", true);
        this.drawRegionsCheckBox = new JCheckBox("Draw dominated regions", true);

        optionsPanel.add(this.drawBorderCheckBox);
        optionsPanel.add(this.drawDotsCheckBox);
        optionsPanel.add(this.drawLinesCheckBox);
        optionsPanel.add(this.drawRegionsCheckBox);
        optionsPanel.add(this.createWidthOption());
        optionsPanel.add(this.createHeightOption());
        optionsPanel.add(this.createFormatOption());
        
        return optionsPanel;
    }

    /* (non-Javadoc)
     * @see java.awt.Component#setVisible(boolean)
     */
    public void setVisible(boolean b) {
        if(b == true) {
            restoreOptions();
            this.result = false;
        }
        super.setVisible(b);
    }

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.okButton) {
            storeOptions();
            this.result = true;
            setVisible(false);
        } else if(e.getSource() == this.cancelButton) {
            setVisible(false);
        }
    }

    /**
     * <h3>Store options in data holder</h3>
     */
    public void storeOptions() {
        DataHolder holder = DataHolder.getInstance();
        
        holder.setOption(DataHolder.OPTION_IMAGE_DRAW_BORDER,
                this.drawBorderCheckBox.isSelected());
        holder.setOption(DataHolder.OPTION_IMAGE_DRAW_DOTS,
                this.drawDotsCheckBox.isSelected());
        holder.setOption(DataHolder.OPTION_IMAGE_DRAW_LINES,
                this.drawLinesCheckBox.isSelected());
        holder.setOption(DataHolder.OPTION_IMAGE_DRAW_REGIONS,
                this.drawRegionsCheckBox.isSelected());
        holder.setOption(DataHolder.OPTION_IMAGE_WIDTH,
                this.widthSpinner.getValue());
        holder.setOption(DataHolder.OPTION_IMAGE_HEIGHT,
                this.heightSpinner.getValue());
        holder.setOption(DataHolder.OPTION_IMAGE_FORMAT,
                this.imageFormatCombo.getSelectedItem());
    }

    /**
     * <h3>Get options from data holder</h3>
     */
    public void restoreOptions() {
        DataHolder holder = DataHolder.getInstance();

        this.drawBorderCheckBox.setSelected(
                holder.getOptionBoolean(DataHolder.OPTION_IMAGE_DRAW_BORDER));
        this.drawDotsCheckBox.setSelected(
                holder.getOptionBoolean(DataHolder.OPTION_IMAGE_DRAW_DOTS));
        this.drawLinesCheckBox.setSelected(
                holder.getOptionBoolean(DataHolder.OPTION_IMAGE_DRAW_LINES));
        this.drawRegionsCheckBox.setSelected(
                holder.getOptionBoolean(DataHolder.OPTION_IMAGE_DRAW_REGIONS));
        this.widthSpinner.setValue(
                holder.getOptionInteger(DataHolder.OPTION_IMAGE_WIDTH));
        this.heightSpinner.setValue(
                holder.getOptionInteger(DataHolder.OPTION_IMAGE_HEIGHT));
        this.imageFormatCombo.setSelectedItem(
                holder.getOptionString(DataHolder.OPTION_IMAGE_FORMAT));
    }

}
