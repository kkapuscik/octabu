/**
 * Created:     2005-11-16
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.gui.dialog;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.octaedr.octabu.app.data.DataHolder;

/**
 * <h3>Results options dialog class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class ResultsOptionsDialog extends JDialog implements ActionListener {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = -9135059038532751895L;

    /** <h3>Dialog cancel button</h3> */
    private JButton cancelButton;
    /** <h3>Dialog ok button</h3> */
    private JButton okButton;
    /** <h3>Draw dots option check box control</h3> */
    private JCheckBox drawDotsCheckBox;
    /** <h3>Draw lines option check box control</h3> */
    private JCheckBox drawLinesCheckBox;
    /** <h3>Draw regions option check box control</h3> */
    private JCheckBox drawRegionsCheckBox;
    /** <h3>Draw solution details option check box control</h3> */
    private JCheckBox drawSolutionDetailsCheckBox;

    /**
     * <h3>Constructor</h3>
     * 
     * @param frame - parent frame.
     * @param title - dialog title.
     * @param modal - modal mode flag.
     */
    public ResultsOptionsDialog(Frame frame, String title, boolean modal) {
        super(frame, title, modal);
        
        createElements();

        int x, y;
        Rectangle frameBounds = frame.getBounds();
        Dimension dimension = getPreferredSize();
        dimension.width = dimension.width * 3 / 2;
        dimension.height = dimension.height * 3 / 2;
        
        x = frameBounds.x + frameBounds.width / 2 - dimension.width / 2;
        y = frameBounds.y + frameBounds.height / 2 - dimension.height / 2;
        setBounds(x, y, dimension.width, dimension.height);
    }

    /**
     * <h3>Create dialog elements</h3>
     */
    private void createElements() {
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        add(mainPanel);

        mainPanel.add(createOptions(), BorderLayout.CENTER);
        mainPanel.add(createButtons(), BorderLayout.SOUTH);
    }

    /**
     * <h3>Create dialog buttons</h3>
     * 
     * @return
     * Created buttons subpanel.
     */
    private JComponent createButtons() {
        JPanel buttonsPanel = new JPanel(new GridLayout(1, 2, 50, 0));
        buttonsPanel.setBorder(new EmptyBorder(10, 0, 0, 0));

        buttonsPanel.add(createOkButton());
        buttonsPanel.add(createCancelButton());
        
        return buttonsPanel;
    }

    /**
     * <h3>Create cancel button</h3>
     * 
     * @return
     * Created cancel button.
     */
    private Component createCancelButton() {
        this.cancelButton = new JButton("Cancel");
        this.cancelButton.addActionListener(this);
        return this.cancelButton;
    }

    /**
     * <h3>Create Ok button</h3>
     * 
     * @return
     * Created Ok button.
     */
    private Component createOkButton() {
        this.okButton = new JButton("Ok");
        this.okButton.addActionListener(this);
        return this.okButton;
    }

    /**
     * <h3>Create options subpanel</h3>
     * 
     * @return
     * Created subpanel.
     */
    private JComponent createOptions() {
        GridLayout layout = new GridLayout(4, 1, 0, 5);
        JPanel optionsPanel = new JPanel(layout);
        optionsPanel.setBorder(
                new CompoundBorder(
                        new TitledBorder("Options"),
                        new EmptyBorder(5, 5, 5, 5)));
        
        this.drawDotsCheckBox = new JCheckBox("Draw result dots", true);
        this.drawLinesCheckBox = new JCheckBox("Draw domiance lines", true);
        this.drawRegionsCheckBox = new JCheckBox("Draw dominated regions", true);
        this.drawSolutionDetailsCheckBox = new JCheckBox("Draw solution details", true);
        
        optionsPanel.add(this.drawDotsCheckBox);
        optionsPanel.add(this.drawLinesCheckBox);
        optionsPanel.add(this.drawRegionsCheckBox);
        optionsPanel.add(this.drawSolutionDetailsCheckBox);

        return optionsPanel;
    }

    /* (non-Javadoc)
     * @see java.awt.Component#setVisible(boolean)
     */
    public void setVisible(boolean b) {
        if(b == true) {
            restoreOptions();
        }
        super.setVisible(b);
    }

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.okButton) {
            storeOptions();
            setVisible(false);
        } else if(e.getSource() == this.cancelButton) {
            setVisible(false);
        }
    }

    /**
     * <h3>Store options in data holder</h3>
     */
    public void storeOptions() {
        DataHolder holder = DataHolder.getInstance();
        
        holder.setOption(DataHolder.OPTION_RESULTS_DRAW_DOTS,
                this.drawDotsCheckBox.isSelected());
        holder.setOption(DataHolder.OPTION_RESULTS_DRAW_LINES,
                this.drawLinesCheckBox.isSelected());
        holder.setOption(DataHolder.OPTION_RESULTS_DRAW_REGIONS,
                this.drawRegionsCheckBox.isSelected());
        holder.setOption(DataHolder.OPTION_RESULTS_DRAW_DETAILED_SOLUTIONS,
                this.drawSolutionDetailsCheckBox.isSelected());
    }

    /**
     * <h3>Get options from data holder</h3>
     */
    public void restoreOptions() {
        DataHolder holder = DataHolder.getInstance();

        this.drawDotsCheckBox.setSelected(
                holder.getOptionBoolean(DataHolder.OPTION_RESULTS_DRAW_DOTS));
        this.drawLinesCheckBox.setSelected(
                holder.getOptionBoolean(DataHolder.OPTION_RESULTS_DRAW_LINES));
        this.drawRegionsCheckBox.setSelected(
                holder.getOptionBoolean(DataHolder.OPTION_RESULTS_DRAW_REGIONS));
        this.drawSolutionDetailsCheckBox.setSelected(
                holder.getOptionBoolean(DataHolder.OPTION_RESULTS_DRAW_DETAILED_SOLUTIONS));
    }
}
