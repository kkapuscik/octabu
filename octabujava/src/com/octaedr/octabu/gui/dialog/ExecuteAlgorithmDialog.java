/**
 * Created:     2005-11-16
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.gui.dialog;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.octaedr.octabu.app.data.DataHolder;


/**
 * <h3>Save image dialog class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class ExecuteAlgorithmDialog extends JDialog implements ActionListener {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = -819468261646367133L;

    /** <h3>Dialog cancel button</h3> */
    private JButton cancelButton;
    /** <h3>Dialog ok button</h3> */
    private JButton okButton;
    /** <h3>Source vertex spinner control</h3> */
    private JSpinner sourceVertexSpinner;
    /** <h3>Target vertex spinner control</h3> */
    private JSpinner targetVertexSpinner;
    /** <h3>Generate random source vertex button</h3> */
    private JButton sourceVertexRandomButton;
    /** <h3>Generate random target vertex button</h3> */
    private JButton targetVertexRandomButton;
    
    /** <h3>Dialog result</h3> */
    public boolean result = false;

    /**
     * <h3>Constructor</h3>
     * 
     * @param frame - parent frame.
     * @param title - dialog title.
     * @param modal - modal mode flag.
     */
    public ExecuteAlgorithmDialog(Frame frame, String title, boolean modal) {
        super(frame, title, modal);
        
        createElements();

        int x, y;
        Rectangle frameBounds = frame.getBounds();
        Dimension dimension = getPreferredSize();
        dimension.width = dimension.width * 5 / 4;
        dimension.height = dimension.height * 5 / 4;
        
        x = frameBounds.x + frameBounds.width / 2 - dimension.width / 2;
        y = frameBounds.y + frameBounds.height / 2 - dimension.height / 2;
        setBounds(x, y, dimension.width, dimension.height);
        
        connectActions();
    }

    /**
     * <h3>Connect button actions</h3>
     */
    private void connectActions() {
        this.sourceVertexRandomButton.addActionListener(
                new ActionListener() {
                    /*
                     * (non-Javadoc)
                     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
                     */
                    public void actionPerformed(ActionEvent e) {
                        randomSourceVertex();
                    }

                });
        this.targetVertexRandomButton.addActionListener(
                new ActionListener() {
                    /*
                     * (non-Javadoc)
                     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
                     */
                    public void actionPerformed(ActionEvent e) {
                        randomTargetVertex();
                    } 
                });
    }

    /**
     * <h3>Set random source vertex</h3>
     */
    protected void randomSourceVertex() {
        this.sourceVertexSpinner.setValue(
                new Random().nextInt(
                        DataHolder.getInstance().getGraphVertexCount()) + 1);
    } 

    /**
     * <h3>Set random target vertex</h3>
     */
    protected void randomTargetVertex() {
        this.targetVertexSpinner.setValue(
                new Random().nextInt(
                        DataHolder.getInstance().getGraphVertexCount()) + 1);
    } 

    /**
     * <h3>Create dialog elements</h3>
     */
    private void createElements() {
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        add(mainPanel);

        mainPanel.add(createOptions(), BorderLayout.CENTER);
        mainPanel.add(createButtons(), BorderLayout.SOUTH);
    }

    /**
     * <h3>Create dialog buttons</h3>
     * 
     * @return
     * Created buttons subpanel.
     */
    private JComponent createButtons() {
        JPanel buttonsPanel = new JPanel(new GridLayout(1, 2, 50, 0));
        buttonsPanel.setBorder(new EmptyBorder(10, 0, 0, 0));

        buttonsPanel.add(createOkButton());
        buttonsPanel.add(createCancelButton());
        
        return buttonsPanel;
    }

    /**
     * <h3>Create cancel button</h3>
     * 
     * @return
     * Created cancel button.
     */
    private Component createCancelButton() {
        this.cancelButton = new JButton("Cancel");
        this.cancelButton.addActionListener(this);
        return this.cancelButton;
    }

    /**
     * <h3>Create Ok button</h3>
     * 
     * @return
     * Created Ok button.
     */
    private Component createOkButton() {
        this.okButton = new JButton("Ok");
        this.okButton.addActionListener(this);
        return this.okButton;
    }

    /**
     * <h3>Create source vertex option subpanel</h3>
     * 
     * @return
     * Created subpanel.
     */
    private JComponent createSourceVertexOption() {
        GridLayout layout = new GridLayout(1, 3, 5, 0);
        JPanel optionPanel = new JPanel(layout);
        
        this.sourceVertexSpinner = new JSpinner(
                new SpinnerNumberModel(
                        1, 1, DataHolder.getInstance().getGraphVertexCount(), 1));

        this.sourceVertexRandomButton = new JButton("Random");

        optionPanel.add(new JLabel("Source vertex"));
        optionPanel.add(this.sourceVertexSpinner);
        optionPanel.add(this.sourceVertexRandomButton);

        return optionPanel;
    }
    
    /**
     * <h3>Create target vertex option subpanel</h3>
     * 
     * @return
     * Created subpanel.
     */
    private JComponent createTargetVertexOption() {
        GridLayout layout = new GridLayout(1, 3, 5, 0);
        JPanel optionPanel = new JPanel(layout);
        
        this.targetVertexSpinner = new JSpinner(
                new SpinnerNumberModel(
                        1, 1, DataHolder.getInstance().getGraphVertexCount(), 1));

        this.targetVertexRandomButton = new JButton("Random");
        
        optionPanel.add(new JLabel("Target vertex"));
        optionPanel.add(this.targetVertexSpinner);
        optionPanel.add(this.targetVertexRandomButton);

        return optionPanel;
    }

    /**
     * <h3>Create options subpanel</h3>
     * 
     * @return
     * Created subpanel.
     */
    private JComponent createOptions() {
        GridLayout layout = new GridLayout(2, 1, 0, 5);
        JPanel optionsPanel = new JPanel(layout);
        optionsPanel.setBorder(
                new CompoundBorder(
                        new TitledBorder("Options"),
                        new EmptyBorder(5, 5, 5, 5)));
        
        optionsPanel.add(this.createSourceVertexOption());
        optionsPanel.add(this.createTargetVertexOption());
        
        return optionsPanel;
    }

    /* (non-Javadoc)
     * @see java.awt.Component#setVisible(boolean)
     */
    public void setVisible(boolean b) {
        if(b == true) {
            restoreOptions();
            this.result = false;
        }
        super.setVisible(b);
    }

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.okButton) {
            storeOptions();
            this.result = true;
            setVisible(false);
        } else if(e.getSource() == this.cancelButton) {
            setVisible(false);
        }
    }

    /**
     * <h3>Store options in data holder</h3>
     */
    public void storeOptions() {
        DataHolder holder = DataHolder.getInstance();
        
        holder.setOption(DataHolder.OPTION_ALGORITHM_SOURCE_VERTEX,
                this.sourceVertexSpinner.getValue());
        holder.setOption(DataHolder.OPTION_ALGORITHM_TARGET_VERTEX,
                this.targetVertexSpinner.getValue());
    }

    /**
     * <h3>Get options from data holder</h3>
     */
    public void restoreOptions() {
        DataHolder holder = DataHolder.getInstance();

        this.sourceVertexSpinner.setValue(
                holder.getOptionInteger(DataHolder.OPTION_ALGORITHM_SOURCE_VERTEX));
        this.targetVertexSpinner.setValue(
                holder.getOptionInteger(DataHolder.OPTION_ALGORITHM_TARGET_VERTEX));
    }

}
