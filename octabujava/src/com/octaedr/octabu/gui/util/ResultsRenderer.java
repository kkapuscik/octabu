/**
 * File:      ResultsRenderer.java
 * 
 * Date:      2005-11-16
 * Author:    Krzysztof Kapuscik
 * Copyright: (C) 2005 Krzysztof Kapuscik
 * Contact:   saveman@op.pl
 */

package com.octaedr.octabu.gui.util;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Iterator;

import com.octaedr.octabu.app.data.DataHolder;
import com.octaedr.octabu.graph.algorithm.AlgorithmResults;
import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Path;

/**
 * <h3>Results renderer class</h3>
 * 
 * <p>This class is used to render current results using
 * virtual graphics device.</p>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class ResultsRenderer {

    /** <h3>Minimum value of time</h3> */
    private double minTimeValue;
    /** <h3>Maximum value of time</h3> */
    private double maxTimeValue;
    /** <h3>Minimum value of cost</h3> */
    private double minCostValue;
    /** <h3>Maximum value of cost</h3> */
    private double maxCostValue;
    /** <h3>Size of time range</h3> */
    private double timeRange;
    /** <h3>Size of cost range</h3> */
    private double costRange;
    /** <h3>Graphics device width</h3> */
    private int deviceWidth;
    /** <h3>Graphics device height</h3> */
    private int deviceHeight;

    /** <h3>Draw border</h3> */
    private boolean flagDrawBorder;
    /** <h3>Draw dots</h3> */
    private boolean flagDrawDots;
    /** <h3>Draw lines</h3> */
    private boolean flagDrawLines;
    /** <h3>Draw dominated regions</h3> */
    private boolean flagDrawDominatedRegions;
    /** <h3>Draw solution details</h3> */
    private boolean flagDrawResultsDetails;
    
    /**
     * <h3>Constructor</h3>
     * 
     * <p>Create renderer that uses options given as
     * parameters.</p>
     * 
     * @param drawDots - draw border.
     * @param drawDots - draw dots at result values.
     * @param drawLines - draw dominance lines.
     * @param drawDominatedRegions - draw dominated regions.
     */
    public ResultsRenderer(
            boolean drawBorder,
            boolean drawDots,
            boolean drawLines,
            boolean drawDominatedRegions) {
        this.flagDrawBorder = drawBorder;
        this.flagDrawDots = drawDots;
        this.flagDrawLines = drawLines;
        this.flagDrawDominatedRegions = drawDominatedRegions;
        this.flagDrawResultsDetails = false;
    }

    /**
     * <h3>Constructor</h3>
     * 
     * <p>Create renderer that uses options given as
     * parameters.</p>
     * 
     * @param drawDots - draw border.
     * @param drawDots - draw dots at result values.
     * @param drawLines - draw dominance lines.
     * @param drawDominatedRegions - draw dominated regions.
     * @param drawResultDetails - drawing detailed view of each solution.
     */
    public ResultsRenderer(
            boolean drawBorder,
            boolean drawDots,
            boolean drawLines,
            boolean drawDominatedRegions,
            boolean drawResultDetails) {
        this.flagDrawBorder = drawBorder;
        this.flagDrawDots = drawDots;
        this.flagDrawLines = drawLines;
        this.flagDrawDominatedRegions = drawDominatedRegions;
        this.flagDrawResultsDetails = drawResultDetails;
    }
    
    /**
     * <h3>Compute results value ranges</h3>
     */
    private void computeMinMax() {
        Iterator<DataHolder.ResultsEntry> iterator;

        iterator = DataHolder.getInstance().getResults();        

        if(iterator.hasNext()) {
            this.minTimeValue = Double.POSITIVE_INFINITY;
            this.maxTimeValue = Double.NEGATIVE_INFINITY;
            this.minCostValue = Double.POSITIVE_INFINITY;
            this.maxCostValue = Double.NEGATIVE_INFINITY;

            while(iterator.hasNext()) {
                DataHolder.ResultsEntry entry = iterator.next();
                AlgorithmResults.ValueRanges ranges = 
                        entry.getResults().getValueRanges();
                
                if(ranges.valid) {
                    if(ranges.minCostValue < this.minCostValue) {
                        this.minCostValue = ranges.minCostValue;
                    }
                    if(ranges.maxCostValue > this.maxCostValue) {
                        this.maxCostValue = ranges.maxCostValue;
                    }
                    if(ranges.minTimeValue < this.minTimeValue) {
                        this.minTimeValue = ranges.minTimeValue;
                    }
                    if(ranges.maxTimeValue > this.maxTimeValue) {
                        this.maxTimeValue = ranges.maxTimeValue;
                    }
                }            
            }        
        } else {
            this.minTimeValue = 0.0;
            this.maxTimeValue = 0.0;
            this.minCostValue = 0.0;
            this.maxCostValue = 0.0;
        }
        
        if(this.minTimeValue == this.maxTimeValue) {
            this.minTimeValue -= 1;
            this.maxTimeValue += 1;
        }
        if(this.minCostValue == this.maxCostValue) {
            this.minCostValue -= 1;
            this.maxCostValue += 1;
        }

        this.timeRange = (this.maxTimeValue - this.minTimeValue) * 0.1;
        this.costRange = (this.maxCostValue - this.minCostValue) * 0.1;
        this.minCostValue -= this.costRange;
        this.maxCostValue += this.costRange;
        this.minTimeValue -= this.timeRange;
        this.maxTimeValue += this.timeRange;
        
        if(this.flagDrawResultsDetails) {
            this.minCostValue = 0.0;
            this.minTimeValue = 0.0;
        }
 
        this.timeRange = this.maxTimeValue - this.minTimeValue;
        this.costRange = this.maxCostValue - this.minCostValue;
        
        if(this.flagDrawResultsDetails) {
            this.minCostValue -= this.costRange / 100;
            this.minTimeValue -= this.timeRange / 100;
            this.timeRange = this.maxTimeValue - this.minTimeValue;
            this.costRange = this.maxCostValue - this.minCostValue;
        }
    }
    
    /**
     * <h3>Translate given values to graphics device coordinates</h3>
     * 
     * @param time - time value.
     * @param cost - cost value.
     * 
     * @return
     * Coordinates form translated values.
     */
    private Point translate(double time, double cost) {
        time -= this.minTimeValue;
        cost -= this.minCostValue;
        
        time /= this.timeRange;
        cost /= this.costRange;
        
        time *= this.deviceWidth;
        cost *= this.deviceHeight;
        
        cost = this.deviceHeight - cost;

        return new Point((int)time, (int)cost);
    }
    
    /**
     * <h3>Render results on device</h3>
     * 
     * @param width - width of device.
     * @param height - height of device.
     * @param g - device graphics context.
     */
    public void render(int width, int height, Graphics g) {
        /* store device size */
        this.deviceWidth = width;
        this.deviceHeight = height;
        
        /* compute value ranges */
        computeMinMax();
        
        /* draw */
        clearDevice(g);
        if(this.flagDrawDominatedRegions) {
            drawDominatedRegions(g);
        }
        if(this.flagDrawDots) {
            drawDots(g);
        }
        if(this.flagDrawLines) {
            drawLines(g);
        }
        drawCrosses(g);
        if(this.flagDrawBorder) {
            drawBorder(g);
        }
        if(this.flagDrawResultsDetails) {
            drawResultsDetails(g);
        }
    }
    
    
    /**
     * <h3>Draw results details</h3>
     * 
     * @param g - device graphics context.
     */
    private void drawResultsDetails(Graphics g) {
        Iterator<DataHolder.ResultsEntry> iterator;

        iterator = DataHolder.getInstance().getResults();   
        while(iterator.hasNext()) {
            DataHolder.ResultsEntry entry = iterator.next();

            g.setColor(entry.getColor().darker().darker().darker());

            Iterator<Path> pathIterator = entry.getResults().getPaths();
            while(pathIterator.hasNext()) {
                Path path = pathIterator.next();
                
                double currentCost = 0.0;
                double currentTime = 0.0;
                // draw initial solution point
                Point start = translate(0.0, 0.0);
                Point end = translate(0.0, 0.0);
                g.drawLine(start.x, start.y, end.x, end.y);

                Iterator edgeIterator = path.getEdges();
                while(edgeIterator.hasNext()) {
                    Edge currentEdge = (Edge)edgeIterator.next();
                    
                    double newCost = currentCost + currentEdge.getCost();
                    double newTime = currentTime + currentEdge.getTime();
                    
                    end = translate(newTime, newCost);
                    
                    g.drawLine(start.x, start.y, end.x, end.y);
                    
                    start = end;
                    currentCost = newCost;
                    currentTime = newTime;
                }
            }
        }
    }

    /**
     * <h3>Draw border</h3>
     * 
     * @param g - device graphics context.
     */
    private void drawBorder(Graphics g) {
        g.setColor(Color.BLACK);
        g.drawRect(0, 0, this.deviceWidth - 1, this.deviceHeight - 1);
//        g.setColor(Color.WHITE);
//        g.drawRect(1, 1, this.deviceWidth - 3, this.deviceHeight - 3);
    }

    /**
     * <h3>Draw results crosses</h3>
     * 
     * @param g - device graphics context.
     */
    private void drawCrosses(Graphics g) {
        Iterator<DataHolder.ResultsEntry> iterator;

        iterator = DataHolder.getInstance().getResults();   
        while(iterator.hasNext()) {
            DataHolder.ResultsEntry entry = iterator.next();

            g.setColor(entry.getColor().darker().darker().darker());

            Iterator<Path> pathIterator = entry.getResults().getPaths();
            while(pathIterator.hasNext()) {
                Path path = pathIterator.next();
                
                Point p = translate(path.getTime(), path.getCost());

                g.drawLine(p.x - 2, p.y - 2, p.x + 2, p.y + 2);
                g.drawLine(p.x - 2, p.y + 2, p.x + 2, p.y - 2);
            }
        }
    }

    /**
     * <h3>Draw dominance lines</h3>
     * 
     * @param g - device graphics context.
     */
    private void drawLines(Graphics g) {
        Iterator<DataHolder.ResultsEntry> iterator;

        iterator = DataHolder.getInstance().getResults();   
        while(iterator.hasNext()) {
            DataHolder.ResultsEntry entry = iterator.next();

            g.setColor(entry.getColor().darker().darker());

            Iterator<Path> pathIterator = entry.getResults().getPaths();
            while(pathIterator.hasNext()) {
                Path path = pathIterator.next();
                
                Point p = translate(path.getTime(), path.getCost());
                
                g.drawLine(p.x, p.y, this.deviceWidth, p.y);
                g.drawLine(p.x, p.y, p.x, 0);
            }
        }
    }

    /**
     * <h3>Draw results dots</h3>
     * 
     * @param g - device graphics context.
     */
    private void drawDots(Graphics g) {
        Iterator<DataHolder.ResultsEntry> iterator;

        iterator = DataHolder.getInstance().getResults();   
        while(iterator.hasNext()) {
            DataHolder.ResultsEntry entry = iterator.next();

            g.setColor(entry.getColor().darker());

            Iterator<Path> pathIterator = entry.getResults().getPaths();
            while(pathIterator.hasNext()) {
                Path path = pathIterator.next();
                
                Point p = translate(path.getTime(), path.getCost());
                
                g.fillOval(p.x - 4, p.y - 4, 9, 9);
            }
        }
    }

    /**
     * <h3>Draw dominated regions</h3>
     * 
     * @param g - device graphics context.
     */
    private void drawDominatedRegions(Graphics g) {
        Iterator<DataHolder.ResultsEntry> iterator;

        iterator = DataHolder.getInstance().getResults();   
        while(iterator.hasNext()) {
            DataHolder.ResultsEntry entry = iterator.next();

            Iterator<Path> pathIterator = entry.getResults().getPaths();
            while(pathIterator.hasNext()) {
                Path path = pathIterator.next();
                
                Point p = translate(path.getTime(), path.getCost());
                
                g.setColor(entry.getColor());
                g.fillRect(p.x, 0, this.deviceWidth, p.y + 1);
            }
        }
    }

    /**
     * <h3>Clear device</h3>
     * 
     * @param g - device graphics context.
     */
    private void clearDevice(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, this.deviceWidth, this.deviceHeight);
    }
}
