/**
 * Created:     2005-11-13
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.generator.full;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * <h3>Options panel class for full graph generator plugin</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
class FullGeneratorPanel extends JPanel {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = -3531425503290696307L;
    
    /** <h3>Vertex count spinner control</h3> */
    private JSpinner vertexCountSpinner;
    /** <h3>Multiplier value spinner control</h3> */
    private JSpinner multiCountSpinner;
    /** <h3>Cost criterium range minimum spinner control</h3> */
    private JSpinner minCostSpinner;
    /** <h3>Cost criterium range maximum spinner control</h3> */
    private JSpinner maxCostSpinner;
    /** <h3>Time criterium range minimum spinner control</h3> */
    private JSpinner minTimeSpinner;
    /** <h3>Time criterium range maximum spinner control</h3> */
    private JSpinner maxTimeSpinner;

    
    /**
     * <h3>Constructor</h3>
     */
    public FullGeneratorPanel() {
        super(new BorderLayout());
        
        this.vertexCountSpinner = new JSpinner(
                new SpinnerNumberModel(
                        FullGeneratorPlugin.VERTEX_COUNT_DEFAULT,
                        FullGeneratorPlugin.VERTEX_COUNT_MIN,
                        FullGeneratorPlugin.VERTEX_COUNT_MAX,
                        FullGeneratorPlugin.VERTEX_COUNT_STEP));
        this.multiCountSpinner = new JSpinner(
                new SpinnerNumberModel(
                        FullGeneratorPlugin.MULTI_COUNT_DEFAULT,
                        FullGeneratorPlugin.MULTI_COUNT_MIN,
                        FullGeneratorPlugin.MULTI_COUNT_MAX,
                        FullGeneratorPlugin.MULTI_COUNT_STEP));
        this.minCostSpinner = new JSpinner(
                new SpinnerNumberModel(
                        FullGeneratorPlugin.CRITERIUM_DEFAULT_MIN,
                        FullGeneratorPlugin.CRITERIUM_MIN,
                        FullGeneratorPlugin.CRITERIUM_MAX,
                        FullGeneratorPlugin.CRITERIUM_STEP));
        this.maxCostSpinner = new JSpinner(
                new SpinnerNumberModel(
                        FullGeneratorPlugin.CRITERIUM_DEFAULT_MAX,
                        FullGeneratorPlugin.CRITERIUM_MIN,
                        FullGeneratorPlugin.CRITERIUM_MAX,
                        FullGeneratorPlugin.CRITERIUM_STEP));
        this.minTimeSpinner = new JSpinner(
                new SpinnerNumberModel(
                        FullGeneratorPlugin.CRITERIUM_DEFAULT_MIN,
                        FullGeneratorPlugin.CRITERIUM_MIN,
                        FullGeneratorPlugin.CRITERIUM_MAX,
                        FullGeneratorPlugin.CRITERIUM_STEP));
        this.maxTimeSpinner = new JSpinner(
                new SpinnerNumberModel(
                        FullGeneratorPlugin.CRITERIUM_DEFAULT_MAX,
                        FullGeneratorPlugin.CRITERIUM_MIN,
                        FullGeneratorPlugin.CRITERIUM_MAX,
                        FullGeneratorPlugin.CRITERIUM_STEP));
        
        JPanel internalPanel = new JPanel(new GridLayout(6, 2, 3, 3));
        internalPanel.add(new JLabel("Vertex count:"));
        internalPanel.add(this.vertexCountSpinner);
        internalPanel.add(new JLabel("Multiplier:"));
        internalPanel.add(this.multiCountSpinner);
        internalPanel.add(new JLabel("Min. cost:"));
        internalPanel.add(this.minCostSpinner);
        internalPanel.add(new JLabel("Max. cost:"));
        internalPanel.add(this.maxCostSpinner);
        internalPanel.add(new JLabel("Min. time:"));
        internalPanel.add(this.minTimeSpinner);
        internalPanel.add(new JLabel("Max. time:"));
        internalPanel.add(this.maxTimeSpinner);

        JPanel subPanel = new JPanel(new BorderLayout());
        subPanel.add(internalPanel, BorderLayout.NORTH);
        add(subPanel, BorderLayout.WEST);        
    }
    
    
    
    /**
     * <h3>Get number of vertices</h3>
     * 
     * @return
     * Number of vertices.
     */
    public int getVertexCount() {
        Integer value = (Integer) this.vertexCountSpinner.getValue();
        return value.intValue();
    }

    /**
     * <h3>Get multiplier value</h3>
     * 
     * @return
     * Multiplier value.
     */
    public int getMultiCount() {
        Integer value = (Integer) this.multiCountSpinner.getValue();
        return value.intValue();
    }

    /**
     * <h3>Get minimum cost value</h3>
     * 
     * @return
     * Minimum cost value.
     */
    public double getMinCostValue() {
        Double value = (Double) this.minCostSpinner.getValue();
        return value.doubleValue();
    }

    /**
     * <h3>Get maximum cost value</h3>
     * 
     * @return
     * Maximum cost value.
     */
    public double getMaxCostValue() {
        Double value = (Double) this.maxCostSpinner.getValue();
        return value.doubleValue();
    }

    /**
     * <h3>Get minimum time value</h3>
     * 
     * @return
     * Minimum time value.
     */
    public double getMinTimeValue() {
        Double value = (Double) this.minTimeSpinner.getValue();
        return value.doubleValue();
    }

    /**
     * <h3>Get maximum time value</h3>
     * 
     * @return
     * Maximum time value.
     */
    public double getMaxTimeValue() {
        Double value = (Double) this.maxTimeSpinner.getValue();
        return value.doubleValue();
    }

    /**
     * <h3>Set number of vertices</h3>
     * 
     * @param i - number of vertices to set.
     */
    public void setVertexCount(int i) {
        this.vertexCountSpinner.setValue(i);
    }

    /**
     * <h3>Set multiplier value</h3>
     * 
     * @param i - multiplier value to set.
     */
    public void setMultiCount(int i) {
        this.multiCountSpinner.setValue(i);
    }

    /**
     * <h3>Set minimum cost value</h3>
     * 
     * @param d - value to set.
     */
    public void setMinCostValue(double d) {
        this.minCostSpinner.setValue(d);
    }

    /**
     * <h3>Set maximum cost value</h3>
     * 
     * @param d - value to set.
     */
    public void setMaxCostValue(double d) {
        this.maxCostSpinner.setValue(d);
    }

    /**
     * <h3>Set minimum time value</h3>
     * 
     * @param d - value to set.
     */
    public void setMinTimeValue(double d) {
        this.minTimeSpinner.setValue(d);
    }

    /**
     * <h3>Set maximum time value</h3>
     * 
     * @param d - value to set.
     */
    public void setMaxTimeValue(double d) {
        this.maxTimeSpinner.setValue(d);
    }

    
    
}
