/**
 * Created:     2005-11-13
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.generator.full;

import javax.swing.JPanel;

import com.octaedr.octabu.app.plugin.IGeneratorPlugin;
import com.octaedr.octabu.exception.InvalidParamsException;
import com.octaedr.octabu.exception.OctabuException;
import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.graph.GraphInfo;
import com.octaedr.octabu.graph.graph.Vertex;
import com.octaedr.octgraph.exception.OctgraphException;

/**
 * <h3>Full graph generator plugin class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class FullGeneratorPlugin implements IGeneratorPlugin {

    /** <h3>Minimum number of vertices</h3> */
    static final int VERTEX_COUNT_MIN       =   2;
    /** <h3>Maximum number of vertices</h3> */
    static final int VERTEX_COUNT_MAX       =   10000000;
    /** <h3>Default number of vertices</h3> */
    static final int VERTEX_COUNT_DEFAULT   =   100;
    /** <h3>Step when modyfying number of vertices</h3> */
    static final int VERTEX_COUNT_STEP      =   1;

    /** <h3>Minimum multiplier value</h3> */
    static final int MULTI_COUNT_MIN       =   1;
    /** <h3>Maximum multiplier value</h3> */
    static final int MULTI_COUNT_MAX       =   100;
    /** <h3>Default multiplier value</h3> */
    static final int MULTI_COUNT_DEFAULT   =   1;
    /** <h3>Step when modyfying multiplier value</h3> */
    static final int MULTI_COUNT_STEP      =   1;
    
    /** <h3>Miminum criterium value</h3> */
    static final double CRITERIUM_MIN           =   1.0;
    /** <h3>Maximum criterium value</h3> */
    static final double CRITERIUM_MAX           =   1000000.0;
    /** <h3>Step when modyfying criterium value range</h3> */
    static final double CRITERIUM_STEP          =   1.0;
    /** <h3>Default start value of criterium value range</h3> */
    static final double CRITERIUM_DEFAULT_MIN   =   1.0;
    /** <h3>Default end value of criterium value range</h3> */
    static final double CRITERIUM_DEFAULT_MAX   =   100.0;

    /** <h3>Number of vertices</h3> */
    private int vertexCount = VERTEX_COUNT_DEFAULT;
    /** <h3>Number of multiplication of full graph (to create multigraph)</h3> */
    private int multiCount = MULTI_COUNT_DEFAULT;
    /** <h3>Minimum cost value</h3> */
    private double minCostValue = CRITERIUM_DEFAULT_MIN;
    /** <h3>Maximum cost value</h3> */
    private double maxCostValue = CRITERIUM_DEFAULT_MAX;
    /** <h3>Minimum time value</h3> */
    private double minTimeValue = CRITERIUM_DEFAULT_MIN;
    /** <h3>Maximum time value</h3> */
    private double maxTimeValue = CRITERIUM_DEFAULT_MAX;

    /** <h3>Gui options panel</h3> */
    private FullGeneratorPanel optionsPanel;
    
    /**
     * <h3>Constructor</h3>
     *
     * <p>Creates initialized full generator plugin.</p>
     */
    public FullGeneratorPlugin() {
        /* create gui options panel */
        this.optionsPanel = new FullGeneratorPanel();

        /* export initial values to gui */
        exportGuiValues();
    }
    
    
    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getShortName()
     */
    public String getShortName() {
        return "plugin/generator/full";
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getName()
     */
    public String getName() {
        return "Full Graph Generator 1.0";
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getOptionsPanel()
     */
    public JPanel getOptionsPanel() {
        return this.optionsPanel;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#checkOptions()
     */
    public boolean checkOptions() {
        importGuiValues();
        
        if(this.vertexCount < 2) {
            return false;
        }

        if(this.multiCount < 1) {
            return false;
        }

        if(this.minCostValue > this.maxCostValue) {
            return false;
        }
        if(this.minTimeValue > this.maxTimeValue) {
            return false;
        }

        if(this.minCostValue < 0.0) {
            return false;
        }
        if(this.minTimeValue < 0.0) {
            return false;
        }
        
        return true;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#validateOptions()
     */
    public void validateOptions() {
        importGuiValues();

        if(this.vertexCount < 2) {
            this.vertexCount = 2;
        }

        if(this.multiCount < 1) {
            this.multiCount = 1;
        }

        if(this.minCostValue > this.maxCostValue) {
            double tmp = this.minCostValue;
            this.minCostValue = this.maxCostValue;
            this.maxCostValue = tmp;
        }
        if(this.minTimeValue > this.maxTimeValue) {
            double tmp = this.minTimeValue;
            this.minTimeValue = this.maxTimeValue;
            this.maxTimeValue = tmp;
        }

        if(this.minCostValue < 0.0) {
            this.minCostValue = 0.0;
        }
        if(this.minTimeValue < 0.0) {
            this.minTimeValue = 0.0;
        }
        if(this.maxCostValue < 0.0) {
            this.maxCostValue = 0.0;
        }
        if(this.maxTimeValue < 0.0) {
            this.maxTimeValue = 0.0;
        }
        
        exportGuiValues();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IGeneratorPlugin#generate()
     */
    public Graph generate() throws OctabuException {
        try {
            /* check parameters */
            if(!checkOptions()) {
                throw new InvalidParamsException();
            }
    
            /* compute values ranges */
            double costRange = this.maxCostValue - this.minCostValue;
            double timeRange = this.maxTimeValue - this.minTimeValue;
    
            /* create graph */
            Graph graph = new Graph();
    
            /* create vertices */
            for(int i = 1; i <= this.vertexCount; ++i) {
                Vertex vertex = new Vertex();
                vertex.setKey(i);
                vertex.addToGraph(graph);
            }
            
            /* create edges */
            for(int multi = 1; multi <= this.multiCount; ++multi) {
                for(int from = 1; from <= this.vertexCount; ++from) {
                    for(int to = 1; to <= this.vertexCount; ++to) {
                        if(from == to) {
                            continue;
                        }
                        
                        Edge edge = new Edge();
                        edge.setVertices(
                                (Vertex)graph.findVertex(from),
                                (Vertex)graph.findVertex(to));
                        edge.setCost(this.minCostValue + Math.random() * costRange);
                        edge.setTime(this.minTimeValue + Math.random() * timeRange);
                        edge.addToGraph(graph);
                    }
                }
            }
    
            /* create graph info */
            GraphInfo graphInfo = new GraphInfo();
            graphInfo.setGenerator(getName());
            graphInfo.addEntry("Vertices", this.vertexCount);
            graphInfo.addEntry("Multi", this.multiCount);
            graphInfo.addEntry("Min. cost", this.minCostValue);
            graphInfo.addEntry("Max. cost", this.maxCostValue);
            graphInfo.addEntry("Cost range", costRange);
            graphInfo.addEntry("Min. time", this.minTimeValue);
            graphInfo.addEntry("Max. time", this.maxTimeValue);
            graphInfo.addEntry("Time range", timeRange);
            graph.setInfo(graphInfo);
            
            return graph;
        } catch(OctgraphException exception) {
            throw new OctabuException("Octgraph exception catched");
        }
    }

    /**
     * <h3>Import options values from gui</h3>
     */
    private void importGuiValues() {
        this.vertexCount = this.optionsPanel.getVertexCount();
        this.multiCount = this.optionsPanel.getMultiCount();
        this.minCostValue = this.optionsPanel.getMinCostValue();
        this.maxCostValue = this.optionsPanel.getMaxCostValue();
        this.minTimeValue = this.optionsPanel.getMinTimeValue();
        this.maxTimeValue = this.optionsPanel.getMaxTimeValue();
    }
    
    /**
     * <h3>Export options values to gui</h3>
     */
    private void exportGuiValues() {
        this.optionsPanel.setVertexCount(this.vertexCount);
        this.optionsPanel.setMultiCount(this.multiCount);
        this.optionsPanel.setMinCostValue(this.minCostValue);
        this.optionsPanel.setMaxCostValue(this.maxCostValue);
        this.optionsPanel.setMinTimeValue(this.minTimeValue);
        this.optionsPanel.setMaxTimeValue(this.maxTimeValue);
    }
    
}
