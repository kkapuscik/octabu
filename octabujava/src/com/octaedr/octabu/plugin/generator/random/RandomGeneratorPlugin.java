/**
 * Created:     2005-11-13
 * Modified:    2005-11-20
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.generator.random;

import java.util.Random;

import javax.swing.JPanel;

import com.octaedr.octabu.app.plugin.IGeneratorPlugin;
import com.octaedr.octabu.exception.InvalidParamsException;
import com.octaedr.octabu.exception.OctabuException;
import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.graph.GraphInfo;
import com.octaedr.octabu.graph.graph.Vertex;
import com.octaedr.octgraph.exception.OctgraphException;
import com.octaedr.octgraph.graph.IDirectedVertex;

/**
 * <h3>Random graph generator class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.2
 */
public class RandomGeneratorPlugin implements IGeneratorPlugin {

    /** <h3>Minimum number of vertices</h3> */
    static final int VERTEX_COUNT_MIN       =   2;
    /** <h3>Maximum number of vertices</h3> */
    static final int VERTEX_COUNT_MAX       =   10000000;
    /** <h3>Default number of vertices</h3> */
    static final int VERTEX_COUNT_DEFAULT   =   100;
    /** <h3>Step when modyfying number of vertices</h3> */
    static final int VERTEX_COUNT_STEP      =   1;

    /** <h3>Minimum number of edges</h3> */
    static final int EDGE_COUNT_MIN       =   1;
    /** <h3>Maximum number of edges</h3> */
    static final int EDGE_COUNT_MAX       =   100000000;
    /** <h3>Default number of edges</h3> */
    static final int EDGE_COUNT_DEFAULT   =   2000;
    /** <h3>Step when modyfying number of edges</h3> */
    static final int EDGE_COUNT_STEP      =   1;
    
    /** <h3>Miminum criterium value</h3> */
    static final double CRITERIUM_MIN           =   1.0;
    /** <h3>Maximum criterium value</h3> */
    static final double CRITERIUM_MAX           =   1000000.0;
    /** <h3>Step when modyfying criterium value range</h3> */
    static final double CRITERIUM_STEP          =   1.0;
    /** <h3>Default start value of criterium value range</h3> */
    static final double CRITERIUM_DEFAULT_MIN   =   1.0;
    /** <h3>Default end value of criterium value range</h3> */
    static final double CRITERIUM_DEFAULT_MAX   =   100.0;

    /** <h3>Hamiltonian cycle random type</h3> */
    static final String HAMILTONIAN_CYCLE_TYPE_RANDOM   =   "Random";
    /** <h3>Hamiltonian cycle loop type</h3> */
    static final String HAMILTONIAN_CYCLE_TYPE_LOOP     =   "Loop";

    /** <h3>Number of vertices</h3> */
    private int vertexCount = VERTEX_COUNT_DEFAULT;
    /** <h3>Number of edges</h3> */
    private int edgeCount = EDGE_COUNT_DEFAULT;
    /** <h3>Minimum cost value</h3> */
    private double minCostValue = CRITERIUM_DEFAULT_MIN;
    /** <h3>Maximum cost value</h3> */
    private double maxCostValue = CRITERIUM_DEFAULT_MAX;
    /** <h3>Minimum time value</h3> */
    private double minTimeValue = CRITERIUM_DEFAULT_MIN;
    /** <h3>Maximum time value</h3> */
    private double maxTimeValue = CRITERIUM_DEFAULT_MAX;
    /** <h3>Hamiltonian cycle type</h3> */
    private String hamiltonianCycleType = HAMILTONIAN_CYCLE_TYPE_RANDOM;

    /** <h3>Gui options panel</h3> */
    private RandomGeneratorPanel optionsPanel;

    
    /**
     * <h3>Constructor</h3>
     *
     * <p>Creates initialized random generator plugin.</p>
     */
    public RandomGeneratorPlugin() {
        /* create gui options panel */
        this.optionsPanel = new RandomGeneratorPanel();

        /* export initial values to gui */
        exportGuiValues();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getShortName()
     */
    public String getShortName() {
        return "plugin/generator/random";
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getName()
     */
    public String getName() {
        return "Random Graph Generator 1.0";
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getOptionsPanel()
     */
    public JPanel getOptionsPanel() {
        return this.optionsPanel;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#checkOptions()
     */
    public boolean checkOptions() {
        importGuiValues();
        
        if(this.vertexCount < 2) {
            return false;
        }

        if(this.edgeCount < this.edgeCount) {
            return false;
        }

        if(this.minCostValue > this.maxCostValue) {
            return false;
        }
        if(this.minTimeValue > this.maxTimeValue) {
            return false;
        }

        if(this.minCostValue < 0.0) {
            return false;
        }
        if(this.minTimeValue < 0.0) {
            return false;
        }
        
        return true;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#validateOptions()
     */
    public void validateOptions() {
        importGuiValues();

        if(this.vertexCount < 2) {
            this.vertexCount = 2;
        }

        if(this.edgeCount < this.vertexCount) {
            this.edgeCount = this.vertexCount;
        }

        if(this.minCostValue > this.maxCostValue) {
            double tmp = this.minCostValue;
            this.minCostValue = this.maxCostValue;
            this.maxCostValue = tmp;
        }
        if(this.minTimeValue > this.maxTimeValue) {
            double tmp = this.minTimeValue;
            this.minTimeValue = this.maxTimeValue;
            this.maxTimeValue = tmp;
        }

        if(this.minCostValue < 0.0) {
            this.minCostValue = 0.0;
        }
        if(this.minTimeValue < 0.0) {
            this.minTimeValue = 0.0;
        }
        if(this.maxCostValue < 0.0) {
            this.maxCostValue = 0.0;
        }
        if(this.maxTimeValue < 0.0) {
            this.maxTimeValue = 0.0;
        }
        
        exportGuiValues();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IGeneratorPlugin#generate()
     */
    public Graph generate() throws OctabuException {
        try {
            /* check parameters */
            if(!checkOptions()) {
                throw new InvalidParamsException();
            }
    
            /* compute values ranges */
            double costRange = this.maxCostValue - this.minCostValue;
            double timeRange = this.maxTimeValue - this.minTimeValue;
            Edge edge;
            Random randomGenerator = new Random();
    
            /* create graph */
            Graph graph = new Graph();
    
            /* creating vertices */
            for(int i = 1; i <= this.vertexCount; ++i)
            {
                Vertex vertex = new Vertex();
                vertex.setKey(i);
                vertex.addToGraph(graph);
            }
    
            /* create Hamiltonian cycle */
            if(this.hamiltonianCycleType == HAMILTONIAN_CYCLE_TYPE_RANDOM) {
                /* creating random Hamiltonian cycle */
                Vertex lastVertex = (Vertex)graph.findVertex(1);
                for(int i = 1; i < this.vertexCount; ) {
                    int next = 2 + randomGenerator.nextInt(this.vertexCount - 1);
                    Vertex nextVertex = (Vertex)graph.findVertex(next);
                    if((nextVertex == lastVertex) || 
                            (nextVertex.getOutDegree() > 0) ||
                            (nextVertex.getInDegree() > 0)) {
                        continue;
                    }
                    edge = new Edge();
                    edge.setVertices(lastVertex, nextVertex);
                    edge.setCost(this.minCostValue + Math.random() * costRange);
                    edge.setTime(this.minTimeValue + Math.random() * timeRange);
                    edge.addToGraph(graph);
                    
                    lastVertex = nextVertex;
                    ++i;
                }
                edge = new Edge();
                edge.setVertices(lastVertex, (Vertex)graph.findVertex(1));
                edge.setCost(this.minCostValue + Math.random() * costRange);
                edge.setTime(this.minTimeValue + Math.random() * timeRange);
                edge.addToGraph(graph);
            } else {
                /* creating loop connecting all vertices */
                for(int i = 2; i <= this.vertexCount; ++i)
                {
                    edge = new Edge();
                    edge.setVertices(
                            (Vertex)graph.findVertex(i-1),
                            (Vertex)graph.findVertex(i));
                    edge.setCost(this.minCostValue + Math.random() * costRange);
                    edge.setTime(this.minTimeValue + Math.random() * timeRange);
                    edge.addToGraph(graph);
                }
                edge = new Edge();
                edge.setVertices(
                        (Vertex)graph.findVertex(this.vertexCount),
                        (Vertex)graph.findVertex(1));
                edge.setCost(this.minCostValue + Math.random() * costRange);
                edge.setTime(this.minTimeValue + Math.random() * timeRange);
                edge.addToGraph(graph);
            }

            /* create random remaining edges */
            int tmpEdgeCount = this.edgeCount - this.vertexCount;
            while(tmpEdgeCount > 0)
            {
                int v1 = 1 + randomGenerator.nextInt(this.vertexCount);
                int v2 = v1;
                /* no self-loops allowed, they have no sense */
                while(v1 == v2)
                {
                    v2 = 1 + randomGenerator.nextInt(this.vertexCount);
                }
    
                edge = new Edge();
                edge.setVertices(
                        (IDirectedVertex)graph.findVertex(v1),
                        (IDirectedVertex)graph.findVertex(v2));
                edge.setCost(this.minCostValue + Math.random() * costRange);
                edge.setTime(this.minTimeValue + Math.random() * timeRange);
                edge.addToGraph(graph);
    
                tmpEdgeCount--;
            }
    
            /* create graph info */
            GraphInfo graphInfo = new GraphInfo();
            graphInfo.setGenerator(getName());
            graphInfo.addEntry("Vertices", this.vertexCount);
            graphInfo.addEntry("Edges", this.edgeCount);
            graphInfo.addEntry("Min. cost", this.minCostValue);
            graphInfo.addEntry("Max. cost", this.maxCostValue);
            graphInfo.addEntry("Cost range", costRange);
            graphInfo.addEntry("Min. time", this.minTimeValue);
            graphInfo.addEntry("Max. time", this.maxTimeValue);
            graphInfo.addEntry("Time range", timeRange);
            graph.setInfo(graphInfo);
    
            return graph; 
        } catch(OctgraphException exception) {
            throw new OctabuException("Octgraph exception catched");
        }
    }

    /**
     * <h3>Import options values from gui</h3>
     */
    private void importGuiValues() {
        this.vertexCount = this.optionsPanel.getVertexCount();
        this.edgeCount = this.optionsPanel.getEdgeCount();
        this.minCostValue = this.optionsPanel.getMinCostValue();
        this.maxCostValue = this.optionsPanel.getMaxCostValue();
        this.minTimeValue = this.optionsPanel.getMinTimeValue();
        this.maxTimeValue = this.optionsPanel.getMaxTimeValue();
        this.hamiltonianCycleType = this.optionsPanel.getHamilonianCycleType();
    }
    
    /**
     * <h3>Export options values from gui</h3>
     */
    private void exportGuiValues() {
        this.optionsPanel.setVertexCount(this.vertexCount);
        this.optionsPanel.setEdgeCount(this.edgeCount);
        this.optionsPanel.setMinCostValue(this.minCostValue);
        this.optionsPanel.setMaxCostValue(this.maxCostValue);
        this.optionsPanel.setMinTimeValue(this.minTimeValue);
        this.optionsPanel.setMaxTimeValue(this.maxTimeValue);
        this.optionsPanel.setHamilonianCycleType(this.hamiltonianCycleType);
    }
    
}
