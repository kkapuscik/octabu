/**
 * Created:     2005-11-13
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.generator.random;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * <h3>Options panel class for random graph generator plugin</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
class RandomGeneratorPanel extends JPanel {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = -3531425503290696307L;
    
    /** <h3>Vertex count spinner control</h3> */
    private JSpinner vertexCountSpinner;
    /** <h3>Edge count spinner control</h3> */
    private JSpinner edgeCountSpinner;
    /** <h3>Cost criterium range minimum spinner control</h3> */
    private JSpinner minCostSpinner;
    /** <h3>Cost criterium range maximum spinner control</h3> */
    private JSpinner maxCostSpinner;
    /** <h3>Time criterium range minimum spinner control</h3> */
    private JSpinner minTimeSpinner;
    /** <h3>Time criterium range maximum spinner control</h3> */
    private JSpinner maxTimeSpinner;
    /** <h3>Hamiltonian cycle type combo box control</h3> */
    private JComboBox hamiltonianCycleCombo;

    /**
     * <h3>Constructor</h3>
     */
    public RandomGeneratorPanel() {
        super(new BorderLayout());
        
        this.vertexCountSpinner = new JSpinner(
                new SpinnerNumberModel(
                        RandomGeneratorPlugin.VERTEX_COUNT_DEFAULT,
                        RandomGeneratorPlugin.VERTEX_COUNT_MIN,
                        RandomGeneratorPlugin.VERTEX_COUNT_MAX,
                        RandomGeneratorPlugin.VERTEX_COUNT_STEP));
        this.edgeCountSpinner = new JSpinner(
                new SpinnerNumberModel(
                        RandomGeneratorPlugin.EDGE_COUNT_DEFAULT,
                        RandomGeneratorPlugin.EDGE_COUNT_MIN,
                        RandomGeneratorPlugin.EDGE_COUNT_MAX,
                        RandomGeneratorPlugin.EDGE_COUNT_STEP));
        this.minCostSpinner = new JSpinner(
                new SpinnerNumberModel(
                        RandomGeneratorPlugin.CRITERIUM_DEFAULT_MIN,
                        RandomGeneratorPlugin.CRITERIUM_MIN,
                        RandomGeneratorPlugin.CRITERIUM_MAX,
                        RandomGeneratorPlugin.CRITERIUM_STEP));
        this.maxCostSpinner = new JSpinner(
                new SpinnerNumberModel(
                        RandomGeneratorPlugin.CRITERIUM_DEFAULT_MAX,
                        RandomGeneratorPlugin.CRITERIUM_MIN,
                        RandomGeneratorPlugin.CRITERIUM_MAX,
                        RandomGeneratorPlugin.CRITERIUM_STEP));
        this.minTimeSpinner = new JSpinner(
                new SpinnerNumberModel(
                        RandomGeneratorPlugin.CRITERIUM_DEFAULT_MIN,
                        RandomGeneratorPlugin.CRITERIUM_MIN,
                        RandomGeneratorPlugin.CRITERIUM_MAX,
                        RandomGeneratorPlugin.CRITERIUM_STEP));
        this.maxTimeSpinner = new JSpinner(
                new SpinnerNumberModel(
                        RandomGeneratorPlugin.CRITERIUM_DEFAULT_MAX,
                        RandomGeneratorPlugin.CRITERIUM_MIN,
                        RandomGeneratorPlugin.CRITERIUM_MAX,
                        RandomGeneratorPlugin.CRITERIUM_STEP));
        
        this.hamiltonianCycleCombo = new JComboBox();
        this.hamiltonianCycleCombo.addItem(
                RandomGeneratorPlugin.HAMILTONIAN_CYCLE_TYPE_LOOP);
        this.hamiltonianCycleCombo.addItem(
                RandomGeneratorPlugin.HAMILTONIAN_CYCLE_TYPE_RANDOM);

        JPanel internalPanel = new JPanel(new GridLayout(7, 2, 3, 3));
        internalPanel.add(new JLabel("Vertex count:"));
        internalPanel.add(this.vertexCountSpinner);
        internalPanel.add(new JLabel("Edge count:"));
        internalPanel.add(this.edgeCountSpinner);
        internalPanel.add(new JLabel("Min. cost:"));
        internalPanel.add(this.minCostSpinner);
        internalPanel.add(new JLabel("Max. cost:"));
        internalPanel.add(this.maxCostSpinner);
        internalPanel.add(new JLabel("Min. time:"));
        internalPanel.add(this.minTimeSpinner);
        internalPanel.add(new JLabel("Max. time:"));
        internalPanel.add(this.maxTimeSpinner);
        internalPanel.add(new JLabel("Hamiltonian cycle:"));
        internalPanel.add(this.hamiltonianCycleCombo);

        JPanel subPanel = new JPanel(new BorderLayout());
        subPanel.add(internalPanel, BorderLayout.NORTH);
        add(subPanel, BorderLayout.WEST);        
    }
    
    
    
    /**
     * <h3>Get number of vertices</h3>
     * 
     * @return
     * Number of vertices.
     */
    public int getVertexCount() {
        Integer value = (Integer) this.vertexCountSpinner.getValue();
        return value.intValue();
    }

    /**
     * <h3>Get number of edges</h3>
     * 
     * @return
     * Multiplier value.
     */
    public int getEdgeCount() {
        Integer value = (Integer) this.edgeCountSpinner.getValue();
        return value.intValue();
    }

    /**
     * <h3>Get minimum cost value</h3>
     * 
     * @return
     * Minimum cost value.
     */
    public double getMinCostValue() {
        Double value = (Double) this.minCostSpinner.getValue();
        return value.doubleValue();
    }

    /**
     * <h3>Get maximum cost value</h3>
     * 
     * @return
     * Maximum cost value.
     */
    public double getMaxCostValue() {
        Double value = (Double) this.maxCostSpinner.getValue();
        return value.doubleValue();
    }

    /**
     * <h3>Get minimum time value</h3>
     * 
     * @return
     * Minimum time value.
     */
    public double getMinTimeValue() {
        Double value = (Double) this.minTimeSpinner.getValue();
        return value.doubleValue();
    }

    /**
     * <h3>Get maximum time value</h3>
     * 
     * @return
     * Maximum time value.
     */
    public double getMaxTimeValue() {
        Double value = (Double) this.maxTimeSpinner.getValue();
        return value.doubleValue();
    }

    /**
     * <h3>Set number of vertices</h3>
     * 
     * @param i - number of vertices to set.
     */
    public void setVertexCount(int i) {
        this.vertexCountSpinner.setValue(i);
    }

    /**
     * <h3>Set number of edges</h3>
     * 
     * @param i - number of edges to set.
     */
    public void setEdgeCount(int i) {
        this.edgeCountSpinner.setValue(i);
    }

    /**
     * <h3>Set minimum cost value</h3>
     * 
     * @param d - value to set.
     */
    public void setMinCostValue(double d) {
        this.minCostSpinner.setValue(d);
    }

    /**
     * <h3>Set maximum cost value</h3>
     * 
     * @param d - value to set.
     */
    public void setMaxCostValue(double d) {
        this.maxCostSpinner.setValue(d);
    }

    /**
     * <h3>Set minimum time value</h3>
     * 
     * @param d - value to set.
     */
    public void setMinTimeValue(double d) {
        this.minTimeSpinner.setValue(d);
    }

    /**
     * <h3>Set maximum time value</h3>
     * 
     * @param d - value to set.
     */
    public void setMaxTimeValue(double d) {
        this.maxTimeSpinner.setValue(d);
    }

    /**
     * <h3>Get hamiltonian cycle type</h3>
     * 
     * @return
     * Type of hamiltonian cycle.
     */
    public String getHamilonianCycleType() {
        return (String)this.hamiltonianCycleCombo.getSelectedItem();
    }

    /**
     * <h3>Set hamiltonian cycle type</h3>
     * 
     * @param type - hamiltionian cycle type to set.
     */
    public void setHamilonianCycleType(String type) {
        this.hamiltonianCycleCombo.setSelectedItem(type);
    }
    
}
