/**
 * Created:     2005-11-13
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.generator.skriver;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * <h3>Options panel class for random graph generator plugin</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
class SkriverGeneratorPanel extends JPanel {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = -3531425503290696307L;
    
    /** <h3>Vertex count spinner control</h3> */
    private JSpinner vertexCountSpinner;
    /** <h3>Edge count range minimum spinner control</h3> */
    private JSpinner edgeMinCountSpinner;
    /** <h3>Edge count range maximum spinner control</h3> */
    private JSpinner edgeMaxCountSpinner;
    /** <h3>Edge distance limit spinner control</h3> */
    private JSpinner distanceSpinner;
    /** <h3>Cost criterium range minimum spinner control</h3> */
    private JSpinner minCostSpinner;
    /** <h3>Cost criterium range maximum spinner control</h3> */
    private JSpinner maxCostSpinner;
    /** <h3>Time criterium range minimum spinner control</h3> */
    private JSpinner minTimeSpinner;
    /** <h3>Time criterium range maximum spinner control</h3> */
    private JSpinner maxTimeSpinner;
    /** <h3>Hamiltonian cycle type combo box control</h3> */
    private JComboBox hamiltonianCycleCombo;
    
    
    /**
     * <h3>Constructor</h3>
     */
    public SkriverGeneratorPanel() {
        super(new BorderLayout());
        
        this.vertexCountSpinner = new JSpinner(
                new SpinnerNumberModel(
                        SkriverGeneratorPlugin.VERTEX_COUNT_DEFAULT,
                        SkriverGeneratorPlugin.VERTEX_COUNT_MIN,
                        SkriverGeneratorPlugin.VERTEX_COUNT_MAX,
                        SkriverGeneratorPlugin.VERTEX_COUNT_STEP));
        this.edgeMinCountSpinner = new JSpinner(
                new SpinnerNumberModel(
                        SkriverGeneratorPlugin.EDGE_COUNT_DEFAULT_MIN,
                        SkriverGeneratorPlugin.EDGE_COUNT_MIN,
                        SkriverGeneratorPlugin.EDGE_COUNT_MAX,
                        SkriverGeneratorPlugin.EDGE_COUNT_STEP));
        this.edgeMaxCountSpinner = new JSpinner(
                new SpinnerNumberModel(
                        SkriverGeneratorPlugin.EDGE_COUNT_DEFAULT_MAX,
                        SkriverGeneratorPlugin.EDGE_COUNT_MIN,
                        SkriverGeneratorPlugin.EDGE_COUNT_MAX,
                        SkriverGeneratorPlugin.EDGE_COUNT_STEP));
        this.distanceSpinner = new JSpinner(
                new SpinnerNumberModel(
                        SkriverGeneratorPlugin.DISTANCE_DEFAULT,
                        SkriverGeneratorPlugin.DISTANCE_MIN,
                        SkriverGeneratorPlugin.DISTANCE_MAX,
                        SkriverGeneratorPlugin.DISTANCE_STEP));
        this.minCostSpinner = new JSpinner(
                new SpinnerNumberModel(
                        SkriverGeneratorPlugin.CRITERIUM_DEFAULT_MIN,
                        SkriverGeneratorPlugin.CRITERIUM_MIN,
                        SkriverGeneratorPlugin.CRITERIUM_MAX,
                        SkriverGeneratorPlugin.CRITERIUM_STEP));
        this.maxCostSpinner = new JSpinner(
                new SpinnerNumberModel(
                        SkriverGeneratorPlugin.CRITERIUM_DEFAULT_MAX,
                        SkriverGeneratorPlugin.CRITERIUM_MIN,
                        SkriverGeneratorPlugin.CRITERIUM_MAX,
                        SkriverGeneratorPlugin.CRITERIUM_STEP));
        this.minTimeSpinner = new JSpinner(
                new SpinnerNumberModel(
                        SkriverGeneratorPlugin.CRITERIUM_DEFAULT_MIN,
                        SkriverGeneratorPlugin.CRITERIUM_MIN,
                        SkriverGeneratorPlugin.CRITERIUM_MAX,
                        SkriverGeneratorPlugin.CRITERIUM_STEP));
        this.maxTimeSpinner = new JSpinner(
                new SpinnerNumberModel(
                        SkriverGeneratorPlugin.CRITERIUM_DEFAULT_MAX,
                        SkriverGeneratorPlugin.CRITERIUM_MIN,
                        SkriverGeneratorPlugin.CRITERIUM_MAX,
                        SkriverGeneratorPlugin.CRITERIUM_STEP));
        
        this.hamiltonianCycleCombo = new JComboBox();
        this.hamiltonianCycleCombo.addItem(
                SkriverGeneratorPlugin.HAMILTONIAN_CYCLE_TYPE_LOOP);
        this.hamiltonianCycleCombo.addItem(
                SkriverGeneratorPlugin.HAMILTONIAN_CYCLE_TYPE_RANDOM);

        JPanel internalPanel = new JPanel(new GridLayout(9, 2, 3, 3));
        internalPanel.add(new JLabel("Vertex count:"));
        internalPanel.add(this.vertexCountSpinner);
        internalPanel.add(new JLabel("Min. edge count:"));
        internalPanel.add(this.edgeMinCountSpinner);
        internalPanel.add(new JLabel("Max. edge count:"));
        internalPanel.add(this.edgeMaxCountSpinner);
        internalPanel.add(new JLabel("Edge distance:"));
        internalPanel.add(this.distanceSpinner);
        internalPanel.add(new JLabel("Min. cost:"));
        internalPanel.add(this.minCostSpinner);
        internalPanel.add(new JLabel("Max. cost:"));
        internalPanel.add(this.maxCostSpinner);
        internalPanel.add(new JLabel("Min. time:"));
        internalPanel.add(this.minTimeSpinner);
        internalPanel.add(new JLabel("Max. time:"));
        internalPanel.add(this.maxTimeSpinner);
        internalPanel.add(new JLabel("Hamiltonian cycle:"));
        internalPanel.add(this.hamiltonianCycleCombo);

        JPanel subPanel = new JPanel(new BorderLayout());
        subPanel.add(internalPanel, BorderLayout.NORTH);
        add(subPanel, BorderLayout.WEST);        
    }
    
    
    
    /**
     * <h3>Get number of vertices</h3>
     * 
     * @return
     * Number of vertices.
     */
    public int getVertexCount() {
        Integer value = (Integer) this.vertexCountSpinner.getValue();
        return value.intValue();
    }

    /**
     * <h3>Get minimum number of edges</h3>
     * 
     * @return
     * Multiplier value.
     */
    public int getMinEdgeCount() {
        Integer value = (Integer) this.edgeMinCountSpinner.getValue();
        return value.intValue();
    }
    
    /**
     * <h3>Get maximum number of edges</h3>
     * 
     * @return
     * Multiplier value.
     */
    public int getMaxEdgeCount() {
        Integer value = (Integer) this.edgeMaxCountSpinner.getValue();
        return value.intValue();
    }

    /**
     * <h3>Get minimum cost value</h3>
     * 
     * @return
     * Minimum cost value.
     */
    public double getMinCostValue() {
        Double value = (Double) this.minCostSpinner.getValue();
        return value.doubleValue();
    }

    /**
     * <h3>Get maximum cost value</h3>
     * 
     * @return
     * Maximum cost value.
     */
    public double getMaxCostValue() {
        Double value = (Double) this.maxCostSpinner.getValue();
        return value.doubleValue();
    }

    /**
     * <h3>Get minimum time value</h3>
     * 
     * @return
     * Minimum time value.
     */
    public double getMinTimeValue() {
        Double value = (Double) this.minTimeSpinner.getValue();
        return value.doubleValue();
    }

    /**
     * <h3>Get maximum time value</h3>
     * 
     * @return
     * Maximum time value.
     */
    public double getMaxTimeValue() {
        Double value = (Double) this.maxTimeSpinner.getValue();
        return value.doubleValue();
    }

    /**
     * <h3>Get edge distance</h3>
     * 
     * @return
     * Edge distance value.
     */
    public int getDistance() {
        Integer value = (Integer) this.distanceSpinner.getValue();
        return value.intValue();
    }
    
    /**
     * <h3>Set number of vertices</h3>
     * 
     * @param i - number of vertices to set.
     */
    public void setVertexCount(int i) {
        this.vertexCountSpinner.setValue(i);
    }

    /**
     * <h3>Set minimum number of edges</h3>
     * 
     * @param i - number of edges to set.
     */
    public void setMinEdgeCount(int i) {
        this.edgeMinCountSpinner.setValue(i);
    }
    
    /**
     * <h3>Set maximum number of edges</h3>
     * 
     * @param i - number of edges to set.
     */
    public void setMaxEdgeCount(int i) {
        this.edgeMaxCountSpinner.setValue(i);
    }

    /**
     * <h3>Set minimum cost value</h3>
     * 
     * @param d - value to set.
     */
    public void setMinCostValue(double d) {
        this.minCostSpinner.setValue(d);
    }

    /**
     * <h3>Set maximum cost value</h3>
     * 
     * @param d - value to set.
     */
    public void setMaxCostValue(double d) {
        this.maxCostSpinner.setValue(d);
    }

    /**
     * <h3>Set minimum time value</h3>
     * 
     * @param d - value to set.
     */
    public void setMinTimeValue(double d) {
        this.minTimeSpinner.setValue(d);
    }

    /**
     * <h3>Set maximum time value</h3>
     * 
     * @param d - value to set.
     */
    public void setMaxTimeValue(double d) {
        this.maxTimeSpinner.setValue(d);
    }

    /**
     * <h3>Set edge distance</h3>
     * 
     * @param i - distance to set.
     */
    public void setDistance(int i) {
        this.distanceSpinner.setValue(i);
    }

    /**
     * <h3>Get hamiltonian cycle type</h3>
     * 
     * @return
     * Type of hamiltonian cycle.
     */
    public String getHamilonianCycleType() {
        return (String)this.hamiltonianCycleCombo.getSelectedItem();
    }

    /**
     * <h3>Set hamiltonian cycle type</h3>
     * 
     * @param type - hamiltionian cycle type to set.
     */
    public void setHamilonianCycleType(String type) {
        this.hamiltonianCycleCombo.setSelectedItem(type);
    }
    
}
