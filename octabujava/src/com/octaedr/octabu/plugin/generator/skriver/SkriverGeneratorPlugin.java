/**
 * Created:     2005-11-13
 * Modified:    2005-11-20
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.generator.skriver;

import java.util.Random;

import javax.swing.JPanel;

import com.octaedr.octabu.app.plugin.IGeneratorPlugin;
import com.octaedr.octabu.exception.InvalidParamsException;
import com.octaedr.octabu.exception.OctabuException;
import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.graph.GraphInfo;
import com.octaedr.octabu.graph.graph.Vertex;
import com.octaedr.octgraph.exception.OctgraphException;

/**
 * <h3>Scriver graph generator</h3>
 * 
 * <p>Graph generator based on Scriver's NETMAKER idea.</p>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.2
 */
public class SkriverGeneratorPlugin implements IGeneratorPlugin {

    /** <h3>Minimum number of vertices</h3> */
    static final int VERTEX_COUNT_MIN       =   2;
    /** <h3>Maximum number of vertices</h3> */
    static final int VERTEX_COUNT_MAX       =   10000000;
    /** <h3>Default number of vertices</h3> */
    static final int VERTEX_COUNT_DEFAULT   =   250;
    /** <h3>Step when modyfying number of vertices</h3> */
    static final int VERTEX_COUNT_STEP      =   1;

    /** <h3>Minimum number of edges</h3> */
    static final int EDGE_COUNT_MIN             =   0;
    /** <h3>Maximum number of edges</h3> */
    static final int EDGE_COUNT_MAX             =   VERTEX_COUNT_MAX;
    /** <h3>Default start value of edges number range</h3> */
    static final int EDGE_COUNT_DEFAULT_MIN     =   10;
    /** <h3>Default end value of edges number range</h3> */
    static final int EDGE_COUNT_DEFAULT_MAX     =   20;
    /** <h3>Step when modyfying number of vertices range</h3> */
    static final int EDGE_COUNT_STEP      =   1;

    /** <h3>Minimum edge distance limit</h3> */
    static final int DISTANCE_MIN           =   1;
    /** <h3>Maximum edge distance limit</h3> */
    static final int DISTANCE_MAX           =   VERTEX_COUNT_MAX;
    /** <h3>Default edge distance limit</h3> */
    static final int DISTANCE_DEFAULT       =   15;
    /** <h3>Step when modyfying number of vertices</h3> */
    static final int DISTANCE_STEP          =   1;
    
    /** <h3>Miminum criterium value</h3> */
    static final double CRITERIUM_MIN           =   1.0;
    /** <h3>Maximum criterium value</h3> */
    static final double CRITERIUM_MAX           =   1000000.0;
    /** <h3>Step when modyfying criterium value range</h3> */
    static final double CRITERIUM_STEP          =   1.0;
    /** <h3>Default start value of criterium value range</h3> */
    static final double CRITERIUM_DEFAULT_MIN   =   1.0;
    /** <h3>Default end value of criterium value range</h3> */
    static final double CRITERIUM_DEFAULT_MAX   =   100.0;

    /** <h3>Hamiltonian cycle random type</h3> */
    static final String HAMILTONIAN_CYCLE_TYPE_RANDOM   =   "Random";
    /** <h3>Hamiltonian cycle loop type</h3> */
    static final String HAMILTONIAN_CYCLE_TYPE_LOOP     =   "Loop";
    

    /** <h3>Number of vertices to create</h3> */
    private int vertexCount     = VERTEX_COUNT_DEFAULT;
    /** <h3>Minimum number of edges from single vertex</h3> */
    private int minEdgeCount    = EDGE_COUNT_DEFAULT_MIN;
    /** <h3>Maximum number of edges from single vertex</h3> */
    private int maxEdgeCount    = EDGE_COUNT_DEFAULT_MAX;
    /** <h3>Distance range of edges</h3> */
    private int distance        = DISTANCE_DEFAULT;
    /** <h3>Minimum cost parameter value</h3> */
    private double minCostValue = CRITERIUM_DEFAULT_MIN;
    /** <h3>Maximum cost parameter value</h3> */
    private double maxCostValue = CRITERIUM_DEFAULT_MAX;
    /** <h3>Minimum time parameter value</h3> */
    private double minTimeValue = CRITERIUM_DEFAULT_MIN;
    /** <h3>Maximum time parameter value</h3> */
    private double maxTimeValue = CRITERIUM_DEFAULT_MAX;
    /** <h3>Hamiltonian cycle type</h3> */
    private String hamiltonianCycleType = HAMILTONIAN_CYCLE_TYPE_RANDOM;
    
    
    /** <h3>Gui options panel</h3> */
    private SkriverGeneratorPanel optionsPanel;

    /** <h3>Cost values range size</h3> */
    double costRange;
    /** <h3>Time values range size</h3> */
    double timeRange;

    /**
     * <h3>Constructor</h3>
     *
     * <p>Creates initialized random generator plugin.</p>
     */
    public SkriverGeneratorPlugin() {
        /* create gui options panel */
        this.optionsPanel = new SkriverGeneratorPanel();

        /* export initial values to gui */
        exportGuiValues();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getShortName()
     */
    public String getShortName() {
        return "plugin/generator/skriver";
    }


    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getName()
     */
    public String getName() {
        return "Skriver NETMAKER Graph Generator 1.0";
    }


    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getOptionsPanel()
     */
    public JPanel getOptionsPanel() {
        return this.optionsPanel;
    }


    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#checkOptions()
     */
    public boolean checkOptions() {
        importGuiValues();
        
        if(this.vertexCount < 2) {
            return false;
        }

        if(this.minEdgeCount > this.maxEdgeCount) {
            return false;
        }
        
        if(this.minEdgeCount < 0) {
            return false;
        }
        
        if(this.distance < 1) {
            return false;
        }
        
        if(this.distance > this.vertexCount) {
            return false;
        }

        if(this.minCostValue > this.maxCostValue) {
            return false;
        }
        if(this.minTimeValue > this.maxTimeValue) {
            return false;
        }

        if(this.minCostValue < 0.0) {
            return false;
        }
        if(this.minTimeValue < 0.0) {
            return false;
        }
        
        return true;
    }


    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#validateOptions()
     */
    public void validateOptions() {
        importGuiValues();

        if(this.vertexCount < 2) {
            this.vertexCount = 2;
        }

        if(this.minEdgeCount > this.maxEdgeCount) {
            int tmp = this.minEdgeCount;
            this.minEdgeCount = this.maxEdgeCount;
            this.maxEdgeCount = tmp;
        }
        
        if(this.minEdgeCount < 0) {
            this.minEdgeCount = 0;
        }
        
        if(this.maxEdgeCount < 0) {
            this.maxEdgeCount = 0;
        }

        if(this.minCostValue > this.maxCostValue) {
            double tmp = this.minCostValue;
            this.minCostValue = this.maxCostValue;
            this.maxCostValue = tmp;
        }
        if(this.minTimeValue > this.maxTimeValue) {
            double tmp = this.minTimeValue;
            this.minTimeValue = this.maxTimeValue;
            this.maxTimeValue = tmp;
        }

        if(this.minCostValue < 0.0) {
            this.minCostValue = 0.0;
        }
        if(this.minTimeValue < 0.0) {
            this.minTimeValue = 0.0;
        }
        if(this.maxCostValue < 0.0) {
            this.maxCostValue = 0.0;
        }
        if(this.maxTimeValue < 0.0) {
            this.maxTimeValue = 0.0;
        }
        
        exportGuiValues();
    }

    /**
     * <h3>Set edge criterium values</h3>
     * 
     * @param edge - edge for which values will be set.
     */
    private void setEdgeValues(Edge edge) {
        double time = Math.random() * 0.33 * this.timeRange;
        double cost = Math.random() * 0.33 * this.costRange;
        if(Math.random() < 0.5) {
            edge.setTime(this.minTimeValue + time);
            edge.setCost(this.minCostValue + cost + 0.66 * this.costRange);
        } else {
            edge.setTime(this.minTimeValue + time + 0.66 * this.timeRange);
            edge.setCost(this.minCostValue + cost);
        }
    }
    
    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IGeneratorPlugin#generate()
     */
    public Graph generate() throws OctabuException {
        try {
            Edge edge;

            /* check parameters */
            if(!checkOptions()) {
                throw new InvalidParamsException();
            }

            /* compute values ranges */
            this.costRange = this.maxCostValue - this.minCostValue;
            this.timeRange = this.maxTimeValue - this.minTimeValue;
            Random randomGenerator = new Random();

            Graph graph = new Graph();
            
            /* create vertices */
            for(int i = 1; i <= this.vertexCount; ++i) {
                Vertex vertex = new Vertex();
                vertex.setKey(i);
                vertex.addToGraph(graph);
            }
            
            /* create Hamiltonian cycle */
            if(this.hamiltonianCycleType == HAMILTONIAN_CYCLE_TYPE_RANDOM) {
                /* creating random Hamiltonian cycle */
                Vertex lastVertex = (Vertex)graph.findVertex(1);
                for(int i = 1; i < this.vertexCount; ) {
                    int next = 2 + randomGenerator.nextInt(this.vertexCount - 1);
                    Vertex nextVertex = (Vertex)graph.findVertex(next);
                    if((nextVertex == lastVertex) || 
                            (nextVertex.getOutDegree() > 0) ||
                            (nextVertex.getInDegree() > 0)) {
                        continue;
                    }
                    edge = new Edge();
                    edge.setVertices(lastVertex, nextVertex);
                    setEdgeValues(edge);
                    edge.addToGraph(graph);
                    
                    lastVertex = nextVertex;
                    ++i;
                }
                edge = new Edge();
                edge.setVertices(lastVertex, (Vertex)graph.findVertex(1));
                setEdgeValues(edge);
                edge.addToGraph(graph);
            } else {
                /* creating loop connecting all vertices */
                for(int i = 2; i <= this.vertexCount; ++i)
                {
                    edge = new Edge();
                    edge.setVertices(
                            (Vertex)graph.findVertex(i-1),
                            (Vertex)graph.findVertex(i));
                    setEdgeValues(edge);
                    edge.addToGraph(graph);
                }
                edge = new Edge();
                edge.setVertices(
                        (Vertex)graph.findVertex(this.vertexCount),
                        (Vertex)graph.findVertex(1));
                setEdgeValues(edge);
                edge.addToGraph(graph);
            }

            /* creating rest of vertices */
            int distHalf = this.distance / 2;
            int edgeCountRange = this.maxEdgeCount - this.minEdgeCount;
            for(int from = 1; from <= this.vertexCount; ++from) {
                Vertex fromVertex = (Vertex)graph.findVertex(from);
                /* number of edges for this vertex */
                int edges = this.minEdgeCount;
                if(edgeCountRange > 0) {
                    edges += randomGenerator.nextInt(edgeCountRange);
                }
                for(int e = 1; e <= edges; ++e) {
                    /* compute destination vertex */
                    int to = 1 + randomGenerator.nextInt(this.distance) - distHalf;
                    if(to <= 0) {
                        --to;
                    }
                    /* position relative to current source */
                    to += from;
                    /* wrap around */
                    if(to <= 0) {
                        to += this.vertexCount;
                    } else if (to > this.vertexCount) {
                        to -= this.vertexCount;
                    }
                    
                    edge = new Edge();
                    edge.setVertices(fromVertex, (Vertex)graph.findVertex(to));
                    setEdgeValues(edge);
                    edge.addToGraph(graph);                    
                }
            }

            /* create graph info */
            GraphInfo graphInfo = new GraphInfo();
            graphInfo.setGenerator(getName());
            graphInfo.addEntry("Vertices", this.vertexCount);
            graphInfo.addEntry("Min. edge count", this.minEdgeCount);
            graphInfo.addEntry("Max. edge count", this.maxEdgeCount);
            graphInfo.addEntry("Distance", this.distance);
            graphInfo.addEntry("Min. cost", this.minCostValue);
            graphInfo.addEntry("Max. cost", this.maxCostValue);
            graphInfo.addEntry("Cost range", this.costRange);
            graphInfo.addEntry("Min. time", this.minTimeValue);
            graphInfo.addEntry("Max. time", this.maxTimeValue);
            graphInfo.addEntry("Time range", this.timeRange);
            graph.setInfo(graphInfo);        
            
            return graph;            
        } catch(OctgraphException exception) {
            throw new OctabuException("Octgraph exception catched");
        }
    }
    
    /**
     * <h3>Import options values from gui</h3>
     */
    private void importGuiValues() {
        this.vertexCount = this.optionsPanel.getVertexCount();
        this.minEdgeCount = this.optionsPanel.getMinEdgeCount();
        this.maxEdgeCount = this.optionsPanel.getMaxEdgeCount();
        this.distance = this.optionsPanel.getDistance();
        this.minCostValue = this.optionsPanel.getMinCostValue();
        this.maxCostValue = this.optionsPanel.getMaxCostValue();
        this.minTimeValue = this.optionsPanel.getMinTimeValue();
        this.maxTimeValue = this.optionsPanel.getMaxTimeValue();
        this.hamiltonianCycleType = this.optionsPanel.getHamilonianCycleType();
    }
    
    /**
     * <h3>Export options values from gui</h3>
     */
    private void exportGuiValues() {
        this.optionsPanel.setVertexCount(this.vertexCount);
        this.optionsPanel.setMinEdgeCount(this.minEdgeCount);
        this.optionsPanel.setMaxEdgeCount(this.maxEdgeCount);
        this.optionsPanel.setDistance(this.distance);
        this.optionsPanel.setMinCostValue(this.minCostValue);
        this.optionsPanel.setMaxCostValue(this.maxCostValue);
        this.optionsPanel.setMinTimeValue(this.minTimeValue);
        this.optionsPanel.setMaxTimeValue(this.maxTimeValue);
        this.optionsPanel.setHamilonianCycleType(this.hamiltonianCycleType);
    }
}
