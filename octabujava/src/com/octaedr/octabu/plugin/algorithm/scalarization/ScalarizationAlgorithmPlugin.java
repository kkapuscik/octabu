/**
 * Created:     2005-11-13
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.scalarization;

import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;

import javax.swing.JPanel;

import com.octaedr.octabu.app.plugin.IAlgorithmPlugin;
import com.octaedr.octabu.graph.algorithm.AlgorithmResults;
import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.graph.Path;
import com.octaedr.octabu.graph.util.GraphDuplicator;
import com.octaedr.octabu.util.Logger;
import com.octaedr.octgraph.exception.BrokenPathException;
import com.octaedr.octgraph.exception.OctgraphException;

/**
 * <h3>Scalarization algorithm implementation class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class ScalarizationAlgorithmPlugin implements IAlgorithmPlugin {

    /** <h3>Default number of iterations</h3> */
    public static final int ITERATIONS_COUNT_DEFAULT = 11;
    /** <h3>Minimum number of iterations</h3> */
    public static final int ITERATIONS_COUNT_MIN = 1;
    /** <h3>Maximum number of iterations</h3> */
    public static final int ITERATIONS_COUNT_MAX = 10001;
    /** <h3>Change step for number of iterations</h3> */
    public static final int ITERATIONS_COUNT_STEP = 1;

    /** <h3>Gui options panel</h3> */
    private ScalarizationPanel optionsPanel;

    /** <h3>Number of iterations</h3> */
    private int iterationsCount = ITERATIONS_COUNT_DEFAULT;

    
    /**
     * <h3>Comparator for vertex priority queue</h3>
     * 
     * @author Krzysztof Kapuscik
     * @version 1.0
     */
    private class VertexComparator implements Comparator<ScalarizationVertex> {

        /**
         * <h3>Compare given vertices</h3>
         * 
         * @param o1 - first vertex.
         * @param o2 - second vertex.
         * 
         * @return
         * Value &lt 0 if o1 &lt o2, value = 0 if o1 = o2 or value &gt; 0 if o1 &gt; o2.
         */
        public int compare(ScalarizationVertex o1, ScalarizationVertex o2) {
            if(o1.getBestDistance() < o2.getBestDistance()) {
                return -1;
            } else if(o1.getBestDistance() > o2.getBestDistance()) {
                return 1;
            } else {
                return 0;
            }
        }
        
    }
    
    /**
     * <h3>Constructor</h3>
     */
    public ScalarizationAlgorithmPlugin() {
        this.optionsPanel = new ScalarizationPanel();
    }
    
    /**
     * <h3>Recursive produce results paths</h3>
     * 
     * @param path - current path (extended during recursion).
     * @param currentVertex - current vertex.
     * @param source - source vertex key.
     * @param results - algorithm results object.
     * 
     * @throws BrokenPathException
     * (should not happen).
     */
    private void producePaths(Path path, ScalarizationVertex currentVertex, int source, AlgorithmResults results) throws BrokenPathException {
        if(currentVertex.getIntKey() == source) {
            results.addPath(new Path(path));
        } else {
            Iterator edgeIterator;

            edgeIterator = currentVertex.getFromEdges();

            while(edgeIterator.hasNext()) {
                Edge edge = (Edge)edgeIterator.next();
                path.addFirst(edge);                
                producePaths(path, (ScalarizationVertex)edge.getSource(), source, results);
                path.removeFirst();                
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.algorithm.IAlgorithm#prepareGraph(com.octaedr.octabu.graph.graph.Graph)
     */
    public Graph prepareGraph(Graph graph) {
        try {
            /* duplicate graph */
            return GraphDuplicator.duplicate(
                    graph, new ScalarizationElementProvider());
        } catch (OctgraphException exception) {
            return null;
        }
    }

    public boolean dijkstra(Graph graph, int source, int target,
            double timeWeight, double costWeight) {
        try {
            Iterator vertexIterator;
            Iterator edgeIterator;
            PriorityQueue<ScalarizationVertex> queue;
    
            ScalarizationVertex sourceVertex = (ScalarizationVertex)graph.findVertex(source);

            /* vertex initialization */
            vertexIterator = graph.getAllVertices();
            while(vertexIterator.hasNext()) {
                ScalarizationVertex vertex = (ScalarizationVertex)vertexIterator.next();   
                vertex.dijkstraInit();
                if(vertex.getIntKey() == source) {
                    vertex.setBestDistance(0.0);
                }
            }

            /* processing time criterium */
            queue = new PriorityQueue<ScalarizationVertex>(
                    graph.getAllVerticesCount(), new VertexComparator());
            queue.add(sourceVertex);
            for(;;) {
                ScalarizationVertex currentVertex = null;
    
                /* find not processed vertex with lowest time (and cost) */
                currentVertex = queue.poll();
    
                if((currentVertex == null) ||
                        (currentVertex.getBestDistance() == java.lang.Double.MAX_VALUE) ||
                        (currentVertex.getIntKey() == target)) {
                    break;
                }
                            
                /* process all edges from current vertex */
                double currentDistance = currentVertex.getBestDistance();
                
                edgeIterator = currentVertex.getOutEdges();
                while(edgeIterator.hasNext()) {
                    Edge edge = (Edge)edgeIterator.next();
    
                    ScalarizationVertex targetVertex = (ScalarizationVertex)edge.getTarget();
                    if(!targetVertex.getProcessed()) {
                        double edgeDistance = 
                            edge.getTime() * timeWeight +
                            edge.getCost() * costWeight;
                        
                        double newDistance = currentDistance + edgeDistance;

                        boolean changeFlag = targetVertex.checkRelax(newDistance);

                        if(changeFlag) {
                            queue.remove(targetVertex);
                        }
                        targetVertex.relax(newDistance, edge);
                        if(changeFlag) {
                            queue.add(targetVertex);
                        }
                    }
                }
                
                currentVertex.setProcessed();
            }
            return true;
        } catch (OctgraphException exception) {
            exception.printStackTrace();
            return false;
        }
    }
    
    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.algorithm.IAlgorithm#process(com.octaedr.octabu.graph.algorithm.AlgorithmResults, com.octaedr.octabu.graph.graph.Graph, int, int)
     */
    public void process(AlgorithmResults results, Graph graph, int source, int target) {
        double step;

        /* check parameters */
        if(!checkOptions()) {
            results.setFail();
            return;
        }

        if(dijkstra(graph, source, target, 0.0, 1.0)) {
            prepareDijkstraResults(results, graph, source, target);
        } else {
            Logger.log("[Scalarization] Dijkstra failed for 0.0, 1.0");
            results.setFail();
            return;
        }

        if(dijkstra(graph, source, target, 1.0, 0.0)) {
            prepareDijkstraResults(results, graph, source, target);
        } else {
            Logger.log("[Scalarization] Dijkstra failed for 1.0, 0.0");
            results.setFail();
            return;
        }
        
        if(this.iterationsCount <= 2) {
            return;
        }

        step = 1.0 / (this.iterationsCount - 1);
        for(int i = 1; i <= this.iterationsCount-2; ++i) {
            double timeWeight = step * i;
            double costWeight = 1.0 - timeWeight;
            if(dijkstra(graph, source, target, timeWeight, costWeight)) {
                prepareDijkstraResults(results, graph, source, target);
            } else {
                Logger.log("[Scalarization] Dijkstra failed for " +
                        timeWeight + ", " + costWeight);
                results.setFail();
                return;
            }
        }
        
    }

    private void prepareDijkstraResults(AlgorithmResults results, Graph graph, int source, int target) {
        try {
            ScalarizationVertex targetVertex;
            targetVertex = (ScalarizationVertex)graph.findVertex(target);
            producePaths(new Path(), targetVertex, source, results);
        } catch (OctgraphException exception) {
            exception.printStackTrace();
            results.setFail();
        }
    }
    
    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.algorithm.IAlgorithm#prepareResults(com.octaedr.octabu.graph.algorithm.AlgorithmResults, com.octaedr.octabu.graph.graph.Graph, int, int)
     */
    public void prepareResults(AlgorithmResults results, Graph graph, int source, int target) {
        results.removeDominatedPaths();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getShortName()
     */
    public String getShortName() {
        return "plugin/algorithm/scalarization";
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getName()
     */
    public String getName() {
        return "Scalarization Algorithm 1.0";
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getOptionsPanel()
     */
    public JPanel getOptionsPanel() {
        return this.optionsPanel;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#checkOptions()
     */
    public boolean checkOptions() {
        importGuiValues();
        return true;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#validateOptions()
     */
    public void validateOptions() {
        importGuiValues();
        exportGuiValues();
    }

    /**
     * <h3>Import options values from gui</h3>
     */
    private void importGuiValues() {
        this.iterationsCount = this.optionsPanel.getIterationsCount();
    }

    /**
     * <h3>Export options values to gui</h3>
     */
    private void exportGuiValues() {
        this.optionsPanel.setIterationsCount(this.iterationsCount);
    }

}
