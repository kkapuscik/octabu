/**
 * Created:     2005-11-13
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.scalarization;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * <h3>Options panel class for scalarization algorithm plugin</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
class ScalarizationPanel extends JPanel {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = -3531425503290696307L;
    
    /** <h3>Number of iteration spinner control</h3> */
    private JSpinner iterationsSpinner;

    
    /**
     * <h3>Constructor</h3>
     */
    public ScalarizationPanel() {
        super(new BorderLayout());
        
        this.iterationsSpinner = new JSpinner(
                new SpinnerNumberModel(
                        ScalarizationAlgorithmPlugin.ITERATIONS_COUNT_DEFAULT,
                        ScalarizationAlgorithmPlugin.ITERATIONS_COUNT_MIN,
                        ScalarizationAlgorithmPlugin.ITERATIONS_COUNT_MAX,
                        ScalarizationAlgorithmPlugin.ITERATIONS_COUNT_STEP));
        
        JPanel internalPanel = new JPanel(new GridLayout(1, 2, 3, 3));
        internalPanel.add(new JLabel("Iterations:"));
        internalPanel.add(this.iterationsSpinner);
 
        JPanel subPanel = new JPanel(new BorderLayout());
        subPanel.add(internalPanel, BorderLayout.NORTH);
        add(subPanel, BorderLayout.WEST);        
    }
    
    
    
    /**
     * <h3>Get number of iterations</h3>
     * 
     * @return
     * Number of iterations.
     */
    public int getIterationsCount() {
        Integer value = (Integer) this.iterationsSpinner.getValue();
        return value.intValue();
    }

    /**
     * <h3>Set number of iterations</h3>
     * 
     * @param i - number of iterations to set.
     */
    public void setIterationsCount(int i) {
        this.iterationsSpinner.setValue(i);
    }
    
}
