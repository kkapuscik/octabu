/**
 * Created:     2005-11-16
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.scalarization;

import java.util.HashSet;
import java.util.Iterator;

import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Vertex;

/**
 * <h3>Vertex class extended for scalarization algorithm</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class ScalarizationVertex extends Vertex {

    private double bestDistance;
    private boolean processed;
    private HashSet<Edge> fromEdgeSet = new HashSet<Edge>();

    /**
     * <h3>Initialize vertex for Dijkstra algorithm</h3>
     */
    public void dijkstraInit() {
        setBestDistance(Double.MAX_VALUE);
        this.processed = false;
        this.fromEdgeSet.clear();
    }
    
    /**
     * <h3>Check if distance changes when relaxation is done</h3>
     * 
     * @param distance - distance value.
     * 
     * @return
     * True if distance changes, false otherwise.
     */
    public boolean checkRelax(double distance) {
        if(distance < this.bestDistance) {
            return true;
        }
        return false;
    }


    /**
     * <h3>Relax vertex</h3>
     * 
     * @param distance - distance value.
     * @param edge - incoming edge forcing the relaxation.
     */
    public void relax(double distance, Edge edge) {
        if(distance == this.bestDistance) {
            this.fromEdgeSet.add(edge);
        }

        if(distance < this.bestDistance) {
            setBestDistance(distance);
            this.fromEdgeSet.clear();
            this.fromEdgeSet.add(edge);
        }
    }

    public void setProcessed() {
        this.processed = true;
    }

    public boolean getProcessed() {
        return this.processed;
    }

    public double getBestDistance() {
        return this.bestDistance;
    }
    
    public void setBestDistance(double distance) {
        this.bestDistance = distance;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.graph.graph.Vertex#toString()
     */
    public String toString() {
        return super.toString() + "; (best distance=" + this.bestDistance + ")";
    }
    
    Iterator getFromEdges() {
        return this.fromEdgeSet.iterator();
    }

}
