/**
 * Created:     2005-11-10
 * Modified:    2005-11-17
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.brumbaugh;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

import com.octaedr.octabu.graph.graph.Vertex;

/**
 * <h3>Brumbaugh-Smith & Shier algorithm extended vertex class</h3>
 * 
 * @author Krzysztof Kapusik
 * @version 1.0
 */
public class LabelVertex extends Vertex {

    /** <h3>List of vertex labels</h3> */
    private LinkedList<Label> labelList =
            new LinkedList<Label>();

    /** <h3>Vertex is in queue</h3> */
    private boolean inQueue;
    
    /**
     * <h3>Initialize vertex for Dijkstra algorithm</h3>
     * 
     * @param isSource - algorithm source vertex flag.
     */
    public void init(boolean isSource) {
        this.labelList.clear();
        if(isSource) {
            merge(new Label(0, 0, null, null));
        }
        this.inQueue = false;
    }
    
    /**
     * <h3>Replace labels list</h3>
     * 
     * @param newLabelList - new label list.
     */
    public void setLabels(LinkedList<Label> newLabelList) {
        this.labelList = newLabelList;
    }

    /**
     * <h3>Set that vertex was added to queue</h3>
     */
    public void setAddedToQueue() {
        this.inQueue = true;
    }
    
    /**
     * <h3>Set that vertex was removed from queue</h3>
     */
    public void setRemovedFromQueue() {
        this.inQueue = false;
    }
    
    /**
     * <h3>Check if element is in queue</h3>
     * 
     * @return
     * True if element is in queue, false otherwise.
     */
    public boolean isInQueue() {
        return this.inQueue;
    }
    
    /**
     * <h3>Merge label to vertex labels</h3>
     * 
     * @param label - label to merge.
     * 
     * @return
     * True if label has been added to label list, false otherwise.
     */
    public boolean merge(Label label) {
        ListIterator<Label> iterator;
        
        /* if list is empty - just add */
        if(this.labelList.size() == 0) {
            this.labelList.add(label);
            return true;
        }
        
        // TODO: Optimize - merge both below operations
        
        /* remove dominated labels */
        iterator = this.labelList.listIterator();
        while(iterator.hasNext()) {
            Label current = iterator.next();
            
            if(label.isSame(current)) {
                return false;
            }
            if(label.isDominated(current)) {
                return false;
            }
            if(current.isDominated(label)) {
                iterator.remove();
            }
        }

        /* add to list */
        iterator = this.labelList.listIterator();
        while(iterator.hasNext()) {
            Label current = iterator.next();
            if(current.getTime() >= label.getTime()) {
                iterator.previous();
                iterator.add(label);
                return true;
            }
        }
        
        this.labelList.addLast(label);

        return true;
    }
    
    
    
    /* (non-Javadoc)
     * @see com.octaedr.octabu.graph.graph.Vertex#toString()
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        ListIterator<Label> iterator = this.labelList.listIterator();

        builder.append(super.toString());
        while(iterator.hasNext()) {
            Label label = iterator.next();
            builder.append(label.toString());
        }
        
        return builder.toString();
    }

    /**
     * <h3>Get all vertex labels</h3>
     * 
     * @return
     * Iterator over vertex labels collection.
     */
    public Iterator<Label> getAllLabels() {
        return this.labelList.iterator();
    }

}
