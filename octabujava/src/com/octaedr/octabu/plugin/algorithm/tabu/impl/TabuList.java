/**
 * Created:   2005-11-23
 * Modified:  2005-11-23
 * Author:    Krzysztof Kapuscik
 * Copyright: (C) 2005 Krzysztof Kapuscik
 * Contact:   saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.tabu.impl;

import java.util.Iterator;
import java.util.PriorityQueue;

/**
 * <h3>Solution tabu list class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class TabuList<T> {

    /**
     * <h3>Tabu list entry class</h3>
     * 
     * @author Krzysztof Kapuscik
     * @version 1.0
     */
    private class Entry implements Comparable<Entry> {
        /** <h3>Stored element</h3> */
        private T tabuElement;
        /** <h3>End time of tabu</h3> */
        private int endTime;

        /**
         * <h3>Constructor</h3>
         * 
         * @param element - element to store.
         * @param end - end time of tabu.
         */
        public Entry(T element, int end) {
            this.tabuElement = element;
            this.endTime = end;
        }
        
        /**
         * <h3>Get tabu end time</h3>
         * 
         * @return
         * Tabu end time.
         */
        public int getEndTime() {
            return this.endTime;
        }

        /**
         * <h3>Get stored element</h3>
         * 
         * @return
         * Stored move.
         */
        public T getTabuElement() {
            return this.tabuElement;
        }

        /* (non-Javadoc)
         * @see java.lang.Comparable#compareTo(T)
         */
        public int compareTo(Entry other) {
            return this.endTime - other.endTime;
        }
    }

    /** <h3>Current 'time'</h3> */
    private int currentTime = 0;
    /** <h3>Priority queue sorting elements by end time</h3> */
    private PriorityQueue<Entry> tabuElements
            = new PriorityQueue<Entry>();


    /**
     * <h3>Check if element is tabu</h3>
     * 
     * @param element - element to check.
     * 
     * @return
     * True if element is marked as tabu, false otherwise.
     */
    public boolean isTabu(T element) {
        Iterator<Entry> entryIterator = this.tabuElements.iterator();
        while(entryIterator.hasNext()) {
            if(entryIterator.next().getTabuElement().equals(element)) {
                return true;
            }
        }
        return false;
    }

    /**
     * <h3>Add element to list</h3>
     * 
     * @param element - element to add.
     * @param tenure - tabu tenure time.
     */
    public void addMove(T element, int tenure) {
        Entry newEntry = new Entry(element, this.currentTime + tenure + 1);
        this.tabuElements.add(newEntry);
    }

    /**
     * <h3>Update 'time'</h3>
     * 
     * <p>This function increments internal 'time' counter and removes
     * all elements for which tabu tenure has elapsed.
     */
    public void updateTime() {
        this.currentTime++;
        
        for(;;) {
            Entry entry = this.tabuElements.peek();
            if(entry.getEndTime() == this.currentTime) {
                this.tabuElements.poll();
            } else {
                break;
            }
        }
    }
    
}
