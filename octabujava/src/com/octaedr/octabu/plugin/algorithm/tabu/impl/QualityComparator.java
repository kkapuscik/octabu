/**
 * Created:   2005-11-22
 * Modified:  2005-11-22
 * Author:    Krzysztof Kapuscik
 * Copyright: (C) 2005 Krzysztof Kapuscik
 * Contact:   saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.tabu.impl;

import java.util.Comparator;

import com.octaedr.octabu.exception.OctabuError;
import com.octaedr.octabu.plugin.algorithm.tabu.TabuAlgorithmPlugin;

/**
 * <h3>Data comparator</h3>
 * 
 * <p>This function is used to compare results of
 * same or different types (currently Move and Solution
 * is supported). If does the comparision using quality
 * function. There is also possibility to get quality
 * function value for given result.
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class QualityComparator implements Comparator {

    /** <h3>Length of 'best match' line</h3> */
    private double length;
    /** <h3>Time values range length</h3> */
    private double timeDiff;
    /** <h3>Cost values range length</h3> */
    private double costDiff;
    /** <h3>Search info structure</h3> */
    private SearchInfo searchInfo;
    /** <h3>Comparator owner</h3> */
    private TabuAlgorithmPlugin owner;

    /**
     * <h3>Constructor</h3>
     * 
     * @param ownerPlugin - comparator owner.
     * @param info - search info project to initialize comparator.
     */
    public QualityComparator(TabuAlgorithmPlugin ownerPlugin, SearchInfo info) {
        this.owner = ownerPlugin;
        this.searchInfo = info;
        this.timeDiff = info.getMaxTime() - info.getMinTime();
        this.costDiff = info.getMaxCost() - info.getMinCost();
        this.length = Math.sqrt(
                this.timeDiff * this.timeDiff + this.costDiff * this.costDiff);
    }

    /**
     * <h3>Get 'match' function value for given results</h3>
     * 
     * @param time - result time.
     * @param cost - result cost.
     * 
     * @return
     * 'Match' function value.
     */
    public double value(double time, double cost) {
        double value = Math.pow(this.timeDiff * time, 1)
                + Math.pow(this.costDiff * cost, 1);
        
        double td = time - this.searchInfo.getMinTime();
        double cd = cost - this.searchInfo.getMinCost();
        double len = Math.sqrt(td * td + cd * cd);
        
        double dotProduct = td * this.timeDiff + cd * this.costDiff;
        double lenProduct = this.length * len;
        
        return value; // * (2.0 - dotProduct / lenProduct);
        
/*
        double td = time - this.searchInfo.getMinTime();
        double cd = cost - this.searchInfo.getMinCost();
        double len = Math.sqrt(td * td + cd * cd);
        
        double dotProduct = td * this.timeDiff + cd * this.costDiff;
        double lenProduct = this.length * len;
        
        return  Math.pow(this.owner.angleWeight * (1.0 - dotProduct / lenProduct),
                        this.owner.anglePower)
                + Math.pow(this.owner.distanceWeight * len / this.length,
                        this.owner.distancePower);
*/
    }

    /* (non-Javadoc)
     * @see Comparator#compare(T, T)
     */
    public int compare(Object o1, Object o2) {
        double v1;
        double v2;
        
        if(o1 instanceof Move) {
            Move m1 = (Move)o1;
            v1 = value(m1.getTime(), m1.getCost());
        } else if(o1 instanceof Solution) {
            Solution s1 = (Solution)o1;
            v1 = value(s1.getTime(), s1.getCost());
        } else {
            throw new OctabuError();
        }

        if(o2 instanceof Move) {
            Move m2 = (Move)o2;
            v2 = value(m2.getTime(), m2.getCost());
        } else if(o2 instanceof Solution) {
            Solution s2 = (Solution)o2;
            v2 = value(s2.getTime(), s2.getCost());
        } else {
            throw new OctabuError();
        }
        if(Math.abs(v1 - v2) < 0.0001) {
            return 0;
        }
        
        if(v1 < v2) {
            return -1;
        } else if(v1 > v2) {
            return 1;
        }
        
        return 0;
    }

}
