/**
 * Created:   2005-11-20
 * Modified:  2005-11-20
 * Author:    Krzysztof Kapuscik
 * Copyright: (C) 2005 Krzysztof Kapuscik
 * Contact:   saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.tabu.impl;

import java.util.TreeSet;


/**
 * <h3>Tree based set class for storing moves</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class MoveSet extends TreeSet<Move> {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = -382131434667174768L;
    
    /**
     * <h3>Constructor</h3>
     * 
     * @param comparator - comparator to use.
     */
    public MoveSet(QualityComparator comparator) {
        super(comparator);
    }
    
}
