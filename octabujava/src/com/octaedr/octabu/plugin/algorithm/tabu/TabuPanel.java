/**
 * Created:     2005-11-13
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.tabu;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * <h3>Options panel class for scalarization algorithm plugin</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
class TabuPanel extends JPanel {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = -3531425503290696307L;
    
    /** <h3>Number of iteration spinner control</h3> */
    private JSpinner iterationsSpinner;
    private JSpinner angleWeightSpinner;
    private JSpinner anglePowerSpinner;
    private JSpinner distanceWeightSpinner;
    private JSpinner distancePowerSpinner;
    private JSpinner failRunLenghtSpinner;
    private JSpinner depthSpinner;

    
    /**
     * <h3>Constructor</h3>
     */
    public TabuPanel() {
        super(new BorderLayout());
        
        this.iterationsSpinner = new JSpinner(
                new SpinnerNumberModel(
                        TabuAlgorithmPlugin.ITERATIONS_COUNT_DEFAULT,
                        TabuAlgorithmPlugin.ITERATIONS_COUNT_MIN,
                        TabuAlgorithmPlugin.ITERATIONS_COUNT_MAX,
                        TabuAlgorithmPlugin.ITERATIONS_COUNT_STEP));

        this.angleWeightSpinner = new JSpinner(
                new SpinnerNumberModel(
                        TabuAlgorithmPlugin.ANGLE_WEIGHT_DEFAULT,
                        TabuAlgorithmPlugin.ANGLE_WEIGHT_MIN,
                        TabuAlgorithmPlugin.ANGLE_WEIGHT_MAX,
                        TabuAlgorithmPlugin.ANGLE_WEIGHT_STEP));

        this.anglePowerSpinner = new JSpinner(
                new SpinnerNumberModel(
                        TabuAlgorithmPlugin.ANGLE_POWER_DEFAULT,
                        TabuAlgorithmPlugin.ANGLE_POWER_MIN,
                        TabuAlgorithmPlugin.ANGLE_POWER_MAX,
                        TabuAlgorithmPlugin.ANGLE_POWER_STEP));

        this.distanceWeightSpinner = new JSpinner(
                new SpinnerNumberModel(
                        TabuAlgorithmPlugin.DISTANCE_WEIGHT_DEFAULT,
                        TabuAlgorithmPlugin.DISTANCE_WEIGHT_MIN,
                        TabuAlgorithmPlugin.DISTANCE_WEIGHT_MAX,
                        TabuAlgorithmPlugin.DISTANCE_WEIGHT_STEP));

        this.distancePowerSpinner = new JSpinner(
                new SpinnerNumberModel(
                        TabuAlgorithmPlugin.DISTANCE_POWER_DEFAULT,
                        TabuAlgorithmPlugin.DISTANCE_POWER_MIN,
                        TabuAlgorithmPlugin.DISTANCE_POWER_MAX,
                        TabuAlgorithmPlugin.DISTANCE_POWER_STEP));

        this.failRunLenghtSpinner = new JSpinner(
                new SpinnerNumberModel(
                        TabuAlgorithmPlugin.FAIL_RUN_LENGTH_DEFAULT,
                        TabuAlgorithmPlugin.FAIL_RUN_LENGTH_MIN,
                        TabuAlgorithmPlugin.FAIL_RUN_LENGTH_MAX,
                        TabuAlgorithmPlugin.FAIL_RUN_LENGTH_STEP));

        this.depthSpinner = new JSpinner(
                new SpinnerNumberModel(
                        TabuAlgorithmPlugin.DEPTH_DEFAULT,
                        TabuAlgorithmPlugin.DEPTH_MIN,
                        TabuAlgorithmPlugin.DEPTH_MAX,
                        TabuAlgorithmPlugin.DEPTH_STEP));

        JPanel internalPanel = new JPanel(new GridLayout(7, 2, 3, 3));
        internalPanel.add(new JLabel("Iterations:"));
        internalPanel.add(this.iterationsSpinner);
        internalPanel.add(new JLabel("Fail run length:"));
        internalPanel.add(this.failRunLenghtSpinner);
        internalPanel.add(new JLabel("Max. depth:"));
        internalPanel.add(this.depthSpinner);
        internalPanel.add(new JLabel("Angle weight:"));
        internalPanel.add(this.angleWeightSpinner);
        internalPanel.add(new JLabel("Angle power:"));
        internalPanel.add(this.anglePowerSpinner);
        internalPanel.add(new JLabel("Distance weight:"));
        internalPanel.add(this.distanceWeightSpinner);
        internalPanel.add(new JLabel("Distance power:"));
        internalPanel.add(this.distancePowerSpinner);

        JPanel subPanel = new JPanel(new BorderLayout());
        subPanel.add(internalPanel, BorderLayout.NORTH);
        add(subPanel, BorderLayout.WEST);        
    }
    
    
    
    /**
     * <h3>Get number of iterations</h3>
     * 
     * @return
     * Number of iterations.
     */
    public int getIterationsCount() {
        Integer value = (Integer) this.iterationsSpinner.getValue();
        return value.intValue();
    }

    public int getFailRunLength() {
        Integer value = (Integer) this.failRunLenghtSpinner.getValue();
        return value.intValue();
    }

    public int getDepth() {
        Integer value = (Integer) this.depthSpinner.getValue();
        return value.intValue();
    }

    /**
     * @return
     */
    public double getAngleWeight() {
        Double value = (Double) this.angleWeightSpinner.getValue();
        return value.doubleValue();
    }
    
    /**
     * @return
     */
    public double getAnglePower() {
        Double value = (Double) this.anglePowerSpinner.getValue();
        return value.doubleValue();
    }

    /**
     * @return
     */
    public double getDistanceWeight() {
        Double value = (Double) this.distanceWeightSpinner.getValue();
        return value.doubleValue();
    }

    /**
     * @return
     */
    public double getDistancePower() {
        Double value = (Double) this.distancePowerSpinner.getValue();
        return value.doubleValue();
    }

    /**
     * <h3>Set number of iterations</h3>
     * 
     * @param i - number of iterations to set.
     */
    public void setIterationsCount(int i) {
        this.iterationsSpinner.setValue(i);
    }

    public void setFailRunLength(int i) {
        this.failRunLenghtSpinner.setValue(i);
    }

    public void setDepth(int i) {
        this.depthSpinner.setValue(i);
    }

    public void setAngleWeight(double d) {
        this.angleWeightSpinner.setValue(d);
    }

    public void setAnglePower(double d) {
        this.anglePowerSpinner.setValue(d);
    }

    public void setDistanceWeight(double d) {
        this.distanceWeightSpinner.setValue(d);
    }

    public void setDistancePower(double d) {
        this.distancePowerSpinner.setValue(d);
    }

}
