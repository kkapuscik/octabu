/**
 * Created:   2005-11-20
 * Modified:  2005-11-20
 * Author:    Krzysztof Kapuscik
 * Copyright: (C) 2005 Krzysztof Kapuscik
 * Contact:   saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.tabu.impl;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;


/**
 * <h3>Set of non-dominated solutions class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class SolutionSet {

    /** <h3>List of solutions</h3> */
    private LinkedList<Solution> solutionsList = new LinkedList<Solution>();

    /**
     * <h3>Get all solutions</h3>
     * 
     * @return
     * Iterator over solutions collection.
     */
    public Iterator<Solution> getAllSolutions() {
        return this.solutionsList.iterator();
    }
    
    /**
     * <h3>Update solution set</h3>
     * 
     * @param solution - solution to add to set (if possible).
     */
    public boolean update(Solution solution) {
        
        ListIterator<Solution> iterator;

        /* if set is empty - just add */
        if(this.solutionsList.isEmpty()) {
            this.solutionsList.add(solution);
            return true;
        }
        
        /* check if given solution is not dominated, remove dominated */
        iterator = this.solutionsList.listIterator();
        while(iterator.hasNext()) {
            Solution currentSolution = iterator.next();
            if(currentSolution.dominate(solution)) {
                return false;
            }
            if(currentSolution.sameAs(solution)) {
                return false;
            }
            if(solution.dominate(currentSolution)) {
                iterator.remove();
            }
        }

        /* add solution in order */
        iterator = this.solutionsList.listIterator();
        while(iterator.hasNext()) {
            Solution currentSolution = iterator.next();
            if(currentSolution.getTime() > solution.getTime()) {
                iterator.previous();
                iterator.add(solution);
                break;
            }
        }

        this.solutionsList.addLast(solution);
        
        return true;
    }

}
