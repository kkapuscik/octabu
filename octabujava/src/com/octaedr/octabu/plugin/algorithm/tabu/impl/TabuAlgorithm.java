/**
 * Created:   2005-11-23
 * Modified:  2005-11-23
 * Author:    Krzysztof Kapuscik
 * Copyright: (C) 2005 Krzysztof Kapuscik
 * Contact:   saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.tabu.impl;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.plugin.algorithm.tabu.TabuAlgorithmPlugin;
import com.octaedr.octabu.util.Logger;
import com.octaedr.octgraph.exception.NotInGraphException;

/**
 * <h3>Tabu search algorithm implementation utility class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class TabuAlgorithm {

    private TabuAlgorithmPlugin algorithmOwner;
    
    public TabuAlgorithm(TabuAlgorithmPlugin owner) {
        this.algorithmOwner = owner;
    }
    
    private MoveSet prepareMoveSet(Solution solution, QualityComparator moveComparator) throws NotInGraphException {
        ListIterator edgeIterator;
        Edge currentEdge;
        Edge previousEdge;
        TabuVertex sourceVertex;
        TabuVertex targetVertex;
        MoveSet movesSet = new MoveSet(moveComparator);
        
        
        /* remove vertex moves */
        edgeIterator = solution.getEdges();
        currentEdge = null;
        while(edgeIterator.hasNext()) {
            previousEdge = currentEdge;
            currentEdge = (Edge)edgeIterator.next();

            if(previousEdge == null) {
                continue;
            }

            sourceVertex = (TabuVertex)previousEdge.getSource();
            targetVertex = (TabuVertex)currentEdge.getTarget();
                        
            
            Iterator outEdgeIterator = sourceVertex.getOutEdges(targetVertex);
            while(outEdgeIterator.hasNext()) {
                Edge outEdge = (Edge)outEdgeIterator.next();
                if(outEdge == currentEdge) {
                    continue;
                }

                Move newMove = new Move(
                        solution,
                        Move.TYPE_REMOVE_VERTEX,
                        previousEdge, currentEdge, outEdge);
                movesSet.add(newMove);
                // Logger.logln("[Tabu] Move - Remove vertex: " + newMove);
            }
            
        }

        
        edgeIterator = solution.getEdges();
        while(edgeIterator.hasNext()) {
            Iterator outEdgeIterator;
            
            currentEdge = (Edge)edgeIterator.next();
            sourceVertex = (TabuVertex)currentEdge.getSource();
            targetVertex = (TabuVertex)currentEdge.getTarget();

            /* change edge move */
            outEdgeIterator = sourceVertex.getOutEdges(targetVertex);
            while(outEdgeIterator.hasNext()) {
                Edge outEdge = (Edge)outEdgeIterator.next();
                if(outEdge == currentEdge) {
                    continue;
                }

                Move newMove = new Move(
                        solution,
                        Move.TYPE_CHANGE_EDGE,
                        currentEdge, outEdge);
                movesSet.add(newMove);
                // Logger.logln("[Tabu] Move - Change edge: " + newMove);
            }

            /* add vertex move */
            outEdgeIterator = sourceVertex.getOutEdges();
            while(outEdgeIterator.hasNext()) {
                Edge outEdge = (Edge)outEdgeIterator.next();
                TabuVertex middleVertex = (TabuVertex)outEdge.getTarget();
                
                if(middleVertex != targetVertex) {
                    Iterator middleEdgeIterator =
                        middleVertex.getOutEdges(targetVertex);
                    
                    while(middleEdgeIterator.hasNext()) {
                        Edge endEdge = (Edge)middleEdgeIterator.next();

                        Move newMove = new Move(
                                solution,
                                Move.TYPE_ADD_VERTEX,
                                currentEdge, outEdge, endEdge);
                        movesSet.add(newMove);
                        // Logger.logln("[Tabu] Move - Change edge: " + newMove);
                    }
                }
            }
        }

        return movesSet;
    }


    private boolean isTabuMove(LinkedList<Move> tabuList, Move move) {
        Iterator<Move> iterator;
        
        iterator = tabuList.iterator();
        while(iterator.hasNext()) {
            Move currentMove = iterator.next();
            
            if(currentMove.equals(move)) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isTabuSolution(LinkedList<Solution> tabuList, Solution solution) {
        Iterator<Solution> iterator;
        
        iterator = tabuList.iterator();
        while(iterator.hasNext()) {
            Solution currentSolution = iterator.next();
            
            if(currentSolution.sameAs(solution)) {
                return true;
            }
        }
        return false;
    }

    private void addToTabu(LinkedList<Move> tabuList, int listSize, Move move) {
        tabuList.addLast(move);
        tabuList.addLast(move.createReversedMove());

        while(tabuList.size() >= listSize) {
            tabuList.removeFirst();
        }
    }
    
    private void addToTabu(LinkedList<Solution> tabuList, int listSize, Solution solution) {
        tabuList.addLast(solution);

        while(tabuList.size() >= listSize) {
            tabuList.removeFirst();
        }
    }
    
    public Solution tabuSearch(SearchInfo searchInfo) throws NotInGraphException {
        Solution bestSolution;
        Solution currentSolution;
        MoveSet moves;
        Iterator<Move> moveIterator;
        QualityComparator moveComparator =
                new QualityComparator(this.algorithmOwner, searchInfo);
        LinkedList<Move> tabuList = new LinkedList<Move>();
        LinkedList<Solution> solutionTabuList = new LinkedList<Solution>();
        
        bestSolution = new Solution(searchInfo.getInitialSolution());
        currentSolution = bestSolution;
        int noBetterFound = 0;

        // FIXME: Change end conditions
        for(int i = 0; noBetterFound < 6 && i < this.algorithmOwner.iterationsCount; ++i) {
            Logger.logln("[Tabu] Step: " + i + "; Current solution: " + 
                    moveComparator.value(currentSolution.getTime(), currentSolution.getCost())
                    + "; " + currentSolution);
            Logger.logln("[Tabu] Step: " + i + "; Best solution: " + 
                    moveComparator.value(bestSolution.getTime(), bestSolution.getCost())
                    + "; " + bestSolution);

            moves = prepareMoveSet(currentSolution, moveComparator);

/*
            moveIterator = moves.iterator();
            while(moveIterator.hasNext()) {
                Move move = moveIterator.next();
                
                Logger.logln("[Tabu] Move: "
                        + moveComparator.value(move.getTime(), move.getCost())
                        + "; " + move);
            }
*/            
            
            moveIterator = moves.iterator();
            boolean success = false;
            while(!success && moveIterator.hasNext()) {
                Move move = moveIterator.next();
                
                if((moveComparator.compare(currentSolution, bestSolution) < 0) ||
                        !isTabuMove(tabuList, move)) {

                    Solution newSolution = move.createSolution();
                    
                    if(isTabuSolution(solutionTabuList, newSolution)) {
                        continue;
                    }
                    
                    currentSolution = newSolution;                    
                    addToTabu(tabuList, 15, move);
                    addToTabu(solutionTabuList, 30, currentSolution);

                    success = true;
                }
            }
            
            if(!success) {
                // FIXME: Restart?
                break;
            }
            
            if(moveComparator.compare(currentSolution, bestSolution) < 0) {
                bestSolution = currentSolution;
                noBetterFound = 0;
            } else {
                noBetterFound++;
            }
        }
        
        return bestSolution;
    }

}
