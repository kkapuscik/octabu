/**
 * Created:     2005-11-22
 * Modified:    2005-11-22
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.tabu.impl;

import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Vertex;

/**
 * <h3>Vertex class extended for scalarization algorithm</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class TabuVertex extends Vertex {

    /** <h3>Best distance so far</h3> */
    private double bestDistance;
    /** <h3>Vertex processed</h3> */
    private boolean processed;
    /** <h3>Previous edge on best distance path</h3> */
    private Edge fromEdge;

    /**
     * <h3>Initialize vertex for Dijkstra algorithm</h3>
     */
    public void dijkstraInit() {
        setBestDistance(Double.MAX_VALUE);
        this.processed = false;
        this.fromEdge = null;
    }

    /**
     * <h3>Initialize vertex for BFS algorithm</h3>
     */
    public void bfsInit() {
        this.processed = false;
    }
    
    /**
     * <h3>Check if distance changes when relaxation is done</h3>
     * 
     * @param distance - distance value.
     * 
     * @return
     * True if distance changes, false otherwise.
     */
    public boolean checkRelax(double distance) {
        if(distance < this.bestDistance) {
            return true;
        }
        return false;
    }


    /**
     * <h3>Relax vertex</h3>
     * 
     * @param distance - distance value.
     * @param edge - incoming edge forcing the relaxation.
     */
    public void relax(double distance, Edge edge) {
        if(distance < this.bestDistance) {
            setBestDistance(distance);
            this.fromEdge = edge;
        }
    }

    /**
     * <h3>Mark vertex as processed</h3>
     */
    public void setProcessed() {
        this.processed = true;
    }

    /**
     * <h3>Get 'processed' status</h3>
     * 
     * @return
     * True if vertex was processed, false otherwise.
     */
    public boolean getProcessed() {
        return this.processed;
    }

    /**
     * <h3>Get best solution distance</h3>
     * 
     * @return
     * Best distance so far.
     */
    public double getBestDistance() {
        return this.bestDistance;
    }

    /**
     * <h3>Set best solution distance</h3>
     * 
     * @param distance - distance to set.
     */
    public void setBestDistance(double distance) {
        this.bestDistance = distance;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.graph.graph.Vertex#toString()
     */
    public String toString() {
        return super.toString() + "; (best distance=" + this.bestDistance + ")";
    }

    /**
     * <h3>Get previous edge on best path</h3>
     * 
     * @return
     * Previous edge or null if there is no previous edge.
     */
    public Edge getFromEdge() {
        return this.fromEdge;
    }

}
