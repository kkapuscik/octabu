/**
 * Created:     2005-11-13
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.tabu;

import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.JPanel;

import com.octaedr.octabu.app.plugin.IAlgorithmPlugin;
import com.octaedr.octabu.graph.algorithm.AlgorithmResults;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.util.GraphDuplicator;
import com.octaedr.octabu.plugin.algorithm.tabu.impl.DijkstraAlgorithm;
import com.octaedr.octabu.plugin.algorithm.tabu.impl.QualityComparator;
import com.octaedr.octabu.plugin.algorithm.tabu.impl.SearchInfo;
import com.octaedr.octabu.plugin.algorithm.tabu.impl.Solution;
import com.octaedr.octabu.plugin.algorithm.tabu.impl.SolutionSet;
import com.octaedr.octabu.plugin.algorithm.tabu.impl.TabuAlgorithm;
import com.octaedr.octabu.util.Logger;
import com.octaedr.octgraph.exception.OctgraphException;

/**
 * <h3>Tabu algorithm implementation class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class TabuAlgorithmPlugin implements IAlgorithmPlugin {

    public static final int ITERATIONS_COUNT_DEFAULT    =   50;
    public static final int ITERATIONS_COUNT_MIN        =   10;
    public static final int ITERATIONS_COUNT_MAX        =   10000;
    public static final int ITERATIONS_COUNT_STEP       =   5;

    public static final double ANGLE_WEIGHT_DEFAULT        =   1.0;
    public static final double ANGLE_WEIGHT_MIN            =   0.1;
    public static final double ANGLE_WEIGHT_MAX            =   100.0;
    public static final double ANGLE_WEIGHT_STEP           =   0.01;

    public static final double ANGLE_POWER_DEFAULT         =   2;
    public static final double ANGLE_POWER_MIN             =   0.1;
    public static final double ANGLE_POWER_MAX             =   100.0;
    public static final double ANGLE_POWER_STEP            =   0.1;

    public static final double DISTANCE_WEIGHT_DEFAULT     =   1.0;
    public static final double DISTANCE_WEIGHT_MIN         =   0.1;
    public static final double DISTANCE_WEIGHT_MAX         =   100.0;
    public static final double DISTANCE_WEIGHT_STEP        =   0.01;

    public static final double DISTANCE_POWER_DEFAULT      =   2;
    public static final double DISTANCE_POWER_MIN          =   0.1;
    public static final double DISTANCE_POWER_MAX          =   100.0;
    public static final double DISTANCE_POWER_STEP         =   0.1;

    public static final int FAIL_RUN_LENGTH_DEFAULT     =   4;
    public static final int FAIL_RUN_LENGTH_MIN         =   0;
    public static final int FAIL_RUN_LENGTH_MAX         =   100;
    public static final int FAIL_RUN_LENGTH_STEP        =   1;

    public static final int DEPTH_DEFAULT               =   10;
    public static final int DEPTH_MIN                   =   1;
    public static final int DEPTH_MAX                   =   1000;
    public static final int DEPTH_STEP                  =   1;
    
    
    private TabuPanel optionsPanel = new TabuPanel();

    public int iterationsCount = ITERATIONS_COUNT_DEFAULT;
    private int failRunLenght = FAIL_RUN_LENGTH_DEFAULT;
    private int depth = DEPTH_DEFAULT;
    public double angleWeight = ANGLE_WEIGHT_DEFAULT;
    public double anglePower = ANGLE_POWER_DEFAULT;
    public double distanceWeight = DISTANCE_WEIGHT_DEFAULT;
    public double distancePower = DISTANCE_POWER_DEFAULT;

    private DijkstraAlgorithm dijkstraAlgorithm;
    private TabuAlgorithm tabuAlgorithm;

    /**
     * <h3>Constructor</h3>
     */
    public TabuAlgorithmPlugin() {
        this.dijkstraAlgorithm = new DijkstraAlgorithm();
        this.tabuAlgorithm = new TabuAlgorithm(this);
    }
    
    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.algorithm.IAlgorithm#prepareGraph(com.octaedr.octabu.graph.graph.Graph)
     */
    public Graph prepareGraph(Graph graph) {
        try {
            /* duplicate graph */
            return GraphDuplicator.duplicate(
                    graph, new TabuElementProvider());
        } catch (OctgraphException exception) {
            return null;
        }
    }
    
    
    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.algorithm.IAlgorithm#process(com.octaedr.octabu.graph.algorithm.AlgorithmResults, com.octaedr.octabu.graph.graph.Graph, int, int)
     */
    public void process(AlgorithmResults results, Graph graph,
            int source, int target) {

        try {
            
            /* check parameters */
            if(!checkOptions()) {
                results.setFail();
                return;
            }

            SolutionSet solutionCollection = new SolutionSet();
            
            /* use Dijkstra's algorithm to get best solutions for both criteriums */
            Solution bestTimeSolution =
                    this.dijkstraAlgorithm.dijkstra(1.0, 0.0, graph, source, target);
            Solution bestCostSolution =
                    this.dijkstraAlgorithm.dijkstra(0.0, 1.0, graph, source, target);

            Solution startSolution = this.dijkstraAlgorithm.dijkstra(
                    0.5, 0.5, graph, source, target);

            Logger.logln("[Tabu] Best time solution: " + bestTimeSolution);
            Logger.logln("[Tabu] Best cost solution: " + bestCostSolution);
            
            /* try to put solutions to results */
            solutionCollection.update(bestTimeSolution);
            solutionCollection.update(bestCostSolution);
            
            /* create list of search operations to perform */
            LinkedList<SearchInfo> searchList = new LinkedList<SearchInfo>();
    
            /* add initial search */
            searchList.add(new SearchInfo(
                    bestTimeSolution.getTime(),
                    bestCostSolution.getTime(),
                    bestCostSolution.getCost(),
                    bestTimeSolution.getCost(),
                    1,
                    0,
                    startSolution
                    ));
                
            /* while there is any search to perform */
            while(!searchList.isEmpty()) {
    
                /* get first search data from list */
                SearchInfo info = searchList.poll();
                
                Logger.logln("[Tabu] Search: " + info);

                /* check if there is sense perform search */
                if(!info.isUsable() ||
                        (info.getFailRunLen() > this.failRunLenght) ||
                        (info.getDepth() > this.depth)) {
                    continue;
                }
                
                /* perform tabu search */
                Solution newSolution = this.tabuAlgorithm.tabuSearch(info);

                Logger.logln("[Tabu] New solution: " + newSolution);

                /* add new solution to result set */
                if((newSolution != null) &&
                        (solutionCollection.update(newSolution))) {

                    /* search subrectangles */
                    searchList.add(
                            new SearchInfo(
                                    info.getMinTime(),
                                    newSolution.getTime(),
                                    newSolution.getCost(),
                                    info.getMaxCost(),
                                    info.getDepth() + 1,
                                    0,
                                    newSolution
                                    ));
                    searchList.add(
                            new SearchInfo(
                                    newSolution.getTime(),
                                    info.getMaxTime(),
                                    info.getMinCost(),
                                    newSolution.getCost(),
                                    info.getDepth() + 1,
                                    0,
                                    newSolution
                                    ));

                } else {
                    double timeWeight;
                    double costWeight;
                    timeWeight = info.getMaxTime() - bestTimeSolution.getTime();
                    costWeight = info.getMaxCost() - bestCostSolution.getCost();
                    
                    startSolution = 
                            this.dijkstraAlgorithm.dijkstra(
                                    timeWeight, costWeight,
                                    graph, source, target);
                    
                    /* search with new lines */
                    searchList.add(
                            new SearchInfo(
                                    info.getMinTime(),
                                    info.getMaxTime() / 2,
                                    info.getMinCost(),
                                    info.getMaxCost(),
                                    info.getDepth() + 1,
                                    info.getFailRunLen() + 1,
                                    startSolution // FIXME: Better initial solution!
                                    ));
                    searchList.add(
                            new SearchInfo(
                                    info.getMinTime(),
                                    info.getMaxTime(),
                                    info.getMinCost(),
                                    info.getMaxCost() / 2,
                                    info.getDepth() + 1,
                                    info.getFailRunLen() + 1,
                                    startSolution // FIXME: Better initial solution!
                                    ));
                }
            }
            
            Iterator<Solution> solutionIterator;
            solutionIterator = solutionCollection.getAllSolutions();
            while(solutionIterator.hasNext()) {
                Solution solution = solutionIterator.next();
                results.addPath(solution);
            }
            
        } catch (OctgraphException exception) {
            Logger.logException(exception);
            results.setFail();
        }
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.algorithm.IAlgorithm#prepareResults(com.octaedr.octabu.graph.algorithm.AlgorithmResults, com.octaedr.octabu.graph.graph.Graph, int, int)
     */
    public void prepareResults(AlgorithmResults results, Graph graph, int source, int target) {
        // nothing to do
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getShortName()
     */
    public String getShortName() {
        return "plugin/algorithm/tabusearch";
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getName()
     */
    public String getName() {
        return "Tabu Search BSP Algorithm 1.0";
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getOptionsPanel()
     */
    public JPanel getOptionsPanel() {
        return this.optionsPanel;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#checkOptions()
     */
    public boolean checkOptions() {
        importGuiValues();
        return true;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#validateOptions()
     */
    public void validateOptions() {
        importGuiValues();
        exportGuiValues();
    }

    /**
     * <h3>Import options values from gui</h3>
     */
    private void importGuiValues() {
        this.iterationsCount = this.optionsPanel.getIterationsCount();
        this.angleWeight = this.optionsPanel.getAngleWeight();
        this.anglePower = this.optionsPanel.getAnglePower();
        this.distanceWeight = this.optionsPanel.getDistanceWeight();
        this.distancePower = this.optionsPanel.getDistancePower();
        this.failRunLenght = this.optionsPanel.getFailRunLength();
        this.depth = this.optionsPanel.getDepth();
    }

    /**
     * <h3>Export options values to gui</h3>
     */
    private void exportGuiValues() {
        this.optionsPanel.setIterationsCount(this.iterationsCount);
        this.optionsPanel.setAngleWeight(this.angleWeight);
        this.optionsPanel.setAnglePower(this.anglePower);
        this.optionsPanel.setDistanceWeight(this.distanceWeight);
        this.optionsPanel.setDistancePower(this.distancePower);
        this.optionsPanel.setFailRunLength(this.failRunLenght);
        this.optionsPanel.setDepth(this.depth);
    }

}
