/**
 * Created:   2005-11-20
 * Modified:  2005-11-20
 * Author:    Krzysztof Kapuscik
 * Copyright: (C) 2005 Krzysztof Kapuscik
 * Contact:   saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.tabu.impl;

import java.util.ListIterator;
import java.util.NoSuchElementException;

import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Path;
import com.octaedr.octabu.util.Logger;

/**
 * <h3>Solution class</h3>
 * 
 * <p>Solution class is a path class with additional
 * functionality.</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class Solution extends Path {

    /**
     * <h3>Constructor</h3>
     * 
     * <p>Creates empty solution.</p>
     */
    public Solution() {
        super();
    }

    /**
     * <h3>Copy constructor</h3>
     * 
     * @param other - solution to copy.
     */
    public Solution(Solution other) {
        super(other);
    }

    /**
     * <h3>Add vertex to solution</h3>
     * 
     * @param edge1 - edge to remove.
     * @param edge2 - first edge to add.
     * @param edge3 - second edge to add.
     */
    public void addVertex(Edge edge1, Edge edge2, Edge edge3) {

//        Logger.logln("[Tabu] Solution - Add vertex");
        
        ListIterator<Edge> edgeIterator = getEdges();
        while(edgeIterator.hasNext()) {
            if(edgeIterator.next() == edge1) {
                edgeIterator.remove();
                edgeIterator.add(edge2);
                edgeIterator.add(edge3);
                
                this.cost -= edge1.getCost();
                this.cost += edge2.getCost();
                this.cost += edge3.getCost();

                this.time -= edge1.getTime();
                this.time += edge2.getTime();
                this.time += edge3.getTime();

                break;
            }
        }

        checkPath();
    }

    /**
     * <h3>Replace edge in solution.</h3>
     * 
     * @param edge1 - edge to remove.
     * @param edge2 - edge to add.
     */
    public void replaceEdge(Edge edge1, Edge edge2) {
//        Logger.logln("[Tabu] Solution - Replace edge");

        ListIterator<Edge> edgeIterator = getEdges();
        while(edgeIterator.hasNext()) {
            if(edgeIterator.next() == edge1) {
                edgeIterator.remove();
                edgeIterator.add(edge2);

                this.cost -= edge1.getCost();
                this.cost += edge2.getCost();

                this.time -= edge1.getTime();
                this.time += edge2.getTime();
                
                break;
            }
        }

        checkPath();
    }

    /**
     * <h3>Remove vertex from solution</h3>
     * 
     * @param edge1 - first edge to remove.
     * @param edge2 - second edge to remove.
     * @param edge3 - edge to add.
     */
    public void removeVertex(Edge edge1, Edge edge2, Edge edge3) {
//        Logger.logln("[Tabu] Solution - Remove vertex");

        ListIterator<Edge> edgeIterator = getEdges();
        Edge currentEdge = null;
        Edge previousEdge = null;
        while(edgeIterator.hasNext()) {
            currentEdge = edgeIterator.next();
            
            if(previousEdge != null) {
                if((previousEdge == edge1) && (currentEdge == edge2)) {
                    edgeIterator.remove();
                    edgeIterator.previous();
                    edgeIterator.remove();

                    this.cost -= edge1.getCost();
                    this.cost -= edge2.getCost();
                    this.cost += edge3.getCost();

                    this.time -= edge1.getTime();
                    this.time -= edge2.getTime();
                    this.time += edge3.getTime();

                    break;
                }
            }
        }

        checkPath();
    }

    /**
     * <h3>Check if solution is same as given</h3>
     * 
     * @param other - solution used to compare.
     * 
     * @return
     * True if results are same (or almost similar), false otherwise.
     */
    public boolean sameAs(Solution other) {
        if((Math.abs(this.time - other.time) < 0.001) &&
                (Math.abs(this.cost - other.cost) < 0.001)) {
            return true;
        }
        return false;
    }
    
}
