/**
 * Created:   2005-11-20
 * Modified:  2005-11-20
 * Author:    Krzysztof Kapuscik
 * Copyright: (C) 2005 Krzysztof Kapuscik
 * Contact:   saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.tabu.impl;

import com.octaedr.octabu.exception.OctabuError;
import com.octaedr.octabu.graph.graph.Edge;

/**
 * <h3>Single move class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class Move {

    /** <h3>Remove vertex move type</h3> */
    public static final int TYPE_REMOVE_VERTEX      =   1;
    /** <h3>Change edge to other move type</h3> */
    public static final int TYPE_CHANGE_EDGE        =   2;
    /** <h3>Add vertex move type</h3> */
    public static final int TYPE_ADD_VERTEX         =   3;
    /** <h3>Change whole solution</h3> */
    public static final int TYPE_CHANGE_SOLUTION    =   4;

    /** <h3>Base solution</h3> */
    private Solution baseSolution;
    /** <h3>Move type</h3> */
    private int moveType;
    /** <h3>First remove edge</h3> */
    private Edge firstRemovedEdge;
    /** <h3>Second remove edge</h3> */
    private Edge secondRemovedEdge;
    /** <h3>First added edge</h3> */
    private Edge firstAddedEdge;
    /** <h3>Second added edge</h3> */
    private Edge secondAddedEdge;
    /** <h3>New solution</h3> */
    private Solution newSolution;
    
    
    /**
     * <h3>Constructor</h3>
     * 
     * @param solution - move base solution.
     * @param type - move type (TYPE_REMOVE_VERTEX or TYPE_ADD_VERTEX).
     * @param edge1 - first removed edge.
     * @param edge2 - second removed edge (TYPE_REMOVE_VERTEX)
     *                  or first added (TYPE_ADD_VERTEX).
     * @param edge3 - first added edge (TYPE_REMOVE_VERTEX)
     *                  or second added (TYPE_ADD_VERTEX).
     */
    public Move(Solution solution, int type, Edge edge1, Edge edge2, Edge edge3) {
        this.baseSolution = solution;
        this.moveType = type;
        if(this.moveType == TYPE_REMOVE_VERTEX) {
            this.firstRemovedEdge = edge1;
            this.secondRemovedEdge = edge2;
            this.firstAddedEdge = edge3;
            this.secondAddedEdge = null;
        } else if (this.moveType == TYPE_ADD_VERTEX) {
            this.firstRemovedEdge = edge1;
            this.secondRemovedEdge = null;
            this.firstAddedEdge = edge2;
            this.secondAddedEdge = edge3;
        } else {
            throw new OctabuError();
        }
    }

    /**
     * <h3>Constructor</h3>
     * 
     * @param solution - base move solution.
     * @param type - move type (TYPE_CHANGE_EDGE).
     * @param edge1 - removed edge.
     * @param edge2 - added edge.
     */
    public Move(Solution solution, int type, Edge edge1, Edge edge2) {
        this.baseSolution = solution;
        this.moveType = type;
        if(this.moveType == TYPE_CHANGE_EDGE) {
            this.firstRemovedEdge = edge1;
            this.secondRemovedEdge = null;
            this.firstAddedEdge = edge2;
            this.secondAddedEdge = null;
        } else {
            throw new OctabuError();
        }
    }

    /**
     * <h3>Constructor</h3>
     * 
     * @param base - base solution.
     * @param result - result solution.
     */
    public Move(Solution base, Solution result) {
        this.baseSolution = base;
        this.newSolution = result;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Move other) {
        if((this.moveType == other.moveType) &&
                (this.firstAddedEdge == other.firstAddedEdge) &&
                (this.secondAddedEdge == other.secondAddedEdge) &&
                (this.firstRemovedEdge == other.firstRemovedEdge) &&
                (this.secondRemovedEdge == other.secondRemovedEdge) &&
                (this.newSolution == other.newSolution)) {
            return true;
        }
        return false;
    }

    /**
     * <h3>Get move cost</h3>
     * 
     * @return
     * Move cost.
     */
    public double getCost() {
        switch(this.moveType) {
        case TYPE_ADD_VERTEX:
            return this.baseSolution.getCost()
                    - this.firstRemovedEdge.getCost()
                    + this.firstAddedEdge.getCost()
                    + this.secondAddedEdge.getCost();
        case TYPE_CHANGE_EDGE:
            return this.baseSolution.getCost()
                    - this.firstRemovedEdge.getTime()
                    + this.firstAddedEdge.getTime();
        case TYPE_REMOVE_VERTEX:
            return this.baseSolution.getCost()
                    - this.firstRemovedEdge.getCost()
                    - this.secondRemovedEdge.getCost()
                    + this.firstAddedEdge.getCost();
        case TYPE_CHANGE_SOLUTION:
            return this.newSolution.getCost();
        default:
            throw new OctabuError();
        }
    }
    
    /**
     * <h3>Get move time</h3>
     * 
     * @return
     * Move time.
     */
    public double getTime() {
        switch(this.moveType) {
        case TYPE_ADD_VERTEX:
            return this.baseSolution.getTime()
                    - this.firstRemovedEdge.getTime()
                    + this.firstAddedEdge.getTime()
                    + this.secondAddedEdge.getTime();
        case TYPE_CHANGE_EDGE:
            return this.baseSolution.getTime()
                    - this.firstRemovedEdge.getTime()
                    + this.firstAddedEdge.getTime();
        case TYPE_REMOVE_VERTEX:
            return this.baseSolution.getTime()
                    - this.firstRemovedEdge.getTime()
                    - this.secondRemovedEdge.getTime()
                    + this.firstAddedEdge.getTime();
        case TYPE_CHANGE_SOLUTION:
            return this.newSolution.getTime();
        default:
            throw new OctabuError();
        }
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return "[Move]"
                + " type=" + this.moveType
                + " time=" + getTime()
                + " cost=" + getCost();
    }

    /**
     * <h3>Create solution</h3>
     * 
     * @return
     * Created move result solution.
     */
    public Solution createSolution() {

        if(this.moveType == TYPE_CHANGE_SOLUTION) {
            return this.newSolution;
        }
        
        Solution createdSolution = new Solution(this.baseSolution);

        switch(this.moveType) {
        case TYPE_ADD_VERTEX:
            createdSolution.addVertex(
                    this.firstRemovedEdge,
                    this.firstAddedEdge,
                    this.secondAddedEdge);
            break;
        case TYPE_CHANGE_EDGE:
            createdSolution.replaceEdge(
                    this.firstRemovedEdge,
                    this.firstAddedEdge);
            break;
        case TYPE_REMOVE_VERTEX:
            createdSolution.removeVertex(
                    this.firstRemovedEdge,
                    this.secondRemovedEdge,
                    this.firstAddedEdge);
            break;
        default:
            throw new OctabuError();
        }
        
        return createdSolution;
    }

    /**
     * <h3>Creted reverse move</h3>
     * @return
     */
    public Move createReversedMove() {
        switch(this.moveType) {
        case TYPE_ADD_VERTEX:
            return new Move(
                    this.baseSolution,
                    TYPE_REMOVE_VERTEX,
                    this.firstAddedEdge,
                    this.secondAddedEdge,
                    this.firstRemovedEdge);
        case TYPE_CHANGE_EDGE:
            return new Move(
                    this.baseSolution,
                    TYPE_CHANGE_EDGE,
                    this.firstAddedEdge,
                    this.firstRemovedEdge);
        case TYPE_REMOVE_VERTEX:
            return new Move(
                    this.baseSolution,
                    TYPE_ADD_VERTEX,
                    this.firstAddedEdge,
                    this.firstRemovedEdge,
                    this.secondRemovedEdge);
        case TYPE_CHANGE_SOLUTION:
            return new Move(
                    this.newSolution,
                    this.baseSolution);
        default:
            throw new OctabuError();
        }
    }
}
