/**
 * Created:   2005-11-23
 * Modified:  2005-11-23
 * Author:    Krzysztof Kapuscik
 * Copyright: (C) 2005 Krzysztof Kapuscik
 * Contact:   saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.tabu.impl;

import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;

import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octgraph.exception.OctgraphException;

/**
 * <h3>Dijkstra algorithm implementation utility class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class DijkstraAlgorithm {

    /**
     * <h3>Comparator for vertex priority queue</h3>
     * 
     * @author Krzysztof Kapuscik
     * @version 1.0
     */
    private class VertexComparator implements Comparator<TabuVertex> {

        /**
         * <h3>Compare given vertices</h3>
         * 
         * @param o1 - first vertex.
         * @param o2 - second vertex.
         * 
         * @return
         * Value &lt 0 if o1 &lt o2, value = 0 if o1 = o2 or value &gt; 0 if o1 &gt; o2.
         */
        public int compare(TabuVertex o1, TabuVertex o2) {
            if(o1.getBestDistance() < o2.getBestDistance()) {
                return -1;
            } else if(o1.getBestDistance() > o2.getBestDistance()) {
                return 1;
            } else {
                return 0;
            }
        }
        
    }

    public Solution dijkstra(double timeWeight, double costWeight, Graph graph, int source, int target) throws OctgraphException {
        Iterator vertexIterator;
        Iterator edgeIterator;
        PriorityQueue<TabuVertex> queue;

        TabuVertex sourceVertex = (TabuVertex)graph.findVertex(source);

        /* vertex initialization */
        vertexIterator = graph.getAllVertices();
        while(vertexIterator.hasNext()) {
            TabuVertex vertex = (TabuVertex)vertexIterator.next();   
            vertex.dijkstraInit();
            if(vertex.getIntKey() == source) {
                vertex.setBestDistance(0.0);
            }
        }

        /* processing time criterium */
        queue = new PriorityQueue<TabuVertex>(
                graph.getAllVerticesCount(), new VertexComparator());
        queue.add(sourceVertex);
        for(;;) {
            TabuVertex currentVertex = null;

            // Logger.logln("[Tabu] Dijkstra - Queue size: " + queue.size());

            /* find not processed vertex with lowest time (and cost) */
            currentVertex = queue.poll();

            if((currentVertex == null) ||
                    (currentVertex.getBestDistance() == java.lang.Double.MAX_VALUE) ||
                    (currentVertex.getIntKey() == target)) {
                break;
            }
            
            // Logger.logln("[Tabu] Dijkstra - Current vertex: " + currentVertex);
                        
            /* process all edges from current vertex */
            double currentDistance = currentVertex.getBestDistance();
            
            edgeIterator = currentVertex.getOutEdges();
            while(edgeIterator.hasNext()) {
                Edge currentEdge = (Edge)edgeIterator.next();

                // Logger.logln("[Tabu] Dijkstra - Current edge: " + currentEdge);

                TabuVertex targetVertex = (TabuVertex)currentEdge.getTarget();
                if(!targetVertex.getProcessed()) {
                    double newDistance;
                    newDistance = currentDistance
                            + costWeight * currentEdge.getCost()
                            + timeWeight * currentEdge.getTime();

                    boolean changeFlag = targetVertex.checkRelax(newDistance);

                    if(changeFlag) {
                        queue.remove(targetVertex);
                    }
                    targetVertex.relax(newDistance, currentEdge);
                    if(changeFlag) {
                        queue.add(targetVertex);
                    }
                }
            }
            
            currentVertex.setProcessed();
        }

        TabuVertex targetVertex = (TabuVertex)graph.findVertex(target);
        Solution solution = new Solution();
        for(;;) {
            Edge edge = targetVertex.getFromEdge();
            if(edge == null) {
                break;
            }
            solution.addFirst(edge);
            targetVertex = (TabuVertex)edge.getSource();
        }
    
        return solution;
    }

    public Solution dijkstra(QualityComparator comparator, Graph graph, int source, int target) throws OctgraphException {
        Iterator vertexIterator;
        Iterator edgeIterator;
        PriorityQueue<TabuVertex> queue;

        TabuVertex sourceVertex = (TabuVertex)graph.findVertex(source);

        /* vertex initialization */
        vertexIterator = graph.getAllVertices();
        while(vertexIterator.hasNext()) {
            TabuVertex vertex = (TabuVertex)vertexIterator.next();   
            vertex.dijkstraInit();
            if(vertex.getIntKey() == source) {
                vertex.setBestDistance(0.0);
            }
        }

        /* processing time criterium */
        queue = new PriorityQueue<TabuVertex>(
                graph.getAllVerticesCount(), new VertexComparator());
        queue.add(sourceVertex);
        for(;;) {
            TabuVertex currentVertex = null;

            // Logger.logln("[Tabu] Dijkstra - Queue size: " + queue.size());

            /* find not processed vertex with lowest time (and cost) */
            currentVertex = queue.poll();

            if((currentVertex == null) ||
                    (currentVertex.getBestDistance() == java.lang.Double.MAX_VALUE) ||
                    (currentVertex.getIntKey() == target)) {
                break;
            }
            
            // Logger.logln("[Tabu] Dijkstra - Current vertex: " + currentVertex);
                        
            /* process all edges from current vertex */
            double currentDistance = currentVertex.getBestDistance();
            
            edgeIterator = currentVertex.getOutEdges();
            while(edgeIterator.hasNext()) {
                Edge currentEdge = (Edge)edgeIterator.next();

                // Logger.logln("[Tabu] Dijkstra - Current edge: " + currentEdge);

                TabuVertex targetVertex = (TabuVertex)currentEdge.getTarget();
                if(!targetVertex.getProcessed()) {
                    double newDistance;

                    newDistance = currentDistance + comparator.value(
                            currentEdge.getCost(), currentEdge.getTime());

                    boolean changeFlag = targetVertex.checkRelax(newDistance);

                    if(changeFlag) {
                        queue.remove(targetVertex);
                    }
                    targetVertex.relax(newDistance, currentEdge);
                    if(changeFlag) {
                        queue.add(targetVertex);
                    }
                }
            }
            
            currentVertex.setProcessed();
        }

        TabuVertex targetVertex = (TabuVertex)graph.findVertex(target);
        Solution solution = new Solution();
        for(;;) {
            Edge edge = targetVertex.getFromEdge();
            if(edge == null) {
                break;
            }
            solution.addFirst(edge);
            targetVertex = (TabuVertex)edge.getSource();
        }
    
        return solution;
    }


}
