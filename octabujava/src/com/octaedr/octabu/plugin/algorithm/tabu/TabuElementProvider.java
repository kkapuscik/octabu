/**
 * Created:     2005-11-20
 * Modified:    2005-11-20
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.tabu;

import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.graph.Vertex;
import com.octaedr.octabu.graph.util.DefaultElementProvider;
import com.octaedr.octabu.plugin.algorithm.tabu.impl.TabuVertex;
import com.octaedr.octgraph.generic.manager.DirectedAdjacencyMatrix;

/**
 * <h3>Graph element provider for Dijkstra algorithm</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class TabuElementProvider extends DefaultElementProvider {

    /* (non-Javadoc)
     * @see com.octaedr.octabu.graph.util.DefaultElementProvider#createVertex()
     */
    public Vertex createVertex() {
        return new TabuVertex();
    }
    
    /* (non-Javadoc)
     * @see com.octaedr.octabu.graph.util.DefaultElementProvider#createGraph()
     */
    public Graph createGraph() {
        return new Graph(new DirectedAdjacencyMatrix());
    }

}
