/**
 * Created:   2005-11-22
 * Modified:  2005-11-22
 * Author:    Krzysztof Kapuscik
 * Copyright: (C) 2005 Krzysztof Kapuscik
 * Contact:   saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.tabu.impl;


/**
 * <h3>Single search info class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class SearchInfo {

    /** <h3>Minimum time</h3> */
    private double minTimeValue;
    /** <h3>Maximum time</h3> */
    private double maxTimeValue;
    /** <h3>Minimum cost</h3> */
    private double minCostValue;
    /** <h3>Maximum cost</h3> */
    private double maxCostValue;
    /** <h3>Search depth level</h3> */
    private int depthValue;
    /** <h3>Length of fail run</h3> */
    private int failRunLen;
    /** <h3>Initial search solution</h3> */
    private Solution tabuInitialSolution;

    /**
     * <h3>Constructor</h3>
     * 
     * @param minTime - minimum time.
     * @param maxTime - maximum time.
     * @param minCost - minimum cost.
     * @param maxCost - maximum cost.
     * @param depth - depth level.
     * @param failRun - length of fail run.
     * @param initialSolution - initial solution.
     */
    public SearchInfo(double minTime, double maxTime,
            double minCost, double maxCost,
            int depth, int failRun,
            Solution initialSolution) {
        
        this.minTimeValue = minTime;
        this.maxTimeValue = maxTime;
        this.minCostValue = minCost;
        this.maxCostValue = maxCost;
        this.depthValue = depth;   
        this.failRunLen = failRun;
        this.tabuInitialSolution = initialSolution;
    }

    /**
     * <h3>Get length of fail run</h3>
     * 
     * @return
     * Length of fail run.
     */
    public int getFailRunLen() {
        return this.failRunLen;
    }
    
    /**
     * <h3>Get minimum time</h3>
     * 
     * @return
     * Mimimum time.
     */
    public double getMinTime() {
        return this.minTimeValue;
    }
    
    /**
     * <h3>Get maximum time</h3>
     * 
     * @return
     * Maximum time.
     */
    public double getMaxTime() {
        return this.maxTimeValue;
    }
    
    /**
     * <h3>Get minimum cost</h3>
     * 
     * @return
     * Mimimum cost.
     */
    public double getMinCost() {
        return this.minCostValue;
    }
    
    /**
     * <h3>Get maximum cost</h3>
     * 
     * @return
     * Maximum cost.
     */
    public double getMaxCost() {
        return this.maxCostValue;
    }
    
    /**
     * <h3>Check if search is 'usable'</h3>
     * 
     * <p>Search is 'usable' if value ranges are valid.
     * 
     * @return
     * True if search if 'usable', false otherwise.
     */
    public boolean isUsable() {
        if((this.minCostValue < this.maxCostValue) &&
                (this.minTimeValue < this.maxTimeValue)) {
            return true;
        }
        return false;
    }

    /**
     * <h3>Get depth level</h3>
     * 
     * @return
     * Depth level.
     */
    public int getDepth() {
        return this.depthValue;
    }
    
    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return "SearchInfo:"
                + " Cost=<" + this.minCostValue + "; " + this.maxCostValue + ">"
                + " Time=<" + this.minTimeValue + "; " + this.maxTimeValue + ">"
                + " Depth=" + this.depthValue
                + " FailRun=" + this.failRunLen;
    }

    /**
     * <h3>Get initial solution</h3>
     * 
     * @return
     * Initial solution.
     */
    public Solution getInitialSolution() {
        return this.tabuInitialSolution;
    }
}

