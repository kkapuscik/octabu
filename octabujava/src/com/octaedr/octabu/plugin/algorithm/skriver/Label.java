/**
 * Created:     2005-11-17
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.skriver;

import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Vertex;

/**
 * <h3>Vertex label class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class Label {

    /** <h3>Value of time criterium</h3> */
    private double timeValue;
    /** <h3>Value of cost criterium</h3> */
    private double costValue;
    /** <h3>Path previous edge</h3> */
    private Edge previousEdge;
    /** <h3>Path previous label</h3> */
    private Label previousLabel;
    
    /**
     * <h3>Constuctor</h3>
     * 
     * @param time - time value.
     * @param cost - cost value. 
     * @param fromEdge - previous edge on path.
     * @param fromLabel - previous label on path.
     */
    public Label(double time, double cost, Edge fromEdge, Label fromLabel) {
        this.timeValue = time;
        this.costValue = cost;
        this.previousEdge = fromEdge;
        this.previousLabel = fromLabel;
    }
    
    /**
     * <h3>Get label time</h3>
     * 
     * @return
     * Label time.
     */
    public double getTime() {
        return this.timeValue;
    }
    
    /**
     * <h3>Get label cost</h3>
     * 
     * @return
     * Label cost.
     */
    public double getCost() {
        return this.costValue;
    }
    
    /**
     * <h3>Get previous edge on path</h3>
     * 
     * @return
     * Previous edge on path.
     */
    public Edge getPreviousEdge() {
        return this.previousEdge;
    }
    
    /**
     * <h3>Get previous label on path</h3>
     * 
     * @return
     * Previous label on path.
     */
    public Label getPreviousLabel() {
        return this.previousLabel;
    }
    
    /**
     * <h3>Check if current label is dominated by given</h3>
     * 
     * @param other - other label.
     * 
     * @return
     * True if current label is dominated, false otherwise.
     */
    public boolean isDominated(Label other) {
        if((other.timeValue < this.timeValue) &&
                (other.costValue <= this.costValue)) {
            return true;
        }
        if((other.costValue < this.costValue) &&
                (other.timeValue <= this.timeValue)) {
            return true;
        }

        return false;
    }

    /**
     * <h3>Check if current label is same as given</h3>
     * 
     * @param other - other label.
     * 
     * @return
     * True if labels are same, false otherwise.
     */
    public boolean isSame(Label other) {
        if((this.timeValue == other.timeValue) &&
                (this.costValue == other.costValue) &&
                (this.previousLabel == other.previousLabel)) {
            return true;
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        String sourceKey = null;
        if(this.previousEdge != null) {
            sourceKey = ((Vertex)this.previousEdge.getSource()).getKey().toString();
        }
        return " [" + this.timeValue + "," + this.costValue + "," +
                sourceKey + "]";
    }

}
