/**
 * Created:     2005-11-13
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.skriver;

import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.JPanel;

import com.octaedr.octabu.app.plugin.IAlgorithmPlugin;
import com.octaedr.octabu.graph.algorithm.AlgorithmResults;
import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.graph.Path;
import com.octaedr.octabu.graph.util.GraphDuplicator;
import com.octaedr.octgraph.exception.BrokenPathException;
import com.octaedr.octgraph.exception.OctgraphException;

/**
 * <h3>Skriver algorithm implementation class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class SkriverAlgorithmPlugin implements IAlgorithmPlugin {

    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.algorithm.IAlgorithm#prepareGraph(com.octaedr.octabu.graph.graph.Graph)
     */
    public Graph prepareGraph(Graph graph) {
        try {
            /* duplicate graph */
            return GraphDuplicator.duplicate(
                    graph, new SkriverElementProvider());
        } catch (OctgraphException exception) {
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.algorithm.IAlgorithm#process(com.octaedr.octabu.graph.algorithm.AlgorithmResults, com.octaedr.octabu.graph.graph.Graph, int, int)
     */
    public void process(AlgorithmResults results, Graph graph, int source, int target) {
        LabelVertex currentVertex;
        Iterator edgeIterator;
        Edge currentEdge;
        boolean skipFlag;
        boolean overtakeFlag;
        LabelVertex targetVertex;
        Label currentFirstLabel;
        Label currentLastLabel;
        Label targetFirstLabel;
        Label targetLastLabel;
        double edgeCost;
        double edgeTime;
        LinkedList<LabelVertex> queue;
        Iterator vertexIterator;

        try {
            queue = new LinkedList<LabelVertex>();

            LabelVertex sourceVertex = (LabelVertex)graph.findVertex(source);

            /* vertex initialization */
            vertexIterator = graph.getAllVertices();
            while(vertexIterator.hasNext()) {
                LabelVertex vertex = (LabelVertex)vertexIterator.next();   
                vertex.init(vertex == sourceVertex);
            }
            queue.add(sourceVertex);
            sourceVertex.setAddedToQueue();
            
            /* while there are still some elements in label queue */
            while(!queue.isEmpty()) {
                /* get vertex from queue */
                currentVertex = queue.poll();
                currentVertex.setRemovedFromQueue();

//                Logger.logln("[Skriver] Current vertex " + currentVertex);
                
                /* process all out vertices */
                edgeIterator = currentVertex.getOutEdges();
                while(edgeIterator.hasNext()) {
                    currentEdge = (Edge)edgeIterator.next();
                    
//                    Logger.logln("[Skriver] Current edge " + currentEdge);

                    /* extract edge cost & time */
                    edgeCost = currentEdge.getCost();
                    edgeTime = currentEdge.getTime();

                    /* get edge target vertex */
                    targetVertex = (LabelVertex)currentEdge.getTarget();

                    /* check skip & overtake conditions */
                    skipFlag = false;
                    overtakeFlag = false;

                    if(!targetVertex.isLabelListEmpty()) {
                        currentFirstLabel = currentVertex.getFirstLabel();
                        currentLastLabel = currentVertex.getLastLabel();
                        targetFirstLabel = targetVertex.getFirstLabel();
                        targetLastLabel = targetVertex.getLastLabel();

                        if((currentFirstLabel.getTime() + edgeTime > targetLastLabel.getTime()) &&
                                currentLastLabel.getCost() + edgeCost > targetFirstLabel.getCost()) {
                            skipFlag = true;
                        }
                        if(!skipFlag) {
                            if((currentLastLabel.getTime() + edgeTime < targetFirstLabel.getTime()) &&
                                    currentFirstLabel.getCost() + edgeCost < targetLastLabel.getCost()) {
                                overtakeFlag = true;
                            }
                        }
                    } else {
                        overtakeFlag = true;
                    }
                    
                    /* merge & overtake actions */
                    if(overtakeFlag) {
                        overtakeLabels(currentVertex, currentEdge, targetVertex);
                        if(!targetVertex.isInQueue()) {
                            queue.add(targetVertex);
                            targetVertex.setAddedToQueue();
                        }
                    } else if(!skipFlag) {
                        if(mergeLabels(currentVertex, currentEdge, targetVertex)) {
                            if(!targetVertex.isInQueue()) {
                                queue.add(targetVertex);
                                targetVertex.setAddedToQueue();
                            }
                        }
                    }
                }
            }

        } catch (OctgraphException exception) {
            exception.printStackTrace();
            results.setFail();
        }
    }
    
    /**
     * <h3>Merge labels action</h3>
     * 
     * @param currentVertex - current vertex.
     * @param currentEdge - processed edge.
     * @param targetVertex - destination vertex.
     * 
     * @return
     * True if target vertex labels collection changed, false otherwise.
     */
    private boolean mergeLabels(LabelVertex currentVertex, 
            Edge currentEdge, LabelVertex targetVertex) {
        
        Iterator<Label> currentLabelIterator;
        Iterator<Label> targetLabelIterator;
        LinkedList<Label> newLabelList = new LinkedList<Label>();
        Label currentLabel = null;
        Label targetLabel = null;
        Label lastLabel = null;
        boolean changed = false;
        Label processedLabel = null;

        currentLabelIterator = currentVertex.getAllLabels();
        targetLabelIterator = targetVertex.getAllLabels();

        /* continue until there are labels to process */
        for(;;) {
            /* construct current label */
            if(currentLabel == null) {
                if(currentLabelIterator.hasNext()) {
                    currentLabel = currentLabelIterator.next();
                    /* create new label */
                    currentLabel = new Label(
                            currentLabel.getTime() + currentEdge.getTime(),
                            currentLabel.getCost() + currentEdge.getCost(),
                            currentEdge,
                            currentLabel);
                }
            }
            
            /* get next target label */
            if(targetLabel == null) {
                if(targetLabelIterator.hasNext()) {
                    targetLabel = targetLabelIterator.next();
                }
            }
            
            if((targetLabel == null) && (currentLabel == null)) {
                break;
            }
            
            if(lastLabel != null) {
                if(currentLabel != null && currentLabel.isDominated(lastLabel)) {
                    currentLabel = null;
                    continue;
                }
                if(currentLabel != null && currentLabel.isSame(lastLabel)) {
                    currentLabel = null;
                    continue;
                }
                if(targetLabel != null && targetLabel.isDominated(lastLabel)) {
                    targetLabel = null;
                    changed = true;
                    continue;
                }
            }
            
            /* check which label should be processed */
            if((targetLabel != null) && (currentLabel != null)) {
                if(targetLabel.getTime() < currentLabel.getTime()) {
                    processedLabel = targetLabel;
                } else if(targetLabel.getTime() > currentLabel.getTime()) {
                    processedLabel = currentLabel;
                } else {
                    if(targetLabel.getCost() <= currentLabel.getCost()) {
                        processedLabel = targetLabel;
                    } else {
                        processedLabel = currentLabel;
                    }
                }
            } else if(targetLabel == null) {
                processedLabel = currentLabel;
            } else {
                processedLabel = targetLabel;
            }
            
            /* process label */
            if(processedLabel == currentLabel) {
                currentLabel = null;
                changed = true;
            } else {
                targetLabel = null;
            }
            lastLabel = processedLabel;
            newLabelList.addLast(lastLabel);
        }

        targetVertex.setLabels(newLabelList);
        return changed;
    }

    /**
     * <h3>Overtake labels action</h3>
     * 
     * @param currentVertex - current vertex.
     * @param currentEdge - processed edge.
     * @param targetVertex - destination vertex.
     */
    private void overtakeLabels(LabelVertex currentVertex,
            Edge currentEdge, LabelVertex targetVertex) {
        Iterator<Label> labelIterator;
        Label currentLabel;
        Label newLabel;

        /* remove old labels from target vertex */
        targetVertex.removeAllLabels();
        
        /* create new labels collection from current vertex labels and edge */
        labelIterator = currentVertex.getAllLabels();
        while(labelIterator.hasNext()) {
            currentLabel = labelIterator.next();

            /* create new label */
            newLabel = new Label(
                    currentLabel.getTime() + currentEdge.getTime(),
                    currentLabel.getCost() + currentEdge.getCost(),
                    currentEdge,
                    currentLabel);
            
            /* append label to end of label list */
            targetVertex.appendLabel(newLabel);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.algorithm.IAlgorithm#prepareResults(com.octaedr.octabu.graph.algorithm.AlgorithmResults, com.octaedr.octabu.graph.graph.Graph, int, int)
     */
    public void prepareResults(AlgorithmResults results, Graph graph, int source, int target) {
        try {
            LabelVertex targetVertex;
            targetVertex = (LabelVertex)graph.findVertex(target);

            Iterator<Label> labelIterator = targetVertex.getAllLabels();
            while(labelIterator.hasNext()) {
                Label label = labelIterator.next();
                
                results.addPath(createPath(label));
            }
        } catch (OctgraphException exception) {
            exception.printStackTrace();
            results.setFail();
        }
    }

    /**
     * @param label
     * @return
     * 
     * @throws BrokenPathException 
     * should not happen.
     */
    private Path createPath(Label label) throws BrokenPathException {
        Path path = new Path();
        
        while(label.getPreviousEdge() != null) {
            path.addFirst(label.getPreviousEdge());
            label = label.getPreviousLabel();
        }
        
        return path;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getShortName()
     */
    public String getShortName() {
        return "plugin/algorithm/skriver";
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getName()
     */
    public String getName() {
        return "Skriver Algorithm 1.0";
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getOptionsPanel()
     */
    public JPanel getOptionsPanel() {
        return null;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#checkOptions()
     */
    public boolean checkOptions() {
        return true;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#validateOptions()
     */
    public void validateOptions() {
        // nothing to do
    }

}
