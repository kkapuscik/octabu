/**
 * Created:     2005-11-17
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.skriver;

import com.octaedr.octabu.graph.graph.Vertex;
import com.octaedr.octabu.graph.util.DefaultElementProvider;

/**
 * <h3>Graph element provider for Skriver algorithm</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class SkriverElementProvider extends DefaultElementProvider {

    /* (non-Javadoc)
     * @see com.octaedr.octabu.graph.util.DefaultElementProvider#createVertex()
     */
    public Vertex createVertex() {
        return new LabelVertex();
    }

}
