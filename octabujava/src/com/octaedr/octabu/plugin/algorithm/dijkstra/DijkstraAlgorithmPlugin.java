/**
 * Created:     2005-11-10
 * Modified:    2005-11-20
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.dijkstra;

import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;

import javax.swing.JPanel;

import com.octaedr.octabu.app.plugin.IAlgorithmPlugin;
import com.octaedr.octabu.graph.algorithm.AlgorithmResults;
import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Graph;
import com.octaedr.octabu.graph.graph.Path;
import com.octaedr.octabu.graph.util.GraphDuplicator;
import com.octaedr.octgraph.exception.BrokenPathException;
import com.octaedr.octgraph.exception.OctgraphException;

/**
 * <h3>Dijkstra algorithm implementation class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.1
 */
public class DijkstraAlgorithmPlugin implements IAlgorithmPlugin {

    /**
     * <h3>Comparator for time priority queue</h3>
     * 
     * @author Krzysztof Kapuscik
     * @version 1.0
     */
    private class TimeVertexComparator implements Comparator<DijkstraVertex> {

        /**
         * <h3>Compare given vertices</h3>
         * 
         * @param o1 - first vertex.
         * @param o2 - second vertex.
         * 
         * @return
         * Value &lt 0 if o1 &lt o2, value = 0 if o1 = o2 or value &gt; 0 if o1 &gt; o2.
         */
        public int compare(DijkstraVertex o1, DijkstraVertex o2) {
            if(o1.getBestTime() < o2.getBestTime()) {
                return -1;
            } else if(o1.getBestTime() > o2.getBestTime()) {
                return 1;
            } else {
                if(o1.getBestTimeCost() < o2.getBestTimeCost()) {
                    return -1;
                } else if(o1.getBestTimeCost() > o2.getBestTimeCost()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
        
    }
    
    /**
     * <h3>Comparator for cost priority queue</h3>
     * 
     * @author Krzysztof Kapuscik
     * @version 1.0
     */
    private class CostVertexComparator implements Comparator<DijkstraVertex> {

        /**
         * <h3>Compare given vertices</h3>
         * 
         * @param o1 - first vertex.
         * @param o2 - second vertex.
         * 
         * @return
         * Value &lt 0 if o1 &lt o2, value = 0 if o1 = o2 or value &gt; 0 if o1 &gt; o2.
         */
        public int compare(DijkstraVertex o1, DijkstraVertex o2) {
            if(o1.getBestCost() < o2.getBestCost()) {
                return -1;
            } else if(o1.getBestCost() > o2.getBestCost()) {
                return 1;
            } else {
                if(o1.getBestCostTime() < o2.getBestCostTime()) {
                    return -1;
                } else if(o1.getBestCostTime() > o2.getBestCostTime()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
        
    }


    /**
     * <h3>Recursive produce results paths</h3>
     * 
     * @param costPaths - produce paths for cost (true) or time (false) optimization.
     * @param path - current path (extended during recursion).
     * @param currentVertex - current vertex.
     * @param source - source vertex key.
     * @param results - algorithm results object.
     * 
     * @throws BrokenPathException
     * (should not happen).
     */
    private void producePaths(boolean costPaths, Path path, DijkstraVertex currentVertex, int source, AlgorithmResults results) throws BrokenPathException {
        /* if this is the last vertex on a path */
        if(currentVertex.getIntKey() == source) {
            results.addPath(new Path(path));
        } else {
            Iterator<Edge> edgeIterator;

            /* get collection of 'from' edges */
            if(costPaths) {
                edgeIterator = currentVertex.getCostFromEdges();
            } else {
                edgeIterator = currentVertex.getTimeFromEdges();
            }
            
            /* continue processing using all 'from' edges at start */
            while(edgeIterator.hasNext()) {
                /* get 'from' edge */
                Edge edge = edgeIterator.next();

                /* construct new path */
                path.addFirst(edge);    
                
                /* process recursively */
                producePaths(costPaths, path, (DijkstraVertex)edge.getSource(), source, results);

                /* reconstruct old path */
                path.removeFirst();
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.algorithm.IAlgorithm#prepareGraph(com.octaedr.octabu.graph.graph.Graph)
     */
    public Graph prepareGraph(Graph graph) {
        try {
            /* duplicate graph */
            return GraphDuplicator.duplicate(
                    graph, new DijkstraElementProvider());
        } catch (OctgraphException exception) {
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.algorithm.IAlgorithm#process(com.octaedr.octabu.graph.algorithm.AlgorithmResults, com.octaedr.octabu.graph.graph.Graph, int, int)
     */
    public void process(AlgorithmResults results, Graph graph, int source, int target) {
        try {
            Iterator vertexIterator;
            Iterator edgeIterator;
            PriorityQueue<DijkstraVertex> queue;
    
            DijkstraVertex sourceVertex = (DijkstraVertex)graph.findVertex(source);

            /* vertex initialization */
            vertexIterator = graph.getAllVertices();
            while(vertexIterator.hasNext()) {
                DijkstraVertex vertex = (DijkstraVertex)vertexIterator.next();   
                vertex.dijkstraInit();
                if(vertex.getIntKey() == source) {
                    vertex.setBestTime(0.0, 0.0);
                    vertex.setBestCost(0.0, 0.0);
                }
            }

            /* processing time criterium */
            queue = new PriorityQueue<DijkstraVertex>(
                    graph.getAllVerticesCount(), new TimeVertexComparator());
            queue.add(sourceVertex);
            for(;;) {
                DijkstraVertex currentVertex;
    
                /* find not processed vertex with lowest time (and cost) */
                currentVertex = queue.poll();
    
                /* check loop end conditions */
                if((currentVertex == null) ||
                        (currentVertex.getBestTime() == java.lang.Double.MAX_VALUE) ||
                        (currentVertex.getIntKey() == target)) {
                    break;
                }

                /* get critierium values for current vertex */
                double currentTime = currentVertex.getBestTime();
                double currentCost = currentVertex.getBestTimeCost();
                
                /* process all edges exiting current vertex */
                edgeIterator = currentVertex.getOutEdges();
                while(edgeIterator.hasNext()) {
                    /* get edge */
                    Edge edge = (Edge)edgeIterator.next();
    
                    /* get edge target vertex */
                    DijkstraVertex targetVertex = (DijkstraVertex)edge.getTarget();

                    /* check if vertex is already processed */
                    if(!targetVertex.getTimeProcessed()) {
                        
                        /* check if best solution will change after relaxation */
                        boolean changeFlag = targetVertex.checkRelaxTime(
                                currentTime + edge.getTime(),
                                currentCost + edge.getCost());
                        
                        /* remove vertex from queue if needed */
                        if(changeFlag) {
                            queue.remove(targetVertex);
                        }
                        
                        /* relaxation */
                        targetVertex.relaxTime(
                                currentTime + edge.getTime(),
                                currentCost + edge.getCost(),
                                edge);
                        
                        /* add vertex back to queue if needed */
                        if(changeFlag) {
                            queue.add(targetVertex);
                        }
                    }
                }
                
                /* mark vertex as processed */
                currentVertex.setTimeProcessed();
            }
    
            /* processing cost criterium */
            queue = new PriorityQueue<DijkstraVertex>(
                    graph.getAllVerticesCount(), new CostVertexComparator());
            queue.add(sourceVertex);
            for(;;) {
                DijkstraVertex currentVertex;
    
                /* find not processed vertex with lowest cost (and time) */
                currentVertex = queue.poll();
    
                /* check loop end conditions */
                if((currentVertex == null) ||
                        (currentVertex.getBestCost() == java.lang.Double.MAX_VALUE) ||
                        (currentVertex.getIntKey() == target)) {
                    break;
                }
                            
                /* get critierium values for current vertex */
                double currentCost = currentVertex.getBestCost();
                double currentTime = currentVertex.getBestCostTime();
                
                /* process all edges from current vertex */
                edgeIterator = currentVertex.getOutEdges();
                while(edgeIterator.hasNext()) {
                    /* get edge */
                    Edge edge = (Edge)edgeIterator.next();
    
                    /* get edge target vertex */
                    DijkstraVertex targetVertex = (DijkstraVertex)edge.getTarget();

                    /* check if vertex is already processed */
                    if(!targetVertex.getCostProcessed()) {
                        /* check if best solution will change after relaxation */
                        boolean changeFlag = targetVertex.checkRelaxCost(
                                currentCost + edge.getCost(),
                                currentTime + edge.getTime());

                        /* remove vertex from queue if needed */
                        if(changeFlag) {
                            queue.remove(targetVertex);
                        }
                        
                        /* relaxation */
                        targetVertex.relaxCost(
                                currentCost + edge.getCost(),
                                currentTime + edge.getTime(),
                                edge);

                        /* add vertex back to queue if needed */
                        if(changeFlag) {
                            queue.add(targetVertex);
                        }
                    }
                }
                
                /* mark vertex as processed */
                currentVertex.setCostProcessed();
            }
        } catch (OctgraphException exception) {
            exception.printStackTrace();
            results.setFail();
        }
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octabu.graph.algorithm.IAlgorithm#prepareResults(com.octaedr.octabu.graph.algorithm.AlgorithmResults, com.octaedr.octabu.graph.graph.Graph, int, int)
     */
    public void prepareResults(AlgorithmResults results, Graph graph, int source, int target) {
        try {
            DijkstraVertex targetVertex;
            
            /* get target vertex */
            targetVertex = (DijkstraVertex)graph.findVertex(target);
            /* produce paths for best cost solutions */
            producePaths(true, new Path(), targetVertex, source, results);
            /* produce paths for best time solutions */
            producePaths(false, new Path(), targetVertex, source, results);
        } catch (OctgraphException exception) {
            exception.printStackTrace();
            results.setFail();
        }
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getShortName()
     */
    public String getShortName() {
        return "plugin/algorithm/dijkstra";
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getName()
     */
    public String getName() {
        return "Dijkstra Bicriterion Algorithm 1.0";
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#getOptionsPanel()
     */
    public JPanel getOptionsPanel() {
        return null;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#checkOptions()
     */
    public boolean checkOptions() {
        return true;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.app.IPlugin#validateOptions()
     */
    public void validateOptions() {
        // nothing to do
    }

}
