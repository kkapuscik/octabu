/**
 * Created:     2005-11-10
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octabu.plugin.algorithm.dijkstra;

import java.util.HashSet;
import java.util.Iterator;

import com.octaedr.octabu.graph.graph.Edge;
import com.octaedr.octabu.graph.graph.Vertex;

/**
 * <h3>Dijkstra algorithm extended vertex class</h3>
 * 
 * @author Krzysztof Kapusik
 * @version 1.0
 */
public class DijkstraVertex extends Vertex {

    private double bestTime;
    private double bestTimeCost;
    private double bestCost;
    private double bestCostTime;
    private boolean timeProcessed;
    private boolean costProcessed;
    private HashSet<Edge> timeFromSet = new HashSet<Edge>();
    private HashSet<Edge> costFromSet = new HashSet<Edge>();

    /**
     * <h3>Initialize vertex for Dijkstra algorithm</h3>
     */
    public void dijkstraInit() {
        setBestTime(Double.MAX_VALUE, Double.MAX_VALUE);
        setBestCost(Double.MAX_VALUE, Double.MAX_VALUE);
        this.timeProcessed = false;
        this.costProcessed = false;
        this.timeFromSet.clear();
        this.costFromSet.clear();
    }
    
    /**
     * <h3>Check if time criterium changes when relaxation is done</h3>
     * 
     * @param time - time value.
     * @param cost - cost value.
     * 
     * @return
     * True if criterium value changes , false otherwise.
     */
    public boolean checkRelaxTime(double time, double cost) {
        if((time < this.bestTime) ||
                ((time == this.bestTime) && (cost < this.bestTimeCost))) {
            return true;
        }
        return false;
    }
    
    /**
     * <h3>Check if cost criterium changes when relaxation is done</h3>
     * 
     * @param cost - cost value.
     * @param time - time value.
     * 
     * @return
     * True if criterium value changes , false otherwise.
     */
    public boolean checkRelaxCost(double cost, double time) {
        if((cost < this.bestCost) ||
                ((cost == this.bestCost) && (time < this.bestCostTime))) {
            return true;
        }
        return false;
    }

    /**
     * <h3>Relax vertex basing on time critierium</h3>
     * 
     * @param time - time value.
     * @param cost - cost value.
     * @param edge - incoming edge forcing the relaxation.
     */
    public void relaxTime(double time, double cost, Edge edge) {
        if((time == this.bestTime) && (cost == this.bestTimeCost)) {
            this.timeFromSet.add(edge);
        }
        
        if((time < this.bestTime) ||
                ((time == this.bestTime) && (cost < this.bestTimeCost))) {
            setBestTime(time, cost);
            this.timeFromSet.clear();
            this.timeFromSet.add(edge);
        }
    }

    /**
     * <h3>Relax vertex basing on cost critierium</h3>
     * 
     * @param cost - cost value.
     * @param time - time value.
     * @param edge - incoming edge forcing the relaxation.
     */
    public void relaxCost(double cost, double time, Edge edge) {
        if((cost == this.bestCost) && (time == this.bestCostTime)) {
            this.costFromSet.add(edge);
        }

        if((cost < this.bestCost) ||
                ((cost == this.bestCost) && (time < this.bestCostTime))) {
            setBestCost(cost, time);
            this.costFromSet.clear();
            this.costFromSet.add(edge);
        }
    }

    /**
     * <h3>Set flag that vertex was processed for time criterium</h3>
     */
    public void setTimeProcessed() {
        this.timeProcessed = true;
    }

    /**
     * <h3>Set flag that vertex was processed for cost criterium</h3>
     */
    public void setCostProcessed() {
        this.costProcessed = true;
    }

    /**
     * <h3>Get time criterium 'processed' status flag</h3>
     */
    public boolean getTimeProcessed() {
        return this.timeProcessed;
    }
    
    /**
     * <h3>Get cost criterium 'processed' status flag</h3>
     */
    public boolean getCostProcessed() {
        return this.costProcessed;
    }

    /**
     * <h3>Get best time (processing time criterium)</h3>
     * 
     * @return
     * Best time.
     */
    public double getBestTime() {
        return this.bestTime;
    }
    
    /**
     * <h3>Get cost for best time solution (processing time criterium)</h3>
     * 
     * @return
     * Best cost.
     */
    public double getBestTimeCost() {
        return this.bestTimeCost;
    }
    
    /**
     * <h3>Set best time and cost (processing time criterium)</h3>
     * 
     * @param time - best time
     * @param cost - cost for given best time solution.
     */
    public void setBestTime(double time, double cost) {
        this.bestTime = time;
        this.bestTimeCost = cost;
    }
    
    /**
     * <h3>Get best cost (processing cost criterium)</h3>
     * 
     * @return
     * Best cost.
     */
    public double getBestCost() {
        return this.bestCost;
    }
    
    /**
     * <h3>Get time for best cost solution (processing cost criterium)</h3>
     * 
     * @return
     * Best cost.
     */
    public double getBestCostTime() {
        return this.bestCostTime;
    }
    
    /**
     * <h3>Set best cost and time (processing cost criterium)</h3>
     * 
     * @param cost - best cost
     * @param time - time for given best cost solution.
     */
    public void setBestCost(double cost, double time) {
        this.bestCost = cost;
        this.bestCostTime = time;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octabu.graph.graph.Vertex#toString()
     */
    public String toString() {
        return super.toString() +
                "; (best time=" + this.bestTime + ", cost=" + this.bestTimeCost + ")" +
                "; (best cost=" + this.bestCost + ", time=" + this.bestCostTime + ")";
    }
    
    /**
     * <h3>Get all 'from' edges for stored solution (processing time criterium)</h3>
     * 
     * @return
     * Iterator over edges collection.
     */
    Iterator<Edge> getTimeFromEdges() {
        return this.timeFromSet.iterator();
    }
    
    /**
     * <h3>Get all 'from' edges for stored solution (processing time criterium)</h3>
     * 
     * @return
     * Iterator over edges collection.
     */
    Iterator<Edge> getCostFromEdges() {
        return this.costFromSet.iterator();
    }
}
