/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.manager;

import com.octaedr.octgraph.exception.AlreadyInGraphException;
import com.octaedr.octgraph.exception.NotInGraphException;
import com.octaedr.octgraph.graph.IUndirectedEdge;
import com.octaedr.octgraph.graph.IUndirectedVertex;

/**
 * <h3>Undirected graph manager interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IUndirectedGraphManager extends IGraphManager {

    /**
     * <h3>Add vertex to graph</h3>
     * 
     * @param vertex - vertex to add.
     * 
     * @return True if vertex has been successfully added or false otherwise.
     * 
     * @throws AlreadyInGraphException 
     * if vertex is already in any graph.
     */
    public boolean addVertex(IUndirectedVertex vertex) throws AlreadyInGraphException;

    /**
     * <h3>Remove vertex from graph</h3>
     * 
     * @param vertex - vertex to remove.
     * 
     * @return True if vertex has been successfully removed or false otherwise.
     * 
     * @throws NotInGraphException 
     * if given vertex is not in this graph.
     */
    public boolean removeVertex(IUndirectedVertex vertex) throws NotInGraphException;

    /**
     * <h3>Add edge to graph</h3>
     * 
     * @param edge - edge to add.
     * 
     * @return True if edge has been successfully added or false otherwise.
     * 
     * @throws AlreadyInGraphException 
     * if edge is already in any graph.
     * 
     * @throws NotInGraphException 
     * if edge source or target vertex does not belong to graph.
     */    
    public boolean addEdge(IUndirectedEdge edge) throws AlreadyInGraphException, NotInGraphException;

    /**
     * <h3>Remove edge from graph</h3>
     * 
     * @param edge - edge to remove.
     * 
     * @return True if edge has been successfully removed or false otherwise.
     * 
     * @throws NotInGraphException 
     * if given edge is not in this graph.
     */
    public boolean removeEdge(IUndirectedEdge edge) throws NotInGraphException;
}
