/**
 * Created:     2005-10-01
 * Modified:    2005-11-20
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.manager;

import java.util.Iterator;

import com.octaedr.octgraph.exception.NotInGraphException;
import com.octaedr.octgraph.graph.IVertex;

/**
 * <h3>Graph manager interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.1
 */
public interface IGraphManager {

    /**
     * <h3>Get all edges connected to given vertex</h3>
     * 
     * @param vertex - vertex for which looking for edges.
     * 
     * @return Iterator over edges collection.
     * 
     * @throws NotInGraphException
     * if vertex is not in this graph.
     */
    public Iterator getEdges(IVertex vertex) throws NotInGraphException;

    /**
     * <h3>Get all edges between pair of vertices</h3>
     * 
     * @param vertex1 - vertex for which looking for edges.
     * @param vertex2 - vertex for which looking for edges.
     * 
     * @return Iterator over edges collection.
     * 
     * @throws NotInGraphException
     * if vertex is not in this graph.
     */
    public Iterator getEdges(IVertex vertex1, IVertex vertex2) throws NotInGraphException;

    /**
     * <h3>Get all neighbour vertices of given vertex</h3>
     * 
     * @param vertex - vertex for which looking for neighbours.
     * 
     * @return Iterator over vertices collection.
     * 
     * @throws NotInGraphException
     * if vertex is not in this graph.
     */
    public Iterator getNeighbours(IVertex vertex) throws NotInGraphException;

    /**
     * <h3>Get given vertex degree</h3>
     * 
     * @param vertex - vertex for which looking for degree.
     * 
     * @return Number of edges connected to this vertex.
     * 
     * @throws NotInGraphException
     * if vertex is not in this graph.
     */
    public int getDegree(IVertex vertex) throws NotInGraphException;
    
    
    /**
     * <h3>Get all vertices in graph</h3>
     * 
     * @return
     * Iterator over vertices collection.
     */
    public Iterator getAllVertices();

    /**
     * <h3>Get all edges in graph</h3>
     * 
     * @return
     * Iterator over edges collection.
     */
    public Iterator getAllEdges();

    /**
     * <h3>Get number of all vertices</h3>
     * 
     * @return
     * Number of all vertices in graph.
     */
    public int getAllVerticesCount();
    
    /**
     * <h3>Get number of all edges</h3>
     * 
     * @return
     * Number of all edges in graph.
     */
    public int getAllEdgesCount();

}
