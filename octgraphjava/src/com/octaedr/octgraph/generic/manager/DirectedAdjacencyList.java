/**
 * Created:     2005-10-01
 * Modified:    2005-11-20
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.generic.manager;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

import com.octaedr.octgraph.exception.AlreadyInGraphException;
import com.octaedr.octgraph.exception.InvalidObjectException;
import com.octaedr.octgraph.exception.NotInGraphException;
import com.octaedr.octgraph.graph.IDirectedEdge;
import com.octaedr.octgraph.graph.IDirectedVertex;
import com.octaedr.octgraph.graph.IVertex;
import com.octaedr.octgraph.manager.IDirectedGraphManager;

/**
 * <h3>Directed adjacency list graph manager</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.1
 */
public class DirectedAdjacencyList implements IDirectedGraphManager {

    /**
     * <h3>Internal vertex edges enumerator class</h3>
     * 
     * @author Krzysztof Kapuscik
     * @version 1.0
     */
    private class VertexEdgesIterator implements Iterator {

        /** <h3>Iterator used for all edges between vertices</h3> */
        public static final int MODE_ALL    =   0;
        /** <h3>Iterator used for out edges between vertices</h3> */
        public static final int MODE_OUT    =   1;

        /** <h3>Iteration mode used by this iterator</h3> */
        private int iterationMode;
        /** <h3>Internal edge enumerator</h3> */
        private Iterator internalIterator;
        /** <h3>Other vertex for which iteration is done</h3> */
        private IVertex otherVertex;
        /** <h3>Next edge prepared</h3> */
        private IDirectedEdge nextEdge;

        /**
         * <h3>Constructor</h3>
         * 
         * @param edgeIterator - iterator over edges of first vertex.
         * @param vertex - second vertex.
         * @param mode - iteration mode.
         */
        public VertexEdgesIterator(Iterator edgeIterator, IVertex vertex, int mode) {
            this.internalIterator = edgeIterator;
            this.iterationMode = mode;
            this.otherVertex = vertex;

            this.nextEdge = prepareNext();
        }

        /**
         * <h3>Prepare next edge</h3>
         * 
         * @return
         * Next edge in iteration.
         */
        private IDirectedEdge prepareNext() {
            while(this.internalIterator.hasNext()) {
                IDirectedEdge edge = (IDirectedEdge)this.internalIterator.next();
                
                switch(this.iterationMode) {
                    case MODE_ALL:
                        if((edge.getSource() == this.otherVertex) ||
                                (edge.getTarget() == this.otherVertex)) {
                            return edge;
                        }
                        break;
                    case MODE_OUT:
                        if(edge.getTarget() == this.otherVertex) {
                            return edge;
                        }
                        break;
                    default:
                        throw new Error("Iteration mode not supported");
                }
            }
            return null;
        }
        
        /* (non-Javadoc)
         * @see java.util.Iterator#hasNext()
         */
        public boolean hasNext() {
            return this.nextEdge != null;
        }

        /* (non-Javadoc)
         * @see java.util.Iterator#next()
         */
        public Object next() {
            IDirectedEdge tmp = this.nextEdge;
            this.nextEdge = prepareNext();
            return tmp;
        }

        /* (non-Javadoc)
         * @see java.util.Iterator#remove()
         */
        public void remove() {
            throw new UnsupportedOperationException();
        }

    }

    
    /** <h3>Collection of graph vertices</h3> */
    LinkedList vertices = new LinkedList();
    /** <h3>Collection of graph edges</h3> */
    LinkedList edges = new LinkedList();
    /** <h3>Collection of collections of vertex out edges</h3> */
    HashMap vertexOutEdges = new HashMap();
    /** <h3>Collection of collections of vertex in edges</h3> */
    HashMap vertexInEdges = new HashMap();
    /** <h3>Collection of collections of vertex all edges</h3> */
    HashMap vertexAllEdges = new HashMap();
    
    /* (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IDirectedGraphManager#addVertex(com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public boolean addVertex(IDirectedVertex vertex) throws AlreadyInGraphException {
        if(vertex.isInGraph()) {
            throw new AlreadyInGraphException();
        }
        
        /** Create all needed structures */
        this.vertices.add(vertex);
        this.vertexOutEdges.put(vertex, new LinkedList());
        this.vertexInEdges.put(vertex, new LinkedList());
        this.vertexAllEdges.put(vertex, new LinkedList());
        
        return true;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IDirectedGraphManager#removeVertex(com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public boolean removeVertex(IDirectedVertex vertex) throws NotInGraphException {
        if(!vertex.isInGraph() || !this.vertices.contains(vertex)) {
            throw new NotInGraphException();
        }

        /* remove all edges first */
        for(;;) {
            try {
                Iterator iter;
                iter = getEdges(vertex);
                if(iter.hasNext()) {
                    IDirectedEdge edge = (IDirectedEdge)iter.next();
                    edge.removeFromGraph();
                } else {
                    break;
                }
            } catch (NotInGraphException exception) {
                exception.printStackTrace();
                break;
            } catch (InvalidObjectException exception) {
                exception.printStackTrace();
                break;
            }
        }
        
        /* remove all data */
        this.vertexOutEdges.remove(vertex);
        this.vertexInEdges.remove(vertex);
        this.vertexAllEdges.remove(vertex);
        this.vertices.remove(vertex);
        
        return true;
    }    
    
    /* (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IDirectedGraphManager#addEdge(com.octaedr.octgraph.graph.IDirectedEdge)
     */
    public boolean addEdge(IDirectedEdge edge) throws AlreadyInGraphException, NotInGraphException {
        if(edge.isInGraph()) {
            throw new AlreadyInGraphException();
        }
        
        this.edges.add(edge);
        getAllEdgesSet(edge.getSource()).add(edge);
        getAllEdgesSet(edge.getTarget()).add(edge);
        getInEdgesSet(edge.getTarget()).add(edge);
        getOutEdgesSet(edge.getSource()).add(edge);

        return true;
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IDirectedGraphManager#removeEdge(com.octaedr.octgraph.graph.IDirectedEdge)
     */
    public boolean removeEdge(IDirectedEdge edge) throws NotInGraphException {
        if(!edge.isInGraph() || !this.edges.contains(edge)) {
            throw new NotInGraphException();
        }

        try {
            getAllEdgesSet(edge.getSource()).remove(edge);
            getAllEdgesSet(edge.getTarget()).remove(edge);
            getInEdgesSet(edge.getTarget()).remove(edge);
            getOutEdgesSet(edge.getSource()).remove(edge);
            this.edges.remove(edge);
        } catch (NotInGraphException exception) {
            exception.printStackTrace();
        }
        return true;
    }

    
    /* (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IGraphManager#getAllEdges()
     */
    public Iterator getAllEdges() {
        return this.edges.iterator();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IGraphManager#getAllVertices()
     */
    public Iterator getAllVertices() {
        return this.vertices.iterator();
    }


    /* (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IDirectedGraphManager#getInDegree(com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public int getInDegree(IDirectedVertex vertex) throws NotInGraphException {
        return getInEdgesSet(vertex).size();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IDirectedGraphManager#getInEdges(com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public Iterator getInEdges(IDirectedVertex vertex) throws NotInGraphException {
        return getInEdgesSet(vertex).iterator();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IDirectedGraphManager#getInNeighbours(com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public Iterator getInNeighbours(IDirectedVertex vertex) throws NotInGraphException {
        /* get iterator over all in edges */
        Iterator edgeIterator = getInEdgesSet(vertex).iterator();
        /* create result collection */
        HashSet resultSet = new HashSet();
        /* put all in vertices in temporaty collection */
        while(edgeIterator.hasNext()) {
            IDirectedEdge edge = (IDirectedEdge)edgeIterator.next();
            resultSet.add(edge.getSource());
        }
        /* return iterator over temporary edge */
        return resultSet.iterator();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IDirectedGraphManager#getOutDegree(com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public int getOutDegree(IDirectedVertex vertex) throws NotInGraphException {
        return getOutEdgesSet(vertex).size();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IDirectedGraphManager#getOutEdges(com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public Iterator getOutEdges(IDirectedVertex vertex) throws NotInGraphException {
        return getOutEdgesSet(vertex).iterator();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IDirectedGraphManager#getOutNeighbours(com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public Iterator getOutNeighbours(IDirectedVertex vertex) throws NotInGraphException {
        /* get iterator over all in edges */
        Iterator edgeIterator = getOutEdgesSet(vertex).iterator();
        /* create result collection */
        HashSet resultSet = new HashSet();
        /* put all in vertices in temporaty collection */
        while(edgeIterator.hasNext()) {
            IDirectedEdge edge = (IDirectedEdge)edgeIterator.next();
            resultSet.add(edge.getTarget());
        }
        /* return iterator over temporary edge */
        return resultSet.iterator();
    }


    /* (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IGraphManager#getDegree(com.octaedr.octgraph.graph.IVertex)
     */
    public int getDegree(IVertex vertex) throws NotInGraphException {
        return getAllEdgesSet(vertex).size();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IGraphManager#getEdges(com.octaedr.octgraph.graph.IVertex)
     */
    public Iterator getEdges(IVertex vertex) throws NotInGraphException {
        return getAllEdgesSet(vertex).iterator();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IGraphManager#getNeighbours(com.octaedr.octgraph.graph.IVertex)
     */
    public Iterator getNeighbours(IVertex vertex) throws NotInGraphException {
        /* get iterator over all in edges */
        Iterator edgeIterator = getAllEdgesSet(vertex).iterator();
        /* create result collection */
        HashSet resultSet = new HashSet();
        /* put all in vertices in temporaty collection */
        while(edgeIterator.hasNext()) {
            IDirectedEdge edge = (IDirectedEdge)edgeIterator.next();
            if(edge.getSource() == vertex) {
                resultSet.add(edge.getTarget());
            } else {
                resultSet.add(edge.getSource());
            }
        }
        /* return iterator over temporary edge */
        return resultSet.iterator();
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IGraphManager#getAllVerticesCount()
     */
    public int getAllVerticesCount() {
        return this.vertices.size();
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IGraphManager#getAllEdgesCount()
     */
    public int getAllEdgesCount() {
        return this.edges.size();
    }

    
    /**
     * <h3>Get collection of in edges for given vertex</h3>
     * 
     * @param vertex - vertex for which operation is called.
     * 
     * @return
     * Collection of in edges for given vertex.
     * 
     * @throws NotInGraphException
     * if given vertex is not in this graph.
     */
    private LinkedList getInEdgesSet(IVertex vertex) throws NotInGraphException {
        if(!vertex.isInGraph() || !this.vertices.contains(vertex)) {
            throw new NotInGraphException();
        }
        return (LinkedList)this.vertexInEdges.get(vertex);
    }
    
    /**
     * <h3>Get collection of out edges for given vertex</h3>
     * 
     * @param vertex - vertex for which operation is called.
     * 
     * @return
     * Collection of out edges for given vertex.
     * 
     * @throws NotInGraphException
     * if given vertex is not in this graph.
     */
    private LinkedList getOutEdgesSet(IVertex vertex) throws NotInGraphException {
        if(!vertex.isInGraph() || !this.vertices.contains(vertex)) {
            throw new NotInGraphException();
        }
        return (LinkedList)this.vertexOutEdges.get(vertex);
    }

    /**
     * <h3>Get collection of all edges for given vertex</h3>
     * 
     * @param vertex - vertex for which operation is called.
     * 
     * @return
     * Collection of all edges for given vertex.
     * 
     * @throws NotInGraphException
     * if given vertex is not in this graph.
     */
    private LinkedList getAllEdgesSet(IVertex vertex) throws NotInGraphException {
        if(!vertex.isInGraph() || !this.vertices.contains(vertex)) {
            throw new NotInGraphException();
        }
        return (LinkedList)this.vertexAllEdges.get(vertex);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IDirectedGraphManager#getOutEdges(com.octaedr.octgraph.graph.IDirectedVertex, com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public Iterator getOutEdges(IDirectedVertex vertex1, IDirectedVertex vertex2) throws NotInGraphException {
        return new VertexEdgesIterator(
                getEdges(vertex1), vertex2, VertexEdgesIterator.MODE_OUT);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IDirectedGraphManager#getInEdges(com.octaedr.octgraph.graph.IDirectedVertex, com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public Iterator getInEdges(IDirectedVertex vertex1, IDirectedVertex vertex2) throws NotInGraphException {
        return getOutEdges(vertex2, vertex1);
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.manager.IGraphManager#getEdges(com.octaedr.octgraph.graph.IVertex, com.octaedr.octgraph.graph.IVertex)
     */
    public Iterator getEdges(IVertex vertex1, IVertex vertex2) throws NotInGraphException {
        return new VertexEdgesIterator(
                getEdges(vertex1), vertex2, VertexEdgesIterator.MODE_ALL);
    }

}
