/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.generic.graph;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

import com.octaedr.octgraph.exception.BrokenPathException;
import com.octaedr.octgraph.exception.OctgraphError;
import com.octaedr.octgraph.graph.IDirectedEdge;
import com.octaedr.octgraph.graph.IEdge;
import com.octaedr.octgraph.graph.IPath;

/**
 * <h3>Directed graph path generic class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class DirectedPath implements IPath {

    /** <h3>List of path edges</h3> */
    protected LinkedList pathEdges;

    /**
     * <h3>Constructor</h3>
     */
    public DirectedPath() {
        this.pathEdges = new LinkedList();
    }

    /**
     * <h3>Constructor</h3>
     * 
     * @param other - path to copy data from.
     */
    public DirectedPath(DirectedPath other) {
        this.pathEdges = (LinkedList)other.pathEdges.clone();
    }
    
    /* (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IPath#addFirst(com.octaedr.octgraph.graph.IEdge)
     */
    public void addFirst(IEdge edge) throws BrokenPathException {
        if(!this.pathEdges.isEmpty()) {
            IDirectedEdge firstEdge = (IDirectedEdge)this.pathEdges.getFirst();
            IDirectedEdge newEdge = (IDirectedEdge)edge;
            if(newEdge.getTarget() != firstEdge.getSource()) {
                throw new BrokenPathException();
            }
        }
        this.pathEdges.addFirst(edge);
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IPath#addLast(com.octaedr.octgraph.graph.IEdge)
     */
    public void addLast(IEdge edge) throws BrokenPathException {
        if(!this.pathEdges.isEmpty()) {
            IDirectedEdge lastEdge = (IDirectedEdge)this.pathEdges.getLast();
            IDirectedEdge newEdge = (IDirectedEdge)edge;
            if(newEdge.getSource() != lastEdge.getTarget()) {
                throw new BrokenPathException();
            }
        }
        this.pathEdges.addLast(edge);
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IPath#removeFirst()
     */
    public IEdge removeFirst() {
        if(this.pathEdges.isEmpty()) {
            return null;
        }
        return (IEdge)this.pathEdges.removeFirst();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IPath#removeLast()
     */
    public IEdge removeLast() {
        if(this.pathEdges.isEmpty()) {
            return null;
        }
        return (IEdge)this.pathEdges.removeLast();
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IPath#getIterator()
     */
    public ListIterator getEdges() {
        return this.pathEdges.listIterator();
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IPath#getEdgeCount()
     */
    public int getEdgeCount() {
        return this.pathEdges.size();
    }

    public void checkPath() {
        DirectedEdge currentEdge;
        DirectedEdge previousEdge = null;
        Iterator edgeIterator = getEdges();
        while(edgeIterator.hasNext()) {
            currentEdge = (DirectedEdge) edgeIterator.next();

            if(previousEdge != null) {
                if(previousEdge.getTarget() != currentEdge.getSource()) {
                    throw new OctgraphError();
                }
            }

            previousEdge = currentEdge;
        }
    }
}
