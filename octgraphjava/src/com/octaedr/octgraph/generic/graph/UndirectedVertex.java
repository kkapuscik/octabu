/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.generic.graph;

import com.octaedr.octgraph.graph.IUndirectedVertex;

/**
 * <h3>Undirected vertex generic class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 0.1
 */
public class UndirectedVertex extends Vertex implements IUndirectedVertex {

    // nothing in this version

}
