/**
 * Created:     2005-10-01
 * Modified:    2005-11-20
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.generic.graph;

import java.util.Iterator;

import com.octaedr.octgraph.exception.NotInGraphException;
import com.octaedr.octgraph.graph.IDirectedGraph;
import com.octaedr.octgraph.graph.IDirectedVertex;

/**
 * <h3>Directed vertex generic class.</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.1
 */
public class DirectedVertex extends Vertex implements IDirectedVertex {

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IDirectedVertex#getOutEdges()
     */
    final public Iterator getOutEdges() throws NotInGraphException {
        IDirectedGraph graph = getDirectedGraph();
        if(graph == null) {
            throw new NotInGraphException();
        }
        return graph.getOutEdges(this);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IDirectedVertex#getOutEdges(com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public Iterator getOutEdges(IDirectedVertex other) throws NotInGraphException {
        IDirectedGraph graph = getDirectedGraph();
        if(graph == null) {
            throw new NotInGraphException();
        }
        return graph.getOutEdges(this, other);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IDirectedVertex#getInEdges()
     */
    final public Iterator getInEdges() throws NotInGraphException {
        IDirectedGraph graph = getDirectedGraph();
        if(graph == null) {
            throw new NotInGraphException();
        }
        return graph.getInEdges(this);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IDirectedVertex#getInEdges(com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public Iterator getInEdges(IDirectedVertex other) throws NotInGraphException {
        IDirectedGraph graph = getDirectedGraph();
        if(graph == null) {
            throw new NotInGraphException();
        }
        return graph.getInEdges(this, other);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IDirectedVertex#getOutNeighbours()
     */
    final public Iterator getOutNeighbours() throws NotInGraphException {
        IDirectedGraph graph = getDirectedGraph();
        if(graph == null) {
            throw new NotInGraphException();
        }
        return graph.getOutNeighbours(this);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IDirectedVertex#getInNeighbours()
     */
    final public Iterator getInNeighbours() throws NotInGraphException {
        IDirectedGraph graph = getDirectedGraph();
        if(graph == null) {
            throw new NotInGraphException();
        }
        return graph.getInNeighbours(this);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IDirectedVertex#getOutDegree()
     */
    final public int getOutDegree() throws NotInGraphException {
        IDirectedGraph graph = getDirectedGraph();
        if(graph == null) {
            throw new NotInGraphException();
        }
        return graph.getOutDegree(this);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IDirectedVertex#getInDegree()
     */
    final public int getInDegree() throws NotInGraphException {
        IDirectedGraph graph = getDirectedGraph();
        if(graph == null) {
            throw new NotInGraphException();
        }
        return graph.getInDegree(this);
    }

    /**
     * <h3>Get vertex owner</h3>
     * 
     * <p>Method returns casted value of getGraph() method.</p>
     * 
     * @return Vertex owner or null if vertex is currently not
     *         owned by any graph.
     *         
     * @see com.octaedr.octgraph.graph.IGraphElement#getGraph()
     */
    private IDirectedGraph getDirectedGraph() {
        return (IDirectedGraph)getGraph();
    }

}
