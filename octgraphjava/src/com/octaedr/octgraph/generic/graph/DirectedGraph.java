/**
 * Created:     2005-10-01
 * Modified:    2005-11-20
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.generic.graph;

import java.util.Iterator;

import com.octaedr.octgraph.exception.AlreadyInGraphException;
import com.octaedr.octgraph.exception.DuplicatedKeyException;
import com.octaedr.octgraph.exception.InvalidObjectException;
import com.octaedr.octgraph.exception.NotInGraphException;
import com.octaedr.octgraph.exception.NotSupportedException;
import com.octaedr.octgraph.graph.IDirectedEdge;
import com.octaedr.octgraph.graph.IDirectedGraph;
import com.octaedr.octgraph.graph.IDirectedVertex;
import com.octaedr.octgraph.graph.IEdge;
import com.octaedr.octgraph.graph.IGraphElement;
import com.octaedr.octgraph.graph.IVertex;
import com.octaedr.octgraph.keytrait.IKeyManager;
import com.octaedr.octgraph.keytrait.IKeyTrait;
import com.octaedr.octgraph.manager.IDirectedGraphManager;

/**
 * <h3>Directed graph base class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.1
 */
public class DirectedGraph implements IDirectedGraph {

    /** <h3>Graph elements manager</h3> */
    private IDirectedGraphManager graphManagerObject;
    
    /** <h3>Manager for vertex key mappings</h3> */
    private IKeyManager vertexKeyManagerObject;
    
    /** <h3>Manager for edges key mappings</h3> */
    private IKeyManager edgeKeyManagerObject;
    
    /**
     * <h3>Constructor</h3>
     * 
     * <p>Creates graph which manages its objects using
     * given object, but does not support key mappings.
     * 
     * @param graphManager - graph objects manager.
     */
    public DirectedGraph(IDirectedGraphManager graphManager) {
        this.graphManagerObject = graphManager;
    }

    /**
     * <h3>Constructor</h3>
     * 
     * <p>Creates graph which manages its objects using
     * given object and supports vertex key mappings.
     * 
     * @param graphManager - graph objects manager.
     * @param vertexKeyManager - manager for vertex key mappings.
     */
    public DirectedGraph(IDirectedGraphManager graphManager,
            IKeyManager vertexKeyManager) {
        this.graphManagerObject = graphManager;
        this.vertexKeyManagerObject = vertexKeyManager;
    }

    /**
     * <h3>Constructor</h3>
     * 
     * <p>Creates graph which manages its objects using
     * given object and supports vertex and edges key mappings.
     * 
     * @param graphManager - graph objects manager.
     * @param vertexKeyManager - manager for vertex key mappings.
     * @param edgeKeyManager - manager for edge key mappings.
     */
    public DirectedGraph(IDirectedGraphManager graphManager,
            IKeyManager vertexKeyManager,
            IKeyManager edgeKeyManager) {
        this.graphManagerObject = graphManager;
        this.vertexKeyManagerObject = vertexKeyManager;
        this.edgeKeyManagerObject = edgeKeyManager;
    }
    
    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IGraph#addElement(com.octaedr.octgraph.IGraphElement)
     */
    public boolean addElement(IGraphElement element) throws InvalidObjectException, DuplicatedKeyException, AlreadyInGraphException, NotInGraphException {
        boolean result;
        boolean useKeyManager;
        if(element instanceof IDirectedVertex) {
            useKeyManager = (this.vertexKeyManagerObject != null) && (element instanceof IKeyTrait);
            if(useKeyManager) {
                IKeyTrait keyTrait = (IKeyTrait)element;
                if(this.vertexKeyManagerObject.find(keyTrait.getKey()) != null) {
                    throw new DuplicatedKeyException();
                }
            }
            result = this.graphManagerObject.addVertex((IDirectedVertex)element);
            if(result && useKeyManager) {
                this.vertexKeyManagerObject.add((IKeyTrait)element);
            }
        } else if(element instanceof IDirectedEdge) {
            useKeyManager = (this.edgeKeyManagerObject != null) && (element instanceof IKeyTrait);
            if(useKeyManager) {
                IKeyTrait keyTrait = (IKeyTrait)element;
                if(this.edgeKeyManagerObject.find(keyTrait.getKey()) != null) {
                    throw new DuplicatedKeyException();
                }
            }
            result = this.graphManagerObject.addEdge((IDirectedEdge)element);
            if(result && useKeyManager) {
                this.edgeKeyManagerObject.add((IKeyTrait)element);
            }
        } else {
            throw new InvalidObjectException();
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IGraph#removeElement(com.octaedr.octgraph.IGraphElement)
     */
    public boolean removeElement(IGraphElement element) throws InvalidObjectException, NotInGraphException {
        boolean result;
        boolean useKeyManager;
        if(element instanceof IDirectedVertex) {
            useKeyManager = (this.vertexKeyManagerObject != null) && (element instanceof IKeyTrait);
            result = this.graphManagerObject.removeVertex((IDirectedVertex)element);
            if(result && useKeyManager) {
                this.vertexKeyManagerObject.remove((IKeyTrait)element);
            }
        } else if(element instanceof IDirectedEdge) {
            useKeyManager = (this.edgeKeyManagerObject != null) && (element instanceof IKeyTrait);
            result = this.graphManagerObject.removeEdge((IDirectedEdge)element);
            if(result && useKeyManager) {
                this.edgeKeyManagerObject.remove((IKeyTrait)element);
            }
        } else {
            throw new InvalidObjectException();
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IDirectedGraph#getInEdges(com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public Iterator getInEdges(IDirectedVertex vertex) throws NotInGraphException {
        return this.graphManagerObject.getInEdges(vertex);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IDirectedGraph#getInNeighbours(com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public Iterator getInNeighbours(IDirectedVertex vertex) throws NotInGraphException {
        return this.graphManagerObject.getInNeighbours(vertex);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IDirectedGraph#getInDegree(com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public int getInDegree(IDirectedVertex vertex) throws NotInGraphException {
        return this.graphManagerObject.getInDegree(vertex);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IDirectedGraph#getOutEdges(com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public Iterator getOutEdges(IDirectedVertex vertex) throws NotInGraphException {
        return this.graphManagerObject.getOutEdges(vertex);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IDirectedGraph#getOutNeighbours(com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public Iterator getOutNeighbours(IDirectedVertex vertex) throws NotInGraphException {
        return this.graphManagerObject.getOutNeighbours(vertex);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IDirectedGraph#getOutDegree(com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public int getOutDegree(IDirectedVertex vertex) throws NotInGraphException {
        return this.graphManagerObject.getOutDegree(vertex);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IGraph#findVertex(java.lang.Object)
     */
    public IVertex findVertex(Object key) throws NotSupportedException {
        if(this.vertexKeyManagerObject == null) {
            throw new NotSupportedException();
        }
        return (IVertex)this.vertexKeyManagerObject.find(key);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IGraph#findEdge(java.lang.Object)
     */
    public IEdge findEdge(Object key) throws NotSupportedException {
        if(this.edgeKeyManagerObject == null) {
            throw new NotSupportedException();
        }
        return (IEdge)this.edgeKeyManagerObject.find(key);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IGraph#getEdges(com.octaedr.octgraph.graph.IVertex)
     */
    public Iterator getEdges(IVertex vertex) throws NotInGraphException {
        return this.graphManagerObject.getEdges(vertex);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IGraph#getNeighbours(com.octaedr.octgraph.graph.IVertex)
     */
    public Iterator getNeighbours(IVertex vertex) throws NotInGraphException {
        return this.graphManagerObject.getNeighbours(vertex);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IGraph#getDegree(com.octaedr.octgraph.graph.IVertex)
     */
    public int getDegree(IVertex vertex) throws NotInGraphException {
        return this.graphManagerObject.getDegree(vertex);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IGraph#getAllVertices()
     */
    public Iterator getAllVertices() {
        return this.graphManagerObject.getAllVertices();
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IGraph#getAllEdges()
     */
    public Iterator getAllEdges() {
        return this.graphManagerObject.getAllEdges();
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IGraph#getAllVerticesCount()
     */
    public int getAllVerticesCount() {
        return this.graphManagerObject.getAllVerticesCount();
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IGraph#getAllEdgesCount()
     */
    public int getAllEdgesCount() {
        return this.graphManagerObject.getAllEdgesCount();
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IDirectedGraph#getOutEdges(com.octaedr.octgraph.graph.IDirectedVertex, com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public Iterator getOutEdges(IDirectedVertex vertex1, IDirectedVertex vertex2) throws NotInGraphException {
        return this.graphManagerObject.getOutEdges(vertex1, vertex2);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IDirectedGraph#getInEdges(com.octaedr.octgraph.graph.IDirectedVertex, com.octaedr.octgraph.graph.IDirectedVertex)
     */
    public Iterator getInEdges(IDirectedVertex vertex1, IDirectedVertex vertex2) throws NotInGraphException {
        return this.graphManagerObject.getInEdges(vertex1, vertex2);
    }

    /* (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IGraph#getEdges(com.octaedr.octgraph.graph.IVertex, com.octaedr.octgraph.graph.IVertex)
     */
    public Iterator getEdges(IVertex vertex1, IVertex vertex2) throws NotInGraphException {
        return this.graphManagerObject.getEdges(vertex1, vertex2);
    }

}
