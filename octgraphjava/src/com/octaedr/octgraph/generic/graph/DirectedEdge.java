/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.generic.graph;

import com.octaedr.octgraph.exception.RestrictedOperationException;
import com.octaedr.octgraph.graph.IDirectedEdge;
import com.octaedr.octgraph.graph.IDirectedVertex;

/**
 * <h3>Directed edge generic class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class DirectedEdge extends GraphElement implements IDirectedEdge {

    /** <h3>Edge source vertex</h3> */
    IDirectedVertex sourceVertex = null;
    /** <h3>Edge target vertex</h3> */
    IDirectedVertex targetVertex = null;


    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IDirectedEdge#setVertices(com.octaedr.octgraph.IDirectedVertex, com.octaedr.octgraph.IDirectedVertex)
     */
    final public void setVertices(IDirectedVertex source, IDirectedVertex target) throws RestrictedOperationException {
        if(isInGraph()) {
            throw new RestrictedOperationException();
        }

        this.sourceVertex = source;
        this.targetVertex = target;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IDirectedEdge#setSource(com.octaedr.octgraph.IDirectedVertex)
     */
    final public void setSource(IDirectedVertex source) throws RestrictedOperationException {
        if(isInGraph()) {
            throw new RestrictedOperationException();
        }

        this.sourceVertex = source;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IDirectedEdge#setTarget(com.octaedr.octgraph.IDirectedVertex)
     */
    final public void setTarget(IDirectedVertex target) throws RestrictedOperationException {
        if(isInGraph()) {
            throw new RestrictedOperationException();
        }

        this.targetVertex = target;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IDirectedEdge#getSource()
     */
    final public IDirectedVertex getSource() {
        return this.sourceVertex;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IDirectedEdge#getTarget()
     */
    final public IDirectedVertex getTarget() {
        return this.targetVertex;
    }

}
