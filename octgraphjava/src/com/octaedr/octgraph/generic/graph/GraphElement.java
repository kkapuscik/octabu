/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.generic.graph;

import com.octaedr.octgraph.exception.AlreadyInGraphException;
import com.octaedr.octgraph.exception.DuplicatedKeyException;
import com.octaedr.octgraph.exception.InvalidObjectException;
import com.octaedr.octgraph.exception.NotInGraphException;
import com.octaedr.octgraph.graph.IGraph;
import com.octaedr.octgraph.graph.IGraphElement;

/**
 * <h3>Generic graph element class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
abstract public class GraphElement implements IGraphElement {

    /** <h3>Element owner</h3> */
    private IGraph owner = null;

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IGraphElement#addToGraph(com.octaedr.octgraph.IGraph)
     */
    final public boolean addToGraph(IGraph graph) throws InvalidObjectException, AlreadyInGraphException, DuplicatedKeyException, NotInGraphException {
        boolean result;

        if(this.owner != null) {
            throw new AlreadyInGraphException();
        }
        
        result = graph.addElement(this);
        if(result) {
            this.owner = graph;
        }

        return result;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IGraphElement#removeFromGraph()
     */
    final public void removeFromGraph() throws InvalidObjectException, NotInGraphException {
        if(this.owner == null) {
            throw new NotInGraphException();
        }

        this.owner.removeElement(this);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IGraphElement#isInGraph()
     */
    final public boolean isInGraph() {
        return this.owner != null;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IGraphElement#getGraph()
     */
    final public IGraph getGraph() {
        return this.owner;
    }

}
