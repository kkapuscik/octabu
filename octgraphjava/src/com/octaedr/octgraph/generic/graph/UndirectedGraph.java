/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.generic.graph;

import com.octaedr.octgraph.graph.IUndirectedGraph;

/**
 * <h3>Undirected graph base class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 0.1
 */
abstract public class UndirectedGraph implements IUndirectedGraph {

    // TODO: Implementation

}
