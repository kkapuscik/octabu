/**
 * Created:     2005-10-01
 * Modified:    2005-11-20
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.generic.graph;

import java.util.Iterator;

import com.octaedr.octgraph.exception.NotInGraphException;
import com.octaedr.octgraph.graph.IGraph;
import com.octaedr.octgraph.graph.IVertex;

/**
 * <h3>Vertex generic class.</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.1
 */
public class Vertex extends GraphElement implements IVertex {

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IVertex#getEdges()
     */
    final public Iterator getEdges() throws NotInGraphException {
        IGraph graph = getGraph();
        if(graph == null) {
            throw new NotInGraphException();
        }
        return graph.getEdges(this);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.graph.IVertex#getEdges(com.octaedr.octgraph.graph.IVertex)
     */
    public Iterator getEdges(IVertex other) throws NotInGraphException {
        IGraph graph = getGraph();
        if(graph == null) {
            throw new NotInGraphException();
        }
        return graph.getEdges(this, other);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IVertex#getNeighbours()
     */
    final public Iterator getNeighbours() throws NotInGraphException {
        IGraph graph = getGraph();
        if(graph == null) {
            throw new NotInGraphException();
        }
        return graph.getNeighbours(this);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IVertex#getDegree()
     */
    final public int getDegree() throws NotInGraphException {
        IGraph graph = getGraph();
        if(graph == null) {
            throw new NotInGraphException();
        }
        return graph.getDegree(this);
    }

}
