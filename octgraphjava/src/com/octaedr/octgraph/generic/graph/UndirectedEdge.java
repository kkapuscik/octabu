/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.generic.graph;

import com.octaedr.octgraph.exception.InvalidObjectException;
import com.octaedr.octgraph.exception.RestrictedOperationException;
import com.octaedr.octgraph.graph.IUndirectedEdge;
import com.octaedr.octgraph.graph.IUndirectedVertex;

/**
 * <h3>Undirected edge generic class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class UndirectedEdge extends GraphElement implements IUndirectedEdge {

    /** <h3>Edge first end vertex</h3> */
    IUndirectedVertex firstEnd = null;
    /** <h3>Edge second end vertex</h3> */
    IUndirectedVertex secondEnd = null;

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IUndirectedEdge#setVertices(com.octaedr.octgraph.IUndirectedVertex, com.octaedr.octgraph.IUndirectedVertex)
     */
    final public void setVertices(IUndirectedVertex first, IUndirectedVertex second) throws RestrictedOperationException {
        if(isInGraph()) {
            throw new RestrictedOperationException();
        }

        this.firstEnd = first;
        this.secondEnd = second;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IUndirectedEdge#setFirstEnd(com.octaedr.octgraph.IUndirectedVertex)
     */
    final public void setFirstEnd(IUndirectedVertex first) throws RestrictedOperationException {
        if(isInGraph()) {
            throw new RestrictedOperationException();
        }
        
        this.firstEnd = first;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IUndirectedEdge#setSecondEnd(com.octaedr.octgraph.IUndirectedVertex)
     */
    final public void setSecondEnd(IUndirectedVertex second) throws RestrictedOperationException {
        if(isInGraph()) {
            throw new RestrictedOperationException();
        }
        
        this.secondEnd = second;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IUndirectedEdge#getFirstEnd()
     */
    final public IUndirectedVertex getFirstEnd() {
        return this.firstEnd;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IUndirectedEdge#getSecondEnd()
     */
    final public IUndirectedVertex getSecondEnd() {
        return this.secondEnd;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IUndirectedEdge#getOtherEnd(com.octaedr.octgraph.IUndirectedVertex)
     */
    final public IUndirectedVertex getOtherEnd(IUndirectedVertex vertex) throws InvalidObjectException {
        if(vertex == null) {
            if(this.firstEnd != null) {
                return this.firstEnd;
            }
            return this.secondEnd;
        }
        else if(vertex == this.firstEnd) {
            return this.secondEnd;
        } else if(vertex == this.secondEnd) {
            return this.firstEnd;
        } else {
            throw new InvalidObjectException("Given vertex is not edge end");
        }
    }

}
