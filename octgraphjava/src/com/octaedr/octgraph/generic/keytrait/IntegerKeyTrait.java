package com.octaedr.octgraph.generic.keytrait;

import com.octaedr.octgraph.exception.OctgraphError;

/**
 * <h3>Integer key trait</h3>
 * 
 * <p>This class provides implementation of IKeyTrait interface that
 * uses int type as a key representation.</p>
 * <p>Note that Integer.MIN_VALUE is not allowed because it is used
 * to check if key has been initialized.</p>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class IntegerKeyTrait extends KeyTrait {

    /**
     * <h3>Constructor</h3>
     */
    public IntegerKeyTrait() {
        super.setKey(new Integer(Integer.MIN_VALUE));
    }
    
    /**
     * <h3>Set key</h3>
     * 
     * @param key - key to set.
     * 
     * @return
     * True if key was successfully set or false otherwise.
     */
    final public boolean setKey(int key) {
        return setKey(new Integer(key));
    }
    
    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IKeyTrait#setKey(java.lang.Object)
     */
    final public boolean setKey(Object key) {
        if(((Integer)key).intValue() == Integer.MIN_VALUE) {
            throw new OctgraphError("Integer.MIN_VALUE is not allowed as a key");                
        }
        return super.setKey(key);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IKeyTrait#getKey()
     */
    final public Object getKey() {
        Integer key = (Integer)super.getKey();
        if(key.intValue() == Integer.MIN_VALUE) {
            throw new OctgraphError("Key has not been initialized");
        }
        return key;
    }

}
