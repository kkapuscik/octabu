package com.octaedr.octgraph.generic.keytrait;

import com.octaedr.octgraph.exception.OctgraphError;
import com.octaedr.octgraph.keytrait.IKeyTrait;
import com.octaedr.octgraph.keytrait.IKeyTraitListener;

/**
 * <h3>Key trait generic class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
abstract public class KeyTrait implements IKeyTrait {

    /** Key value */
    private Object keyValue = null;
    /** Key trait listener */
    private IKeyTraitListener traitListener = null;
    
    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IKeyTrait#setKey(java.lang.Object)
     */
    public boolean setKey(Object key) {
        boolean permission = true;

        if(this.traitListener != null) {
            permission &= this.traitListener.keyChangeRequested(this, key);
        }
        
        if(permission) {
            Object oldKey = this.keyValue;
            this.keyValue = key;
            
            if(this.traitListener != null) {
                this.traitListener.keyChanged(this, oldKey);
            }
        }
        
        return permission;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IKeyTrait#getKey()
     */
    public Object getKey() {
        return this.keyValue;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IKeyTrait#addKeyTraitListener(com.octaedr.octgraph.IKeyTraitListener)
     */
    final public void addKeyTraitListener(IKeyTraitListener listener) {
        if(this.traitListener != null) {
            throw new OctgraphError("Listener already set (only one at a time supported)");
        }
        this.traitListener = listener;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IKeyTrait#removeKeyTraitListener(com.octaedr.octgraph.IKeyTraitListener)
     */
    final public void removeKeyTraitListener(IKeyTraitListener listener) {
        if(this.traitListener == null) {
            throw new OctgraphError("Listener not set");
        } else if(this.traitListener == listener) {
            this.traitListener = null;
        } else {
            throw new OctgraphError("Not a valid listener (given listener has not been set previously)");
        }      
    }

}
