package com.octaedr.octgraph.generic.keytrait;

import java.util.TreeMap;

import com.octaedr.octgraph.keytrait.IKeyManager;
import com.octaedr.octgraph.keytrait.IKeyTrait;
import com.octaedr.octgraph.keytrait.IKeyTraitListener;

public class KeyManager implements IKeyManager, IKeyTraitListener {
    
    /** <h3>Ordered collection of key traits</h3> */
    private TreeMap collection = new TreeMap();

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IKeyManager#add(com.octaedr.octgraph.IKeyTrait)
     */
    public boolean add(IKeyTrait element) {
        if(this.collection.containsKey(element.getKey())) {
            return false;
        }
        this.collection.put(element.getKey(), element);
        return true;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IKeyManager#remove(com.octaedr.octgraph.IKeyTrait)
     */
    public void remove(IKeyTrait element) {
        this.collection.remove(element.getKey());

        /* EXTENDED VERSION
        IKeyTrait<T> object = collection.get(element.getKey());
        if(object.equals(element)) {
            collection.remove(element.getKey());
        } else {
            throw new OctgraphException();
        }
        */
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IKeyManager#find(java.lang.Object)
     */
    public IKeyTrait find(Object key) {
        return (IKeyTrait)this.collection.get(key);
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IKeyTraitListener#keyChangeRequested(com.octaedr.octgraph.IKeyTrait, java.lang.Object)
     */
    public boolean keyChangeRequested(IKeyTrait trait, Object newValue) {
        /* Do not allow to change the key */
        return false;
    }

    /*
     * (non-Javadoc)
     * @see com.octaedr.octgraph.IKeyTraitListener#keyChanged(com.octaedr.octgraph.IKeyTrait, java.lang.Object)
     */
    public void keyChanged(IKeyTrait trait, Object oldValue) {
        /* nothing to do */
    }

}
