/**
 * Created:     2005-10-01
 * Modified:    2005-11-20
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.graph;

import java.util.Iterator;

import com.octaedr.octgraph.exception.NotInGraphException;

/**
 * <h3>Vertex interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.1
 */
public interface IVertex extends IGraphElement {

    /**
     * <h3>Get all edges connected to this vertex</h3>
     * 
     * @return Iterator over edges collection.
     * 
     * @throws NotInGraphException
     * if vertex is not in graph.
     */
    public Iterator getEdges() throws NotInGraphException;
    
    /**
     * <h3>Get all edges connected between this vertex and given as a parameter</h3>
     * 
     * @param other - other vertex.
     * 
     * @return Iterator over edges collection.
     * 
     * @throws NotInGraphException
     * if current vertex or this given as parameter is not in graph.
     */
    public Iterator getEdges(IVertex other) throws NotInGraphException;

    /**
     * <h3>Get all neighbour vertices</h3>
     * 
     * @return Iterator over vertices collection.
     * 
     * @throws NotInGraphException
     * if vertex is not in graph.
     */
    public Iterator getNeighbours() throws NotInGraphException;

    /**
     * <h3>Get vertex degree</h3>
     * 
     * @return Number of edges connected to this vertex.
     * 
     * @throws NotInGraphException
     * if vertex is not in graph.
     */
    public int getDegree() throws NotInGraphException;

}
