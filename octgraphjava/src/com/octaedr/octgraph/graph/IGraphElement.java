/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.graph;

import com.octaedr.octgraph.exception.AlreadyInGraphException;
import com.octaedr.octgraph.exception.DuplicatedKeyException;
import com.octaedr.octgraph.exception.InvalidObjectException;
import com.octaedr.octgraph.exception.NotInGraphException;

/**
 * <h3>Graph element base interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IGraphElement {

    /**
     * <h3>Add element to graph</h3>
     * 
     * @param graph - graph to which element will be added.
     * 
     * @throws InvalidObjectException
     * if element is not of allowed type.
     * 
     * @throws AlreadyInGraphException
     * if element is already in any graph.
     * 
     * @throws DuplicatedKeyException 
     * if element with same key is already in graph.
     * 
     * @throws NotInGraphException 
     * if source or target vertex of edge is not in given graph.
     */
    public boolean addToGraph(IGraph graph) throws InvalidObjectException, AlreadyInGraphException, DuplicatedKeyException, NotInGraphException;
    
    /**
     * <h3>Remove element from graph</h3>
     * 
     * @throws InvalidObjectException
     * if element is not of allowed type.
     * 
     * @throws NotInGraphException
     * if element is not in graph.
     */
    public void removeFromGraph() throws InvalidObjectException, NotInGraphException;

    /**
     * <h3>Check if element is in graph</h3>
     * 
     * @return True if element is in graph, false otherwise.
     */
    public boolean isInGraph();

    /**
     * <h3>Get graph to which element belongs</h3>
     * 
     * @return Graph to which element belongs or null if it
     *         does not belong to any.
     */
    public IGraph getGraph();

}
