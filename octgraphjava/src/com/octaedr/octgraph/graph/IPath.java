/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.graph;

import java.util.ListIterator;

import com.octaedr.octgraph.exception.BrokenPathException;

/**
 * <h3>Graph path</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IPath {
   
    /**
     * <h3>Get number of edges in path</h3>
     * 
     * @return
     * Number of edges in path.
     */
    public int getEdgeCount();
    
    /**
     * <h3>Add edge at start of path</h3>
     * 
     * @param edge - edge to add.
     * 
     * @throws BrokenPathException 
     * if given edge brokes the path (path will not be continuous.
     */
    public void addFirst(IEdge edge) throws BrokenPathException;

    /**
     * <h3>Add edge at end of path</h3>
     * 
     * @param edge - edge to add.
     * 
     * @throws BrokenPathException 
     * if given edge brokes the path (path will not be continuous.
     */
    public void addLast(IEdge edge) throws BrokenPathException;

    /**
     * <h3>Remove first edge of path</h3>
     * 
     * @return
     * Removed edge or null if path was empty.
     */
    public IEdge removeFirst();
    
    /**
     * <h3>Remove last edge of path</h3>
     * 
     * @return
     * Removed edge or null if path was empty.
     */
    public IEdge removeLast();
    
    /**
     * <h3>Get iterator over path edges</h3>
     * 
     * <p>Iterator starts at first path edge</p>
     * 
     * @return
     * Iterator over path edges.
     */
    public ListIterator getEdges();

}
