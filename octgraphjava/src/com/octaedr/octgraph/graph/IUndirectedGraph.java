/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.graph;

/**
 * <h3>Undirected graph interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 0.1
 */
public interface IUndirectedGraph extends IGraph {

    // nothing in this version

}
