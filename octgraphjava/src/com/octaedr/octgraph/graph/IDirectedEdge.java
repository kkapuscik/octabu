/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.graph;

import com.octaedr.octgraph.exception.RestrictedOperationException;

/**
 * <h3>Directed edge interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IDirectedEdge extends IEdge {

    /**
     * <h3>Set source and target of vertex</h3>
     * 
     * @param source - vertex to set as edge source.
     * @param target - vertex to set as edge target.
     * 
     * @throws RestrictedOperationException
     * if edge is in graph.
     */
    public void setVertices(IDirectedVertex source, IDirectedVertex target)
            throws RestrictedOperationException;
    
    /**
     * <h3>Set source vertex</h3>
     * 
     * @param source - vertex to set as a source.
     * 
     * @throws RestrictedOperationException
     * if edge is in graph.
     */
    public void setSource(IDirectedVertex source)
            throws RestrictedOperationException;

    /**
     * <h3>Set target vertex</h3>
     * 
     * @param target - vertex to set as a target.
     * 
     * @throws RestrictedOperationException
     * if edge is in graph.
     */
    public void setTarget(IDirectedVertex target)
            throws RestrictedOperationException;

    /**
     * <h3>Get source vertex</h3>
     * 
     * @return Source vertex or null if it has not been set.
     */
    public IDirectedVertex getSource();

    /**
     * <h3>Get target vertex</h3>
     * 
     * @return Target vertex or null if it has not been set.
     */
    public IDirectedVertex getTarget();

}
