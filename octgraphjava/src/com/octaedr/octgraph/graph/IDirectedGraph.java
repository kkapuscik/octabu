/**
 * Created:     2005-10-01
 * Modified:    2005-11-20
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.graph;

import java.util.Iterator;

import com.octaedr.octgraph.exception.NotInGraphException;

/**
 * <h3>Directed graph interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.1
 */
public interface IDirectedGraph extends IGraph {

    /**
     * <h3>Get all edges exiting given vertex</h3>
     * 
     * @param vertex - vertex for which operation is performed.
     * 
     * @return Iterator over edges collection.
     * 
     * @throws NotInGraphException
     * if vertex is not in graph.
     */
    public Iterator getOutEdges(IDirectedVertex vertex) throws NotInGraphException;

    /**
     * <h3>Get all edges exiting first vertex and entering the second</h3>
     * 
     * @param vertex1 - vertex for which looking for edges.
     * @param vertex2 - vertex for which looking for edges.
     * 
     * @return Iterator over edges collection.
     * 
     * @throws NotInGraphException
     * if vertex is not in this graph.
     */
    public Iterator getOutEdges(IDirectedVertex vertex1, IDirectedVertex vertex2) throws NotInGraphException;

    /**
     * <h3>Get all vertices at other end of edges exiting given vertex</h3>
     * 
     * @param vertex - vertex for which operation is performed.
     * 
     * @return Iterator over vertices collection.
     * 
     * @throws NotInGraphException
     * if vertex is not in graph.
     */
    public Iterator getOutNeighbours(IDirectedVertex vertex) throws NotInGraphException;

    /**
     * <h3>Get given vertex out degree</h3>
     * 
     * @param vertex - vertex for which operation is performed.
     * 
     * @return Number of edges exiting this vertex.
     * 
     * @throws NotInGraphException
     * if vertex is not in graph.
     */
    public int getOutDegree(IDirectedVertex vertex) throws NotInGraphException;

    /**
     * <h3>Get all edges entering given vertex</h3>
     * 
     * @param vertex - vertex for which operation is performed.
     * 
     * @return Iterator over edges collection.
     * 
     * @throws NotInGraphException
     * if vertex is not in graph.
     */
    public Iterator getInEdges(IDirectedVertex vertex) throws NotInGraphException;

    /**
     * <h3>Get all edges entering first vertex and exiting the second</h3>
     * 
     * @param vertex1 - vertex for which looking for edges.
     * @param vertex2 - vertex for which looking for edges.
     * 
     * @return Iterator over edges collection.
     * 
     * @throws NotInGraphException
     * if vertex is not in this graph.
     */
    public Iterator getInEdges(IDirectedVertex vertex1, IDirectedVertex vertex2) throws NotInGraphException;

    /**
     * <h3>Get all vertices at other end of edges entering this vertex</h3>
     * 
     * @param vertex - vertex for which operation is performed.
     * 
     * @return Iterator over vertices collection.
     * 
     * @throws NotInGraphException
     * if vertex is not in graph.
     */
    public Iterator getInNeighbours(IDirectedVertex vertex) throws NotInGraphException;

    /**
     * <h3>Get given vertex in degree</h3>
     * 
     * @param vertex - vertex for which operation is performed.
     * 
     * @return Number of edges entering this vertex.
     * 
     * @throws NotInGraphException
     * if vertex is not in graph.
     */
    public int getInDegree(IDirectedVertex vertex) throws NotInGraphException;
    
}
