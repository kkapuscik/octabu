/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.graph;

/**
 * <h3>Edge interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 0.1
 */
public interface IEdge extends IGraphElement {

    // nothing in this version

}
