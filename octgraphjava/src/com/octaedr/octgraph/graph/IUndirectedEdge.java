/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.graph;

import com.octaedr.octgraph.exception.InvalidObjectException;
import com.octaedr.octgraph.exception.RestrictedOperationException;

/**
 * <h3>Undirected edge interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IUndirectedEdge extends IEdge {

    /**
     * <h3>Set source and target of vertex</h3>
     * 
     * @param source - vertex to set as first end of edge.
     * @param target - vertex to set as second end of edge.
     * 
     * @throws RestrictedOperationException
     * if edge is in graph.
     */
    public void setVertices(IUndirectedVertex first, IUndirectedVertex second)
            throws RestrictedOperationException;

    /**
     * <h3>Set first end vertex</h3>
     * 
     * @param first - vertex to set as first end of edge.
     * 
     * @throws RestrictedOperationException
     * if edge is in graph.
     */
    public void setFirstEnd(IUndirectedVertex first)
            throws RestrictedOperationException;

    /**
     * <h3>Set second end vertex</h3>
     * 
     * @param second - vertex to set as second end of edge.
     * 
     * @throws RestrictedOperationException
     * if edge is in graph.
     */
    public void setSecondEnd(IUndirectedVertex second)
            throws RestrictedOperationException;

    /**
     * <h3>Get first end vertex</h3>
     * 
     * @return Vertex at first end of edge or null if it has not been set.
     */
    public IUndirectedVertex getFirstEnd();

    /**
     * <h3>Get second end vertex</h3>
     * 
     * @return Vertex at second end of edge or null if it has not been set.
     */
    public IUndirectedVertex getSecondEnd();

    /**
     * <h3>Get other end vertex</h3>
     * 
     * @param vertex - vertex at end of edge to which oposite end 
     *                 vertex will be returned. If it is null one of
     *                 vertices at edge ends will be returned.
     *                 
     * @return Vertex at oposite end of edge. This could be null if
     *         this vertex has not been set.
     *         
     * @throws InvalidObjectException 
     * when given vertex is not one of edge ends.
     */
    public IUndirectedVertex getOtherEnd(IUndirectedVertex vertex) throws InvalidObjectException;
    
}
