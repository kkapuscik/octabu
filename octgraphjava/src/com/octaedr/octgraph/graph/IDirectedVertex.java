/**
 * Created:     2005-10-01
 * Modified:    2005-11-20
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.graph;

import java.util.Iterator;

import com.octaedr.octgraph.exception.NotInGraphException;

/**
 * <h3>Directed vertex interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.1
 */
public interface IDirectedVertex extends IVertex {

    /**
     * <h3>Get all edges exiting this vertex</h3>
     * 
     * @return Iterator over edges collection.
     * 
     * @throws NotInGraphException
     * if vertex is not in graph.
     */
    public Iterator getOutEdges() throws NotInGraphException;

    /**
     * <h3>Get all edges existing this vertex and entering vertex given</h3>
     * 
     * @param other - other vertex.
     * 
     * @return Iterator over edges collection.
     * 
     * @throws NotInGraphException
     * if current vertex or this given as parameter is not in graph.
     */
    public Iterator getOutEdges(IDirectedVertex other) throws NotInGraphException;

    /**
     * <h3>Get all edges entering this vertex</h3>
     * 
     * @return Iterator over edges collection.
     * 
     * @throws NotInGraphException
     * if vertex is not in graph.
     */
    public Iterator getInEdges() throws NotInGraphException;

    /**
     * <h3>Get all edges entering this vertex and exiting vertex given</h3>
     * 
     * @param other - other vertex.
     * 
     * @return Iterator over edges collection.
     * 
     * @throws NotInGraphException
     * if current vertex or this given as parameter is not in graph.
     */
    public Iterator getInEdges(IDirectedVertex other) throws NotInGraphException;

    /**
     * <h3>Get all vertices at other end of edges exiting this vertex</h3>
     * 
     * @return Iterator over vertices collection.
     * 
     * @throws NotInGraphException
     * if vertex is not in graph.
     */
    public Iterator getOutNeighbours() throws NotInGraphException;

    /**
     * <h3>Get all vertices at other end of edges entering this vertex</h3>
     * 
     * @return Iterator over vertices collection.
     * 
     * @throws NotInGraphException
     * if vertex is not in graph.
     */
    public Iterator getInNeighbours() throws NotInGraphException;

    /**
     * <h3>Get vertex out degree</h3>
     * 
     * @return Number of edges exiting this vertex.
     * 
     * @throws NotInGraphException
     * if vertex is not in graph.
     */
    public int getOutDegree() throws NotInGraphException;

    /**
     * <h3>Get vertex in degree</h3>
     * 
     * @return Number of edges entering this vertex.
     * 
     * @throws NotInGraphException
     * if vertex is not in graph.
     */
    public int getInDegree() throws NotInGraphException;

}
