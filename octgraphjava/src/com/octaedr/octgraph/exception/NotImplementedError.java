/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.exception;

/**
 * <h3>Feature not implemented error</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class NotImplementedError extends OctgraphError {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = 866204822234216906L;

    /**
     * <h3>Constructor</h3>
     */
    public NotImplementedError() {
        super();
    }

    /**
     * <h3>Constructor</h3>
     * 
     * @param message - error message.
     */
    public NotImplementedError(String message) {
        super(message);
    }

}
