/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.exception;

/**
 * <h3>Restricted operation performed exception class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class RestrictedOperationException extends OctgraphException {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = -2193886675741428111L;

    /**
     * <h3>Constructor</h3>
     */
    public RestrictedOperationException() {
        super();
    }

    /**
     * <h3>Constructor</h3>
     * 
     * @param message - exception message.
     */
    public RestrictedOperationException(String message) {
        super(message);
    }

}
