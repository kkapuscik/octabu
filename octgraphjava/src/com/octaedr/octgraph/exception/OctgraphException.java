/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.exception;

/**
 * <h3>OCTgraph base exception class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class OctgraphException extends Exception {

    /** <h3>Generated serial number</h3> */
    private static final long serialVersionUID = 4588156872137612378L;

    /**
     * <h3>Constructor</h3>
     */
    public OctgraphException() {
        super();
    }

    /**
     * <h3>Constructor</h3>
     * 
     * @param message - detailed exception message.
     */
    public OctgraphException(String message) {
        super(message);
    }

}
