/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.exception;

/**
 * <h3>Element already in graph</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class AlreadyInGraphException extends OctgraphException {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = 8250194044507089084L;

    /**
     * <h3>Constructor</h3>
     */
    public AlreadyInGraphException() {
        super();
    }

    /**
     * <h3>Constructor</h3>
     * 
     * @param message - exception message.
     */
    public AlreadyInGraphException(String message) {
        super(message);
    }

}
