/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.exception;

/**
 * <h3>Invalid object used in operation</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class InvalidObjectException extends OctgraphException {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = -8870373072971857657L;

    /**
     * <h3>Constructor</h3>
     */
    public InvalidObjectException() {
        super();
    }

    /**
     * <h3>Constructor</h3>
     * 
     * @param message - exception message.
     */
    public InvalidObjectException(String message) {
        super(message);
    }

}
