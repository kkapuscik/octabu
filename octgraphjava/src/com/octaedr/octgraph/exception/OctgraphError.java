/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.exception;

/**
 * <h3>OCTgraph error class</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public class OctgraphError extends Error {

    /** <h3>Generated serial version UID</h3> */
    private static final long serialVersionUID = 7345455512086067041L;

    /**
     * <h3>Constructor</h3>
     */
    public OctgraphError() {
        super();
    }

    /**
     * <h3>Constructor</h3>
     * 
     * @param message - error message.
     */
    public OctgraphError(String message) {
        super(message);
    }

}
