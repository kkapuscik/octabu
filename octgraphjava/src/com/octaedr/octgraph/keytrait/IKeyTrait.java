package com.octaedr.octgraph.keytrait;

/**
 * <h3>Key trait interface.</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IKeyTrait {
    
    /**
     * <h3>Set key value</h3>
     * 
     * <p>Before change all registered listeners should be asked for
     * permission to change using keyChangeRequested() method. If all
     * of them return true key could be changed to new value and all
     * listeners should be informed that this was done using
     * keyChanged() method.</p>
     * 
     * @param key - key value to set.
     * 
     * @return
     * True if key was successfully changed or false otherwise.
     */
    public boolean setKey(Object key);
    
    /**
     * <h3>Get key value</h3>
     * 
     * @return Current key value.
     */
    public Object getKey();
    
    /**
     * <h3>Add key trait listener</h3>
     * 
     * @param listener - listener to add.
     */
    public void addKeyTraitListener(IKeyTraitListener listener);

    /**
     * <h3>Remove key trait listener</h3>
     * 
     * @param listener - listener to remove.
     */
    public void removeKeyTraitListener(IKeyTraitListener listener);
    
}
