/**
 * Created:     2005-10-01
 * Modified:    2005-11-18
 * Author:      Krzysztof Kapuscik
 * Copyright:   (C) 2005 Krzysztof Kapuscik
 * Contact:     saveman@op.pl
 */

package com.octaedr.octgraph.keytrait;

/**
 * <h3>Key manager interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IKeyManager {

    /**
     * <h3>Add object to managed collection</h3>
     * 
     * @param element - object to add.
     * 
     * @return
     * True if object was successfully added or false if another
     * object with same key is already in collection.
     */
    public boolean add(IKeyTrait element);
    
    /**
     * <h3>Remove object from managed collection</h3>
     * 
     * @param element - object to remove
     */
    public void remove(IKeyTrait element);

    /**
     * <h3>Find object in collection</h3>
     * 
     * @param key - key of the object to find.
     * 
     * @return
     * Object found or null if no such object is in collection.
     */
    public IKeyTrait find(Object key);

}
