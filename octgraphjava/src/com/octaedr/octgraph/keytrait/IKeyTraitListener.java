package com.octaedr.octgraph.keytrait;

/**
 * <h3>Key trait listener interface</h3>
 * 
 * @author Krzysztof Kapuscik
 * @version 1.0
 */
public interface IKeyTraitListener {

    /**
     * <h3>Key change is requested</h3>
     * 
     * <p>This method is used to check if listener permits changing
     * the key hold by key trait.</p>
     * 
     * @param trait - key trait which want to change key.
     * @param newValue - new key value.
     * 
     * @return True if listener permits key change or false otherwise.
     */
    public boolean keyChangeRequested(IKeyTrait trait, Object newValue);
    
    /**
     * <h3>Key has been changed</h3>
     * 
     * @param trait - key trait that has changed key.
     * @param oldValue - old key value.
     */
    public void keyChanged(IKeyTrait trait, Object oldValue);
    
}
